import * as NodeRSA from 'node-rsa';
import * as fs from 'fs';

let pubKey = '';

export default function genDisbursementMethod(data: string) {
  if (pubKey == '') {
    pubKey = fs
      .readFileSync(
        process.env.ENVIRONMENT == 'pro' ? 'rsa.pub' : 'rsa_dev.pub',
      )
      .toString();
  }

  const key = new NodeRSA(pubKey, { encryptionScheme: 'pkcs1' });
  const encrypted = key.encrypt(data, 'base64');
  return encrypted;
}
