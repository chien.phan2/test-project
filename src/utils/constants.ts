import { BadRequestException } from '@nestjs/common';
import * as process from 'process';
import { MY_MESSAGE } from '../contants/message.contants';

export enum RewardType {
  CHALLENGE = 'Challenge',
  MONEY = 'Money',
  NO_REWARD = 'No reward',
}

export enum RewardCode {
  Challenge_Ticket = 'ChallengeTicket',
  Good_Luck = 'GoodLuck',
  Energy_100 = 'Energy100',
}

export const RewardList: { code: string; id: string }[] = [
  {
    code: RewardCode.Challenge_Ticket,
    id: `${RewardCode.Challenge_Ticket}_0`,
  },
  {
    code: RewardCode.Good_Luck,
    id: `${RewardCode.Good_Luck}_1`,
  },
  {
    code: RewardCode.Energy_100,
    id: `${RewardCode.Energy_100}_2`,
  },
  {
    code: RewardCode.Good_Luck,
    id: `${RewardCode.Good_Luck}_3`,
  },
  {
    code: RewardCode.Good_Luck,
    id: `${RewardCode.Good_Luck}_4`,
  },
  {
    code: RewardCode.Energy_100,
    id: `${RewardCode.Energy_100}_5`,
  },
  {
    code: RewardCode.Good_Luck,
    id: `${RewardCode.Good_Luck}_6`,
  },
  {
    code: RewardCode.Good_Luck,
    id: `${RewardCode.Good_Luck}_7`,
  },
];

export const authenticatedPortal = (secretKey: string) => {
  const SECRET_PORTAL_KEY = process.env.PORTAL_SECRET_KEY;
  if (secretKey !== SECRET_PORTAL_KEY) {
    throw new BadRequestException(MY_MESSAGE.AUTHENTICATE_PORTAL);
  }
};

export enum ActionType {
  GIVE_AWAY = 'give-away',
  SPONSOR = 'sponsor',
  BUY = 'buy',
  MARKET_BUY = 'market-buy',
}

export enum ActionGroup {
  CREATE_GROUP = 'create-group',
  EDIT_GROUP = 'edit-group',
  DELETE_GROUP = 'delete-group',
  JOIN_GROUP = 'join-group',
  LEAVE_GROUP = 'leave-group',
  CANCEL_REQUEST = 'cancel-request',
}

export enum GroupParticipationHistoryType {
  PARTICIPATION = 'participation',
  GROUP_INFO = 'group-info',
}

export const OP_Signature =
  'iYqPcgmeRY8To5du-TiocWyKTTlRVaYYiIYHW9pHHCYHxzvh-oaDFkKlh-_pfKCcgK4z2GV1wu95-izRJ1Mad-mPETx2vA18NaIba2nqUeA7Oy7YgHrcXsl4O6-XRmhx-v0SbcQ3KkPwy7jbnThz8gjAStL9YPYDOES-uK2xBrj7xDHzcO6Nca8qG7Jnu6e1uBqDu67L2PXQYLiHOU8Z8NxTOmpq6qDjeKSAjauJzQlWr-fSR9bx4FfRJ2yH8AhRbM0-ygl7-pHCePS1nORJ0BCeiHxxoN6ihz4rEzi5xy80iwKOt_4sU3HIkqosf9JTFA3fGEYsJQX6eDuE__AJFg==';
