import * as hmacSHA256 from 'crypto-js/hmac-sha256';

export default function genSignature(rawSignature: string): string {
  const signature = hmacSHA256(rawSignature, process.env.SECRET_KEY).toString();

  return signature;
}
