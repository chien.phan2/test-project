import * as AES from 'crypto-js/aes';
import * as CryptoJS from 'crypto-js';

export function genToken(value: string) {
  const encrypted = AES.encrypt(value, process.env.SECRET_KEY).toString();
  return encrypted;
}

export function getToken(value: string) {
  const decrypted = AES.decrypt(value, process.env.SECRET_KEY).toString(
    CryptoJS.enc.Utf8,
  );
  return decrypted;
}
