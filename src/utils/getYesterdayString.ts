export default function getYesterdayString(): string {
  const today = new Date();
  const yesterday = new Date();
  yesterday.setDate(today.getDate() - 1);

  if (yesterday.getDay() === 6) {
    yesterday.setDate(yesterday.getDate() - 1);
  } else if (yesterday.getDay() === 0) {
    console.log('Sunday');
    yesterday.setDate(yesterday.getDate() - 2);
  }

  return yesterday.toLocaleDateString();
}
