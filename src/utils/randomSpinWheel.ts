import { Reward } from '../schema/reward.schema';

export default (
  prizes: any[],
  probabilities: number[],
): {
  prize: Reward & { rank: string; rewardId: Reward };
  prizeKey: string;
} | null => {
  if (!prizes || !probabilities) {
    return null;
  }

  if (prizes.length !== probabilities.length) {
    throw new Error('Số lượng phần thưởng và xác suất phải giống nhau');
  }

  const totalProbability = probabilities.reduce((acc, curr) => acc + curr, 0);
  const normalizedProbabilities = probabilities.map(
    (p) => p / totalProbability,
  );

  const spinResult = Math.random();

  let cumulativeProbability = 0;
  for (let i = 0; i < normalizedProbabilities.length; i++) {
    cumulativeProbability += normalizedProbabilities[i];
    if (spinResult <= cumulativeProbability) {
      return {
        prize: prizes[i],
        prizeKey: prizes[i]?.id,
      };
    }
  }

  return null;
};
