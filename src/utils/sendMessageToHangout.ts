import fetch from 'node-fetch';

export const sendHangoutMessage = async (title = '') => {
  try {
    const fullMess = JSON.stringify({
      text: `\`\`\`${title}\`\`\``,
    });

    const response = await fetch(
      'https://chat.googleapis.com/v1/spaces/AAAAU5PDKN4/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=7BcR6z4qA--YMx8GIgPu7OlfWMzIMq6zJsC-sCrcnew%3D&threadKey=123314',
      {
        method: 'POST',
        body: fullMess,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    );
  } catch (err) {
    console.log("Can't connect to google chat api ", err);
  }
};

export const sendHangoutMessageWithLink = async (
  title = '',
  attachments = [],
) => {
  try {
    // eslint-disable-next-line prettier/prettier
    const listAttachments = attachments.map((item, index) => `<${item}|images${index + 1}>`)
      .join(', ');
    const fullMess = JSON.stringify({
      text: `\`\`\`${title}\`\`\`` + `\nAttachments: ${listAttachments}.`,
    });

    const response = await fetch(
      'https://chat.googleapis.com/v1/spaces/AAAAU5PDKN4/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=7BcR6z4qA--YMx8GIgPu7OlfWMzIMq6zJsC-sCrcnew%3D&threadKey=123314',
      {
        method: 'POST',
        body: fullMess,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    );
  } catch (err) {
    console.log("Can't connect to google chat api ", err);
  }
};
