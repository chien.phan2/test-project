import * as NodeRSA from 'node-rsa';

export default function genOPSignature(body, timestamp) {
  function base64URLEncode(data) {
    const base64 = Buffer.from(data, 'utf8').toString('base64');
    return base64.replace(/\+/g, '-').replace(/\//g, '_');
  }
  const openSecretKey = 'MR16_7iKk6LCQaGd97Zh3_f0KVODP4up7tQ';
  const openPrivateKey =
    'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCXN4uSuT4WzJe-nCbepxzs4JtMoL7mNBKyF-KxMYsKliO6D63_KDFCZH0xCicMo41AViH_M9bCSQrs8Ouaps3nkFsRiQRspkTWqvlFqdy6Z1X_iK-cwd2lXZ-BrQXpOu5PJbP4wS3GeFfJoRBf4_A71QnAZV7aD2si28uH7SvRUpV-gdnNq-n_KllzzVxsrYLAoxvJ9EiOyFsORezMJVFi8mQLJpagYdLuizWSRXvcip_4NVyclRZeLFpqyiJlXIxRTLAFpCoOvbR_qHyOmMMxl7yGrwFXmu73ggVKAeVGsQjFDEmVBLmxn3jSmgQOIohU2bgEt14n1AdkhzIIOOQLAgMBAAECggEAah_XWm-a_ZE0ZdddCCGsESpIk2y0zQZ6oHZW6BhnBlT8WdYrPMlOWNQBcGs5oIsf1KkeTuZosA1nF4a1MuIh24PaBeJtMns2EJoJOzTM7h0bo6yf4pnEHgwop29wmJB9Mxq4xbl9kK5gneblgQaNUNBgeaArA0qS2_H5UznF6K7BxSUUU3Sxus35gSzLPr6cQrld1NgJ5MCvMv4L7uAe1-y63Iywsl__o8CGIIQp2ol5WEN0l4nUg6_ALM13o-Ho13iposWwVxVjgkmnr-b7W_3fDPCs4AX883yhvODMDFS8xzN5VQbdR3XLZOFl-3FIfjVigeWIQMaQ5ucm7VtKAQKBgQDTzVHFQYBDZDWZ6swApYFKvTIkwfSl82-4sSMCMk0-2zhF1FW7ClBO9n1AEMxTKrKLfb62-DQ169g3hwyZxyvOABiUGksFyVsGUovtEc2WFTuF6M9XBLWoq_k51U8gKAUqGZ9nI19QjkwgpTW63e9QFR_WIUWunt5hCLauk0wugQKBgQC2xbXVNEGeWSVe3AZ3OH8DHlDS-VD7n8YENCoDbln40LXFr61fnZb26DaDXV72rNxYGyXV_lU10dmTMjYF0mbGCPi0AZ2hRx-hNZlFaxnzFgIYTNAU6zsNLiYDw3968RfSF0fhEmloeronXxGmKxKGljuQm3-cYBQnKy3SaZGkiwKBgGOVoCLc_0-waDtsWGrdZoMbnd4Gi98TLK5sKwe3WvZ44GaExaJh4QyffJiIzWk1JCnN_8PLw9CuiUTibJxJN2FuTiAklK2t1OyOF1hqDH3KkegOZJ6Hilo6Mb5tevyllHgNABPgX8pifYzD4RVxkDnZFNT7Y-YMsIZzMGG4xBMBAoGBAJRsyHZEgok6MbvwCBwnOBXrN42sw9QjcuYVCo1QvyXtL8BDIxND4cXO2_4gBC9kP2gZW8eFS-Nx8DO2EeL2c1mdDYL40O6Y2JnM8hRKnn8mvzrGnjBuurzAXkSuLUdEpKiLGWcw0YL0L1orvSRpVBkRxXtwm9f8ukD4srRt4MVzAoGAaoRCHH2-mmWqrHriODRad4zO4-7_A6jA30kH0Tttbv5-inVJFEYbbqi-WVGQBCH5RJ06tuvf9Czv3MKWalSq1SvQf4mYXPX4bk5-1mHEU2OLk1to2pxECY1h8I4SnZxqc9if0LRosz_Clg7ji2VFsGBc9L-7GEE_Y6Whj83ZKoY=';

  const payload = JSON.stringify(body) + timestamp + openSecretKey;

  const privateKey = new NodeRSA(openPrivateKey, 'pkcs8-private-pem');

  const signedPayload = privateKey.sign(payload);
  const opSignature = base64URLEncode(signedPayload);

  return opSignature;
}
