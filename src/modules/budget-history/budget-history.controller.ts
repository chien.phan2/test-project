import { Controller, Get, Param, UseInterceptors } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { BudgetHistoryService } from './budget-history.service';

@ApiTags('Challenge')
@Controller('challenge')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class BudgetHistoryController {
  constructor(private readonly budgetHistoryService: BudgetHistoryService) {}

  @Get('get-list-sponsor/:id')
  getSponsor(@Param('id') id: string) {
    return this.budgetHistoryService.getListSponsorByChallengeId(id);
  }

  @Get('budget/get-budget-challenge/:id')
  getBudgetChallenge(@Param('id') id: string) {
    return this.budgetHistoryService.calcBudgetByChallengeId(id);
  }
}
