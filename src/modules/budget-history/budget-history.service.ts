import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EventsGateway } from 'src/events/events.gateway';
import {
  BudgetHistory,
  BudgetHistoryDocument,
} from 'src/schema/budget-history.schema';
import { RunnerService } from '../runner/runner.service';
import { CreateBudgetHistoryDto } from './dto/create-budget-history.dto';

@Injectable()
export class BudgetHistoryService {
  constructor(
    @InjectModel(BudgetHistory.name)
    private readonly budgetHistoryModel: Model<BudgetHistoryDocument>,

    private readonly eventsGateway: EventsGateway,

    private readonly runnerService: RunnerService,
  ) {}

  async createBudgetHistory(createBudgetHistoryDto: CreateBudgetHistoryDto) {
    try {
      const createOneBudgetHistory = new this.budgetHistoryModel({
        ...createBudgetHistoryDto,
      });
      const data = await createOneBudgetHistory.save();

      const runner = await this.runnerService.findRunnerById(
        createBudgetHistoryDto?.creator || '',
      );
      const message = `${runner?.name} đã đóng góp vào ngân sách ${createBudgetHistoryDto?.amount} VNĐ.}`;

      setTimeout(() => {
        this.eventsGateway.onNotification({
          status: 'success',
          message: message,
          date: new Date().toLocaleString(),
        });
      }, 10000);

      return data;
    } catch (error) {
      console.log('createBudgetHistory: ', error);
      throw error;
    }
  }

  async findSponsorByChallengeIdAndRunnerId(
    challengeID: string,
    runnerID: string,
  ) {
    try {
      const data = await this.budgetHistoryModel.find({
        challenge: challengeID,
        creator: runnerID,
        type: 'sponsor',
      });

      return data;
    } catch (error) {
      throw error;
    }
  }

  async getListSponsorByChallengeId(challengeID: string) {
    try {
      const data = await this.budgetHistoryModel
        .find({
          challenge: challengeID,
          type: 'sponsor',
        })
        .populate('creator')
        .populate('product')
        .sort({
          amount: -1,
        });
      return data;
    } catch (error) {
      throw error;
    }
  }

  async calcBudgetByChallengeId(challengeID: string) {
    try {
      const data = await this.budgetHistoryModel.aggregate([
        {
          $group: {
            _id: '$challenge',
            total: {
              $sum: '$amount',
            },
          },
        },
      ]);
      return data;
    } catch (error) {
      console.log('calcBudgetByChallengeId: ', error);
      throw error;
    }
  }
}
