import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  BudgetHistory,
  BudgetHistorySchema,
} from 'src/schema/budget-history.schema';
import { BudgetHistoryService } from './budget-history.service';
import { EventsModule } from 'src/events/events.module';
import { RunnerModule } from '../runner/runner.module';
import { BudgetHistoryController } from './budget-history.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: BudgetHistory.name, schema: BudgetHistorySchema },
    ]),
    EventsModule,
    RunnerModule,
  ],
  providers: [BudgetHistoryService],
  exports: [BudgetHistoryService],
  controllers: [BudgetHistoryController],
})
export class BudgetHistoryModule {}
