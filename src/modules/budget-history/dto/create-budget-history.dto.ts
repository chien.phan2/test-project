import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional } from 'class-validator';
import { Challenge } from 'src/schema/challenge.schema';

export class CreateBudgetHistoryDto {
  @ApiProperty()
  @IsNotEmpty()
  creator: string;

  @ApiProperty()
  @IsNotEmpty()
  challenge: Challenge;

  @ApiProperty()
  @IsOptional()
  product: string;

  @ApiProperty()
  @IsNotEmpty()
  type: string;

  @ApiProperty({ minimum: 0 })
  @IsNotEmpty()
  @IsNumber()
  amount: number;
}
