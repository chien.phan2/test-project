import { Test, TestingModule } from '@nestjs/testing';
import { ExcelExportController } from './excel-export.controller';

describe('ExcelExportController', () => {
  let controller: ExcelExportController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ExcelExportController],
    }).compile();

    controller = module.get<ExcelExportController>(ExcelExportController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
