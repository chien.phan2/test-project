import { Controller, Get, Res } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { unlinkSync } from 'fs';
import { ExcelExportService } from './excel-export.service';

@ApiTags('Challenge')
@Controller('challenge')
export class ExcelExportController {
  constructor(private readonly excelExportService: ExcelExportService) {}

  @Get('generate-excel-export')
  async generateExcel(@Res() res: Response) {
    // Generate the Excel file using the service
    const filePath = await this.excelExportService.generateExcelFile();

    // Set the response headers for the Excel file

    res.setHeader(
      'Content-Type',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    );
    res.setHeader('Content-Disposition', 'attachment; filename=example.xlsx');

    // Send the Excel file as the response
    res.sendFile(filePath, () => {
      // Cleanup the temporary file after sending
      res.on('finish', () => {
        unlinkSync(filePath);
      });
    });
  }
}
