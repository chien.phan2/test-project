import { BadRequestException, Injectable } from '@nestjs/common';
import * as ExcelJS from 'exceljs';
import * as tmp from 'tmp';
import { GroupParticipationService } from '../group-participation/group-participation.service';
import { ParticipationService } from '../participation/participation.service';
import { GroupParticipationRequestService } from '../group-participation-request/group-participation-request.service';

@Injectable()
export class ExcelExportService {
  constructor(
    private readonly groupParticipationService: GroupParticipationService,
    private readonly groupParticipationRequestService: GroupParticipationRequestService,
    private readonly participationService: ParticipationService,
  ) {}

  async generateExcelFile(): Promise<any> {
    try {
      const roundDistanceKm = (distance) => {
        return (distance || 0) / 1000;
      };

      // Create a new Excel workbook and worksheet
      const workbook = new ExcelJS.Workbook();

      const worksheetGroup = workbook.addWorksheet('Thành tích Nhóm');

      const dataGroup =
        await this.groupParticipationService.getAllGroupParticipation(
          '651651b901b75594969672c6',
        );

      worksheetGroup.addRow([
        'STT',
        'Tên nhóm',
        'Số luợng thành viên',
        'Thành tích',
      ]);
      dataGroup.forEach((group, index) => {
        worksheetGroup.addRow([
          `${index + 1}`,
          group?.name,
          group?.members?.length,
          [roundDistanceKm(group?.distance).toFixed(2), 'km'].join(''),
        ]);
      });

      const worksheetMember = workbook.addWorksheet('Thành tích Cá nhân');

      const dataMember = await this.participationService.getChallengeDetail(
        '64b4e4367d3644b003f18956',
        '651651b901b75594969672c6',
      );

      const getGroupOfRunner = async (athlete) => {
        const runnerInGroup =
          await this.groupParticipationRequestService.getGroupOfRunner(
            athlete?.runnerId?._id?.toString(),
          );
        return {
          id: athlete?.runnerId?._id?.toString(),
          name: runnerInGroup?.[0]?.group?.name,
        };
      };

      worksheetMember.addRow([
        'STT',
        'Tên VĐV',
        'SĐT',
        'Thành tích',
        'Nhóm tham gia',
        'Top 500',
        'Size áo T500',
        'Top 150',
        'Size áo T150',
        'Loại áo T150',
      ]);

      const listAthletes = dataMember?.listAthletes || [];
      const promises = [];
      listAthletes.forEach((athlete) => {
        promises.push(getGroupOfRunner(athlete));
      });

      const dataPromise = await Promise.all(promises);

      (dataMember?.listAthletes || [])
        ?.sort(
          (a, b) =>
            (b?.athleteStats?.distance || 0) - (a?.athleteStats?.distance || 0),
        )
        .forEach((athlete: any, index) => {
          worksheetMember.addRow([
            `${index + 1}`,
            athlete?.runnerId?.name,
            athlete?.runnerId?.phone,
            [
              roundDistanceKm(athlete?.athleteStats?.distance).toFixed(2),
              'km',
            ].join(''),
            dataPromise?.find(
              (item) => item?.id === athlete?.runnerId?._id?.toString(),
            )?.name || '',
            index + 1 <= 500 ? 'X' : '',
            index + 1 <= 500 ? athlete?.runnerId?.size : '',
            index + 1 <= 150 ? 'X' : '',
            index + 1 <= 150 ? athlete?.runnerId?.sizeOption : '',
            index + 1 <= 150 ? athlete?.runnerId?.typeClothes : '',
          ]);
        });

      const worksheetMale = workbook.addWorksheet('Thành tích Cá nhân Nam');
      worksheetMale.addRow([
        'STT',
        'Tên VĐV',
        'SĐT',
        'Thành tích',
        'Nhóm tham gia',
      ]);
      (dataMember?.listAthletes || [])
        ?.filter((item: any) => item?.runnerId?.sex == 'Male')
        ?.sort(
          (a, b) =>
            (b?.athleteStats?.distance || 0) - (a?.athleteStats?.distance || 0),
        )
        .forEach((athlete: any, index) => {
          worksheetMale.addRow([
            `${index + 1}`,
            athlete?.runnerId?.name,
            athlete?.runnerId?.phone,
            [
              roundDistanceKm(athlete?.athleteStats?.distance).toFixed(2),
              'km',
            ].join(''),
            dataPromise?.find(
              (item) => item?.id === athlete?.runnerId?._id?.toString(),
            )?.name || '',
          ]);
        });

      const worksheetFemale = workbook.addWorksheet('Thành tích Cá nhân Nữ');
      worksheetFemale.addRow([
        'STT',
        'Tên VĐV',
        'SĐT',
        'Thành tích',
        'Nhóm tham gia',
      ]);
      (dataMember?.listAthletes || [])
        ?.filter((item: any) => item?.runnerId?.sex == 'Female')
        ?.sort(
          (a, b) =>
            (b?.athleteStats?.distance || 0) - (a?.athleteStats?.distance || 0),
        )
        .forEach((athlete: any, index) => {
          worksheetFemale.addRow([
            `${index + 1}`,
            athlete?.runnerId?.name,
            athlete?.runnerId?.phone,
            [
              roundDistanceKm(athlete?.athleteStats?.distance).toFixed(2),
              'km',
            ].join(''),
            dataPromise?.find(
              (item) => item?.id === athlete?.runnerId?._id?.toString(),
            )?.name || '',
          ]);
        });

      // Generate a temporary file path
      const tmpFile = tmp.fileSync({ postfix: '.xlsx' });

      // Write the workbook to the temporary file
      await workbook.xlsx.writeFile(tmpFile.name);

      // Return the path to the temporary Excel file
      return tmpFile.name;
    } catch (error) {
      console.log('=> generateExcelFile => error: ', error);
      throw new BadRequestException(error);
    }
  }
}
