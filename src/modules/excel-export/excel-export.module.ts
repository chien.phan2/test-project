import { Module } from '@nestjs/common';
import { ExcelExportService } from './excel-export.service';
import { ExcelExportController } from './excel-export.controller';
import { GroupParticipationModule } from '../group-participation/group-participation.module';
import { ParticipationModule } from '../participation/participation.module';
import { GroupParticipationRequestModule } from '../group-participation-request/group-participation-request.module';

@Module({
  imports: [
    GroupParticipationModule,
    GroupParticipationRequestModule,
    ParticipationModule,
  ],
  controllers: [ExcelExportController],
  providers: [ExcelExportService],
  exports: [ExcelExportService],
})
export class ExcelExportModule {}
