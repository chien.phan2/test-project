import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class ReferralRunnerDto {
  @ApiProperty()
  @IsNotEmpty()
  referralCode: string;
}
