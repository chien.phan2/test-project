import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateMedalDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  medal: string;
}
