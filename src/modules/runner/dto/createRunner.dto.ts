import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString, Matches } from 'class-validator';

export class CreateRunnerDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  userID: string;

  @ApiProperty()
  @IsOptional()
  @Matches(/^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9|7]|9[0-4|6-9])[0-9]{7}$/)
  phone: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  name: string;

  @ApiProperty()
  @IsOptional()
  stravaData: any;

  @ApiProperty()
  @IsOptional()
  @IsString()
  size: 'XS' | 'S' | 'M' | 'L' | 'XL' | '2XL' | '3XL';

  @ApiProperty()
  @IsOptional()
  @IsString()
  sizeOption: 'XS' | 'S' | 'M' | 'L' | 'XL' | '2XL' | '3XL';

  @ApiProperty()
  @IsOptional()
  @IsString()
  typeClothes: 'T-shirt' | 'Singlet';

  @ApiProperty()
  @IsOptional()
  @IsString()
  sex: 'Male' | 'Female';

  @ApiProperty()
  @IsOptional()
  @IsString()
  department: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  authCode: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  partnerUserId: string;
}
