import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber } from 'class-validator';

export class UpdateEnergyDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  energy: number;
}
