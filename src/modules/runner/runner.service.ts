import {
  BadRequestException,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Model, Connection } from 'mongoose';
import { EventsGateway } from 'src/events/events.gateway';
import { Runner, RunnerDocument } from 'src/schema/runner.schema';
import { ActivityService } from '../activity/activity.service';
import { EnergyService } from '../energy/energy.service';
import { ParticipationService } from '../participation/participation.service';
import { QueryWheelPagination } from '../wheel/dto/queryWheelPagination.dto';
import { WheelService } from '../wheel/wheel.service';
import { CreateRunnerDto } from './dto/createRunner.dto';
import { PublicRunner } from './dto/public-runner.dto';
import { ChallengeService } from '../challenge/challenge.service';
import { ReferralRunnerDto } from './dto/referral-runner.dto';
import { MissionService } from '../mission/mission.service';
import { MissionHistoryService } from '../mission-history/mission-history.service';

@Injectable()
export class RunnerService {
  constructor(
    @InjectModel(Runner.name)
    private readonly runnerModel: Model<RunnerDocument>,

    private readonly activityService: ActivityService,

    private readonly energyService: EnergyService,

    private readonly wheelService: WheelService,

    private readonly eventsGateway: EventsGateway,

    private readonly participationService: ParticipationService,

    private readonly missionService: MissionService,

    @Inject(forwardRef(() => MissionHistoryService))
    private readonly missionHistoryService: MissionHistoryService,

    @InjectConnection() private connection: Connection,
  ) {}

  async createRunner(createRunnerDto: CreateRunnerDto) {
    const runner = await this.runnerModel.findOne({
      userID: createRunnerDto.userID,
    });
    if (runner) {
      let updatedData = runner;
      if (!runner.phone || !runner.name || runner.stravaData) {
        if (
          createRunnerDto?.stravaData &&
          createRunnerDto?.stravaData?.athlete &&
          !createRunnerDto.stravaData.athlete.id
        ) {
          throw new BadRequestException(
            'Thiếu dữ liệu StravaID của vận động viên',
          );
        }
        const avatar = createRunnerDto?.stravaData?.athlete?.avatar || null;
        updatedData = await this.runnerModel.findByIdAndUpdate(
          {
            _id: runner.id,
          },
          {
            ...createRunnerDto,
            avatar: avatar ? avatar : runner.avatar ? runner.avatar : null,
          },
          { returnOriginal: false },
        );
      }
      return {
        data: new PublicRunner(updatedData?.toJSON()),
        message: 'Sync runner successfully',
      };
    }

    const newRunner = await this.runnerModel.create({
      ...createRunnerDto,
      isBlocked: false,
      createdAt: new Date().toLocaleString(),
      updatedAt: new Date().toLocaleString(),
    });
    await newRunner.save();

    return {
      data: new PublicRunner(newRunner.toJSON()),
      message: 'Create runner successfully',
    };
  }

  async updateRunner(id: string, data: any) {
    try {
      const runner = await this.runnerModel.findByIdAndUpdate(
        {
          _id: id,
        },
        { ...data },
        { returnOriginal: false },
      );
      return {
        data: new PublicRunner(runner?.toJSON()),
        message: 'Update runner successfully',
      };
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async findRunnerById(id: string) {
    try {
      const runner = await this.runnerModel
        .findOne({
          _id: id,
        })
        .populate('medals')
        .exec();
      if (!runner) throw new BadRequestException('Runner not found');
      const athleteStats =
        await this.activityService.getActivityStatsByRunnerId(runner?._id);
      const data = {
        ...runner.toJSON(),
        ...athleteStats,
        energy: runner?.energy + runner?.giftEnergy,
      };
      return data;
    } catch (error) {
      console.log('findRunnerById: ', error);
      throw new BadRequestException(error.message);
    }
  }

  async getStravaIdFromRunner(id: string) {
    try {
      const runner = await this.runnerModel
        .findOne({
          _id: id,
        })
        .select('stravaData')
        .exec();
      if (!runner) throw new BadRequestException('Runner not found');
      return runner;
    } catch (error) {
      console.log('getStravaIdFromRunner: ', error);
      throw new BadRequestException(error.message);
    }
  }

  async findInfoRunner(id: string) {
    try {
      const runner = await this.runnerModel
        .findOne({
          _id: id,
        })
        .select('_id name avatar phone')
        .exec();
      if (!runner) throw new BadRequestException('Runner not found');
      return runner;
    } catch (error) {
      console.log('findInfoRunner: ', error);
      throw new BadRequestException(error.message);
    }
  }

  async findAuthenRunner(id: string) {
    try {
      const runner = await this.runnerModel
        .findOne({
          _id: id,
        })
        .select('_id authCode partnerUserId name')
        .exec();
      if (!runner) throw new BadRequestException('Runner not found');
      return runner;
    } catch (error) {
      console.log('findAuthenRunner: ', error);
      throw new BadRequestException(error.message);
    }
  }

  async findPublicRunnerById(id: string) {
    try {
      const runner = await this.runnerModel
        .findOne({
          _id: id,
        })
        .select('-userID -phone -isBlocked -__v -createdAt -updatedAt -energy')
        .populate('medals')
        .exec();
      const participant =
        await this.participationService.getParticipationByRunnerId(id);

      if (!runner) throw new BadRequestException('Runner not found');

      const dataRunner = new PublicRunner(runner.toJSON());
      dataRunner.stravaData = {
        athlete: {
          firstname: runner?.stravaData?.athlete?.firstname,
          lastname: runner?.stravaData?.athlete?.lastname,
          bio: runner?.stravaData?.athlete?.bio,
          avatar: runner?.stravaData?.athlete?.avatar,
          id: runner?.stravaData?.athlete?.id,
        },
      };

      const athleteStats =
        await this.activityService.getActivityStatsByRunnerId(runner?._id);
      const data = {
        ...dataRunner,
        ...athleteStats,
        challenges: [...participant] || [],
      };
      delete data._id;
      return data;
    } catch (error) {
      console.log('findPublicRunnerById: ', error);
      throw new BadRequestException('Không tìm thấy thông tin vận động viên');
    }
  }

  async findRunnerByStravaId(stravaId: number) {
    const runner = await this.runnerModel.findOne({
      'stravaData.athlete.id': stravaId,
    });
    if (!runner) throw new BadRequestException('Runner not found');
    return runner;
  }

  async updateEnergyForRunner(
    id: string,
    product: {
      productId: string;
      quantity: number;
    },
  ) {
    try {
      const runner = await this.findRunnerById(id);

      const productData = await this.energyService.getProductEnergyById(
        product?.productId,
      );

      const energy =
        (productData?.productQuantity || 0) * (product?.quantity || 0);

      const newEnergy = (runner?.energy || 0) + energy;
      const updateEnergy = await this.runnerModel.findByIdAndUpdate(
        {
          _id: id,
        },
        {
          energy: newEnergy,
          updatedAt: new Date().toLocaleString(),
        },
      );

      const totalEnergy = newEnergy + (runner?.giftEnergy || 0);

      setTimeout(() => {
        this.eventsGateway.onChangeEnergy({
          userId: runner?._id.toString(),
          status: 'success',
          energy: totalEnergy,
        });
      }, 7000);

      return updateEnergy.toJSON();
    } catch (err) {
      console.log('updateEnergyForRunner: ', err);
      throw err;
    }
  }

  async updateMedalForRunner(id: string, medalId: any) {
    try {
      const runner = await this.runnerModel.findById({
        _id: id,
      });
      if (runner.medals?.includes(medalId.toString())) {
        return runner;
      }
      const newMedals = [...(runner?.medals || []), medalId.toString()];
      console.log('====> newMedals:', newMedals);
      await runner.updateOne(
        {
          $set: { medals: newMedals },
          updatedAt: new Date().toLocaleString(),
        },
        { new: true },
      );

      return runner;
    } catch (error) {
      console.log('updateMedalForRunner: ', error);
      throw error;
    }
  }

  async updateTurnRotation(id: string) {
    try {
      console.log('update turn to user', {
        userId: id,
      });
      const runner = await this.findRunnerById(id);
      if (runner.energy === 0) {
        return true;
      }
      const newEnergy = runner.energy - 1;
      await this.runnerModel.findByIdAndUpdate(
        {
          _id: id,
        },
        {
          energy: newEnergy,
          updatedAt: new Date().toLocaleString(),
        },
      );

      const totalEnergy = newEnergy + (runner?.giftEnergy || 0);

      setTimeout(() => {
        this.eventsGateway.onChangeEnergy({
          userId: runner?._id.toString(),
          status: 'success',
          energy: totalEnergy,
        });
      }, 2500);
    } catch (err) {
      console.log('updateTurnRotation', err);
      throw err;
    }
  }

  async addEnergyAfterWinPrize(id: string, amount: number) {
    try {
      console.info('addEnergyAfterWinPrize', {
        userId: id,
        amount: amount,
      });
      const runner = await this.findRunnerById(id);
      if (runner.energy === 0) {
        return true;
      }
      const newEnergy = (runner?.energy || 0) + amount;

      await this.runnerModel.findByIdAndUpdate(
        {
          _id: id,
        },
        {
          energy: newEnergy,
          updatedAt: new Date().toLocaleString(),
        },
      );

      const totalEnergy = newEnergy + (runner?.giftEnergy || 0);

      setTimeout(() => {
        this.eventsGateway.onChangeEnergy({
          userId: id,
          status: 'success',
          energy: totalEnergy,
        });
      }, 2500);
    } catch (err) {
      console.log('addEnergyAfterWinPrize', err);
      throw err;
    }
  }

  async getTurnRotation(id: string) {
    try {
      const runner = await this.findRunnerById(id);
      return runner.energy;
    } catch (err) {
      throw err;
    }
  }

  async getListRunner(query?: QueryWheelPagination) {
    try {
      const totalPage = await this.runnerModel.countDocuments();
      const listUser = await this.runnerModel.find({});
      const listStatistical = listUser?.map(
        (item) =>
          new Promise((resolve) =>
            this.wheelService
              .getStatiscalWheelByUserId(item?._id?.toString())
              .then((res) =>
                resolve({
                  ...item?.toJSON(),
                  ...res,
                }),
              )
              .catch((err) => resolve(err)),
          ),
      );
      const listData = await Promise.all(listStatistical);
      const field = query?.sortBy || 'createdAt';
      const sortData = listData
        ?.sort((a, b) =>
          query?.sortType === -1
            ? a?.[field] - b?.[field]
            : b?.[field] - a?.[field],
        )
        ?.slice(query?.skip, query?.limit);
      return {
        users: sortData,
        total: totalPage,
      };
    } catch (error) {
      throw error;
    }
  }

  async updateWheelAttribue() {
    try {
      const listUser = await this.runnerModel.find({});
      const listStatistical = listUser?.map(
        (item) =>
          new Promise((resolve) =>
            this.wheelService
              .getHistoryWheelByUserId(item?._id?.toString())
              .then(async () => {
                await this.runnerModel
                  .findByIdAndUpdate(item?._id?.toString(), {
                    wheels: [],
                  })
                  .then((newRes) => resolve(newRes))
                  .catch((newErr) => resolve(newErr));
              })
              .catch((err) => resolve(err)),
          ),
      );
      await Promise.all(listStatistical);
      return true;
    } catch (error) {
      throw error;
    }
  }

  async getAllUser() {
    try {
      const listUser = await this.runnerModel.find({});
      return listUser;
    } catch (error) {
      console.log('getAllUser', error);
      throw error;
    }
  }

  async updateReferral(id: string, payload: ReferralRunnerDto) {
    try {
      const currentRunner = await this.runnerModel.findById(id);
      if (!currentRunner)
        throw new BadRequestException('Không tìm thấy vận động viên');
      if (currentRunner?.referralCode)
        throw new BadRequestException(
          'Bạn đã được giới thiệu bởi vận động viên khác',
        );
      await currentRunner.updateOne(
        {
          referralCode: payload?.referralCode,
          updatedAt: new Date().toLocaleString(),
        },
        {
          new: true,
        },
      );
      const referralRunner = await this.runnerModel.findById(
        payload?.referralCode,
      );
      const newReferralList = [
        ...(referralRunner?.referralList || []),
        currentRunner?._id?.toString(),
      ];
      await referralRunner.updateOne(
        {
          $set: { referralList: newReferralList },
          updatedAt: new Date().toLocaleString(),
        },
        { new: true },
      );
      return {
        message: 'Successfully',
      };
    } catch (error) {
      console.log('updateReferral', error);
      throw error;
    }
  }

  async updateGiftEnergy(id: string, energy: number) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const runner = await this.runnerModel.findById(id);
      const currGiftEnergy = runner?.giftEnergy || 0;
      console.log('====> currGiftEnergy:', currGiftEnergy);
      const newGiftEnergy = currGiftEnergy + energy;
      console.log('====> newGiftEnergy:', newGiftEnergy);
      await runner.updateOne(
        {
          $set: { giftEnergy: newGiftEnergy },
          updatedAt: new Date().toLocaleString(),
        },
        { new: true },
      );

      const totalEnergy = newGiftEnergy + (runner?.energy || 0);
      console.log('====> totalEnergy:', totalEnergy);

      setTimeout(() => {
        this.eventsGateway.onChangeEnergy({
          userId: id,
          status: 'success',
          energy: totalEnergy,
        });
      }, 7000);

      await session.commitTransaction();
      session.endSession();
      return runner;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      console.log('updateGiftEnergy', error);
      throw error;
    }
  }

  async checkFirstPayment(id: string) {
    try {
      const MISSION_KEY = 'first-payment';

      const mission = await this.missionService.getMissionByKey(MISSION_KEY);
      console.log('====> mission:', mission);
      if (!mission) throw new BadRequestException('Không tìm thấy nhiệm vụ');

      const runner = await this.runnerModel.findById(id);
      if (!runner)
        throw new BadRequestException('Không tìm thấy vận động viên');

      const isFirstPay = runner?.isFirstPay || false;
      console.log('====> isFirstPay:', isFirstPay);
      if (isFirstPay) return;

      console.log('====> update isFirstPay');
      await runner.updateOne({
        isFirstPay: true,
      });

      await this.updateGiftEnergy(
        runner?._id?.toString(),
        mission?.reward || 0,
      );

      await this.missionHistoryService.updateMissionHistory(
        runner?._id?.toString(),
        mission,
      );

      const referralCode = runner?.referralCode || '';
      if (referralCode) {
        await this.checkReferralReward(referralCode?.toString());
      }
    } catch (error) {
      console.log('checkFistPayment', error);
      throw error;
    }
  }

  async checkReferralReward(id: string) {
    try {
      const MISSION_KEY = 'referral-reward';

      const mission = await this.missionService.getMissionByKey(MISSION_KEY);
      if (!mission) throw new BadRequestException('Không tìm thấy nhiệm vụ');

      await this.updateGiftEnergy(id?.toString(), mission?.reward || 0);

      await this.missionHistoryService.updateMissionHistory(
        id?.toString(),
        mission,
      );
    } catch (error) {
      console.log('checkReferralReward', error);
      throw error;
    }
  }

  async getRunnerBySex(sex: string) {
    try {
      const runner = await this.runnerModel
        .find({
          sex,
        })
        .select('_id')
        .exec();

      return runner;
    } catch (error) {
      console.log('getRunnerBySex', error);
      throw new BadRequestException(error);
    }
  }
}
