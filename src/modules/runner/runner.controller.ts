import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { CreateRunnerDto } from './dto/createRunner.dto';
import { RunnerService } from './runner.service';
import { ReferralRunnerDto } from './dto/referral-runner.dto';

@ApiTags('Challenge')
@Controller('challenge')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class RunnerController {
  constructor(private readonly runnerService: RunnerService) {}

  @Post('/sync-runner')
  findRunnerById(@Body() createRunnerDto: CreateRunnerDto) {
    return this.runnerService.createRunner(createRunnerDto);
  }

  @Get('/get-info-runner')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  getInfoRunner(@Req() req: Request) {
    const userID = req.userID;
    return this.runnerService.findRunnerById(userID);
  }

  @Get('/get-runner-public/:id')
  getRunnerPublic(@Param('id') id: string) {
    return this.runnerService.findPublicRunnerById(id);
  }

  @Patch('/runner/referral')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  updateReferral(@Req() req: Request, @Body() payload: ReferralRunnerDto) {
    const userID = req.userID;
    return this.runnerService.updateReferral(userID, payload);
  }

  @Get('/get-list-runner')
  getListRunner() {
    return this.runnerService.getAllUser();
  }
}
