import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EventsModule } from 'src/events/events.module';
import { Runner, RunnerSchema } from 'src/schema/runner.schema';
import { ActivityModule } from '../activity/activity.module';
import { EnergyModule } from '../energy/energy.module';
import { ParticipationModule } from '../participation/participation.module';
import { RunnerController } from './runner.controller';
import { RunnerService } from './runner.service';
import { WheelModule } from '../wheel/wheel.module';
import { ChallengeModule } from '../challenge/challenge.module';
import { MissionModule } from '../mission/mission.module';
import { MissionHistoryModule } from '../mission-history/mission-history.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Runner.name, schema: RunnerSchema }]),
    ActivityModule,
    ParticipationModule,
    EnergyModule,
    EventsModule,
    WheelModule,
    forwardRef(() => ChallengeModule),
    MissionModule,
    forwardRef(() => MissionHistoryModule),
  ],
  providers: [RunnerService],
  exports: [RunnerService],
  controllers: [RunnerController],
})
export class RunnerModule {}
