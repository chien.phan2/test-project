import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Model, Connection } from 'mongoose';
import { MY_MESSAGE } from '../../contants/message.contants';
import { Reward } from '../../schema/reward.schema';
import { Wheel } from '../../schema/wheel.schema';
import { RewardCode, RewardList } from '../../utils/constants';
import randomSpinWheel from '../../utils/randomSpinWheel';
import { ChallengeService } from '../challenge/challenge.service';
import { InventoryService } from '../inventory/inventory.service';
import { RankRewardService } from '../rank-reward/rank-reward.service';
import { RewardService } from '../reward/reward.service';
import { RunnerService } from '../runner/runner.service';
import { QueryWheelPagination } from './dto/queryWheelPagination.dto';
import * as _ from 'lodash';

const PLUS_ENERGY = 1;

@Injectable()
export class WheelService {
  constructor(
    @InjectModel(Wheel.name) private readonly wheelModel: Model<Wheel>,
    @Inject(forwardRef(() => RewardService))
    private readonly rewardService: RewardService,

    @Inject(forwardRef(() => InventoryService))
    private readonly inventoryService: InventoryService,

    @Inject(forwardRef(() => RunnerService))
    private readonly runnerService: RunnerService,

    @Inject(forwardRef(() => ChallengeService))
    private readonly challengeService: ChallengeService,

    @Inject(forwardRef(() => RankRewardService))
    private readonly rankRewardService: RankRewardService,

    @InjectConnection() private connection: Connection,
  ) {}

  async getListConfigRewards() {
    try {
      const listService = await this.rewardService.getRewards();
      return listService;
    } catch (err) {
      throw err;
    }
  }

  async getListRewards() {
    try {
      const listService = await this.rewardService.getRewards();
      const transformListService = listService.reduce((arr, item) => {
        return {
          ...arr,
          [item?.code]: item,
        };
      }, {});
      return RewardList.map((item) => ({
        ...transformListService?.[item?.code].toJSON(),
        prizeKey: item?.id,
      }));
    } catch (err) {
      throw err;
    }
  }

  async saveHistoryWheel({
    userId,
    dataReward,
    isSuccess,
    challengeId,
    source,
  }: {
    userId: string;
    dataReward: Reward;
    isSuccess: boolean;
    challengeId: string;
    source: string;
  }) {
    try {
      const newEntity = new this.wheelModel({
        creator: userId,
        value: dataReward?.value,
        type: dataReward?.type,
        code: dataReward?.code,
        name: dataReward?.name,
        isSuccess: isSuccess,
        url: dataReward?.url,
        challengeId: challengeId,
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString(),
      });
      return await newEntity.save();
    } catch (err) {
      console.log('saveHistoryWheel', err);
      throw err;
    }
  }

  async spinLuckyWheelService(userId: string) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const paidChallenge =
        await this.challengeService.getAvailablePaidChallenge();
      const runner = await this.runnerService.findRunnerById(userId);
      const isUseGiftedEnergy = runner?.giftEnergy > 0;
      console.log('isUseGiftedEnergy', isUseGiftedEnergy);
      const availableTurn = isUseGiftedEnergy
        ? runner.giftEnergy
        : runner.energy;
      if (availableTurn === 0) {
        return {
          errorMessage: MY_MESSAGE.NOT_AVAILABLE_TURN,
          spinSuccess: false,
          availableTurn: availableTurn,
        };
      }
      const listService = await this.rankRewardService.getRankReward();
      const countWheels = await this.wheelModel
        .find({
          creator: userId,
        })
        .count();
      console.log('countWheels', countWheels);
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const rate = listService.map((item) =>
        countWheels > 30 ? item?.rewardId?.rate_30 : item?.rewardId?.rate,
      );
      const turnSpin = randomSpinWheel(listService, rate);
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      const isSuccess = !!listService?.find(
        (item) => item?.rank === turnSpin?.prize?.rank,
      );

      // console.info('/challenge/lucky-spin', {
      //   userId: userId,
      //   availableTurn: availableTurn,
      //   prize: turnSpin,
      //   isSuccess: isSuccess,
      // });
      await this.saveHistoryWheel({
        userId,
        dataReward: turnSpin?.prize?.rewardId,
        isSuccess,
        challengeId: paidChallenge?._id,
        source: isUseGiftedEnergy ? 'gifted' : '',
      });
      // await this.runnerService.updateNewWheels(
      //   userId,
      //   wheelEntity?._id?.toString(),
      // );
      if (isSuccess) {
        const filterPrizeList = listService.filter(
          (item) => item?.rank === turnSpin?.prize?.rank,
        );
        const randomIndex = Math.floor(
          Math.random() * (filterPrizeList?.length || 0),
        );
        const prizeKey = filterPrizeList?.[randomIndex]?.prizeKey;

        //Win ticket
        if (turnSpin?.prize?.rewardId?.code === RewardCode.Challenge_Ticket) {
          await this.inventoryService.createInventoryAfterWinPrize(
            userId,
            turnSpin.prize?.rewardId,
            prizeKey,
          );
          if (!isUseGiftedEnergy) {
            await this.challengeService.updateBudgetChallenge(
              userId,
              1000,
              'wheel',
              null,
            );
          }
        } else if (turnSpin?.prize?.rewardId?.code === RewardCode.Energy_100) {
          //Win one more turn
          await this.runnerService.addEnergyAfterWinPrize(userId, PLUS_ENERGY);
        } else if (!isUseGiftedEnergy) {
          //Good Luck
          await this.challengeService.updateBudgetChallenge(
            userId,
            1000,
            'wheel',
            null,
          );
        }

        if (isUseGiftedEnergy) {
          await this.runnerService.updateGiftEnergy(userId, -1);
        } else {
          await this.runnerService.updateTurnRotation(userId);
        }

        await session.commitTransaction();
        session.endSession();

        return {
          reward: turnSpin?.prize,
          prizeKey:
            filterPrizeList?.length > 1
              ? filterPrizeList?.[
                  Math.floor(Math.random() * filterPrizeList?.length)
                ]?.prizeKey
              : filterPrizeList?.[0]?.prizeKey,
          availableTurn: availableTurn - 1,
          spinSuccess: isSuccess,
        };
      }
      await session.commitTransaction();
      session.endSession();
      return {
        availableTurn: availableTurn,
        errorMessage: MY_MESSAGE.SOMETHING_WRONG,
        spinSuccess: isSuccess,
      };
    } catch (err) {
      await session.abortTransaction();
      session.endSession();
      console.log('spinLuckyWheelService', err);
      throw err;
    }
  }

  async getHistoryWheelByUserId(userId: string, query?: QueryWheelPagination) {
    try {
      return this.wheelModel
        .find({
          creator: userId,
        })
        .limit(query?.limit || 0)
        .skip(query?.skip || 0)
        .sort({ createdAt: -1 });
    } catch (error) {
      console.log('getHistoryWheelByUserId: ', error);
      throw error;
    }
  }
  async getStatiscalWheelByUserId(userId: string) {
    try {
      const paidChallenges =
        await this.challengeService.getAvailablePaidChallenge();
      const listWheel = await this.wheelModel.find({
        creator: userId,
        challengeId: paidChallenges?._id,
      });
      const ticketWheel = await this.inventoryService.getInventoriesByUserId(
        userId,
        true,
      );
      const goodLuckWheel = listWheel?.filter(
        (item) => item?.code === 'GoodLuck',
      )?.length;
      const energyWheel = listWheel?.filter(
        (item) => item?.code === 'Energy100',
      )?.length;
      return {
        totalWheel: listWheel?.length,
        goodLuckWheel: goodLuckWheel,
        energyWheel: energyWheel,
        ticketWheel: ticketWheel?.length,
      };
    } catch (error) {
      throw error;
    }
  }

  async getCountSpinByAllUser() {
    try {
      const data = await this.wheelModel.aggregate([
        {
          $group: {
            _id: '$creator',
            count: { $sum: 1 },
            // totalGoodLuck: {
            //   $sum: {
            //     $cond: [{ $eq: ['$code', 'GoodLuck'] }, 1, 0],
            //   },
            // },
            // totalEnergy: {
            //   $sum: {
            //     $cond: [{ $eq: ['$code', 'Energy100'] }, 1, 0],
            //   },
            // },
          },
        },
      ]);

      const listIdsUserHasWheel = data?.map((item) => item?._id);
      console.log('====> listIdsUserHaseWheel:', listIdsUserHasWheel?.length);

      // const updatePromises = listIdsUserHasWheel.map(async (runnerId) => {
      //   try {
      //     return await this.runnerService.updateRunner(runnerId, {
      //       isFirstPay: true,
      //     });
      //   } catch (error) {
      //     console.error(
      //       `Error updating runner with ID ${runnerId}: ${error.message}`,
      //     );
      //     return null;
      //   }
      // });

      // const updateResults = await Promise.all(updatePromises);
      // console.log('====> updateResults:', updateResults);

      const allUser = await this.runnerService.getAllUser();

      const listIdsAllUser = allUser?.map((item) => item?._id);
      console.log('===> listIdsAllUser:', listIdsAllUser?.length);

      const usersWithoutWheel = _.difference(
        listIdsUserHasWheel,
        listIdsAllUser,
      );
      console.log('======> usersWithoutWheel:', usersWithoutWheel.length);
      return {
        data: {
          totalUser: listIdsAllUser?.length,
          totalUserHasWheel: listIdsUserHasWheel?.length,
        },
      };
    } catch (error) {
      console.log('getCountSpinByAllUser: ', error);
      throw error;
    }
  }
}
