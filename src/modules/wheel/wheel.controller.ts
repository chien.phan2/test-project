import {
  BadRequestException,
  Controller,
  Get,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { MY_MESSAGE } from '../../contants/message.contants';
import { AuthGuard } from '../../middleware/auth.middleware';
import { RankRewardService } from '../rank-reward/rank-reward.service';
import { QueryWheelPagination } from './dto/queryWheelPagination.dto';
import { WheelService } from './wheel.service';

@ApiTags('Challenge')
@Controller('challenge/wheel')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class WheelController {
  constructor(
    private readonly wheelService: WheelService,
    private readonly rankRewardService: RankRewardService,
  ) {}
  @Get('/history')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  async getHistoryWheel(
    @Req() req: Request,
    @Query() query: QueryWheelPagination,
  ) {
    try {
      const userID = req.userID;
      if (!userID) {
        return new BadRequestException(MY_MESSAGE.MISMATCH_USERID);
      }
      return this.wheelService.getHistoryWheelByUserId(userID, query);
    } catch (err) {
      throw err;
    }
  }

  @Get('/rewards')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  async getListRewards(@Req() req: Request) {
    try {
      const userID = req.userID;
      if (!userID) {
        return new BadRequestException(MY_MESSAGE.MISMATCH_USERID);
      }
      return this.rankRewardService.getRankReward();
    } catch (err) {
      throw err;
    }
  }

  @Get('/portal/rewards')
  async getListRewardsPortal(@Req() req: Request) {
    try {
      return this.wheelService.getListConfigRewards();
    } catch (err) {
      throw err;
    }
  }

  @Get('count-spin-by-all-user')
  async getCountSpinByAllUser() {
    try {
      return this.wheelService.getCountSpinByAllUser();
    } catch (err) {
      throw err;
    }
  }
}
