import { IsOptional } from 'class-validator';
import FilterDto from 'src/type/filter.type';

export class QueryWheelPagination extends FilterDto {
  @IsOptional()
  sortBy?: string;

  @IsOptional()
  sortType?: number;
}
