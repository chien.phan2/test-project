import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDataURI,
  IsEnum,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsString,
  Max,
  Min
} from "class-validator";
import { RewardCode, RewardType } from "src/utils/constants";

export class CreateWheelHistoryDto {
  @IsString()
  name: string;

  @ApiProperty()
  @IsNumber()
  @Min(0, { message: 'MIN_MAX_PRICE' })
  @Max(500, { message: 'MIN_MAX_PRICE' })
  value: number;

  @ApiProperty()
  @IsString()
  @IsEnum(RewardType, { message: 'Type must be Enum' })
  type: string;

  @ApiProperty()
  @IsString()
  @IsEnum(RewardCode, { message: 'Code must be Enum' })
  code: string;

  @ApiProperty()
  @IsString()
  @IsDataURI()
  url: string;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  isActive: boolean;
}
