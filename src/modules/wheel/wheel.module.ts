import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { WheelService } from './wheel.service';
import { Wheel, WheelSchema } from '../../schema/wheel.schema';
import { WheelController } from './wheel.controller';
import { RewardModule } from '../reward/reward.module';
import { RunnerModule } from '../runner/runner.module';
import { InventoryModule } from '../inventory/inventory.module';
import { ChallengeModule } from '../challenge/challenge.module';
import {RankRewardModule} from "../rank-reward/rank-reward.module";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Wheel.name, schema: WheelSchema }]),
    forwardRef(() => RewardModule),
    forwardRef(() => RunnerModule),
    forwardRef(() => InventoryModule),
    forwardRef(() => ChallengeModule),
    forwardRef(() => RankRewardModule),
  ],
  controllers: [WheelController],
  providers: [WheelService],
  exports: [WheelService],
})
export class WheelModule {}
