import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { TicketChallengeService } from './ticket-challenge.service';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { CreateTicketChallengeDto } from './dto/create-ticket-challenge.dto';
import { Request } from 'express';
import { UpdateTicketChallengeDto } from './dto/update-ticket-challenge.dto';

@ApiTags('Challenge')
@Controller('/challenge/ticket-challenge')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class TicketChallengeController {
  constructor(
    private readonly ticketChallengeService: TicketChallengeService,
  ) {}

  @Post('/create-ticket')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  createTicketChallenge(
    @Body() createTicketChallengeDto: CreateTicketChallengeDto,
    @Req() req: Request,
  ) {
    const userID = req.userID;
    return this.ticketChallengeService.createTicketChallenge(
      userID,
      createTicketChallengeDto,
    );
  }

  @Get('/get-ticket')
  getTicketChallenge() {
    return this.ticketChallengeService.getTicketChallenge();
  }

  @Patch('/update-ticket/:id')
  @ApiBearerAuth('JWT-auth')
  update(
    @Param('id') id: string,
    @Body() updateTicketDto: UpdateTicketChallengeDto,
  ) {
    return this.ticketChallengeService.updateSellTicket(id, updateTicketDto);
  }

  @Get('/my-ticket/:id')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  findOneMyTicket(@Param('id') id: string, @Req() req: Request) {
    const userID = req.userID;
    return this.ticketChallengeService.findOneMyTicket(id, userID);
  }

  @Get('/my-tickets')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  findMyTicket(@Req() req: Request) {
    const userID = req.userID;
    return this.ticketChallengeService.findMyTicket(userID);
  }

  @Get('/my-purchase')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  findMyPurchase(@Req() req: Request) {
    const userID = req.userID;
    return this.ticketChallengeService.findMyPurchase(userID);
  }

  @Get('/my-pending')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  findMyPending(@Req() req: Request) {
    const userID = req.userID;
    return this.ticketChallengeService.findMyPending(userID);
  }
}
