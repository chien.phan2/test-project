import {
  BadRequestException,
  Inject,
  Injectable,
  Logger,
  NotFoundException,
  forwardRef,
} from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import { DEFAULT_LIMIT, DEFAULT_SKIP } from 'src/contants/filter.contants';
import { MY_MESSAGE } from 'src/contants/message.contants';
import { EventsGateway } from 'src/events/events.gateway';
import {
  TicketChallenge,
  TicketChallengeDocument,
} from 'src/schema/ticketChallenge.schema';
import { InventoryService } from '../inventory/inventory.service';
import { ProductTransactionService } from '../product-transaction/product-transaction.service';
import { RunnerService } from '../runner/runner.service';
import { PublicTicket } from '../ticket/dto/public-ticket.dto';
import { CreateTicketChallengeDto } from './dto/create-ticket-challenge.dto';
import { PublicChallengeTicket } from './dto/public-ticket-challenge.dto';
import { UpdateTicketChallengeDto } from './dto/update-ticket-challenge.dto';

@Injectable()
export class TicketChallengeService {
  constructor(
    @InjectModel(TicketChallenge.name)
    private readonly ticketChallengeModel: Model<TicketChallengeDocument>,

    @Inject(forwardRef(() => RunnerService))
    private readonly runnerService: RunnerService,

    private readonly eventsGateway: EventsGateway,

    @Inject(forwardRef(() => ProductTransactionService))
    private readonly productTransactionService: ProductTransactionService,

    private readonly inventoryService: InventoryService,

    @InjectConnection() private connection: Connection,
  ) {}

  async createTicketChallenge(
    userID: string,
    createPayload: CreateTicketChallengeDto,
  ) {
    try {
      const runner = await this.runnerService.findRunnerById(userID);

      const isGiveAway = await this.inventoryService.checkItemIsGiveAway(
        createPayload?.code,
      );
      if (isGiveAway) {
        throw new BadRequestException('Vật phẩm được tặng không thể bán');
      }

      if (createPayload?.price < 1000) {
        throw new BadRequestException('Giá vé không hợp lệ');
      }

      const price = Math.round(createPayload?.price * 0.8);
      const donate = Math.round(createPayload?.price * 0.2);

      const existedTicketOnSell = await this.ticketChallengeModel.findOne({
        code: createPayload?.code,
        owner: runner._id?.toString(),
      });

      if (
        existedTicketOnSell?.status === 'available' ||
        existedTicketOnSell?.status === 'pending' ||
        existedTicketOnSell?.status === 'captured'
      ) {
        throw new BadRequestException('Vé đã được đăng bán');
      }

      if (existedTicketOnSell?.status === 'cancel') {
        await this.inventoryService.setInValidInventoryItem(
          createPayload?.code,
          true,
        );

        await existedTicketOnSell.updateOne({
          status: 'available',
          price,
          donate,
        });

        const ticket = await this.ticketChallengeModel
          .findOne({ code: createPayload?.code })
          .populate('owner')
          .exec();

        this.eventsGateway.onCreateChallengeTicket(
          new PublicChallengeTicket(ticket.toJSON()),
        );
        return {
          data: new PublicChallengeTicket(ticket.toJSON()),
          meta: createPayload,
        };
      }

      await this.inventoryService.setInValidInventoryItem(
        createPayload?.code,
        true,
      );

      const newTicketChallengeEntity = new this.ticketChallengeModel({
        code: createPayload?.code,
        price,
        donate,
        owner: runner._id?.toString(),
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString(),
        timestamp: Date.now(),
      });

      const dataTicketChallengeEntity = await (
        await newTicketChallengeEntity.save()
      ).populate('owner');

      this.eventsGateway.onCreateChallengeTicket(
        new PublicChallengeTicket(dataTicketChallengeEntity.toJSON()),
      );
      return {
        data: new PublicChallengeTicket(dataTicketChallengeEntity.toJSON()),
        meta: createPayload,
      };
    } catch (error) {
      console.log('ticketChallengeModel', error);
      throw new BadRequestException(error);
    }
  }

  async cancelSellTicket(userID: string, requestID: string) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const ticket = await this.ticketChallengeModel.findOne(
        {
          buyer: userID,
          _id: requestID,
          status: 'available',
        },
        null,
        {
          session,
        },
      );

      if (!ticket) throw new NotFoundException('Vé không tồn tại');

      const updatedTicket = await this.ticketChallengeModel.findOneAndUpdate(
        {
          _id: ticket.id,
        },
        {
          status: 'cancel',
        },
        { session, returnOriginal: false },
      );

      const updateTicketData = new PublicChallengeTicket(
        updatedTicket.toJSON(),
      );
      this.eventsGateway.onChangeChallengeTicket({ ...updateTicketData });

      await this.productTransactionService.updateReceiver(
        String(ticket.transaction),
        userID,
        session,
      );

      //back money for buyer

      await this.productTransactionService.sendBackMoneyForBuyer(
        String(ticket.transaction),
        session,
      );

      await session.commitTransaction();
      session.endSession();

      return {
        data: updateTicketData,
        message: 'Cancel successfully',
      };
    } catch (err) {
      console.log('cancelSellTicket', err);
      await session.abortTransaction();
      session.endSession();

      Logger.error('Cancel request failed', err);
      throw err;
    }
  }

  async updateSellTicket(id: string, updatePayload: UpdateTicketChallengeDto) {
    try {
      const data = await this.ticketChallengeModel.findOne({
        _id: id,
        type: 'sell',
        status: { $in: ['available', 'cancel'] },
        // status: 'available' | 'cancel',  or condition here
      });
      if (!data) throw new NotFoundException('Vé không tồn tại');
      const price = updatePayload?.price * 0.8 ?? data.price ?? 0;
      const donate = updatePayload?.price * 0.2 ?? data.donate ?? 0;

      const result = await this.ticketChallengeModel.findOneAndUpdate(
        {
          _id: data?._id,
        },
        {
          status: updatePayload?.status,
          price,
          donate,
          updatedAt: new Date().toLocaleString(),
        },
        { returnOriginal: false },
      );

      await data.updateOne(
        {
          status: updatePayload?.status,
          price,
          donate,
        },
        { returnOriginal: false },
      );

      if (updatePayload?.status === 'cancel') {
        await this.inventoryService.setInValidInventoryItem(data.code, false);
      } else {
        await this.inventoryService.setInValidInventoryItem(data.code, true);
      }

      const ticket = await this.ticketChallengeModel
        .findOne({ _id: id })
        .populate('owner')
        .exec();
      const publicTicket = new PublicChallengeTicket(ticket.toJSON());
      this.eventsGateway.onChangeChallengeTicket({ ...publicTicket });

      return {
        data: new PublicTicket(result.toJSON()),
        meta: { payload: updatePayload },
      };
    } catch (error) {
      console.log('updateSellTicket', error);
      throw new BadRequestException(error);
    }
  }

  async getAvailableTicketChallenge(id: string, session: any) {
    try {
      const data = await this.ticketChallengeModel
        .findOne(
          {
            _id: id,
            status: 'available',
            // createdAt: { $regex: getDateString(), $options: 'i' },
          },
          null,
          { session },
        )
        .populate('owner')
        .exec();

      if (!data) throw new BadRequestException(MY_MESSAGE.NOT_FOUND_TICKET);

      return data;
    } catch (error) {
      console.log('getAvailableTicketChallenge', error);
      throw new BadRequestException(error);
    }
  }

  async setPendingTicketChallenge(id: string, userID: string, session: any) {
    try {
      await this.ticketChallengeModel.findOneAndUpdate(
        { _id: id },
        { status: 'pending', buyer: userID },
        {
          session,
        },
      );
    } catch (err) {
      console.log('setPendingTicketChallenge', err);
      throw new BadRequestException('Vé không còn khả dụng');
    }
  }

  async setCancelTicketChallenge(id: string, session: any) {
    try {
      await this.ticketChallengeModel.findOneAndUpdate(
        { _id: id },
        { status: 'cancel' },
        {
          session,
        },
      );
      await this.inventoryService.setInValidInventoryItem(id, false);
    } catch (err) {
      throw new BadRequestException(MY_MESSAGE.NOT_FOUND_TICKET);
    }
  }

  async releaseTicketChallengeToAvailable(
    id: string,
    session: any,
    isRequest = false,
  ) {
    try {
      if (!isRequest) {
        await this.ticketChallengeModel.findOneAndUpdate(
          { _id: id },
          { status: 'available', transaction: '', buyer: '' },
          { session },
        );

        const ticket = await this.ticketChallengeModel
          .findOne({ _id: id }, null, { session })
          .populate('owner')
          .exec();

        const publicTicket = new PublicChallengeTicket(ticket.toJSON());
        this.eventsGateway.onChangeChallengeTicket({ ...publicTicket });
      } else {
        await this.ticketChallengeModel.findOneAndUpdate(
          { _id: id },
          { status: 'available' },
          { session },
        );

        const ticket = await this.ticketChallengeModel
          .findOne({ _id: id }, null, { session })
          .populate('buyer')
          .exec();

        const publicTicket = new PublicChallengeTicket(ticket.toJSON());
        this.eventsGateway.onChangeChallengeTicket({ ...publicTicket });
      }
    } catch (error) {
      console.log('releaseTicketChallengeToAvailable', error);
      throw new BadRequestException(error);
    }
  }

  async setTicketChallengeTransaction(
    id: string,
    transaction: string,
    session: any,
  ) {
    try {
      await this.ticketChallengeModel.findOneAndUpdate(
        { _id: id },
        { transaction: transaction },
        {
          session,
        },
      );
    } catch (error) {
      console.log('setTicketChallengeTransaction', error);
      throw new BadRequestException(error);
    }
  }

  async sendSocketPending(id: string, session: any) {
    try {
      const ticket = await this.ticketChallengeModel
        .findOne({ _id: id }, null, { session })
        .populate('owner')
        .populate('buyer')
        .exec();

      const publicTicket = new PublicChallengeTicket(ticket.toJSON());

      this.eventsGateway.onChangeChallengeTicket({ ...publicTicket });
    } catch (error) {
      console.log('sendSocketPending', error);
      throw new BadRequestException(error);
    }
  }

  async setTicketChallengeTransactionCaptured(
    id: string,
    buyerID: string,
    session: any,
  ) {
    try {
      await this.ticketChallengeModel
        .findOneAndUpdate(
          { _id: id },
          { status: 'captured', buyer: buyerID },
          session,
        )
        .then(async () => {
          const ticket = await this.ticketChallengeModel
            .findOne({ _id: id })
            .populate('owner')
            .exec();
          const publicTicket = new PublicChallengeTicket(ticket.toJSON());

          this.eventsGateway.onChangeChallengeTicket({ ...publicTicket });
        });
    } catch (error) {
      console.log('setTicketChallengeTransactionCaptured', error);
      throw new BadRequestException(error);
    }
  }

  async getTicketChallenge() {
    try {
      const data = await this.ticketChallengeModel
        .find({
          $or: [
            {
              status: 'available',
              type: 'sell',
            },
            {
              status: 'pending',
              type: 'sell',
            },
          ],
        })
        .limit(DEFAULT_LIMIT)
        .skip(DEFAULT_SKIP)
        .populate('owner')
        // .populate('ticket')
        .exec();

      return {
        data: [...data].map((item) => new PublicChallengeTicket(item.toJSON())),
        meta: {},
      };
    } catch (error) {
      console.log('getTicketChallenge', error);
      throw new BadRequestException(error);
    }
  }

  async findOneMyTicket(id: string, userID: string) {
    try {
      const data = await this.ticketChallengeModel
        .findOne({
          owner: userID,
          code: id,
        })
        .populate('owner');

      if (!data) throw new NotFoundException('Vé không tồn tại');

      return {
        data: { ...new PublicChallengeTicket(data.toJSON()), code: data.code },
        meta: { id },
      };
    } catch (error) {
      console.log('findOneMyTicket', error);
      throw new BadRequestException(error);
    }
  }

  async findMyTicket(userID: string) {
    const data = await this.ticketChallengeModel
      .find({
        owner: userID,
      })
      .limit(DEFAULT_LIMIT)
      .skip(DEFAULT_SKIP)
      .exec();

    return {
      data: data.map((item) => ({
        ...new PublicChallengeTicket(item.toJSON()),
        code: item.code,
      })),
      meta: {},
    };
  }

  async findMyPurchase(userID: string) {
    const data = await this.ticketChallengeModel
      .find({
        buyer: userID,
        status: 'captured',
      })
      .limit(DEFAULT_LIMIT)
      .skip(DEFAULT_SKIP)
      .populate('owner')
      .exec();

    return {
      data: data.map((item) => new PublicChallengeTicket(item.toJSON())),
      meta: {},
    };
  }

  async findMyPending(userID: string) {
    const data = await this.ticketChallengeModel
      .find({
        buyer: userID,
        status: 'pending',
        type: 'sell',
      })
      .limit(DEFAULT_LIMIT)
      .skip(DEFAULT_SKIP)
      .populate('owner')
      .exec();

    return {
      data: data.map((item) => new PublicChallengeTicket(item.toJSON())),
      meta: {},
    };
  }
}
