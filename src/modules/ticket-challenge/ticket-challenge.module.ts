import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EventsModule } from 'src/events/events.module';
import {
  TicketChallenge,
  TicketChallengeSchema,
} from 'src/schema/ticketChallenge.schema';
import { InventoryModule } from '../inventory/inventory.module';
import { ProductTransactionModule } from '../product-transaction/product-transaction.module';
import { RunnerModule } from '../runner/runner.module';
import { TicketChallengeController } from './ticket-challenge.controller';
import { TicketChallengeService } from './ticket-challenge.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: TicketChallenge.name,
        schema: TicketChallengeSchema,
      },
    ]),
    forwardRef(() => RunnerModule),
    EventsModule,
    forwardRef(() => ProductTransactionModule),
    InventoryModule,
  ],
  controllers: [TicketChallengeController],
  exports: [TicketChallengeService],
  providers: [TicketChallengeService],
})
export class TicketChallengeModule {}
