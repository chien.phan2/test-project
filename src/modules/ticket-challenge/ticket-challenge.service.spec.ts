import { Test, TestingModule } from '@nestjs/testing';
import { TicketChallengeService } from './ticket-challenge.service';

describe('TicketChallengeService', () => {
  let service: TicketChallengeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TicketChallengeService],
    }).compile();

    service = module.get<TicketChallengeService>(TicketChallengeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
