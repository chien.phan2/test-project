import { ApiProperty } from '@nestjs/swagger';
import { IsIn, IsNumber, IsOptional, Min } from 'class-validator';

export class UpdateTicketChallengeDto {
  @ApiProperty()
  @IsIn(['cancel', 'available'])
  @IsOptional()
  status: string;

  @ApiProperty()
  @IsNumber()
  @Min(1000)
  @IsOptional()
  price: number;
}
