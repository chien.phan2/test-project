import { Test, TestingModule } from '@nestjs/testing';
import { GroupParticipationHistoryController } from './group-participation-history.controller';

describe('GroupParticipationHistoryController', () => {
  let controller: GroupParticipationHistoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GroupParticipationHistoryController],
    }).compile();

    controller = module.get<GroupParticipationHistoryController>(GroupParticipationHistoryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
