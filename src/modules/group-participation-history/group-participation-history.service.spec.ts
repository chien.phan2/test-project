import { Test, TestingModule } from '@nestjs/testing';
import { GroupParticipationHistoryService } from './group-participation-history.service';

describe('GroupParticipationHistoryService', () => {
  let service: GroupParticipationHistoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GroupParticipationHistoryService],
    }).compile();

    service = module.get<GroupParticipationHistoryService>(
      GroupParticipationHistoryService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
