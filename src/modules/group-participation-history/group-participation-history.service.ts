import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  GroupParticipationHistory,
  GroupParticipationHistoryDocument,
} from 'src/schema/groupParticipationHistory.schema';
import { Model } from 'mongoose';

@Injectable()
export class GroupParticipationHistoryService {
  constructor(
    @InjectModel(GroupParticipationHistory.name)
    private readonly groupParticipationHistoryModel: Model<GroupParticipationHistoryDocument>,
  ) {}

  async createGroupParticipationHistory(
    runnerId: string,
    groupId: string,
    type: string,
    action: string,
  ) {
    try {
      const newGroupParticipationHistory =
        await this.groupParticipationHistoryModel.create({
          runner: runnerId,
          type: type,
          group: groupId,
          action: action,
        });
      await newGroupParticipationHistory.save();
      return {};
    } catch (error) {
      console.log('createGroupParticipationHistory: ', error);
      throw new BadRequestException(error);
    }
  }
}
