import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  GroupParticipationHistory,
  GroupParticipationHistorySchema,
} from 'src/schema/groupParticipationHistory.schema';
import { GroupParticipationHistoryController } from './group-participation-history.controller';
import { GroupParticipationHistoryService } from './group-participation-history.service';

@Module({
  controllers: [GroupParticipationHistoryController],
  providers: [GroupParticipationHistoryService],
  imports: [
    MongooseModule.forFeature([
      {
        name: GroupParticipationHistory.name,
        schema: GroupParticipationHistorySchema,
      },
    ]),
  ],
  exports: [GroupParticipationHistoryService],
})
export class GroupParticipationHistoryModule {}
