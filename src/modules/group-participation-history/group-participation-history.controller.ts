import { Controller, UseInterceptors } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { GroupParticipationHistoryService } from './group-participation-history.service';

@ApiTags('Challenge')
@Controller('challenge')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class GroupParticipationHistoryController {
  constructor(
    private readonly groupParticipationHistoryService: GroupParticipationHistoryService,
  ) {}
}
