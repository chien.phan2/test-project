import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  UseInterceptors,
  Req,
  UseGuards,
} from '@nestjs/common';
import { BetMatchService } from './bet-match.service';
import { CreateBetMatchDto } from './dto/create-bet-match.dto';
import { UpdateBetMatchDto } from './dto/update-bet-match.dto';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { Request } from 'express';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { ApiBearerAuth } from '@nestjs/swagger';

@Controller('betMatch')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class BetMatchController {
  constructor(private readonly betMatchService: BetMatchService) {}

  @Post()
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  create(@Body() createBetMatchDto: CreateBetMatchDto, @Req() req: Request) {
    const userID = req.userID;
    return this.betMatchService.createNewBetMatch(userID, createBetMatchDto);
  }

  @Get()
  findAll() {
    return this.betMatchService.findAll();
  }

  @Get(':id')
  @ApiBearerAuth('JWT-auth')
  findOne(@Param('id') id: string) {
    return this.betMatchService.findById(id);
  }

  @Patch(':id')
  @ApiBearerAuth('JWT-auth')
  update(
    @Param('id') id: string,
    @Body() updateBetMatchDto: UpdateBetMatchDto,
  ) {
    return this.betMatchService.updateBetMatch(id, updateBetMatchDto);
  }
}
