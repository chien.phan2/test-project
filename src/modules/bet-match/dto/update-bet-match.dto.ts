import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';

export class UpdateBetMatchDto {
  @IsOptional()
  @IsString()
  @ApiProperty()
  code: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  name: string;

  @IsOptional()
  @IsNumber()
  @ApiProperty()
  value: number;

  @IsOptional()
  @IsString()
  @ApiProperty()
  playerACode: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  playerAName: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  playerAImage: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  playerBCode: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  playerBName: string;

  @IsOptional()
  @IsString()
  @ApiProperty()
  playerBImage: string;
}
