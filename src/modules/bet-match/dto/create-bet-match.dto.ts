import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';

export class CreateBetMatchDto {
  @IsString()
  @ApiProperty()
  code: string;

  @IsString()
  @ApiProperty()
  name: string;

  @IsNumber()
  @ApiProperty()
  value: number;

  @IsString()
  @ApiProperty()
  playerACode: string;

  @IsString()
  @ApiProperty()
  playerAName: string;

  @IsString()
  @ApiProperty()
  playerAImage: string;

  @IsString()
  @ApiProperty()
  playerBCode: string;

  @IsString()
  @ApiProperty()
  playerBName: string;

  @IsString()
  @ApiProperty()
  playerBImage: string;
}
