import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { BetMatch, BetMatchDocument } from 'src/schema/betMatch.schema';
import { Model } from 'mongoose';
import { CreateBetMatchDto } from './dto/create-bet-match.dto';
import { UpdateBetMatchDto } from './dto/update-bet-match.dto';
import { UserService } from '../user/user.service';

@Injectable()
export class BetMatchService {
  constructor(
    @InjectModel(BetMatch.name) private betMatchModel: Model<BetMatchDocument>,
    private readonly userService: UserService,
  ) {}

  async findAll() {
    try {
      const betMatchs = await this.betMatchModel.find();
      return betMatchs;
    } catch (err) {
      throw err;
    }
  }

  async findById(id: string) {
    try {
      const betMatchs = await this.betMatchModel.findById(id).populate({
        path: 'user',
      });
      return betMatchs;
    } catch (err) {
      throw err;
    }
  }

  async createNewBetMatch(userID: string, newBetMatch: CreateBetMatchDto) {
    try {
      const user = await this.userService.getUserInfo(userID);
      const createOneBetMatch = new this.betMatchModel({
        ...newBetMatch,
        user: user._id.toString(),
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString(),
      });
      const data = await createOneBetMatch.save();
      return data;
    } catch (error) {
      throw error;
    }
  }

  async updateBetMatch(id: string, updateBetMatch: UpdateBetMatchDto) {
    try {
      const data = await this.betMatchModel.findByIdAndUpdate(
        {
          _id: id,
        },
        { ...updateBetMatch, updatedAt: new Date().toLocaleString() },
      );

      return data;
    } catch (error) {
      throw new BadRequestException('Not found bet match');
    }
  }

  async deleteBetMatch(id: string) {
    try {
      const data = await this.betMatchModel.findByIdAndDelete({
        _id: id,
      });
      return data;
    } catch (error) {
      throw new BadRequestException('Not found bet match');
    }
  }
}
