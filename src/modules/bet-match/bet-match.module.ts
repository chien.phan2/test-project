import { Module } from '@nestjs/common';
import { BetMatchService } from './bet-match.service';
import { BetMatchController } from './bet-match.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { BetMatch, BetMatchSchema } from 'src/schema/betMatch.schema';
import { UserModule } from '../user/user.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: BetMatch.name, schema: BetMatchSchema },
    ]),
    UserModule,
  ],
  controllers: [BetMatchController],
  providers: [BetMatchService],
  exports: [BetMatchService],
})
export class BetMatchModule {}
