import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateProducTransactionDto {
  @IsString()
  @ApiProperty()
  ticketID: string;
}
