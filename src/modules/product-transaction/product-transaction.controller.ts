import {
  Body,
  Controller,
  Get,
  HttpCode,
  Logger,
  Post,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { ProductTransactionService } from './product-transaction.service';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { CreateProducTransactionDto } from './dto/create-product-transaction.dto';
import { Request } from 'express';

@ApiTags('Challenge')
@Controller('/challenge/product-transaction')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class ProductTransactionController {
  constructor(
    private readonly productTransactionService: ProductTransactionService,
  ) {}

  @Post('/create-transaction')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  createProducTransaction(
    @Body() createProducTransactionDto: CreateProducTransactionDto,
    @Req() req: Request,
  ) {
    const userID = req.userID;
    Logger.log(
      `${userID} Create new product transaction by:`,
      createProducTransactionDto,
    );
    return this.productTransactionService.createProductTransaction(
      userID,
      createProducTransactionDto,
    );
  }

  @Post('/ipn')
  @HttpCode(204)
  ipnNoti(@Body() body: any, @Req() req: Request, @Query() query: any) {
    Logger.log('Receive ticket challenge IPN:', body);
    Logger.log('Ma', query.type);
    if (query.type == 1) {
      return this.productTransactionService.handleIpnNoti(body);
    } else if (query.type == 2) {
      return this.productTransactionService.handleIpnNotiRequestPay(body);
    }
  }

  @Get('/get-my-transactions')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  getMyTransactions(@Req() req: Request) {
    const userID = req.userID;
    return this.productTransactionService.getMyTransactions(userID);
  }
}
