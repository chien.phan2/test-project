import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EventsModule } from 'src/events/events.module';
import {
  ProductTransaction,
  ProductTransactionSchema,
} from 'src/schema/productTransaction.schema';
import { ChallengeModule } from '../challenge/challenge.module';
import { InventoryModule } from '../inventory/inventory.module';
import { RunnerModule } from '../runner/runner.module';
import { TicketChallengeModule } from '../ticket-challenge/ticket-challenge.module';
import { ProductTransactionController } from './product-transaction.controller';
import { ProductTransactionService } from './product-transaction.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: ProductTransaction.name,
        schema: ProductTransactionSchema,
      },
    ]),
    forwardRef(() => RunnerModule),
    TicketChallengeModule,
    EventsModule,
    forwardRef(() => ChallengeModule),
    InventoryModule,
  ],
  controllers: [ProductTransactionController],
  exports: [ProductTransactionService],
  providers: [ProductTransactionService],
})
export class ProductTransactionModule {}
