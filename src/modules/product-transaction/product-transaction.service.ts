import {
  BadRequestException,
  Inject,
  Injectable,
  Logger,
  forwardRef,
} from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import fetch from 'node-fetch';
import { MY_MESSAGE } from 'src/contants/message.contants';
import { EventsGateway } from 'src/events/events.gateway';
import {
  ProductTransaction,
  ProductTransactionDocument,
} from 'src/schema/productTransaction.schema';
import { ActionType } from 'src/utils/constants';
import genBase64, { decodeBase64 } from 'src/utils/genBase64String';
import genDisbursementMethod from 'src/utils/genDisbursmentMethod';
import genSignature from 'src/utils/genSignature';
import getDateString from 'src/utils/getDateString';
import { ChallengeService } from '../challenge/challenge.service';
import { InventoryService } from '../inventory/inventory.service';
import { RunnerService } from '../runner/runner.service';
import { TicketChallengeService } from '../ticket-challenge/ticket-challenge.service';
import { MomoIpn } from '../transaction/dto/momo-ipn.dto';
import { CreateProducTransactionDto } from './dto/create-product-transaction.dto';

@Injectable()
export class ProductTransactionService {
  constructor(
    @InjectModel(ProductTransaction.name)
    private productTransactionModel: Model<ProductTransactionDocument>,

    @Inject(forwardRef(() => RunnerService))
    private readonly runnerService: RunnerService,

    private readonly ticketChallengeService: TicketChallengeService,

    private readonly eventsGateway: EventsGateway,

    @Inject(forwardRef(() => ChallengeService))
    private readonly challengeService: ChallengeService,

    private readonly inventoryService: InventoryService,

    @InjectConnection()
    private connection: Connection,
  ) {}

  async getCurrentTransactionOfChallengeTicket(
    userID: string,
    ticketID: string,
  ) {
    const activeTransaction = await this.productTransactionModel.findOne({
      status: 'pending',
      creator: userID,
      ticket: ticketID,
      // createdAt: { $regex: getDateString(), $options: 'i' },
    });
    return activeTransaction;
  }

  async getSuccessTransactionByTicketID(ticketID: string) {
    try {
      const data = await this.productTransactionModel
        .find({ ticket: ticketID, status: 'success' })
        .exec();

      if (data?.length == 0) {
        throw new BadRequestException(
          'Giao dịch mua vé tham gia thử thách không tồn tại',
        );
      }

      return data?.[0];
    } catch (error) {
      console.log('getSuccessTransactionByTicketID', error);
      throw new BadRequestException(error);
    }
  }

  async checkCanCreateTransaction(userID: string, session: any) {
    const activeTransaction = await this.productTransactionModel.find(
      {
        status: 'pending',
        creator: userID,
        createdAt: { $regex: getDateString(), $options: 'i' },
      },
      null,
      { session },
    );

    if (activeTransaction.length == 0) return true;
    else throw new BadRequestException(MY_MESSAGE.PROCESSING_TRANS);
  }

  async createMomoTicketChallengeTransaction(
    transaction: ProductTransaction,
    session: any,
  ) {
    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        ipnUrl: process.env.IPN_URL_RECEIVE_CHALLENGE_TICKET,
        partnerName: 'Phiếu Cơm MoMo',
        storeId: 'MomoTestStore',
        requestType: 'captureWallet',
        orderId: transaction._id.toString(),
        orderInfo: 'Thanh toán vé tham gia thử thách',
        amount: transaction.value,
        lang: 'vi',
        redirectUrl: `momo://?refId=${
          process.env.ENVIRONMENT == 'pro'
            ? 'miniapp.8TZb9qBBnwSFqmTap1vw.momorun'
            : 'miniapp.8TZb9qBBnwSFqmTap1vw.momorun'
        }&transaction=${transaction._id.toString()}&appId=miniapp.8TZb9qBBnwSFqmTap1vw.momorun&deeplink=true`,
        requestId: transaction._id.toString(),
        extraData: transaction.ticket,
        autoCapture: false,
        signature: '',
      };
      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&extraData=${data.extraData}&ipnUrl=${data.ipnUrl}&orderId=${data.orderId}&orderInfo=${data.orderInfo}&partnerCode=${data.partnerCode}&redirectUrl=${data.redirectUrl}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_TRANSACTION, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData = await response.json();
      Logger.log('Receive response', responseData);

      if (responseData['resultCode'] != 0) {
        await this.productTransactionModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            status: 'error',
            paymentInfo: responseData,
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        throw new BadRequestException(MY_MESSAGE.CREATE_TRANS_FAILED);
      }

      return [signature, responseData];
    } catch (err) {
      console.log('createMomoTicketChallengeTransaction', err);
      await this.ticketChallengeService.releaseTicketChallengeToAvailable(
        String(transaction.ticket),
        session,
      );
      await this.productTransactionModel.findOneAndUpdate(
        { _id: transaction._id.toString() },
        { status: 'error', updatedAt: new Date().toLocaleString() },
        { session },
      );
      throw new BadRequestException(MY_MESSAGE.CREATE_TRANS_FAILED);
    }
  }

  setTransactionFailedAfterTime(
    transactionID: string,
    ticketID: string,
    timeout: number,
  ) {
    setTimeout(async () => {
      const session = await this.connection.startSession();
      session.startTransaction();
      try {
        const transaction = await this.productTransactionModel.findOne(
          {
            _id: transactionID,
            status: 'pending',
          },
          null,
          { session },
        );
        if (transaction) {
          await this.productTransactionModel.findOneAndUpdate(
            { _id: transactionID, status: 'pending' },
            { status: 'failed', updatedAt: new Date().toLocaleString() },
            { session },
          );
          await this.ticketChallengeService.releaseTicketChallengeToAvailable(
            ticketID,
            session,
          );
        }
        await session.commitTransaction();
        await session.endSession();
      } catch (err) {
        console.log('setTransactionFailedAfterTimeTicket', err);
        Logger.error(err);
        Logger.warn('ERROR: Can not set transaction status for', transactionID);
        await session.abortTransaction();
        await session.endSession();
      }
    }, timeout);
  }

  async sendMoneyToSeller(transactionID: string, session?: any) {
    const transaction = await this.productTransactionModel
      .findById(transactionID)
      .populate('receiver')
      .session(session);

    const controller = new AbortController();
    const setTimeoutId = setTimeout(() => controller.abort(), 30000);
    if (transaction.value - transaction.fee <= 0) return;
    try {
      const now = Date.now();
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        ipnUrl: process.env.IPN_URL_SEND_CHALLENGE_TICKET,
        orderId: transaction._id.toString() + now,
        orderInfo: 'Nhận tiền thanh toán vé tham gia thử thách',
        extraData: '',
        lang: 'vi',
        amount: transaction.value - transaction.fee,
        walletId: transaction.receiver.phone,
        requestId: transaction._id.toString() + now,
        requestType: 'disburseToWallet',
        disbursementMethod: '',
        signature: '',
      };

      data.extraData = genBase64(`{"id": "${transaction._id.toString()}"}`);

      const disbursementData = {
        partnerCode: process.env.PARTNER_CODE,
        partnerRefId: transaction._id.toString() + now,
        partnerTransId: transaction._id.toString() + now,
        amount: transaction.value - transaction.fee,
        description: 'Nhận tiền thanh toán vé tham gia thử thách',
        walletId: transaction.receiver.phone,
      };

      data.disbursementMethod = genDisbursementMethod(
        JSON.stringify(disbursementData),
      );

      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&disbursementMethod=${data.disbursementMethod}&extraData=${data.extraData}&orderId=${data.orderId}&orderInfo=${data.orderInfo}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);
      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_REQUEST, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
        signal: controller.signal,
      });
      const responseData: any = await response.json();

      if (setTimeoutId) clearTimeout(setTimeoutId);

      if (responseData['resultCode'] != 0) {
        await this.productTransactionModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            statusRequestPay: 'failed',
            requestPayInfo: responseData,
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        Logger.error(
          `Request payment to wallet ${transaction.receiver.phone} failed`,
          responseData,
        );
      } else {
        await this.productTransactionModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            statusRequestPay: 'success',
            requestPayInfo: { ...transaction.requestPayInfo, ...responseData },
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        Logger.log(
          `Request payment to wallet ${transaction.receiver.phone} success`,
          responseData,
        );
      }
    } catch (err) {
      if (setTimeoutId) clearTimeout(setTimeoutId);

      await this.productTransactionModel.findOneAndUpdate(
        { _id: transaction._id.toString() },
        { statusRequestPay: 'error', updatedAt: new Date().toLocaleString() },
        { session },
      );
      Logger.error(
        `Request payment to wallet ${transaction.receiver.phone} error`,
        err,
      );
    } finally {
      if (setTimeoutId) clearTimeout(setTimeoutId);
    }
  }

  async handleIpnNotiRequestPay(data: MomoIpn) {
    console.log('==> handleIpnNotiRequestPay >>> data:', data);
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const extraData = JSON.parse(decodeBase64(data.extraData));
      const transactionID = extraData.id;

      //check signature later

      const transaction = await this.productTransactionModel.findOne({
        _id: transactionID,
        statusRequestPay: 'pending',
      });

      if (transaction) {
        if (data.resultCode == 0) {
          console.log('--> handleIpnNotiRequestPayTicket', data);
          await this.productTransactionModel.findOneAndUpdate(
            { _id: transactionID },
            {
              statusRequestPay: 'success',
              requestPayInfo: { ...transaction.requestPayInfo, ...data },
            },
            session,
          );

          await session.commitTransaction();
        } else {
          // transaction failed
          await this.productTransactionModel.findOneAndUpdate(
            { _id: transactionID },
            {
              statusRequestPay: 'failed',
              requestPayInfo: { ...transaction.requestPayInfo, ...data },
            },
            { session },
          );

          await session.commitTransaction();
        }
      }
    } catch (err) {
      console.log('handleIpnNotiRequestPayTicket', err);
      Logger.error(`Update pay info for transaction error`, err);
    } finally {
      session.endSession();
      return { data: 'Success' };
    }
  }

  async sendConfirmToMomo(transaction: ProductTransaction) {
    console.log('=====> :', transaction);
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        requestId: transaction._id.toString(),
        orderId: transaction._id.toString(),
        storeId: 'MomoTestStore',
        requestType: 'capture',
        amount: transaction.value,
        lang: 'vi',
        description: 'Thanh toán vé tham gia thử thách',
        signature: '',
      };
      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&description=${data.description}&orderId=${data.orderId}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CONFIRM_PAYMENT, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData: any = await response.json();

      Logger.log('Receive response', responseData);

      const transactionID = responseData.orderId;
      const ticketID = String(transaction.ticket);

      if (responseData.resultCode == 0) {
        console.log('--> sendConfirmToMomoTicket', responseData);
        await this.productTransactionModel.findOneAndUpdate(
          { _id: transactionID, status: 'pending' },
          {
            status: 'success',
            statusRequestPay: 'pending',
            paymentInfo: { ...transaction.paymentInfo, ...responseData },
          },
          session,
        );

        // Set ticket status to sold of owner ticket
        await this.inventoryService.setTicketSold(
          transaction?.ticket?.code?.toString(),
        );

        // Send ticket to buyer
        await this.challengeService.giveAwayChallengeTicket(
          transaction?.creator?._id?.toString(),
          ActionType.MARKET_BUY,
          transaction?.ticket?.code?.toString(),
        );

        // Update budget of challenge
        await this.challengeService.updateBudgetChallenge(
          transaction?.ticket?.owner?.toString(),
          transaction?.fee,
          'market_buy_ticket',
          null,
        );

        // Change status of ticket in market place
        if (transaction.type == 'buy') {
          await this.ticketChallengeService.setTicketChallengeTransactionCaptured(
            transaction.ticket._id?.toString(),
            transaction.creator._id.toString(),
            session,
          );
        }

        await session.commitTransaction();
        // socket here

        setTimeout(() => {
          this.eventsGateway.onTicketChallengeTransaction({
            ticketID: ticketID,
            status: 'success',
            transactionID: responseData.orderId,
          });
        }, 10000);

        // Send money to seller
        this.sendMoneyToSeller(transaction._id.toString());

        // disable if donate === fee

        // if (
        //   transaction.fee !== transaction.value &&
        //   transaction.type == 'buy'
        // ) {
        //   this.sendMoneyToSeller(transaction._id.toString());
        // } else if (transaction.type == 'request') {
        //   await this.ticketChallengeService.releaseTicketChallengeToAvailable(
        //     ticketID,
        //     session,
        //     true,
        //   );
        // }
      }
    } catch (err) {
      console.log('sendConfirmToMomoTicket', err);
      Logger.error('Send confirm to momo failed', err);
      throw new BadRequestException(MY_MESSAGE.CONFIRM_FAILED);
    }
  }

  async sendCancelToMomo(transaction: ProductTransaction) {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        requestId: transaction._id.toString(),
        orderId: transaction._id.toString(),
        storeId: 'MomoTestStore',
        requestType: 'cancel',
        amount: transaction.value,
        lang: 'vi',
        description: 'Thanh toán vé tham gia thử thách',
        signature: '',
      };
      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&description=${data.description}&orderId=${data.orderId}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CONFIRM_PAYMENT, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData: any = await response.json();
      Logger.log('Receive response', responseData);

      const ticketID = String(transaction.ticket);

      if (responseData.resultCode == 0) {
        // socket here
        setTimeout(() => {
          this.eventsGateway.onTicketChallengeTransaction({
            ticketID: ticketID,
            status: 'failed',
            transactionID: responseData.orderId,
          });
        }, 10000);
      }
    } catch (err) {
      console.log('sendCancelToMomoTicket', err);
      Logger.error('Send confirm to momo failed', err);
      throw new BadRequestException(MY_MESSAGE.CONFIRM_FAILED);
    }
  }

  async createProductTransaction(
    userID: string,
    createPayload: CreateProducTransactionDto,
  ) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const runner = await this.runnerService.findRunnerById(userID);
      const currentTrans = await this.getCurrentTransactionOfChallengeTicket(
        userID,
        createPayload.ticketID,
      );
      if (currentTrans) {
        return {
          data: currentTrans.paymentInfo,
        };
      }
      const ticketID = createPayload.ticketID;
      const ticket =
        await this.ticketChallengeService.getAvailableTicketChallenge(
          ticketID,
          session,
        );

      await this.checkCanCreateTransaction(userID, session);

      if (ticket.owner._id.equals(runner._id) && ticket.type == 'sell') {
        throw new BadRequestException('Bạn không thể mua vé của chính mình');
      }

      await this.ticketChallengeService.setPendingTicketChallenge(
        ticketID,
        userID,
        session,
      );

      const createTime = new Date();

      const newTicketChallengeTransactions =
        await this.productTransactionModel.create(
          [
            {
              creator: runner._id.toString(),
              receiver: ticket.owner._id.toString(),
              ticket: ticket._id.toString(),
              status: 'pending', // add one more status for cancel,
              value: ticket.price + ticket.donate,
              fee: ticket.donate,
              paymentInfo: {}, // save momo response for payment
              statusRequestPay: 'pending',
              requestPayInfo: {},
              createdAt: createTime.toLocaleString(),
              updatedAt: createTime.toLocaleString(),
              timestamp: createTime.getTime(),
            },
          ],
          { session },
        );

      const newTicketChallengeTransaction = newTicketChallengeTransactions[0];

      const [signature, paymentInfo]: any =
        await this.createMomoTicketChallengeTransaction(
          newTicketChallengeTransaction,
          session,
        );

      newTicketChallengeTransaction.paymentInfo = paymentInfo;
      newTicketChallengeTransaction.signature = signature;
      await newTicketChallengeTransaction.save({ session });

      await this.ticketChallengeService.setTicketChallengeTransaction(
        ticket.id,
        newTicketChallengeTransaction.id,
        session,
      );

      await this.ticketChallengeService.sendSocketPending(ticket.id, session);

      this.setTransactionFailedAfterTime(
        newTicketChallengeTransaction.id,
        ticket.id,
        70000,
      );

      await session.commitTransaction();
      session.endSession();

      return {
        data: paymentInfo,
      };
    } catch (error) {
      console.log('createProductTransaction:', error);
      Logger.error('Create rroduc transaction failed', error);
      await session.abortTransaction();
      session.endSession();
      throw new BadRequestException(error);
    }
  }

  async handleIpnNoti(data: MomoIpn) {
    console.log('===> handleIpnNoti >> data:', data);
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const transactionID = data.orderId;
      const ticketID = data.extraData;

      const transaction = await this.productTransactionModel
        .findOne({
          _id: transactionID,
          ticket: ticketID,
          value: data.amount,
        })
        .populate('creator')
        .populate('ticket');

      if (transaction) {
        if (data.resultCode == 9000) {
          if (transaction.status == 'pending')
            this.sendConfirmToMomo(transaction);
          else this.sendCancelToMomo(transaction);
        } else if (data.resultCode != 0 && data.resultCode != 9000) {
          // transaction failed

          await this.productTransactionModel.findOneAndUpdate(
            { _id: transactionID },
            {
              status: 'failed',
              paymentInfo: { ...transaction.paymentInfo, ...data },
            },
            { session },
          );

          if (transaction.type == 'buy')
            await this.ticketChallengeService.releaseTicketChallengeToAvailable(
              ticketID,
              session,
            );
          else
            await this.ticketChallengeService.setCancelTicketChallenge(
              ticketID,
              session,
            );

          setTimeout(() => {
            this.eventsGateway.onTicketChallengeTransaction({
              ticketID: ticketID,
              status: 'fail',
              transactionID: data.orderId,
            });
          }, 10000);

          await session.commitTransaction();
        }

        // how about data.resultCode == 0
      }
    } catch (error) {
      const ticketID = data.extraData;
      setTimeout(() => {
        this.eventsGateway.onTicketChallengeTransaction({
          ticketID: ticketID,
          status: 'fail',
          transactionID: data.orderId,
        });
      }, 10000);
      console.log('handleIpnNotiTicket', error);
      Logger.log('Update transaction status failed', error);
    } finally {
      session.endSession();

      return { data: 'Success' };
    }
  }

  async updateReceiver(id: string, userID: string, session: any) {
    try {
      return this.productTransactionModel.findOneAndUpdate(
        { _id: id },
        {
          receiver: userID,
        },
        { session },
      );
    } catch (error) {
      console.log('updateReceiver', error);
      throw new BadRequestException(error);
    }
  }

  async sendBackMoneyForBuyer(transactionID: string, session?: any) {
    const transaction = await this.productTransactionModel
      .findOne({ _id: transactionID, status: 'success' })
      .populate('creator')
      .session(session);

    const controller = new AbortController();
    const setTimeoutId = setTimeout(() => controller.abort(), 30000);
    if (transaction.value - transaction.fee <= 0) return;
    try {
      const now = Date.now();
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        ipnUrl: process.env.IPN_URL_SEND_CHALLENGE_TICKET,
        orderId: transaction._id.toString() + now,
        orderInfo: 'Hoàn tiền thanh toán vé tham gia thử thách',
        extraData: '',
        lang: 'vi',
        amount: transaction.value,
        walletId: transaction.creator.phone,
        requestId: transaction._id.toString() + now,
        requestType: 'disburseToWallet',
        disbursementMethod: '',
        signature: '',
      };

      data.extraData = genBase64(`{"id": "${transaction._id.toString()}"}`);

      const disbursementData = {
        partnerCode: process.env.PARTNER_CODE,
        partnerRefId: transaction._id.toString() + now,
        partnerTransId: transaction._id.toString() + now,
        amount: transaction.value,
        description: 'Hoàn tiền thanh toán vé tham gia thử thách',
        walletId: transaction.creator.phone,
      };

      data.disbursementMethod = genDisbursementMethod(
        JSON.stringify(disbursementData),
      );

      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&disbursementMethod=${data.disbursementMethod}&extraData=${data.extraData}&orderId=${data.orderId}&orderInfo=${data.orderInfo}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);
      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_REQUEST, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
        signal: controller.signal,
      });
      const responseData: any = await response.json();

      if (setTimeoutId) clearTimeout(setTimeoutId);

      if (responseData['resultCode'] != 0) {
        console.log(
          'sendBackMoneyForBuyer => responseData[resultCode]',
          responseData,
        );
        await this.productTransactionModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            statusRequestPay: 'failed',
            requestPayInfo: responseData,
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        Logger.error(
          `Request refund to wallet ${transaction.creator.phone} failed`,
          responseData,
        );
      } else {
        await this.productTransactionModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            status: 'refund',
            statusRequestPay: 'success',
            requestPayInfo: { ...transaction.requestPayInfo, ...responseData },
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        console.log(
          'sendBackMoneyForBuyer => responseData[resultCode]',
          responseData,
        );
        Logger.log(
          `Request refund to wallet ${transaction.creator.phone} success`,
          responseData,
        );
      }
    } catch (err) {
      if (setTimeoutId) clearTimeout(setTimeoutId);

      await this.productTransactionModel.findOneAndUpdate(
        { _id: transaction._id.toString() },
        { statusRequestPay: 'error', updatedAt: new Date().toLocaleString() },
        { session },
      );
      console.log('sendBackMoneyForBuyer => err', err);
      Logger.error(
        `Request payment to wallet ${transaction.creator.phone} error`,
        err,
      );
    } finally {
      if (setTimeoutId) clearTimeout(setTimeoutId);
    }
  }

  async getMyTransactions(userID: string) {
    try {
      const data = await this.productTransactionModel
        .find({
          status: 'success',
          $or: [{ creator: userID }, { receiver: userID }],
        })
        .populate('ticket')
        .sort({ createdAt: -1 })
        .exec();

      return data;
    } catch (error) {
      console.log('getMyTransactions', error);
      throw new BadRequestException(error);
    }
  }
}
