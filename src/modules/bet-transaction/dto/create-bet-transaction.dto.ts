import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateBetTransactionDto {
  @IsString()
  @ApiProperty()
  betID: string;

  @IsString()
  @ApiProperty()
  matchID: string;
}
