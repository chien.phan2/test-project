import { PartialType } from '@nestjs/mapped-types';
import { CreateBetTransactionDto } from './create-bet-transaction.dto';

export class UpdateBetTransactionDto extends PartialType(
  CreateBetTransactionDto,
) {}
