import { forwardRef, Module } from '@nestjs/common';
import { BetTransactionService } from './bet-transaction.service';
import { BetTransactionController } from './bet-transaction.controller';
import { BetModule } from '../bet/bet.module';
import { UserModule } from '../user/user.module';
import { MongooseModule } from '@nestjs/mongoose';
import {
  BetTransaction,
  BetTransactionSchema,
} from 'src/schema/betTransaction.schema';
import { EventsModule } from '../../events/events.module';
@Module({
  imports: [
    EventsModule,
    forwardRef(() => BetModule),
    UserModule,
    MongooseModule.forFeature([
      { name: BetTransaction.name, schema: BetTransactionSchema },
    ]),
  ],
  controllers: [BetTransactionController],
  providers: [BetTransactionService],
  exports: [BetTransactionService],
})
export class BetTransactionModule {}
