import {
  BadGatewayException,
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  Logger,
  // NotFoundException,
} from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import fetch from 'node-fetch';
import { MY_MESSAGE } from 'src/contants/message.contants';
// import { EventsGateway } from 'src/events/events.gateway';
// import { TicketDocument } from 'src/schema/ticket.schema';
import {
  BetTransaction,
  BetTransactionDocument,
} from 'src/schema/betTransaction.schema';
// import genBase64, { decodeBase64 } from 'src/utils/genBase64String';
// import genDisbursementMethod from 'src/utils/genDisbursmentMethod';
import genSignature from 'src/utils/genSignature';
import getDateString from 'src/utils/getDateString';
import { BetService } from '../bet/bet.service';
// import { UserService } from '../user/user.service';
// import { CreateBetTransactionDto } from './dto/create-bet-transaction.dto';
import { MomoIpn } from './dto/momo-ipn.dto';
import { BetDocument } from 'src/schema/bet.schema';
// import { BetMatchDocument } from 'src/schema/betMatch.schema';

@Injectable()
export class BetTransactionService {
  constructor(
    @InjectModel(BetTransaction.name)
    private betTransactionModel: Model<BetTransactionDocument>,
    @InjectConnection() private connection: Connection,
    @Inject(forwardRef(() => BetService))
    private readonly betService: BetService, // private readonly userService: UserService, // private readonly eventsGateway: EventsGateway,
  ) {}

  async checkCanCreateBetTransaction(userID: string, session: any) {
    const activeBetTransaction = await this.betTransactionModel.find(
      {
        status: 'pending',
        creator: userID,
        createdAt: { $regex: getDateString(), $options: 'i' },
      },
      null,
      { session },
    );

    if (activeBetTransaction.length == 0) return true;
    else throw new BadRequestException(MY_MESSAGE.PROCESSING_TRANS);
  }

  async getCurrentBetTransactionOfTicket(userID: string, betID: string) {
    const activeBetTransaction = await this.betTransactionModel.findOne({
      status: 'pending',
      creator: userID,
      bet: betID,
      createdAt: { $regex: getDateString(), $options: 'i' },
    });
    return activeBetTransaction;
  }

  async createMomoBetTransaction(betTransaction: BetTransaction, session: any) {
    // const redirectUrl = `momo://?refId=miniapp.8TZb9qBBnwSFqmTap1vw.lunchticket&transaction=${betTransaction._id.toString()}&appId=miniapp.8TZb9qBBnwSFqmTap1vw.lunchticket&deeplink=true`;

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        ipnUrl: process.env.IPN_URL_RECEIVE_BET,
        partnerName: 'Phiếu Cơm MoMo',
        storeId: 'MomoTestStore',
        requestType: 'captureWallet',
        orderId: betTransaction._id.toString(),
        orderInfo: 'Thanh toán phiếu cơm',
        amount: betTransaction.value,
        lang: 'vi',
        redirectUrl: `momo://?refId=${
          process.env.ENVIRONMENT == 'pro'
            ? 'miniapp.8TZb9qBBnwSFqmTap1vw.lunchticket'
            : 'miniapp.8TZb9qBBnwSFqmTap1vw.lunchticket'
        }&transaction=${betTransaction._id.toString()}&appId=miniapp.8TZb9qBBnwSFqmTap1vw.lunchticket&deeplink=true`,
        requestId: betTransaction._id.toString(),
        extraData: betTransaction.bet?._id || betTransaction.bet,
        autoCapture: false,
        signature: '',
      };
      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&extraData=${data.extraData}&ipnUrl=${data.ipnUrl}&orderId=${data.orderId}&orderInfo=${data.orderInfo}&partnerCode=${data.partnerCode}&redirectUrl=${data.redirectUrl}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_TRANSACTION, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData = await response.json();
      Logger.log('Receive response', responseData);

      if (responseData['resultCode'] != 0) {
        await this.betTransactionModel.findOneAndUpdate(
          { _id: betTransaction._id.toString() },
          {
            status: 'error',
            paymentInfo: responseData,
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        throw new BadRequestException(MY_MESSAGE.CREATE_TRANS_FAILED);
      }

      return [signature, responseData];
    } catch (err) {
      await this.betTransactionModel.findOneAndUpdate(
        { _id: betTransaction._id.toString() },
        { status: 'error', updatedAt: new Date().toLocaleString() },
        { session },
      );
      throw new BadRequestException(MY_MESSAGE.CREATE_TRANS_FAILED);
    }
  }

  async handleIpnNoti(data: MomoIpn) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const betTransactionID = data.orderId;
      const betID = data.extraData;

      //check signature later
      const betTransaction = await this.betTransactionModel
        .findOne({
          _id: betTransactionID,
          bet: betID,
          value: data.amount,
        })
        .populate('creator');
      // .populate('receiver');

      if (betTransaction) {
        if (data.resultCode == 9000) {
          this.sendConfirmToMomo(betTransaction);
        } else if (data.resultCode != 0 && data.resultCode != 9000) {
          // betTransaction failed

          await this.betTransactionModel.findOneAndUpdate(
            { _id: betTransactionID },
            {
              status: 'failed',
              paymentInfo: { ...betTransaction.paymentInfo, ...data },
            },
            { session },
          );

          await this.betService.update(betID, {
            status: 'fail',
          });

          await session.commitTransaction();
        }

        // how about data.resultCode == 0
      }
    } catch (err) {
      Logger.log('Update betTransaction status failed', err);
    } finally {
      session.endSession();

      return { data: 'Success' };
    }
  }

  async checkStatus(userID: string, id: string) {
    const betTransaction = await this.betTransactionModel.findOne({
      _id: id,
      creator: userID,
    });

    if (!betTransaction) throw new BadRequestException(MY_MESSAGE.TRANS_OWN);

    return {
      data: {
        _id: betTransaction.id,
        status: betTransaction.status,
        momoBetTransactionId: betTransaction.paymentInfo?.transId,
        timestamp: betTransaction.timestamp,
        value: betTransaction.value,
        createdAt: betTransaction.createdAt,
        updatedAt: betTransaction.createdAt,
      },
      message: betTransaction.status,
    };
  }

  async sendConfirmToMomo(betTransaction: BetTransaction) {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        requestId: betTransaction._id.toString(),
        orderId: betTransaction._id.toString(),
        storeId: 'MomoTestStore',
        requestType: 'capture',
        amount: betTransaction.value,
        lang: 'vi',
        description: 'Thanh toán phiếu cơm',
        signature: '',
      };
      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&description=${data.description}&orderId=${data.orderId}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CONFIRM_PAYMENT, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData: any = await response.json();
      Logger.log('Receive response', responseData);

      const betTransactionID = responseData.orderId;
      const betID = String(betTransaction._id);

      if (responseData.resultCode == 0) {
        await this.betTransactionModel.findOneAndUpdate(
          { _id: betTransactionID, status: 'pending' },
          {
            status: 'success',
            statusRequestPay: 'pending',
            paymentInfo: { ...betTransaction.paymentInfo, ...responseData },
          },
          session,
        );

        await this.betService.update(betID, {
          status: 'success',
        });

        await session.commitTransaction();
      }
    } catch (err) {
      Logger.error('Send confirm to momo failed', err);
      throw new BadRequestException(MY_MESSAGE.CONFIRM_FAILED);
    }
  }

  // async sendCancelToMomo(betTransaction: BetTransaction) {
  //   const session = await this.connection.startSession();
  //   session.startTransaction();

  //   try {
  //     const data = {
  //       partnerCode: process.env.PARTNER_CODE,
  //       requestId: betTransaction._id.toString(),
  //       orderId: betTransaction._id.toString(),
  //       storeId: 'MomoTestStore',
  //       requestType: 'cancel',
  //       amount: betTransaction.value,
  //       lang: 'vi',
  //       description: 'Thanh toán phiếu cơm',
  //       signature: '',
  //     };
  //     const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&description=${data.description}&orderId=${data.orderId}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
  //     const signature = genSignature(rawSignature);

  //     data.signature = signature;

  //     const response = await fetch(process.env.URL_CONFIRM_PAYMENT, {
  //       method: 'POST',
  //       body: JSON.stringify(data),
  //       headers: { 'Content-Type': 'application/json' },
  //     });
  //     const responseData = await response.json();
  //     Logger.log('Receive response', responseData);

  //     const ticketID = String(betTransaction.ticket);

  //     if (responseData.resultCode == 0) {
  //       // socket here
  //       setTimeout(() => {
  //         this.eventsGateway.onBetTransaction({
  //           ticketID: ticketID,
  //           status: 'failed',
  //           betTransactionID: responseData.orderId,
  //         });
  //       }, 10000);
  //     }
  //   } catch (err) {
  //     Logger.error('Send confirm to momo failed', err);
  //     throw new BadRequestException(MY_MESSAGE.CONFIRM_FAILED);
  //   }
  // }

  // async transferMoneyOfBetTransaction(betTransactionID: string) {
  //   try {
  //     const betTransaction = await this.betTransactionModel
  //       .findById(betTransactionID)
  //       .populate('bet match user')
  //       .populate('creator')
  //       .populate('ticket');

  //     if (!betTransaction)
  //       throw new NotFoundException(MY_MESSAGE.NOT_FOUND_TRANSACTION);
  //     if (betTransaction.statusRequestPay != 'success') {
  //       await this.sendMoneyToSeller(betTransactionID);
  //       return {
  //         message: 'Success',
  //       };
  //     } else return { message: 'BetTransaction has request pay success' };
  //   } catch (err) {
  //     throw err;
  //   }
  // }

  async createBetTransaction(userID: string, bet: BetDocument) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const createTime = new Date();

      const newBetTransactions = await this.betTransactionModel.create(
        [
          {
            user: userID,
            bet: bet._id,
            match: bet.match?._id || bet.match,
            status: 'pending', // add one more status for cancel,
            value: bet.match.value,
            paymentInfo: {}, // save momo response for payment
            statusRequestPay: 'pending',
            requestPayInfo: {},
            createdAt: createTime.toLocaleString(),
            updatedAt: createTime.toLocaleString(),
            timestamp: createTime.getTime(),
          },
        ],
        { session },
      );

      const newBetTransaction = newBetTransactions[0];

      const [signature, paymentInfo]: any = await this.createMomoBetTransaction(
        newBetTransaction,
        session,
      );
      newBetTransaction.paymentInfo = paymentInfo;
      newBetTransaction.signature = signature;
      await newBetTransaction.save({ session });

      await this.betService.setBetTransaction(
        bet._id,
        newBetTransaction.id,
        session,
      );

      this.setBetTransactionRequestFailedAfterTime(newBetTransaction.id, 70000);

      await session.commitTransaction();
      session.endSession();

      return {
        data: paymentInfo,
      };
    } catch (err) {
      Logger.error('Create betTransaction failed', err);
      //handle set ticket free here
      await session.abortTransaction();
      session.endSession();

      throw err;
    }
  }

  setBetTransactionRequestFailedAfterTime(
    betTransactionID: string,
    timeout: number,
  ) {
    setTimeout(async () => {
      const session = await this.connection.startSession();
      session.startTransaction();
      try {
        const betTransaction = await this.betTransactionModel.findOne(
          {
            _id: betTransactionID,
            status: 'pending',
          },
          null,
          { session },
        );
        if (betTransaction) {
          await this.betTransactionModel.findOneAndUpdate(
            { _id: betTransactionID, status: 'pending' },
            { status: 'failed', updatedAt: new Date().toLocaleString() },
            { session },
          );
        }
        await session.commitTransaction();
        await session.endSession();
      } catch (err) {
        Logger.error(err);
        Logger.warn(
          'ERROR: Can not set betTransaction status for',
          betTransactionID,
        );
        await session.abortTransaction();
        await session.endSession();
      }
    }, timeout);
  }

  async fetchAll() {
    try {
      const betTransactions = await this.betTransactionModel
        .find({
          status: 'success',
        })
        .populate('bet match user')
        .exec();
      return betTransactions
        .sort((a: any, b: any) => a.createdAt - b.createdAt)
        ?.reverse();
    } catch (err) {
      console.log(err);
      throw new BadGatewayException('');
    }
  }
}
