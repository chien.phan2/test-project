import {
  Controller,
  Post,
  Body,
  Req,
  UseGuards,
  HttpCode,
  Logger,
  UseInterceptors,
  Query,
  Get,
  Param,
  // BadRequestException,
} from '@nestjs/common';
import { BetTransactionService } from './bet-transaction.service';
// import { CreateBetTransactionDto } from './dto/create-bet-transaction.dto';
import { Request } from 'express';
import { AuthGuard } from 'src/middleware/auth.middleware';
// import { ApiKeyAuthGuard } from 'src/middleware/apiKey-auth.middleware';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { ApiBearerAuth } from '@nestjs/swagger';

@Controller('betTransaction')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class BetTransactionController {
  constructor(private readonly transactionService: BetTransactionService) {}

  // @Post()
  // @UseGuards(new AuthGuard())
  // @ApiBearerAuth('JWT-auth')
  // create(
  //   @Body() createBetTransactionDto: CreateBetTransactionDto,
  //   @Req() req: Request,
  // ) {
  //   const userID = req.userID;
  //   Logger.log(`${userID} Create new transaction by:`, createBetTransactionDto);
  //   return this.transactionService.create(userID, createBetTransactionDto);
  // }

  @Get(`:id`)
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  checkStatus(@Param('id') id: string, @Req() req: Request) {
    const userID = req.userID;
    return this.transactionService.checkStatus(userID, id);
  }

  @Post('/ipn')
  @HttpCode(204)
  ipnNoti(@Body() body: any, @Req() req: Request, @Query() query: any) {
    Logger.log('Receive IPN:', body);
    Logger.log('Ma', query.type);
    return this.transactionService.handleIpnNoti(body);
  }
}
