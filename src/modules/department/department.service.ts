import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Department, DepartmentDocument } from 'src/schema/department.schema';
import { Model } from 'mongoose';

@Injectable()
export class DepartmentService {
  constructor(
    @InjectModel(Department.name)
    private readonly departmentModel: Model<DepartmentDocument>,
  ) {}

  async getDepartment() {
    try {
      const department = await this.departmentModel.find();
      return department;
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error);
    }
  }
}
