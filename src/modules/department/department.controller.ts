import { Controller, Get, UseInterceptors } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { DepartmentService } from './department.service';

@ApiTags('Challenge')
@Controller('challenge')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class DepartmentController {
  constructor(private readonly departmentService: DepartmentService) {}

  @Get('department')
  async getDepartment() {
    return await this.departmentService.getDepartment();
  }
}
