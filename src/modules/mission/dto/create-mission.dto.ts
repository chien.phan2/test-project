import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateMissionDto {
  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  description: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  key: string;

  @ApiProperty()
  @IsOptional()
  bannerUrl: string;

  @ApiProperty()
  @IsOptional()
  startTime: Date;

  @ApiProperty()
  @IsOptional()
  endTime: Date;

  @ApiProperty({ default: 0 })
  @IsNotEmpty()
  @IsNumber()
  reward: number;

  @ApiProperty({ default: 1 })
  @IsNotEmpty()
  @IsNumber()
  taskRequired: number;
}
