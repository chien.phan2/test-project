import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export class UpdateMissionDto {
  @ApiProperty()
  @IsOptional()
  name: string;

  @ApiProperty()
  @IsOptional()
  description: string;

  @ApiProperty()
  @IsOptional()
  key: string;

  @ApiProperty()
  @IsOptional()
  bannerUrl: string;

  @ApiProperty()
  @IsOptional()
  startTime: Date;

  @ApiProperty()
  @IsOptional()
  endTime: Date;

  @ApiProperty({ default: 0 })
  @IsOptional()
  @IsNumber()
  reward: number;

  @ApiProperty()
  @IsOptional()
  isDisabled: boolean;

  @ApiProperty({ default: 1, required: true })
  @IsNotEmpty()
  @IsNumber()
  taskRequired: number;
}
