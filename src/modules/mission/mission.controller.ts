import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { MissionService } from './mission.service';
import { CreateMissionDto } from './dto/create-mission.dto';
import { UpdateMissionDto } from './dto/update-mission.dto';

@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
@ApiTags('Challenge')
@Controller('challenge/mission')
export class MissionController {
  constructor(private readonly missionService: MissionService) {}

  @Get('/get-list-missions')
  getListMissions() {
    return this.missionService.getListMissions();
  }

  @Get('/get-mission-detail/:id')
  getMissionDetail(@Param('id') id: string) {
    return this.missionService.getMissionDetail(id);
  }

  @Post('/create-mission')
  createMission(@Body() createMissionDto: CreateMissionDto) {
    return this.missionService.createMission(createMissionDto);
  }

  @Patch('/update-mission/:id')
  updateMission(
    @Param('id') id: string,
    @Body() updateMissionDto: UpdateMissionDto,
  ) {
    return this.missionService.updateMission(id, updateMissionDto);
  }

  @Delete('/delete-mission/:id')
  deleteMission(@Param('id') id: string) {
    return this.missionService.deleteMission(id);
  }
}
