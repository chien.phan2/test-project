import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Mission, MissionDocument } from 'src/schema/mission.schema';
import { Model } from 'mongoose';
import { CreateMissionDto } from './dto/create-mission.dto';
import { UpdateMissionDto } from './dto/update-mission.dto';

@Injectable()
export class MissionService {
  constructor(
    @InjectModel(Mission.name)
    private readonly missionModel: Model<MissionDocument>,
  ) {}

  async getListMissions() {
    try {
      return await this.missionModel
        .find({
          isDisabled: false,
        })
        .exec();
    } catch (error) {
      console.log('getListMissions', error);
      throw error;
    }
  }

  async getMissionDetail(id: string) {
    try {
      return await this.missionModel.findById(id).exec();
    } catch (error) {
      console.log('getMissionDetail', error);
      throw error;
    }
  }

  async createMission(createPayload: CreateMissionDto) {
    try {
      const newEntity = new this.missionModel({
        name: createPayload.name,
        description: createPayload.description,
        key: createPayload.key,
        bannerUrl: createPayload?.bannerUrl || '',
        startTime: createPayload?.startTime || null,
        endTime: createPayload?.endTime || null,
        reward: createPayload?.reward || 0,
        taskRequired: createPayload?.taskRequired || 1,
        isCompleted: false,
        isDisabled: false,
        createdAt: Date.now(),
        updatedAt: Date.now(),
      });
      return await newEntity.save();
    } catch (error) {
      console.log('createMission', error);
      throw error;
    }
  }

  async updateMission(id: string, updatePayload: UpdateMissionDto) {
    try {
      return await this.missionModel
        .findByIdAndUpdate(
          id,
          {
            ...updatePayload,
            updatedAt: Date.now(),
          },
          {
            new: true,
          },
        )
        .exec();
    } catch (error) {
      console.log('updateMission', error);
      throw error;
    }
  }

  async deleteMission(id: string) {
    try {
      return await this.missionModel.findByIdAndDelete(id).exec();
    } catch (error) {
      console.log('deleteMission', error);
      throw error;
    }
  }

  async getMissionByKey(key: string) {
    try {
      const mission = await this.missionModel.findOne({ key }).exec();
      return mission;
    } catch (error) {
      console.log('getMissionByKey', error);
      throw error;
    }
  }
}
