import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from '../../schema/user.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PublicUser } from './dto/public-user.dto';
import { MY_MESSAGE } from 'src/contants/message.contants';
@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<User>,
  ) {}
  async sync(createUserDto: CreateUserDto) {
    try {
      const user = await this.userModel.findOne({
        userID: createUserDto.userID,
      });
      if (user) {
        let updatedData = user;
        if (!user.phone || !user.name) {
          updatedData = await this.userModel.findOneAndUpdate(
            {
              _id: user.id,
            },
            { ...createUserDto },
            { returnOriginal: false },
          );
        }
        return {
          data: new PublicUser(updatedData?.toJSON()),
          message: MY_MESSAGE.SYNC_USER,
        };
      }

      const newUser = await this.userModel.create({
        ...createUserDto,
        isBlocked: false,
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString(),
      });
      await newUser.save();

      return {
        data: new PublicUser(newUser.toJSON()),
        message: MY_MESSAGE.CREATE_USER,
      };
    } catch (error) {
      console.log('syncUser:', error);
      throw new BadRequestException(MY_MESSAGE.INVALID_USER);
    }
  }

  async getUserInfo(id: string) {
    const user = await this.userModel.findOne({
      _id: id,
      isBlocked: false,
    });
    if (!user || !user.phone)
      throw new BadRequestException(MY_MESSAGE.INVALID_USER);
    return user;
  }
}
