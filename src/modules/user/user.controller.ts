import { Controller, Post, Body, UseInterceptors } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';

@Controller('user')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('/sync')
  sync(@Body() createUserDto: CreateUserDto) {
    return this.userService.sync(createUserDto);
  }
}
