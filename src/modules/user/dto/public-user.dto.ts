import { Exclude } from 'class-transformer';
import { genToken } from 'src/utils/genToken';

export class PublicUser {
  _id: any;
  name: string;
  phone: string;
  token: string;

  @Exclude({ toPlainOnly: true })
  userID: string;

  constructor(partial: any) {
    Object.assign(this, partial);
    this._id = partial._id.toString();
    this.token = genToken(partial._id.toString());
  }
}
