import { Test, TestingModule } from '@nestjs/testing';
import { GroupParticipationRequestService } from './group-participation-request.service';

describe('GroupParticipationRequestService', () => {
  let service: GroupParticipationRequestService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GroupParticipationRequestService],
    }).compile();

    service = module.get<GroupParticipationRequestService>(
      GroupParticipationRequestService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
