import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { CreateGroupParticipationRequestDto } from './dto/create-group-participation-request.dto';
import { UpdateGroupParticipationRequestDto } from './dto/update-group-participation-request.dto';
import { GroupParticipationRequestService } from './group-participation-request.service';

@ApiTags('Challenge')
@Controller('challenge')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class GroupParticipationRequestController {
  constructor(
    private readonly groupParticipationRequestService: GroupParticipationRequestService,
  ) {}

  @Get('/group/get-my-request')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  getMyRequest(@Req() req: Request) {
    const userID = req.userID;
    return this.groupParticipationRequestService.getMyRequest(userID);
  }

  @Get('/group/get-my-request')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  getMemberRequest(@Req() req: Request) {
    const userID = req.userID;
    return this.groupParticipationRequestService.getMyRequest(userID);
  }

  @Get('/group/get-member-requests/:groupId')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  getMemberRequests(@Param('groupId') groupId: string, @Req() req: Request) {
    const userID = req.userID;
    return this.groupParticipationRequestService.getMemberRequests(
      userID,
      groupId,
    );
  }

  @Post('/group/request-join')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  createRequestJoinGroup(
    @Body() payload: CreateGroupParticipationRequestDto,
    @Req() req: Request,
  ) {
    const userID = req.userID;
    return this.groupParticipationRequestService.requestJoinGroup(
      userID,
      payload,
    );
  }

  @Patch('/group/left-group')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  leaveGroup(
    @Body() payload: CreateGroupParticipationRequestDto,
    @Req() req: Request,
  ) {
    const userID = req.userID;
    return this.groupParticipationRequestService.leaveGroup(
      userID,
      payload?.groupId,
    );
  }

  @Patch('/group/cancel-request')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  cancelRequestJoinGroup(@Req() req: Request) {
    const userID = req.userID;
    return this.groupParticipationRequestService.cancelRequestJoinGroup(userID);
  }

  @Patch('/group/accept-request/:requestId')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  acceptRequest(@Param('requestId') requestId: string, @Req() req: Request) {
    const userID = req.userID;
    return this.groupParticipationRequestService.acceptRequest(
      requestId,
      userID,
    );
  }

  @Patch('/group/reject-request/:requestId')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  rejectRequest(@Param('requestId') requestId: string, @Req() req: Request) {
    const userID = req.userID;
    return this.groupParticipationRequestService.rejectRequest(
      requestId,
      userID,
    );
  }
}
