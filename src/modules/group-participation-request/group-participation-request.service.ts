import {
  BadRequestException,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
  GroupParticipationRequest,
  GroupParticipationRequestDocument,
} from 'src/schema/groupParticipationRequest.schema';
import {
  ActionGroup,
  GroupParticipationHistoryType,
} from 'src/utils/constants';
import { GroupParticipationHistoryService } from '../group-participation-history/group-participation-history.service';
import { GroupParticipationService } from '../group-participation/group-participation.service';
import { CreateGroupParticipationRequestDto } from './dto/create-group-participation-request.dto';
import { ParticipationService } from '../participation/participation.service';

@Injectable()
export class GroupParticipationRequestService {
  constructor(
    @InjectModel(GroupParticipationRequest.name)
    private readonly groupParticipationRequestModel: Model<GroupParticipationRequestDocument>,
    private readonly groupParticipationHistoryService: GroupParticipationHistoryService,
    @Inject(forwardRef(() => GroupParticipationService))
    private readonly groupParticipationService: GroupParticipationService,
    private readonly participationService: ParticipationService,
  ) {}

  async requestJoinGroup(
    runnerId: string,
    payload: CreateGroupParticipationRequestDto,
  ) {
    try {
      const groupInfo =
        await this.groupParticipationService.getInfoGroupParticipation(
          payload?.groupId,
        );

      const existedParticipation =
        await this.participationService.checkParticipationByRunnerId(
          runnerId,
          groupInfo?.challenge?.toString(),
        );

      if (!existedParticipation) {
        throw new BadRequestException('Bạn chưa tham gia thử thách!');
      }

      const existedRequest = await this.groupParticipationRequestModel.find({
        creator: runnerId,
        challenge: groupInfo?.challenge?.toString(),
        $or: [{ status: 'accepted' }, { status: 'pending' }],
      });
      if (existedRequest?.length > 0) {
        throw new BadRequestException(
          'Bạn đã tham gia hoặc yêu cầu ở một nhóm khác!',
        );
      }

      const newGroupParticipationRequest =
        await this.groupParticipationRequestModel.create({
          creator: runnerId,
          group: payload?.groupId,
          challenge: groupInfo?.challenge?.toString(),
        });
      await newGroupParticipationRequest.save();
      return {
        data: newGroupParticipationRequest.toJSON(),
        message: 'Request join group successfully',
      };
    } catch (error) {
      console.log('requestJoinGroup: ', error);
      throw new BadRequestException(error);
    }
  }

  async joinGroupWithOwner(
    runnerId: string,
    groupId: string,
    challengeId: string,
  ) {
    try {
      const existedGroupParticipation =
        await this.groupParticipationService.checkExistedInGroup(
          groupId,
          runnerId,
        );
      if (existedGroupParticipation) {
        throw new BadRequestException('Bạn đã tham gia nhóm này!');
      }

      await this.groupParticipationService.addMemberToGroupParticipation(
        runnerId,
        groupId,
      );

      const newGroupParticipationRequest =
        await this.groupParticipationRequestModel.create({
          creator: runnerId,
          group: groupId,
          challenge: challengeId,
          status: 'accepted',
        });
      await newGroupParticipationRequest.save();

      await this.groupParticipationHistoryService.createGroupParticipationHistory(
        runnerId,
        groupId,
        GroupParticipationHistoryType?.PARTICIPATION,
        ActionGroup.JOIN_GROUP,
      );

      return {
        data: {},
        message: 'Join group successfully',
      };
    } catch (error) {
      console.log('joinGroupWithOwner: ', error);
      throw new BadRequestException(error);
    }
  }

  async leaveGroup(runnerId: string, groupId: string) {
    try {
      const existedGroupParticipation =
        await this.groupParticipationService.checkExistedInGroup(
          groupId,
          runnerId,
        );
      if (!existedGroupParticipation) {
        throw new BadRequestException('Bạn chưa tham gia nhóm nào!');
      }

      await this.groupParticipationService.removeMemberFromGroupParticipation(
        runnerId,
        groupId,
      );

      const existedRequest = await this.groupParticipationRequestModel.findOne({
        creator: runnerId,
        group: groupId,
        status: 'accepted',
      });
      if (existedRequest) {
        await existedRequest.updateOne(
          {
            status: 'left',
            updatedAt: new Date().toLocaleString(),
          },
          { new: true },
        );
      }

      await this.groupParticipationHistoryService.createGroupParticipationHistory(
        runnerId,
        groupId,
        GroupParticipationHistoryType?.PARTICIPATION,
        ActionGroup.LEAVE_GROUP,
      );

      return {
        data: {},
        message: 'Leave group successfully',
      };
    } catch (error) {
      console.log('leaveGroup: ', error);
      throw new BadRequestException(error);
    }
  }

  async cancelRequestJoinGroup(runnerId: string) {
    try {
      const existedRequest = await this.groupParticipationRequestModel
        .findOne({
          creator: runnerId,
          status: 'pending',
        })
        .populate('group');
      if (!existedRequest) {
        throw new BadRequestException('Bạn chưa yêu cầu tham gia nhóm nào!');
      }

      await existedRequest.updateOne(
        {
          status: 'cancelled',
          updatedAt: new Date().toLocaleString(),
        },
        { new: true },
      );

      await this.groupParticipationHistoryService.createGroupParticipationHistory(
        runnerId,
        existedRequest?.group?._id?.toString(),
        GroupParticipationHistoryType?.PARTICIPATION,
        ActionGroup.CANCEL_REQUEST,
      );

      return {
        data: existedRequest.toJSON(),
        message: 'Cancel request join group successfully',
      };
    } catch (error) {
      console.log('cancelRequestJoinGroup: ', error);
      throw new BadRequestException(error);
    }
  }

  async checkExistedRequest(runnerId: string) {
    try {
      const existedRequest = await this.groupParticipationRequestModel
        .find({
          creator: runnerId,
          $or: [{ status: 'accepted' }, { status: 'pending' }],
        })
        .populate('group');
      return existedRequest;
    } catch (error) {
      console.log('checkExistedRequest: ', error);
      throw new BadRequestException(error);
    }
  }

  async getMyRequest(runnerId: string) {
    try {
      const existedRequest = await this.groupParticipationRequestModel
        .find({
          creator: runnerId,
          $or: [{ status: 'accepted' }, { status: 'pending' }],
        })
        .populate('group', 'onwer name');
      return existedRequest;
    } catch (error) {
      console.log('getMyRequest: ', error);
      throw new BadRequestException(error);
    }
  }

  async getMemberRequests(runnerId: string, groupId: string) {
    try {
      const groupParticipation =
        await this.groupParticipationService.checkOwnerGroupParticipation(
          runnerId,
          groupId,
        );

      if (!groupParticipation?.length) {
        const userRequest = await this.groupParticipationRequestModel
          .find({
            creator: runnerId,
            status: 'pending',
            group: groupId,
          })
          .populate('creator', 'name createdAt');

        return userRequest;
      }

      const memberRequests = await this.groupParticipationRequestModel
        .find({
          group: groupParticipation[0]?._id?.toString(),
          status: 'pending',
        })
        .populate('creator', 'name');

      return memberRequests;
    } catch (error) {
      console.log('getMemberRequest: ', error);
      throw new BadRequestException(error);
    }
  }

  async acceptRequest(requestId: string, runnerId: string) {
    try {
      const requestInfo = await this.groupParticipationRequestModel.findOne({
        _id: requestId,
      });
      if (!requestInfo) {
        throw new BadRequestException('Yêu cầu không tồn tại!');
      }

      const groupParticipation =
        await this.groupParticipationService.checkOwnerGroupParticipation(
          runnerId,
          requestInfo?.group?.toString(),
        );

      if (!groupParticipation?.length) {
        throw new BadRequestException('Bạn không phải là trưởng nhóm!');
      }

      if (requestInfo?.status !== 'pending') {
        throw new BadRequestException('Yêu cầu đã hết hạn!');
      }

      const groupInfo =
        await this.groupParticipationService.getInfoGroupParticipation(
          requestInfo?.group?.toString(),
        );
      if (!groupInfo) {
        throw new BadRequestException('Không tìm thấy nhóm này!');
      }
      if (groupInfo?.members?.length >= groupInfo?.memberSize) {
        throw new BadRequestException('Nhóm đã đủ thành viên!');
      }

      await requestInfo.updateOne(
        {
          status: 'accepted',
          updatedAt: new Date().toLocaleString(),
        },
        { new: true },
      );

      await this.groupParticipationService.addMemberToGroupParticipation(
        requestInfo?.creator?.toString(),
        requestInfo?.group?.toString(),
      );

      await this.groupParticipationHistoryService.createGroupParticipationHistory(
        requestInfo?.creator?.toString(),
        requestInfo?.group?.toString(),
        GroupParticipationHistoryType?.PARTICIPATION,
        ActionGroup.JOIN_GROUP,
      );

      return requestInfo;
    } catch (error) {
      console.log('acceptRequest: ', error);
      throw new BadRequestException(error);
    }
  }

  async rejectRequest(requestId: string, runnerId: string) {
    try {
      const requestInfo = await this.groupParticipationRequestModel.findOne({
        _id: requestId,
      });
      if (!requestInfo) {
        throw new BadRequestException('Yêu cầu không tồn tại!');
      }

      const groupParticipation =
        await this.groupParticipationService.checkOwnerGroupParticipation(
          runnerId,
          requestInfo?.group?.toString(),
        );
      if (!groupParticipation?.length) {
        throw new BadRequestException('Bạn không phải là trưởng nhóm!');
      }

      if (requestInfo?.status !== 'pending') {
        throw new BadRequestException('Yêu cầu đã hết hạn!');
      }

      await requestInfo.updateOne(
        {
          status: 'rejected',
          updatedAt: new Date().toLocaleString(),
        },
        { new: true },
      );

      return requestInfo;
    } catch (error) {
      console.log('rejectRequest: ', error);
      throw new BadRequestException(error);
    }
  }

  async deleteRequestWithOwner(runnerId: string, groupId: string) {
    try {
      const groupParticipation =
        await this.groupParticipationService.checkOwnerGroupParticipation(
          runnerId,
          groupId,
        );
      if (!groupParticipation?.length) {
        throw new BadRequestException('Bạn không phải là trưởng nhóm!');
      }

      const existedRequest = await this.groupParticipationRequestModel.findOne({
        group: groupId,
        creator: runnerId,
        status: 'accepted',
      });
      if (!existedRequest) {
        throw new BadRequestException('Không tìm thấy yêu cầu!');
      }

      await existedRequest.updateOne(
        {
          status: 'left',
          updatedAt: new Date().toLocaleString(),
        },
        { new: true },
      );

      return existedRequest;
    } catch (error) {
      console.log('deleteRequestWithOwner: ', error);
      throw new BadRequestException(error);
    }
  }

  async cancelRequestWithDeleteGroup(groupId: string) {
    try {
      const pendingRequest = await this.groupParticipationRequestModel.find({
        group: groupId,
        status: 'pending',
      });
      if (pendingRequest?.length) {
        const promise = [];
        for (const request of pendingRequest) {
          promise.push(
            await request.updateOne(
              {
                $set: {
                  status: 'cancelled',
                },
                updatedAt: new Date().toLocaleString(),
              },
              { new: true },
            ),
          );
        }
        const data = await Promise.all(promise);
      }
    } catch (error) {
      console.log('cancelRequestWithDeleteGroup: ', error);
      throw new BadRequestException(error);
    }
  }

  async updateStatsGroupParticipationRequest(
    requestId: string,
    runnerId: string,
    athleteStats: any,
  ) {
    try {
      if (!athleteStats?.distance) {
        return null;
      }
      const existedRequest = await this.groupParticipationRequestModel.findOne({
        _id: requestId,
        creator: runnerId?.toString(),
      });
      if (!existedRequest) {
        throw new BadRequestException('Yêu cầu không tồn tại!');
      }

      const data = await this.groupParticipationRequestModel.findByIdAndUpdate(
        {
          _id: requestId,
        },
        {
          athleteStats: {
            distance: Number(athleteStats.distance),
            moving_time: Number(athleteStats.moving_time),
            elapsed_time: Number(athleteStats.elapsed_time),
          },
        },
        {
          new: true,
        },
      );

      return data;
    } catch (error) {
      console.log('updateStatsGroupParticipationRequest: ', error);
      throw new BadRequestException(error);
    }
  }

  async findCurrentRequestByChallenge(challengeId: string, runnerId: string) {
    try {
      const existedRequest = await this.groupParticipationRequestModel.findOne({
        challenge: challengeId?.toString(),
        status: 'accepted',
        creator: runnerId?.toString(),
      });
      if (!existedRequest) {
        return null;
      }
      return existedRequest;
    } catch (error) {
      console.log('findCurrentRequestByChallenge: ', error);
      throw new BadRequestException(error);
    }
  }

  async updateAllRequest() {
    try {
      //651651b901b75594969672c6
      const allRequest = await this.groupParticipationRequestModel.updateMany({
        athleteStats: {},
      });

      return {};
    } catch (error) {
      console.log('updateAllRequest: ', error);
      throw new BadRequestException(error);
    }
  }

  async getStatsGroupParticipationRequest(challengeId) {
    try {
      const data = await this.groupParticipationRequestModel.aggregate([
        {
          $match: {
            challenge: challengeId,
            status: 'accepted',
          },
        },
        {
          $group: {
            _id: '$group',
            totalDistance: { $sum: '$athleteStats.distance' },
          },
        },
      ]);
      return data;
    } catch (error) {
      console.log('getStatsGroupParticipationRequest: ', error);
      throw new BadRequestException(error);
    }
  }

  async getStatsGroupParticipationRequestMembers(groupId) {
    try {
      const data = await this.groupParticipationRequestModel.find({
        group: groupId,
        status: 'accepted',
      });
      return data;
    } catch (error) {
      console.log('getStatsGroupParticipationRequestMembers: ', error);
      throw new BadRequestException(error);
    }
  }

  async getGroupOfRunner(runnerId: string) {
    try {
      const groupInfo = await this.groupParticipationRequestModel
        .find({
          creator: runnerId,
          status: 'accepted',
        })
        .populate([
          {
            path: 'group',
            select: 'name',
          },
        ])
        .select('group');

      return groupInfo;
    } catch (error) {
      console.log('getGroupOfRunner: ', error);
      throw new BadRequestException(error);
    }
  }
}
