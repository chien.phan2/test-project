import { Test, TestingModule } from '@nestjs/testing';
import { GroupParticipationRequestController } from './group-participation-request.controller';

describe('GroupParticipationRequestController', () => {
  let controller: GroupParticipationRequestController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GroupParticipationRequestController],
    }).compile();

    controller = module.get<GroupParticipationRequestController>(
      GroupParticipationRequestController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
