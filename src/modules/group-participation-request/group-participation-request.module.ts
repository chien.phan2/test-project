import { Module, forwardRef } from '@nestjs/common';
import { GroupParticipationRequestService } from './group-participation-request.service';
import { GroupParticipationRequestController } from './group-participation-request.controller';
import { MongooseModule } from '@nestjs/mongoose';
import {
  GroupParticipationRequest,
  GroupParticipationRequestSchema,
} from 'src/schema/groupParticipationRequest.schema';
import { GroupParticipationHistoryModule } from '../group-participation-history/group-participation-history.module';
import { GroupParticipationModule } from '../group-participation/group-participation.module';
import { ParticipationModule } from '../participation/participation.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: GroupParticipationRequest.name,
        schema: GroupParticipationRequestSchema,
      },
    ]),
    GroupParticipationHistoryModule,
    forwardRef(() => GroupParticipationModule),
    ParticipationModule,
  ],
  controllers: [GroupParticipationRequestController],
  providers: [GroupParticipationRequestService],
  exports: [GroupParticipationRequestService],
})
export class GroupParticipationRequestModule {}
