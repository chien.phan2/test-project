import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Medal, MedalSchema } from 'src/schema/medal.schema';
import { ChallengeModule } from '../challenge/challenge.module';
import { MedalController } from './medal.controller';
import { MedalService } from './medal.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Medal.name, schema: MedalSchema }]),
  ],
  providers: [MedalService],
  exports: [MedalService],
  controllers: [MedalController],
})
export class MedalModule {}
