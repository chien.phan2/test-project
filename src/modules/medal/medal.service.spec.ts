import { Test, TestingModule } from '@nestjs/testing';
import { MedalService } from './medal.service';

describe('MedalService', () => {
  let service: MedalService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MedalService],
    }).compile();

    service = module.get<MedalService>(MedalService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
