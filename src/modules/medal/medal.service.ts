import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Medal, MedalDocument } from 'src/schema/medal.schema';
import { CreateMedalDto } from './dto/create-medal.dto';
import { UpdateMedalDto } from './dto/update-medal.dto';

@Injectable()
export class MedalService {
  constructor(
    @InjectModel(Medal.name) private readonly medalModel: Model<MedalDocument>,
  ) {}

  async createMedal(createPayload: CreateMedalDto) {
    try {
      const medal = new this.medalModel(createPayload);
      return await medal.save();
    } catch (error) {
      console.log('createMedal: ', error);
      throw error;
    }
  }

  async updateMedal(id: string, updatePayload: UpdateMedalDto) {
    try {
      const updatedMedal = await this.medalModel.findByIdAndUpdate(
        {
          _id: id,
        },
        { ...updatePayload, updatedAt: new Date().toLocaleString() },
        { returnOriginal: false },
      );
      return updatedMedal;
    } catch (error) {
      console.log('updateMedal: ', error);
      throw error;
    }
  }

  async deleteMedal(id: string) {
    try {
      const deletedMedal = await this.medalModel.findByIdAndDelete(id);
      return deletedMedal;
    } catch (error) {
      console.log('deleteMedal: ', error);
      throw error;
    }
  }

  async getListMedal(type?: string) {
    try {
      const listMedal = await this.medalModel.find({
        ...(type && { type }),
      });
      return listMedal;
    } catch (error) {
      console.log('getListMedal: ', error);
      throw error;
    }
  }
}
