import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { CreateMedalDto } from './dto/create-medal.dto';
import { UpdateMedalDto } from './dto/update-medal.dto';
import { MedalService } from './medal.service';

@ApiTags('Challenge')
@Controller('challenge')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class MedalController {
  constructor(private readonly medalService: MedalService) {}

  @Post('/create-medal')
  createMedal(@Body() createMedalDto: CreateMedalDto) {
    return this.medalService.createMedal(createMedalDto);
  }

  @Delete('/delete-medal/:id')
  deleteMedal(@Param('id') id: string) {
    return this.medalService.deleteMedal(id);
  }

  @Get('get-list-medal')
  @ApiQuery({
    name: 'type',
    required: false,
    type: String,
  })
  getListMedal(@Query('type') type: string) {
    return this.medalService.getListMedal(type);
  }

  @Patch('/update-medal/:id')
  updateMedal(@Body() updateMedalDto: UpdateMedalDto, @Param('id') id: string) {
    return this.medalService.updateMedal(id, updateMedalDto);
  }
}
