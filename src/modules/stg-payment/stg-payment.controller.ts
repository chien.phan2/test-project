import {
  Body,
  Controller,
  HttpCode,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { StgPaymentService } from './stg-payment.service';
import { CreateStgPaymentDto } from './dto/create-stg-payment.dto';

@ApiTags('Payment Checklist - Staging/Prod')
@Controller('payment-stg')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class StgPaymentController {
  constructor(private readonly stgPaymentService: StgPaymentService) {}

  @Post()
  create(@Body() payload: CreateStgPaymentDto) {
    return this.stgPaymentService.createStgRequestPayment(payload);
  }

  @Post('/ipn')
  @HttpCode(204)
  ipnNoti(@Body() body: any) {
    return this.stgPaymentService.handleIpnNotiStgPayment(body);
  }
}
