import { Test, TestingModule } from '@nestjs/testing';
import { StgPaymentService } from './stg-payment.service';

describe('StgPaymentService', () => {
  let service: StgPaymentService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StgPaymentService],
    }).compile();

    service = module.get<StgPaymentService>(StgPaymentService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
