import { Test, TestingModule } from '@nestjs/testing';
import { StgPaymentController } from './stg-payment.controller';

describe('StgPaymentController', () => {
  let controller: StgPaymentController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StgPaymentController],
    }).compile();

    controller = module.get<StgPaymentController>(StgPaymentController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
