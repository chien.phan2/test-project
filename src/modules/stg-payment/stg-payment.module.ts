import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { StgPayment, StgPaymentSchema } from 'src/schema/stg-payment';
import { StgPaymentController } from './stg-payment.controller';
import { StgPaymentService } from './stg-payment.service';

@Module({
  controllers: [StgPaymentController],
  exports: [StgPaymentService],
  providers: [StgPaymentService],
  imports: [
    MongooseModule.forFeature([
      { name: StgPayment.name, schema: StgPaymentSchema },
    ]),
  ],
})
export class StgPaymentModule {}
