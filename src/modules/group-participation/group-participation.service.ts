import {
  BadRequestException,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import * as dayjs from 'dayjs';
import * as customParseFormat from 'dayjs/plugin/customParseFormat';
import * as timezone from 'dayjs/plugin/timezone';
import * as utc from 'dayjs/plugin/utc';
import { Model } from 'mongoose';
import {
  GroupParticipation,
  GroupParticipationDocument,
} from 'src/schema/groupParticipation.schema';
import {
  ActionGroup,
  GroupParticipationHistoryType,
} from 'src/utils/constants';
import { ActivityService } from '../activity/activity.service';
import { GroupParticipationHistoryService } from '../group-participation-history/group-participation-history.service';
import { GroupParticipationRequestService } from '../group-participation-request/group-participation-request.service';
import { CreateGroupParticipationDto } from './dto/create-group-participation.dto';
import { UpdateGroupParticipationDto } from './dto/update-group-participation.dto';

dayjs.extend(customParseFormat);
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault('Asia/Ho_Chi_Minh');
dayjs().format();
// dayjs.extend(AdvancedFormat);
@Injectable()
export class GroupParticipationService {
  constructor(
    @InjectModel(GroupParticipation.name)
    private readonly groupParticipationModel: Model<GroupParticipationDocument>,
    @Inject(forwardRef(() => GroupParticipationRequestService))
    private readonly groupParticipationRequestService: GroupParticipationRequestService,
    private readonly groupParticipationHistoryService: GroupParticipationHistoryService,
    @Inject(forwardRef(() => ActivityService))
    private readonly activityService: ActivityService,
  ) {}

  async createGroupParticipation(
    runnerId: string,
    payload: CreateGroupParticipationDto,
  ) {
    try {
      const existedGroupParticipation = await this.groupParticipationModel.find(
        {
          owner: runnerId,
          challenge: payload?.challenge?.toString(),
          isDeleted: false,
        },
      );
      const existedRequest =
        await this.groupParticipationRequestService.checkExistedRequest(
          runnerId,
        );
      if (existedGroupParticipation?.length > 0 || existedRequest?.length > 0) {
        throw new BadRequestException('Bạn đã tham gia hoặc tạo nhóm khác!');
      }

      const newGroupParticipation = await this.groupParticipationModel.create({
        owner: runnerId,
        ...payload,
      });

      await newGroupParticipation.save();

      await this.groupParticipationHistoryService.createGroupParticipationHistory(
        runnerId,
        newGroupParticipation?._id?.toString(),
        GroupParticipationHistoryType?.GROUP_INFO,
        ActionGroup?.CREATE_GROUP,
      );

      await this.groupParticipationRequestService.joinGroupWithOwner(
        runnerId,
        newGroupParticipation?._id?.toString(),
        payload?.challenge?.toString(),
      );

      return {
        data: newGroupParticipation.toJSON(),
        message: 'Create group participation successfully',
      };
    } catch (error) {
      console.log('createGroupParticipation: ', error);
      throw new BadRequestException(error);
    }
  }

  async getInfoGroupParticipation(groupId: string) {
    try {
      const groupParticipation = await this.groupParticipationModel
        .findOne({
          _id: groupId,
        })
        .exec();

      return groupParticipation;
    } catch (error) {
      console.log('getInfoGroupParticipation: ', error);
      throw new BadRequestException(error);
    }
  }

  async updateGroupParticipation(
    runnerId: string,
    payload: UpdateGroupParticipationDto,
  ) {
    try {
      const existedGroupParticipation = await this.checkOwnerGroupParticipation(
        runnerId,
        payload?.groupId?.toString(),
      );
      if (!existedGroupParticipation?.length) {
        throw new BadRequestException('Bạn không phải là nhóm trưởng!');
      }

      const groupParticipation = await this.groupParticipationModel.findOne({
        _id: payload?.groupId?.toString(),
      });
      if (!groupParticipation) {
        throw new BadRequestException('Không tìm thấy nhóm này!');
      }

      await groupParticipation.updateOne(
        {
          $set: {
            ...payload,
          },
          updatedAt: new Date().toLocaleString(),
        },
        { new: true },
      );

      return groupParticipation;
    } catch (error) {
      console.log('updateGroupParticipation: ', error);
      throw new BadRequestException(error);
    }
  }

  async deleteGroupParticipation(runnerId: string, groupId: string) {
    try {
      const existedGroupParticipation =
        await this.groupParticipationModel.findOne({
          _id: groupId,
          owner: runnerId,
        });
      if (!existedGroupParticipation) {
        throw new BadRequestException('Bạn chưa có nhóm nào!');
      }

      if (existedGroupParticipation?.members?.length > 1) {
        throw new BadRequestException(
          'Nhóm của bạn đang có thành viên, không thể xóa!',
        );
      }

      await this.groupParticipationRequestService.deleteRequestWithOwner(
        runnerId,
        groupId,
      );

      await this.removeMemberFromGroupParticipation(runnerId, groupId);

      const updatedGroupParticipation =
        await this.groupParticipationModel.findByIdAndUpdate(
          groupId,
          {
            $set: {
              isDeleted: true,
            },
            updatedAt: new Date().toLocaleString(),
          },
          { new: true },
        );

      await this.groupParticipationRequestService.cancelRequestWithDeleteGroup(
        groupId,
      );

      await this.groupParticipationHistoryService.createGroupParticipationHistory(
        runnerId,
        groupId,
        GroupParticipationHistoryType?.GROUP_INFO,
        ActionGroup?.DELETE_GROUP,
      );

      return {
        data: null,
        message: 'Delete group participation successfully',
      };
    } catch (error) {
      console.log('deleteGroupParticipation: ', error);
      throw new BadRequestException(error);
    }
  }

  async getAllGroupParticipation(challengeId: string) {
    try {
      const allGroupParticipation = await this.groupParticipationModel
        .find({
          isDeleted: false,
          ...(challengeId && { challenge: challengeId }),
        })
        .populate([
          {
            path: 'owner',
            select: 'name avatar',
          },
          {
            path: 'members',
            select: 'name avatar',
          },
        ])
        .select('-updatedAt -__v -department');

      const statsOfGroup =
        await this.groupParticipationRequestService.getStatsGroupParticipationRequest(
          challengeId,
        );

      const groupParticipation = allGroupParticipation
        ?.map((group) => {
          const groupInfo = group?.toJSON();
          const stats = (statsOfGroup || [])?.find(
            (stat) => stat?._id?.toString() === groupInfo?._id?.toString(),
          );
          return {
            ...groupInfo,
            distance: stats?.totalDistance || 0,
          };
        })
        ?.sort((a, b) => b?.distance - a?.distance);

      return groupParticipation;
    } catch (error) {
      console.log('getAllGroupParticipation: ', error);
      throw new BadRequestException(error);
    }
  }

  async checkOwnerGroupParticipation(runnerId: string, groupId: string) {
    try {
      const existedGroupParticipation = await this.groupParticipationModel.find(
        {
          owner: runnerId,
          _id: groupId,
        },
      );
      return existedGroupParticipation;
    } catch (error) {
      console.log('checkOwnerGroupParticipation: ', error);
      throw new BadRequestException(error);
    }
  }

  async addMemberToGroupParticipation(runnerId: string, groupId: string) {
    try {
      const groupInfo = await this.groupParticipationModel.findOne({
        _id: groupId,
      });
      if (!groupInfo) {
        throw new BadRequestException('Không tìm thấy nhóm này!');
      }

      const newMembers = [...(groupInfo?.members || []), runnerId?.toString()];
      await groupInfo.updateOne(
        {
          $set: {
            members: newMembers,
          },
          updatedAt: new Date().toLocaleString(),
        },
        { new: true },
      );

      return groupInfo;
    } catch (error) {
      console.log('addMemberToGroupParticipation: ', error);
      throw new BadRequestException(error);
    }
  }

  async removeMemberFromGroupParticipation(runnerId: string, groupId: string) {
    try {
      const groupInfo = await this.groupParticipationModel.findOne({
        _id: groupId,
      });
      if (!groupInfo) {
        throw new BadRequestException('Không tìm thấy nhóm này!');
      }

      const newMembers = ([...groupInfo?.members] || [])?.filter(
        (member) => member?.toString() !== runnerId?.toString(),
      );
      await groupInfo.updateOne(
        {
          $set: {
            members: newMembers,
          },
          updatedAt: new Date().toLocaleString(),
        },
        { new: true },
      );

      return groupInfo;
    } catch (error) {
      console.log('removeMemberFromGroupParticipation: ', error);
      throw new BadRequestException(error);
    }
  }

  async checkExistedInGroup(groupId: string, runnerId: string) {
    try {
      const groupInfo = await this.groupParticipationModel.findOne({
        _id: groupId,
      });
      if (!groupInfo) {
        throw new BadRequestException('Không tìm thấy nhóm này!');
      }

      const existed = groupInfo?.members?.find(
        (member) => member?.toString() === runnerId?.toString(),
      );
      return existed;
    } catch (error) {
      console.log('checkExistedInGroup: ', error);
      throw new BadRequestException(error);
    }
  }

  async getMemberOfGroup(groupId: string) {
    try {
      const groupInfo = await this.groupParticipationModel
        .findOne({
          _id: groupId,
        })
        .populate([
          {
            path: 'members',
            select: 'name avatar',
          },
        ]);
      if (!groupInfo) {
        throw new BadRequestException('Không tìm thấy nhóm này!');
      }

      const members = groupInfo?.members;

      const statsOfMembers =
        await this.groupParticipationRequestService.getStatsGroupParticipationRequestMembers(
          groupId?.toString(),
        );

      const membersInfo = members?.map((member) => {
        const memberInfo = member;
        const stats = (statsOfMembers || [])?.find(
          (stat) => stat?.creator?.toString() === memberInfo?._id?.toString(),
        );
        return {
          _id: memberInfo?._id,
          name: memberInfo?.name,
          avatar: memberInfo?.avatar,
          athleteStats: stats?.athleteStats || {},
          // createdAt: stats?.createdAt,
        };
      });

      return [...membersInfo]?.sort(
        (a, b) =>
          (b?.athleteStats?.distance || 0) - (a?.athleteStats?.distance || 0),
      );
    } catch (error) {
      console.log('getMemberOfGroup: ', error);
      throw new BadRequestException(error);
    }
  }

  // Get data activities of user in group and group by day
  async getTrendOfGroup(groupId: string) {
    try {
      // Get all members of group
      const groupInfo = await this.groupParticipationModel.findOne({
        _id: groupId,
      });
      const members = groupInfo?.members || [];
      const challengeId = groupInfo?.challenge?.toString();

      const promises = [];

      // Get all activities of all members
      members?.forEach((member) => {
        promises.push(
          this.activityService.getListActivityByRunner(
            member?.toString(),
            challengeId,
          ),
        );
      });

      // Get all activities of all members
      const activities = await Promise.all(promises);

      // Get data of activities is valid
      const data = (activities || [])
        .flat()
        ?.filter((activity) => activity?.isInValid !== true)
        ?.map((activity) => ({
          activityId: activity?.activityId,
          runnerId: activity?.runnerId,
          stravaId: activity?.stravaId,
          distance: activity?.distance,
          start_date: activity?.start_date,
          start_date_local: activity?.start_date_local,
          isInValid: activity?.isInValid,
        }));

      // Group this data by start_date
      const groupByDate = data?.reduce((acc, activity) => {
        const date = dayjs(activity?.start_date)
          .utcOffset(7)
          .format('MM/DD/YYYY');
        if (!acc[date]) {
          acc[date] = [];
        }
        acc[date].push(activity);
        return acc;
      }, {});

      // Sort by date
      const sortedGroupByDate = Object.keys(groupByDate)
        ?.sort((a, b) => new Date(b).getTime() - new Date(a).getTime())
        ?.reduce((acc, key) => {
          acc[key] = groupByDate[key];
          return acc;
        }, {});

      // Create new variable and Calculate total distance of each day
      Object.keys(sortedGroupByDate)?.forEach((key) => {
        const totalDistance = sortedGroupByDate[key]
          ?.reduce((acc, activity) => acc + activity?.distance, 0)
          ?.toFixed(2);
        sortedGroupByDate[key] = {
          totalDistance,
          activities: sortedGroupByDate[key],
        };
      });

      // Return data with date and total distance
      const dataDistance = Object.keys(sortedGroupByDate)?.map((key) => {
        return {
          date: key,
          totalDistance: sortedGroupByDate[key]?.totalDistance,
        };
      });

      return {
        trend: dataDistance,
        group: {
          groupid: groupInfo?._id,
          name: groupInfo?.name,
        },
      };
    } catch (error) {
      console.log('getActivitiesOfUserInGroup: ', error);
      throw new BadRequestException(error);
    }
  }

  async getTrendMembersOfGroup(groupId: string) {
    try {
      const groupInfo = await this.groupParticipationModel.findOne({
        _id: groupId,
      });
      const members = groupInfo?.members || [];
      const challengeId = groupInfo?.challenge?.toString();

      const promises = [];
      members?.forEach((member) => {
        promises.push(
          this.activityService.getTrendOfUser(member?.toString(), challengeId),
        );
      });
      const trendMembers = await Promise.all(promises);

      return trendMembers;
    } catch (error) {
      console.log('getTrendMembersOfGroup: ', error);
      throw new BadRequestException(error);
    }
  }
}
