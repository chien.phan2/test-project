import { Test, TestingModule } from '@nestjs/testing';
import { GroupParticipationController } from './group-participation.controller';

describe('GroupParticipationController', () => {
  let controller: GroupParticipationController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GroupParticipationController],
    }).compile();

    controller = module.get<GroupParticipationController>(
      GroupParticipationController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
