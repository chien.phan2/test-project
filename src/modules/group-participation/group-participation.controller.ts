import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { CreateGroupParticipationDto } from './dto/create-group-participation.dto';
import { GroupParticipationService } from './group-participation.service';
import { UpdateGroupParticipationDto } from './dto/update-group-participation.dto';

@ApiTags('Challenge')
@Controller('challenge')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class GroupParticipationController {
  constructor(
    private readonly groupParticipationService: GroupParticipationService,
  ) {}

  @Get('/group/get-all-group')
  getAllGroup(@Query('challengeId') challengeId: string) {
    return this.groupParticipationService.getAllGroupParticipation(challengeId);
  }

  @Get('/group/member-of-group/:groupId')
  getMemberOfGroup(@Param('groupId') groupId: string) {
    return this.groupParticipationService.getMemberOfGroup(groupId);
  }

  @Post('/group/create-group')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  createGroup(
    @Body() payload: CreateGroupParticipationDto,
    @Req() req: Request,
  ) {
    const userID = req.userID;
    return this.groupParticipationService.createGroupParticipation(
      userID,
      payload,
    );
  }

  @Patch('/group/update-group')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  updateGroup(
    @Body() payload: UpdateGroupParticipationDto,
    @Req() req: Request,
  ) {
    const userID = req.userID;
    return this.groupParticipationService.updateGroupParticipation(
      userID,
      payload,
    );
  }

  @Delete('/group/delete-group/:groupId')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  deleteGroup(@Req() req: Request, @Param('groupId') groupId: string) {
    const userID = req.userID;
    return this.groupParticipationService.deleteGroupParticipation(
      userID,
      groupId,
    );
  }

  @Get('/group/trend/:groupId')
  @ApiOperation({
    description: 'Get the change in distance of that group by day.',
  })
  getTrendOfGroup(@Param('groupId') groupId: string) {
    return this.groupParticipationService.getTrendOfGroup(groupId);
  }

  @Get('/group/trend-members/:groupId')
  @ApiOperation({
    description: 'Get the change in distance of member in that group by day.',
  })
  getTrendMembersOfGroup(@Param('groupId') groupId: string) {
    return this.groupParticipationService.getTrendMembersOfGroup(groupId);
  }
}
