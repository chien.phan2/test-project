import { Test, TestingModule } from '@nestjs/testing';
import { GroupParticipationService } from './group-participation.service';

describe('GroupParticipationService', () => {
  let service: GroupParticipationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GroupParticipationService],
    }).compile();

    service = module.get<GroupParticipationService>(GroupParticipationService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
