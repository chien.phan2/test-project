import { Module, forwardRef } from '@nestjs/common';
import { GroupParticipationController } from './group-participation.controller';
import { GroupParticipationService } from './group-participation.service';
import { MongooseModule } from '@nestjs/mongoose';
import {
  GroupParticipation,
  GroupParticipationSchema,
} from 'src/schema/groupParticipation.schema';
import { GroupParticipationRequestModule } from '../group-participation-request/group-participation-request.module';
import { GroupParticipationHistoryModule } from '../group-participation-history/group-participation-history.module';
import { ActivityModule } from '../activity/activity.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: GroupParticipation.name,
        schema: GroupParticipationSchema,
      },
    ]),
    forwardRef(() => GroupParticipationRequestModule),
    GroupParticipationHistoryModule,
    forwardRef(() => ActivityModule),
  ],
  controllers: [GroupParticipationController],
  providers: [GroupParticipationService],
  exports: [GroupParticipationService],
})
export class GroupParticipationModule {}
