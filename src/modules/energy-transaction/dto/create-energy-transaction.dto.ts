import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateEnergyTransactionDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  productId: string;
}
