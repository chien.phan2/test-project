import { Test, TestingModule } from '@nestjs/testing';
import { EnergyTransactionService } from './energy-transaction.service';

describe('EnergyTransactionService', () => {
  let service: EnergyTransactionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EnergyTransactionService],
    }).compile();

    service = module.get<EnergyTransactionService>(EnergyTransactionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
