import { Test, TestingModule } from '@nestjs/testing';
import { EnergyTransactionController } from './energy-transaction.controller';

describe('EnergyTransactionController', () => {
  let controller: EnergyTransactionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EnergyTransactionController],
    }).compile();

    controller = module.get<EnergyTransactionController>(
      EnergyTransactionController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
