import {
  BadRequestException,
  Inject,
  Injectable,
  Logger,
  forwardRef,
} from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import fetch from 'node-fetch';
import { EventsGateway } from 'src/events/events.gateway';
import {
  EnergyTransaction,
  EnergyTransactionDocument,
} from 'src/schema/energyTransaction.schema';
import { ActionType } from 'src/utils/constants';
import { decodeBase64 } from 'src/utils/genBase64String';
import genSignature from 'src/utils/genSignature';
import { BudgetHistoryService } from '../budget-history/budget-history.service';
import { ChallengeService } from '../challenge/challenge.service';
import { EnergyService } from '../energy/energy.service';
import { RunnerService } from '../runner/runner.service';
import { MomoIpn } from '../transaction/dto/momo-ipn.dto';

@Injectable()
export class EnergyTransactionService {
  constructor(
    @InjectModel(EnergyTransaction.name)
    private readonly energyTransactionModel: Model<EnergyTransactionDocument>,
    @InjectConnection() private connection: Connection,
    private readonly eventsGateway: EventsGateway,

    @Inject(forwardRef(() => RunnerService))
    private readonly runnerService: RunnerService,

    @Inject(forwardRef(() => BudgetHistoryService))
    private readonly budgetHistoryService: BudgetHistoryService,

    @Inject(forwardRef(() => ChallengeService))
    private readonly challengeService: ChallengeService,

    private readonly energyService: EnergyService,
  ) {}

  async createMomoTransactionEnergy(
    transaction: EnergyTransaction,
    productName: string,
    session: any,
  ) {
    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        ipnUrl: process.env.IPN_URL_RECEIVE_ENERGY,
        partnerName: 'MoMo Challenge',
        storeId: 'MomoTestStore',
        requestType: 'captureWallet',
        orderId: transaction._id.toString(),
        orderInfo: `Thanh toán ${productName}`,
        amount: transaction.value,
        lang: 'vi',
        redirectUrl: `momo://?refId=${
          process.env.ENVIRONMENT == 'pro'
            ? 'miniapp.8TZb9qBBnwSFqmTap1vw.momorun'
            : 'miniapp.8TZb9qBBnwSFqmTap1vw.momorun'
        }&transaction=${transaction._id.toString()}&appId=miniapp.8TZb9qBBnwSFqmTap1vw.momorun&deeplink=true`,
        requestId: transaction._id.toString(),
        extraData: transaction.product,
        autoCapture: false,
        signature: '',
      };
      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&extraData=${data.extraData}&ipnUrl=${data.ipnUrl}&orderId=${data.orderId}&orderInfo=${data.orderInfo}&partnerCode=${data.partnerCode}&redirectUrl=${data.redirectUrl}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_TRANSACTION, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });

      const responseData = await response.json();
      Logger.log('Receive response energy', responseData);

      if (responseData['resultCode'] != 0) {
        await this.energyTransactionModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            status: 'error',
            paymentInfo: responseData,
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        throw new BadRequestException('Create energy transaction failed');
      }

      return [signature, responseData];
    } catch (error) {
      await this.energyTransactionModel.findOneAndUpdate(
        { _id: transaction._id.toString() },
        { status: 'error', updatedAt: new Date().toLocaleString() },
        { session },
      );
      console.log('createMomoTransactionEnergy: ', error);
      throw new BadRequestException('Create energy transaction failed');
    }
  }

  async createRequestTransaction(
    userID: string,
    energyProductRequest: any,
    type: string,
    challengeID?: string,
  ) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const createTime = new Date();
      const newEnergyTransactions = await this.energyTransactionModel.create(
        [
          {
            creator: userID,
            product: energyProductRequest?.product?._id.toString(),
            status: 'pending',
            quantity: energyProductRequest.quantity,
            value:
              (energyProductRequest?.product?.productPrice || 0) *
              (energyProductRequest?.quantity || 1),
            type: energyProductRequest?.product?.type,
            challenge: challengeID || '',
            signature: '',
            paymentInfo: {},
            createdAt: createTime.toLocaleString(),
            updatedAt: createTime.toLocaleString(),
            timestamp: createTime.getTime(),
          },
        ],
        {
          session,
        },
      );

      const newEnergyTransaction = newEnergyTransactions[0];
      const [signature, paymentInfo]: any = await this.createMomoTransactionEnergy(
        newEnergyTransaction,
        energyProductRequest?.product?.productName || 'Challenge',
        session,
      );

      newEnergyTransaction.paymentInfo = paymentInfo;
      newEnergyTransaction.signature = signature;

      await newEnergyTransaction.save({ session });

      this.setTransactionRequestFailedAfterTime(newEnergyTransaction.id, 70000);

      await session.commitTransaction();
      session.endSession();

      return {
        data: paymentInfo,
      };
    } catch (error) {
      console.log('createRequestTransaction: ', error);
      Logger.error('Create energy transaction failed', error);

      await session.abortTransaction();
      session.endSession();

      throw error;
    }
  }

  setTransactionRequestFailedAfterTime(transactionID: string, timeout: number) {
    setTimeout(async () => {
      const session = await this.connection.startSession();
      session.startTransaction();
      try {
        const transaction = await this.energyTransactionModel.findOne(
          {
            _id: transactionID,
            status: 'pending',
          },
          null,
          { session },
        );
        if (transaction) {
          await this.energyTransactionModel.findOneAndUpdate(
            { _id: transactionID, status: 'pending' },
            { status: 'failed', updatedAt: new Date().toLocaleString() },
            { session },
          );
        }
        await session.commitTransaction();
        await session.endSession();
      } catch (err) {
        console.log('setTransactionRequestFailedAfterTime: ', err);
        Logger.error(err);
        Logger.warn(
          'ERROR: Can not set energy transaction status for',
          transactionID,
        );
        await session.abortTransaction();
        await session.endSession();
      }
    }, timeout);
  }

  async sendConfirmToMomo(transaction: EnergyTransaction) {
    console.log('===> sendConfirmToMomo transaction:', transaction);
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        requestId: transaction._id.toString(),
        orderId: transaction._id.toString(),
        storeId: 'MomoTestStore',
        requestType: 'capture',
        amount: transaction.value,
        lang: 'vi',
        description: 'Thanh toán energy',
        signature: '',
      };
      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&description=${data.description}&orderId=${data.orderId}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CONFIRM_PAYMENT, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData: any = await response.json();
      Logger.log('Receive response energy', responseData);

      const transactionID = responseData.orderId;
      const productID = String(transaction.product);

      if (responseData.resultCode == 0) {
        await this.energyTransactionModel.findOneAndUpdate(
          { _id: transactionID, status: 'pending' },
          {
            status: 'success',
            paymentInfo: { ...transaction.paymentInfo, ...responseData },
          },
          session,
        );

        if (transaction?.type === 'sponsor') {
          try {
            await this.challengeService.updateBudgetChallenge(
              transaction?.creator?._id.toString(),
              transaction?.value || 0,
              'sponsor',
              transaction?.product,
            );
            if (transaction?.value >= 200000) {
              await this.challengeService.giveAwayChallengeTicket(
                transaction?.creator?._id?.toString(),
                ActionType.SPONSOR,
              );
            }
          } catch (error) {
            console.log('updateBudgetChallengeRes: ', error);
          }
        } else if (transaction?.type === 'ticket') {
          try {
            const updateInventoryByTicket =
              await this.challengeService.giveAwayChallengeTicket(
                transaction?.creator?._id.toString(),
                ActionType.BUY,
              );
          } catch (error) {
            console.log('updateInventory: ', error);
          }
        } else {
          console.log('Run ===> updateEnergyForRunner');
          const purchaseEnergy: any = {
            productId: transaction.product,
            quantity: transaction.quantity,
          };

          const updateEnergyRes =
            await this.runnerService.updateEnergyForRunner(
              transaction?.creator?._id.toString(),
              purchaseEnergy,
            );

          const productData = await this.energyService.getProductEnergyById(
            productID,
          );
          const producQuantity = productData?.productQuantity || 0;

          console.log('Run ===> checkFirstPayment');
          await this.runnerService.checkFirstPayment(
            transaction?.creator?._id.toString(),
          );

          setTimeout(() => {
            const message = `${transaction?.creator?.name} đã mua ${producQuantity} thanh năng lượng.`;
            this.eventsGateway.onEnergyTransaction({
              productID: productID,
              status: 'success',
              transactionID: responseData.orderId,
            });
            this.eventsGateway.onNotification({
              status: 'success',
              message: message,
              date: new Date().toLocaleString(),
            });
          }, 10000);
        }

        await session.commitTransaction();
        session.endSession();
        // socket here
      }
    } catch (err) {
      await session.abortTransaction();
      session.endSession();
      console.log('sendConfirmToMomo: ', err);
      Logger.error('Send confirm energy trans to MoMo failed', err);
      throw new BadRequestException('Send confirm energy trans to MoMo failed');
    }
  }

  async sendCancelToMomo(transaction: EnergyTransaction) {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        requestId: transaction._id.toString(),
        orderId: transaction._id.toString(),
        storeId: 'MomoTestStore',
        requestType: 'cancel',
        amount: transaction.value,
        lang: 'vi',
        description: 'Thanh toán energy',
        signature: '',
      };
      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&description=${data.description}&orderId=${data.orderId}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CONFIRM_PAYMENT, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData:any = await response.json();
      Logger.log('Receive response energy', responseData);

      const productID = String(transaction.product);

      if (responseData.resultCode == 0) {
        // socket here
        setTimeout(() => {
          this.eventsGateway.onEnergyTransaction({
            productID: productID,
            status: 'failed',
            transactionID: responseData.orderId,
          });
        }, 10000);
      }
    } catch (err) {
      console.log('sendCancelToMomo: ', err);
      Logger.error('Send confirm energy trans to MoMo failed', err);
      throw new BadRequestException('Send confirm energy trans to MoMo failed');
    }
  }

  async handleIpnNoti(data: MomoIpn) {
    console.log('=====> handleIpnNoti data:', data);
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const transactionID = data.orderId;
      const productID = data.extraData;

      //check signature later
      const transaction = await this.energyTransactionModel
        .findOne({
          _id: transactionID,
          product: productID,
          value: data.amount,
        })
        .populate('creator');

      if (transaction) {
        if (data.resultCode == 9000) {
          if (transaction.status == 'pending') {
            this.sendConfirmToMomo(transaction);
          } else this.sendCancelToMomo(transaction);
        } else if (data.resultCode != 0 && data.resultCode != 9000) {
          // transaction failed

          await this.energyTransactionModel.findOneAndUpdate(
            { _id: transactionID },
            {
              status: 'failed',
              paymentInfo: { ...transaction.paymentInfo, ...data },
            },
            { session },
          );

          setTimeout(() => {
            this.eventsGateway.onEnergyTransaction({
              productID: productID,
              status: 'fail',
              transactionID: data.orderId,
            });
          }, 10000);

          await session.commitTransaction();
        }
      }
    } catch (error) {
      const productID = data.extraData;
      setTimeout(() => {
        this.eventsGateway.onEnergyTransaction({
          productID: productID,
          status: 'fail',
          transactionID: data.orderId,
        });
      }, 10000);
      console.log('handleIpnNoti: ', error);
      Logger.log('Update energy transaction status failed', error);
    } finally {
      session.endSession();

      return { data: 'Success' };
    }
  }

  async handleIpnNotiRequestPay(data: MomoIpn) {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const extraData = JSON.parse(decodeBase64(data.extraData));
      const transactionID = extraData.id;

      //check signature later

      const transaction = await this.energyTransactionModel.findOne({
        _id: transactionID,
        statusRequestPay: 'pending',
      });

      if (transaction) {
        if (data.resultCode == 0) {
          await this.energyTransactionModel.findOneAndUpdate(
            { _id: transactionID },
            {
              statusRequestPay: 'success',
              // requestPayInfo: { ...transaction.requestPayInfo, ...data },
            },
            session,
          );

          await session.commitTransaction();
        } else {
          // transaction failed
          await this.energyTransactionModel.findOneAndUpdate(
            { _id: transactionID },
            {
              statusRequestPay: 'failed',
              // requestPayInfo: { ...transaction.requestPayInfo, ...data },
            },
            { session },
          );

          await session.commitTransaction();
        }
      }
    } catch (err) {
      console.log('handleIpnNotiRequestPay: ', err);
      Logger.error(`Update pay info for energy transaction error`, err);
    } finally {
      session.endSession();
      return { data: 'Success' };
    }
  }

  async getAllTransaction(filter: any) {
    try {
      const data = await this.energyTransactionModel
        .find({
          $expr: {
            $gte: [
              {
                $toDate: '$createdAt',
              },
              new Date(filter?.fromDate || '2021-01-01'),
            ],
          },
          $or: [{ status: 'pending' }, { status: 'success' }],
        })
        .populate({
          path: 'creator',
          select:
            '-stravaData -userID -isBlocked -energy -medals -createdAt -updatedAt',
        })
        .populate({
          path: 'product',
          select:
            '-createdAt -updatedAt -productImage -productDescription -productQuantity -productPrice -key',
        })
        .select(
          '-signature -paymentInfo.partnerCode -paymentInfo.requestId -paymentInfo.responseTime -paymentInfo.payUrl -paymentInfo.deeplink -paymentInfo.deeplinkMiniApp',
        );
      return data;
    } catch (error) {
      console.log('getAllTransaction: ', error);
      throw error;
    }
  }
}
