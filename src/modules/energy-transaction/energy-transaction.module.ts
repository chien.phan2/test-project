import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EventsModule } from 'src/events/events.module';
import {
  EnergyTransaction,
  EnergyTransactionSchema,
} from 'src/schema/energyTransaction.schema';
import { BudgetHistoryModule } from '../budget-history/budget-history.module';
import { ChallengeModule } from '../challenge/challenge.module';
import { EnergyModule } from '../energy/energy.module';
import { RunnerModule } from '../runner/runner.module';
import { EnergyTransactionController } from './energy-transaction.controller';
import { EnergyTransactionService } from './energy-transaction.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: EnergyTransaction.name, schema: EnergyTransactionSchema },
    ]),
    EventsModule,
    forwardRef(() => RunnerModule),
    forwardRef(() => BudgetHistoryModule),
    forwardRef(() => ChallengeModule),
    EnergyModule,
  ],
  providers: [EnergyTransactionService],
  exports: [EnergyTransactionService],
  controllers: [EnergyTransactionController],
})
export class EnergyTransactionModule {}
