import {
  Body,
  Controller,
  Get,
  HttpCode,
  Logger,
  Post,
  Query,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { EnergyTransactionService } from './energy-transaction.service';

@ApiTags('Challenge')
@Controller('challenge')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class EnergyTransactionController {
  constructor(
    private readonly energyTransactionService: EnergyTransactionService,
  ) {}

  @Post('/ipn')
  @HttpCode(204)
  ipnNoti(@Body() body: any, @Query() query: any) {
    console.log('Receive energy IPN:', body);
    Logger.log('Receive energy IPN:', body);
    console.log('Ma energy', query.type);
    Logger.log('Ma energy', query.type);
    if (query.type == 1)
      return this.energyTransactionService.handleIpnNoti(body);
    else if (query.type == 2)
      return this.energyTransactionService.handleIpnNotiRequestPay(body);
  }

  @Get('/get-transaction')
  getAllTransaction(@Query() filter: any) {
    return this.energyTransactionService.getAllTransaction(filter);
  }
}
