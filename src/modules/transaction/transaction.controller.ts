import {
  Controller,
  Post,
  Body,
  Req,
  UseGuards,
  HttpCode,
  Logger,
  UseInterceptors,
  Query,
  Get,
  Param,
  BadRequestException,
} from '@nestjs/common';
import { TransactionService } from './transaction.service';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { Request } from 'express';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { ApiKeyAuthGuard } from 'src/middleware/apiKey-auth.middleware';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { ApiBearerAuth } from '@nestjs/swagger';

@Controller('transaction')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class TransactionController {
  constructor(private readonly transactionService: TransactionService) {}

  @Post()
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  create(
    @Body() createTransactionDto: CreateTransactionDto,
    @Req() req: Request,
  ) {
    const userID = req.userID;
    Logger.log(`${userID} Create new transaction by:`, createTransactionDto);
    return this.transactionService.create(userID, createTransactionDto);
  }

  @Get(`/donate`)
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  async getDonate() {
    const result = await this.transactionService.getDonate();
    return result;
  }

  @Get(`:id`)
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  checkStatus(@Param('id') id: string, @Req() req: Request) {
    const userID = req.userID;
    return this.transactionService.checkStatus(userID, id);
  }

  @Post('/ipn')
  @HttpCode(204)
  ipnNoti(@Body() body: any, @Req() req: Request, @Query() query: any) {
    Logger.log('Receive IPN:', body);
    Logger.log('Ma', query.type);
    if (query.type == 1) return this.transactionService.handleIpnNoti(body);
    else if (query.type == 2)
      return this.transactionService.handleIpnNotiRequestPay(body);
  }

  @Post('/transferMoney')
  transferMoney(@Body() body: any, @Req() req: Request, @Query() query: any) {
    if (
      query.transactionID &&
      query.authCode &&
      query.authCode == '@12nasdjAOOOn112'
    ) {
      return this.transactionService.transferMoneyOfTransaction(
        query.transactionID,
      );
    } else return new BadRequestException();
  }

  @Post('/cashout')
  cashOut(@Body() body: any, @Req() req: Request, @Query() query: any) {
    if (
      query.amount &&
      query.phone &&
      query.authCode &&
      query.authCode == '@12nasdjAOOOn112'
    ) {
      return this.transactionService.cashOut(query.phone, Number(query.amount));
    } else return new BadRequestException();
  }

  @UseGuards(new ApiKeyAuthGuard())
  @Get('/portal/all-tran')
  getAllTran(@Query() filter: any) {
    return this.transactionService.fetchAll(filter);
  }

  @UseGuards(new ApiKeyAuthGuard())
  @Get('/portal/overview-tran-by-month')
  getOverviewTranByMonth() {
    return this.transactionService.fetchOverviewTransactionInMonth();
  }

  @UseGuards(new ApiKeyAuthGuard())
  @Get('/portal/transaction-last-week')
  getTransactionLastWeek() {
    return this.transactionService.fetchTransactionLastWeek();
  }

  @UseGuards(new ApiKeyAuthGuard())
  @Get('/portal/transaction-in-year')
  getTransactionInYear() {
    return this.transactionService.fetchTransactionInYear();
  }
}
