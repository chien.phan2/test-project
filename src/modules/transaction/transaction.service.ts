import {
  BadGatewayException,
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, FilterQuery, Model } from 'mongoose';
import fetch from 'node-fetch';
import { MY_MESSAGE } from 'src/contants/message.contants';
import { EventsGateway } from 'src/events/events.gateway';
import { TicketDocument } from 'src/schema/ticket.schema';
import {
  Transaction,
  TransactionDocument,
} from 'src/schema/transaction.schema';
import formatText from 'src/utils/formatText.utils';
import genBase64, { decodeBase64 } from 'src/utils/genBase64String';
import genDisbursementMethod from 'src/utils/genDisbursmentMethod';
import genSignature from 'src/utils/genSignature';
import getDateString from 'src/utils/getDateString';
import { TicketService } from '../ticket/ticket.service';
import { UserService } from '../user/user.service';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { DonateResponse } from './dto/donate-respone.dto';
import { MomoIpn } from './dto/momo-ipn.dto';
import startOfDay from 'date-fns/startOfDay';
import endOfDay from 'date-fns/endOfDay';
import getYesterdayString from 'src/utils/getYesterdayString';

@Injectable()
export class TransactionService {
  constructor(
    @InjectModel(Transaction.name)
    private transactionModel: Model<TransactionDocument>,
    @InjectConnection() private connection: Connection,
    @Inject(forwardRef(() => TicketService))
    private readonly ticketService: TicketService,
    private readonly userService: UserService,
    private readonly eventsGateway: EventsGateway,
  ) {}

  async create(userID: string, createTransactionDto: CreateTransactionDto) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const user = await this.userService.getUserInfo(userID);
      const currentTrans = await this.getCurrentTransactionOfTicket(
        userID,
        createTransactionDto.ticketID,
      );
      if (currentTrans)
        return {
          data: currentTrans.paymentInfo,
        };

      const ticketID = createTransactionDto.ticketID;
      const ticket = await this.ticketService.getAvailable(ticketID, session);

      await this.checkCanCreateTransaction(userID, session);

      if (ticket.owner._id.equals(user._id) && ticket.type == 'sell')
        throw new BadRequestException(MY_MESSAGE.BUY_OWN);

      await this.ticketService.setPending(ticketID, userID, session);

      const createTime = new Date();

      const newTransactions = await this.transactionModel.create(
        [
          {
            creator: user._id.toString(),
            receiver: ticket.owner._id.toString(),
            ticket: ticket._id.toString(),
            status: 'pending', // add one more status for cancel,
            value: ticket.price,
            fee: ticket.donate,
            paymentInfo: {}, // save momo response for payment
            statusRequestPay: 'pending',
            requestPayInfo: {},
            createdAt: createTime.toLocaleString(),
            updatedAt: createTime.toLocaleString(),
            timestamp: createTime.getTime(),
          },
        ],
        { session },
      );

      const newTransaction = newTransactions[0];

      const [signature, paymentInfo]: any = await this.createMomoTransaction(
        newTransaction,
        session,
      );

      newTransaction.paymentInfo = paymentInfo;
      newTransaction.signature = signature;
      await newTransaction.save({ session });

      await this.ticketService.setTransaction(
        ticket.id,
        newTransaction.id,
        session,
      );

      await this.ticketService.sendSocketPending(ticket.id, session);

      this.setTransactionFailedAfterTime(newTransaction.id, ticket.id, 70000);

      await session.commitTransaction();
      session.endSession();

      return {
        data: paymentInfo,
      };
    } catch (err) {
      Logger.error('Create transaction failed', err);
      //handle set ticket free here
      await session.abortTransaction();
      session.endSession();

      throw err;
    }
  }

  async checkCanCreateTransaction(userID: string, session: any) {
    const activeTransaction = await this.transactionModel.find(
      {
        status: 'pending',
        creator: userID,
        createdAt: { $regex: getDateString(), $options: 'i' },
      },
      null,
      { session },
    );

    if (activeTransaction.length == 0) return true;
    else throw new BadRequestException(MY_MESSAGE.PROCESSING_TRANS);
  }

  async getCurrentTransactionOfTicket(userID: string, ticketID: string) {
    const activeTransaction = await this.transactionModel.findOne({
      status: 'pending',
      creator: userID,
      ticket: ticketID,
      createdAt: { $regex: getDateString(), $options: 'i' },
    });
    return activeTransaction;
  }

  setTransactionFailedAfterTime(
    transactionID: string,
    ticketID: string,
    timeout: number,
  ) {
    setTimeout(async () => {
      const session = await this.connection.startSession();
      session.startTransaction();
      try {
        const transaction = await this.transactionModel.findOne(
          {
            _id: transactionID,
            status: 'pending',
          },
          null,
          { session },
        );
        if (transaction) {
          await this.transactionModel.findOneAndUpdate(
            { _id: transactionID, status: 'pending' },
            { status: 'failed', updatedAt: new Date().toLocaleString() },
            { session },
          );
          await this.ticketService.releaseToAvailable(ticketID, session);
        }
        await session.commitTransaction();
        await session.endSession();
      } catch (err) {
        Logger.error(err);
        Logger.warn('ERROR: Can not set transaction status for', transactionID);
        await session.abortTransaction();
        await session.endSession();
      }
    }, timeout);
  }

  async createMomoTransaction(transaction: Transaction, session: any) {
    const redirectUrl = `momo://?refId=miniapp.8TZb9qBBnwSFqmTap1vw.lunchticket&transaction=${transaction._id.toString()}&appId=miniapp.8TZb9qBBnwSFqmTap1vw.lunchticket&deeplink=true`;

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        ipnUrl: process.env.IPN_URL_RECEIVE,
        partnerName: 'Phiếu Cơm MoMo',
        storeId: 'MomoTestStore',
        requestType: 'captureWallet',
        orderId: transaction._id.toString(),
        orderInfo: 'Thanh toán phiếu cơm',
        amount: transaction.value,
        lang: 'vi',
        redirectUrl: `momo://?refId=${
          process.env.ENVIRONMENT == 'pro'
            ? 'miniapp.8TZb9qBBnwSFqmTap1vw.lunchticket'
            : 'miniapp.8TZb9qBBnwSFqmTap1vw.lunchticket'
        }&transaction=${transaction._id.toString()}&appId=miniapp.8TZb9qBBnwSFqmTap1vw.lunchticket&deeplink=true`,
        requestId: transaction._id.toString(),
        extraData: transaction.ticket,
        autoCapture: false,
        signature: '',
      };
      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&extraData=${data.extraData}&ipnUrl=${data.ipnUrl}&orderId=${data.orderId}&orderInfo=${data.orderInfo}&partnerCode=${data.partnerCode}&redirectUrl=${data.redirectUrl}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_TRANSACTION, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData = await response.json();
      Logger.log('Receive response', responseData);

      if (responseData['resultCode'] != 0) {
        await this.transactionModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            status: 'error',
            paymentInfo: responseData,
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        throw new BadRequestException(MY_MESSAGE.CREATE_TRANS_FAILED);
      }

      return [signature, responseData];
    } catch (err) {
      await this.ticketService.releaseToAvailable(
        String(transaction.ticket),
        session,
      );
      await this.transactionModel.findOneAndUpdate(
        { _id: transaction._id.toString() },
        { status: 'error', updatedAt: new Date().toLocaleString() },
        { session },
      );
      throw new BadRequestException(MY_MESSAGE.CREATE_TRANS_FAILED);
    }
  }

  async handleIpnNoti(data: MomoIpn) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const transactionID = data.orderId;
      const ticketID = data.extraData;

      //check signature later
      const transaction = await this.transactionModel
        .findOne({
          _id: transactionID,
          ticket: ticketID,
          value: data.amount,
        })
        .populate('creator');
      // .populate('receiver');

      if (transaction) {
        if (data.resultCode == 9000) {
          if (transaction.status == 'pending')
            this.sendConfirmToMomo(transaction);
          else this.sendCancelToMomo(transaction);
        } else if (data.resultCode != 0 && data.resultCode != 9000) {
          // transaction failed

          await this.transactionModel.findOneAndUpdate(
            { _id: transactionID },
            {
              status: 'failed',
              paymentInfo: { ...transaction.paymentInfo, ...data },
            },
            { session },
          );

          if (transaction.type == 'buy')
            await this.ticketService.releaseToAvailable(ticketID, session);
          else await this.ticketService.setCancel(ticketID, session);

          setTimeout(() => {
            this.eventsGateway.onTransaction({
              ticketID: ticketID,
              status: 'fail',
              transactionID: data.orderId,
            });
          }, 10000);

          await session.commitTransaction();
        }

        // how about data.resultCode == 0
      }
    } catch (err) {
      const ticketID = data.extraData;
      setTimeout(() => {
        this.eventsGateway.onTransaction({
          ticketID: ticketID,
          status: 'fail',
          transactionID: data.orderId,
        });
      }, 10000);
      Logger.log('Update transaction status failed', err);
    } finally {
      session.endSession();

      return { data: 'Success' };
    }
  }

  async sendMoneyToSeller(transactionID: string, session?: any) {
    const transaction = await this.transactionModel
      .findById(transactionID)
      .populate('receiver')
      .session(session);

    const controller = new AbortController();
    const setTimeoutId = setTimeout(() => controller.abort(), 30000);
    if (transaction.value - transaction.fee <= 0) return;
    try {
      const now = Date.now();
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        ipnUrl: process.env.IPN_URL_SEND,
        orderId: transaction._id.toString() + now,
        orderInfo: 'Nhận tiền thanh toán phiếu cơm',
        extraData: '',
        lang: 'vi',
        amount: transaction.value - transaction.fee,
        walletId: transaction.receiver.phone,
        requestId: transaction._id.toString() + now,
        requestType: 'disburseToWallet',
        disbursementMethod: '',
        signature: '',
      };

      data.extraData = genBase64(`{"id": "${transaction._id.toString()}"}`);

      const disbursementData = {
        partnerCode: process.env.PARTNER_CODE,
        partnerRefId: transaction._id.toString() + now,
        partnerTransId: transaction._id.toString() + now,
        amount: transaction.value - transaction.fee,
        description: 'Nhận tiền thanh toán phiếu cơm',
        walletId: transaction.receiver.phone,
      };

      data.disbursementMethod = genDisbursementMethod(
        JSON.stringify(disbursementData),
      );

      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&disbursementMethod=${data.disbursementMethod}&extraData=${data.extraData}&orderId=${data.orderId}&orderInfo=${data.orderInfo}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);
      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_REQUEST, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
        signal: controller.signal,
      });
      const responseData: any = await response.json();

      if (setTimeoutId) clearTimeout(setTimeoutId);

      if (responseData['resultCode'] != 0) {
        await this.transactionModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            statusRequestPay: 'failed',
            requestPayInfo: responseData,
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        Logger.error(
          `Request payment to wallet ${transaction.receiver.phone} failed`,
          responseData,
        );
      } else {
        await this.transactionModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            statusRequestPay: 'success',
            requestPayInfo: { ...transaction.requestPayInfo, ...responseData },
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        Logger.log(
          `Request payment to wallet ${transaction.receiver.phone} success`,
          responseData,
        );
      }
    } catch (err) {
      if (setTimeoutId) clearTimeout(setTimeoutId);

      await this.transactionModel.findOneAndUpdate(
        { _id: transaction._id.toString() },
        { statusRequestPay: 'error', updatedAt: new Date().toLocaleString() },
        { session },
      );
      Logger.error(
        `Request payment to wallet ${transaction.receiver.phone} error`,
        err,
      );
    } finally {
      if (setTimeoutId) clearTimeout(setTimeoutId);
    }
  }

  async handleIpnNotiRequestPay(data: MomoIpn) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const extraData = JSON.parse(decodeBase64(data.extraData));
      const transactionID = extraData.id;

      //check signature later

      const transaction = await this.transactionModel.findOne({
        _id: transactionID,
        statusRequestPay: 'pending',
      });

      if (transaction) {
        if (data.resultCode == 0) {
          await this.transactionModel.findOneAndUpdate(
            { _id: transactionID },
            {
              statusRequestPay: 'success',
              requestPayInfo: { ...transaction.requestPayInfo, ...data },
            },
            session,
          );

          await session.commitTransaction();
        } else {
          // transaction failed
          await this.transactionModel.findOneAndUpdate(
            { _id: transactionID },
            {
              statusRequestPay: 'failed',
              requestPayInfo: { ...transaction.requestPayInfo, ...data },
            },
            { session },
          );

          await session.commitTransaction();
        }
      }
    } catch (err) {
      Logger.error(`Update pay info for transaction error`, err);
    } finally {
      session.endSession();
      return { data: 'Success' };
    }
  }

  async checkStatus(userID: string, id: string) {
    const transaction = await this.transactionModel.findOne({
      _id: id,
      creator: userID,
    });

    if (!transaction) throw new BadRequestException(MY_MESSAGE.TRANS_OWN);

    return {
      data: {
        _id: transaction.id,
        status: transaction.status,
        momoTransactionId: transaction.paymentInfo?.transId,
        timestamp: transaction.timestamp,
        value: transaction.value,
        type: transaction.type,
        createdAt: transaction.createdAt,
        updatedAt: transaction.createdAt,
      },
      message: transaction.status,
    };
  }

  async sendConfirmToMomo(transaction: Transaction) {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        requestId: transaction._id.toString(),
        orderId: transaction._id.toString(),
        storeId: 'MomoTestStore',
        requestType: 'capture',
        amount: transaction.value,
        lang: 'vi',
        description: 'Thanh toán phiếu cơm',
        signature: '',
      };
      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&description=${data.description}&orderId=${data.orderId}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CONFIRM_PAYMENT, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData: any = await response.json();
      Logger.log('Receive response', responseData);

      const transactionID = responseData.orderId;
      const ticketID = String(transaction.ticket);

      if (responseData.resultCode == 0) {
        await this.transactionModel.findOneAndUpdate(
          { _id: transactionID, status: 'pending' },
          {
            status: 'success',
            statusRequestPay: 'pending',
            paymentInfo: { ...transaction.paymentInfo, ...responseData },
          },
          session,
        );

        if (transaction.type == 'buy') {
          await this.ticketService.setCaptured(
            ticketID,
            transaction.creator._id.toString(),
            session,
          );
        }

        await session.commitTransaction();
        // socket here

        setTimeout(() => {
          this.eventsGateway.onTransaction({
            ticketID: ticketID,
            status: 'success',
            transactionID: responseData.orderId,
          });
        }, 10000);
        // disable if donate === fee

        if (
          transaction.fee !== transaction.value &&
          transaction.type == 'buy'
        ) {
          this.sendMoneyToSeller(transaction._id.toString());
        } else if (transaction.type == 'request') {
          await this.ticketService.releaseToAvailable(ticketID, session, true);
        }
      }
    } catch (err) {
      Logger.error('Send confirm to momo failed', err);
      throw new BadRequestException(MY_MESSAGE.CONFIRM_FAILED);
    }
  }

  async sendCancelToMomo(transaction: Transaction) {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        requestId: transaction._id.toString(),
        orderId: transaction._id.toString(),
        storeId: 'MomoTestStore',
        requestType: 'cancel',
        amount: transaction.value,
        lang: 'vi',
        description: 'Thanh toán phiếu cơm',
        signature: '',
      };
      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&description=${data.description}&orderId=${data.orderId}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);

      data.signature = signature;

      const response = await fetch(process.env.URL_CONFIRM_PAYMENT, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData: any = await response.json();
      Logger.log('Receive response', responseData);

      const ticketID = String(transaction.ticket);

      if (responseData.resultCode == 0) {
        // socket here
        setTimeout(() => {
          this.eventsGateway.onTransaction({
            ticketID: ticketID,
            status: 'failed',
            transactionID: responseData.orderId,
          });
        }, 10000);
      }
    } catch (err) {
      Logger.error('Send confirm to momo failed', err);
      throw new BadRequestException(MY_MESSAGE.CONFIRM_FAILED);
    }
  }

  async transferMoneyOfTransaction(transactionID: string) {
    try {
      const transaction = await this.transactionModel
        .findById(transactionID)
        .populate('receiver')
        .populate('creator')
        .populate('ticket');

      if (!transaction)
        throw new NotFoundException(MY_MESSAGE.NOT_FOUND_TRANSACTION);
      if (transaction.statusRequestPay != 'success') {
        await this.sendMoneyToSeller(transactionID);
        return {
          message: 'Success',
        };
      } else return { message: 'Transaction has request pay success' };
    } catch (err) {
      throw err;
    }
  }

  async getDonate() {
    const sumThisMonth = await this.transactionModel.aggregate([
      { $match: { status: 'success' } },
      { $match: { statusRequestPay: 'success' } },
      {
        $group: {
          _id: {
            month: {
              $month: {
                date: {
                  $toDate: {
                    $toLong: '$timestamp',
                  },
                },
              },
            },
            year: {
              $year: {
                date: {
                  $toDate: {
                    $toLong: '$timestamp',
                  },
                },
              },
            },
          },
          sum: { $sum: '$fee' },
        },
      },
      {
        $match: {
          _id: {
            month: new Date().getMonth() + 1,
            year: new Date().getFullYear(),
          },
        },
      },
    ]);

    const donatesSell = await this.transactionModel
      .find({
        createdAt: { $regex: getDateString(), $options: 'i' },
        type: 'request',
        status: 'success',
        statusRequestPay: 'success',
      })
      .populate('receiver');

    const donatesBuy = await this.transactionModel
      .find({
        createdAt: { $regex: getDateString(), $options: 'i' },
        type: 'buy',
        status: 'success',
        statusRequestPay: 'success',
      })
      .populate('creator');

    const donates = [...donatesBuy, ...donatesSell].sort(
      (a, b) => a.timestamp - b.timestamp,
    );

    const result: DonateResponse = {
      thisMonthSum: sumThisMonth[0]?.sum,
      todayDonate: [
        ...donates.map((item) => ({
          transactionID: item.id,
          createdAt: item.createdAt,
          updatedAt: item.updatedAt,
          timestamp: item.timestamp,
          price: item.value,
          donate: item.fee,
          sellerName: formatText(item.receiver?.name ?? ''),
          status: item.status,
        })),
      ],
    };
    return { data: result };
  }

  async createRequestTransaction(userID: string, ticket: TicketDocument) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const createTime = new Date();

      const newTransactions = await this.transactionModel.create(
        [
          {
            creator: userID,
            receiver: '',
            ticket: ticket.id,
            status: 'pending', // add one more status for cancel,
            value: ticket.price,
            fee: ticket.donate,
            type: ticket.type == 'buy' ? 'request' : 'buy',
            paymentInfo: {}, // save momo response for payment
            statusRequestPay: 'pending',
            requestPayInfo: {},
            createdAt: createTime.toLocaleString(),
            updatedAt: createTime.toLocaleString(),
            timestamp: createTime.getTime(),
          },
        ],
        { session },
      );

      const newTransaction = newTransactions[0];

      const [signature, paymentInfo]: any = await this.createMomoTransaction(
        newTransaction,
        session,
      );
      newTransaction.paymentInfo = paymentInfo;
      newTransaction.signature = signature;
      await newTransaction.save({ session });

      await this.ticketService.setTransaction(
        ticket.id,
        newTransaction.id,
        session,
      );

      if (newTransaction.type == 'buy')
        await this.ticketService.sendSocketPending(ticket.id, session);

      this.setTransactionRequestFailedAfterTime(
        newTransaction.id,
        ticket.id,
        70000,
      );

      await session.commitTransaction();
      session.endSession();

      return {
        data: paymentInfo,
      };
    } catch (err) {
      Logger.error('Create transaction failed', err);
      //handle set ticket free here
      await session.abortTransaction();
      session.endSession();

      throw err;
    }
  }

  setTransactionRequestFailedAfterTime(
    transactionID: string,
    ticketID: string,
    timeout: number,
  ) {
    setTimeout(async () => {
      const session = await this.connection.startSession();
      session.startTransaction();
      try {
        const transaction = await this.transactionModel.findOne(
          {
            _id: transactionID,
            status: 'pending',
          },
          null,
          { session },
        );
        if (transaction) {
          await this.transactionModel.findOneAndUpdate(
            { _id: transactionID, status: 'pending' },
            { status: 'failed', updatedAt: new Date().toLocaleString() },
            { session },
          );
          await this.ticketService.setCancel(ticketID, session);
        }
        await session.commitTransaction();
        await session.endSession();
      } catch (err) {
        Logger.error(err);
        Logger.warn('ERROR: Can not set transaction status for', transactionID);
        await session.abortTransaction();
        await session.endSession();
      }
    }, timeout);
  }

  async updateReceiver(id: string, userID: string, session: any) {
    return this.transactionModel.findOneAndUpdate(
      { _id: id },
      {
        receiver: userID,
      },
      { session },
    );
  }

  async sendBackMoneyForBuyer(transactionID: string, session?: any) {
    const transaction = await this.transactionModel
      .findOne({ _id: transactionID, status: 'success' })
      .populate('creator')
      .session(session);

    const controller = new AbortController();
    const setTimeoutId = setTimeout(() => controller.abort(), 30000);
    if (transaction.value - transaction.fee <= 0) return;
    try {
      const now = Date.now();
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        ipnUrl: process.env.IPN_URL_SEND,
        orderId: transaction._id.toString() + now,
        orderInfo: 'Hoàn tiền thanh toán phiếu cơm',
        extraData: '',
        lang: 'vi',
        amount: transaction.value,
        walletId: transaction.creator.phone,
        requestId: transaction._id.toString() + now,
        requestType: 'disburseToWallet',
        disbursementMethod: '',
        signature: '',
      };

      data.extraData = genBase64(`{"id": "${transaction._id.toString()}"}`);

      const disbursementData = {
        partnerCode: process.env.PARTNER_CODE,
        partnerRefId: transaction._id.toString() + now,
        partnerTransId: transaction._id.toString() + now,
        amount: transaction.value,
        description: 'Hoàn tiền thanh toán phiếu cơm',
        walletId: transaction.creator.phone,
      };

      data.disbursementMethod = genDisbursementMethod(
        JSON.stringify(disbursementData),
      );

      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&disbursementMethod=${data.disbursementMethod}&extraData=${data.extraData}&orderId=${data.orderId}&orderInfo=${data.orderInfo}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);
      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_REQUEST, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
        signal: controller.signal,
      });
      const responseData: any = await response.json();

      if (setTimeoutId) clearTimeout(setTimeoutId);

      if (responseData['resultCode'] != 0) {
        await this.transactionModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            statusRequestPay: 'failed',
            requestPayInfo: responseData,
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        Logger.error(
          `Request refund to wallet ${transaction.creator.phone} failed`,
          responseData,
        );
      } else {
        await this.transactionModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            status: 'refund',
            statusRequestPay: 'success',
            requestPayInfo: { ...transaction.requestPayInfo, ...responseData },
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        Logger.log(
          `Request refund to wallet ${transaction.creator.phone} success`,
          responseData,
        );
      }
    } catch (err) {
      if (setTimeoutId) clearTimeout(setTimeoutId);

      await this.transactionModel.findOneAndUpdate(
        { _id: transaction._id.toString() },
        { statusRequestPay: 'error', updatedAt: new Date().toLocaleString() },
        { session },
      );
      Logger.error(
        `Request payment to wallet ${transaction.creator.phone} error`,
        err,
      );
    } finally {
      if (setTimeoutId) clearTimeout(setTimeoutId);
    }
  }

  async cashOut(phone: string, amount: number) {
    const controller = new AbortController();
    const setTimeoutId = setTimeout(() => controller.abort(), 30000);

    try {
      const now = Date.now();
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        ipnUrl: process.env.IPN_URL_SEND,
        orderId: 'cashout_' + phone + now,
        orderInfo: 'Rút tiền về ví',
        extraData: '',
        lang: 'vi',
        amount: amount,
        walletId: phone,
        requestId: 'cashout_' + phone + now,
        requestType: 'disburseToWallet',
        disbursementMethod: '',
        signature: '',
      };

      data.extraData = genBase64(`{"id": "${'cashout_' + phone + now}"}`);

      const disbursementData = {
        partnerCode: process.env.PARTNER_CODE,
        partnerRefId: 'cashout_' + phone + now,
        partnerTransId: 'cashout_' + phone + now,
        amount: amount,
        description: 'Rút tiền về ví',
        walletId: phone,
      };

      data.disbursementMethod = genDisbursementMethod(
        JSON.stringify(disbursementData),
      );

      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&disbursementMethod=${data.disbursementMethod}&extraData=${data.extraData}&orderId=${data.orderId}&orderInfo=${data.orderInfo}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);
      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_REQUEST, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
        signal: controller.signal,
      });
      const responseData = await response.json();

      if (setTimeoutId) clearTimeout(setTimeoutId);

      console.log(responseData);
      return {};
    } catch (err) {
      console.log(err);
      throw new BadGatewayException('Cash out failed');
    }
  }

  async distributionReward(phone: string, amount: number, historyId: string) {
    const controller = new AbortController();
    const setTimeoutId = setTimeout(() => controller.abort(), 30000);

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        ipnUrl: process.env.IPN_URL_SEND_DIS,
        orderId: historyId,
        orderInfo: 'Chia thưởng cho vận động viên tham gia thử thách NPNG',
        extraData: '',
        lang: 'vi',
        amount: amount,
        walletId: phone,
        requestId: historyId,
        requestType: 'disburseToWallet',
        disbursementMethod: '',
        signature: '',
      };

      data.extraData = genBase64(`{"id": "${historyId}"}`);

      const disbursementData = {
        partnerCode: process.env.PARTNER_CODE,
        partnerRefId: historyId,
        partnerTransId: historyId,
        amount: amount,
        description: 'Chia thưởng cho vận động viên tham gia thử thách NPNG',
        walletId: phone,
      };

      data.disbursementMethod = genDisbursementMethod(
        JSON.stringify(disbursementData),
      );

      const rawSignature = `accessKey=${process.env.ACCESS_KEY}&amount=${data.amount}&disbursementMethod=${data.disbursementMethod}&extraData=${data.extraData}&orderId=${data.orderId}&orderInfo=${data.orderInfo}&partnerCode=${data.partnerCode}&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = genSignature(rawSignature);
      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_REQUEST, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
        signal: controller.signal,
      });
      const responseData = await response.json();

      if (setTimeoutId) clearTimeout(setTimeoutId);

      // console.log(responseData);
      return responseData;
    } catch (err) {
      console.log(err);
      throw new BadGatewayException('Cash out failed');
    }
  }

  async fetchAll(filter) {
    console.log('filter: ', filter);
    try {
      const buyTransaction = await this.transactionModel
        .find({
          $expr: {
            $gte: [
              {
                $toDate: '$createdAt',
              },
              new Date(filter.fromDate),
            ],
          },
          status: 'success',
          requestPayStatus: 'success',
          type: 'buy',
        })
        .populate('creator')
        .populate('ticket')
        .populate('receiver')
        .exec();
      const databuyTransaction = buyTransaction?.map((el) => {
        const data = {
          _id: el?._id,
          createdAt: el?.createdAt,
          creator: {
            phone: el?.creator?.phone,
            name: el?.creator?.name,
          },
          type: el?.type,
          ticket: {
            code: el?.ticket?.code,
            price: el?.ticket?.price,
            donate: el?.ticket?.donate,
          },
          receiver: {
            phone: el?.receiver?.phone,
            name: el?.receiver?.name,
          },
          paymentInfo: {
            status: el?.status,
            amount: el?.paymentInfo?.amount,
            message: el?.paymentInfo?.message,
          },
          requestPayInfo: {
            ...el.requestPayInfo,
            amount: el?.requestPayInfo?.amount,
            message: el?.requestPayInfo?.message,
            status: el?.statusRequestPay,
          },
        };
        return data;
      });

      const requestPendingTransaction = await this.transactionModel
        .find({
          $expr: {
            $gte: [
              {
                $toDate: '$createdAt',
              },
              new Date(filter.fromDate),
            ],
          },
          status: 'success',
          statusRequestPay: 'pending',
          type: 'request',
        })
        .populate('creator')
        .populate('ticket')
        .exec();

      const requestSuccessTransaction = await this.transactionModel
        .find({
          $expr: {
            $gte: [
              {
                $toDate: '$createdAt',
              },
              new Date(filter.fromDate),
            ],
          },
          status: 'success',
          statusRequestPay: 'success',
          type: 'request',
        })
        .populate('creator')
        .populate('ticket')
        .populate('receiver')
        .exec();

      const requestTransaction = [
        ...requestPendingTransaction,
        ...requestSuccessTransaction,
      ];

      const datarequestTransaction = requestTransaction?.map((el) => {
        const data = {
          _id: el?._id,
          createdAt: el?.createdAt,
          creator: {
            phone: el?.creator?.phone,
            name: el?.creator?.name,
          },
          type: el?.type,
          ticket: {
            code: el?.ticket?.code,
            price: el?.ticket?.price,
            donate: el?.ticket?.donate,
          },
          receiver: {
            phone: el?.receiver?.phone || '',
            name: el?.receiver?.name || '',
          },
          paymentInfo: {
            status: el?.status,
            amount: el?.paymentInfo?.amount,
            message: el?.paymentInfo?.message,
          },
          requestPayInfo: {
            ...el.requestPayInfo,
            amount: el?.requestPayInfo?.amount,
            message: el?.requestPayInfo?.message,
            status: el?.statusRequestPay,
          },
        };
        return data;
      });

      const transactions = [...databuyTransaction, ...datarequestTransaction]
        .sort((a: any, b: any) => a.createdAt - b.createdAt)
        ?.reverse();

      return transactions;
    } catch (err) {
      console.log(err);
      throw new BadGatewayException('');
    }
  }

  async getDataTransactionYesterday() {
    const donatesSell = await this.transactionModel.find({
      createdAt: { $regex: getYesterdayString(), $options: 'i' },
      type: 'request',
      status: 'success',
      statusRequestPay: 'success',
    });

    const donatesBuy = await this.transactionModel.find({
      createdAt: { $regex: getYesterdayString(), $options: 'i' },
      type: 'buy',
      status: 'success',
      statusRequestPay: 'success',
    });

    const donates = [...donatesBuy, ...donatesSell].sort(
      (a, b) => a.timestamp - b.timestamp,
    );

    const dataTransactionYesterday = [
      ...donates.map((item) => ({
        transactionID: item.id,
        createdAt: item.createdAt,
        updatedAt: item.updatedAt,
        timestamp: item.timestamp,
        price: item.value,
        donate: item.fee,
        sellerName: formatText(item.receiver?.name ?? ''),
        status: item.status,
      })),
    ];

    const result = {
      donate: [...donates].reduce((a, b) => a + b.fee, 0),
      count: dataTransactionYesterday.length,
      total: [...donates].reduce((a, b) => a + b.value, 0),
    };
    return { data: result };
  }

  async fetchOverviewTransactionInMonth() {
    try {
      const sumThisMonth = await this.transactionModel.aggregate([
        { $match: { status: 'success' } },
        { $match: { statusRequestPay: 'success' } },
        {
          $group: {
            _id: {
              month: {
                $month: {
                  date: {
                    $toDate: {
                      $toLong: '$timestamp',
                    },
                  },
                },
              },
              year: {
                $year: {
                  date: {
                    $toDate: {
                      $toLong: '$timestamp',
                    },
                  },
                },
              },
            },
            sum: { $sum: '$fee' },
            count: { $count: {} },
            total: { $sum: '$value' },
          },
        },
        {
          $match: {
            _id: {
              month: new Date().getMonth() + 1,
              year: new Date().getFullYear(),
            },
          },
        },
      ]);

      const sumLastMonth = await this.transactionModel.aggregate([
        { $match: { status: 'success' } },
        { $match: { statusRequestPay: 'success' } },
        {
          $group: {
            _id: {
              month: {
                $month: {
                  date: {
                    $toDate: {
                      $toLong: '$timestamp',
                    },
                  },
                },
              },
              year: {
                $year: {
                  date: {
                    $toDate: {
                      $toLong: '$timestamp',
                    },
                  },
                },
              },
            },
            sum: { $sum: '$fee' },
            count: { $count: {} },
            total: { $sum: '$value' },
          },
        },
        {
          $match: {
            _id: {
              month: new Date().getMonth(),
              year: new Date().getFullYear(),
            },
          },
        },
      ]);

      const donatesSell = await this.transactionModel.find({
        createdAt: { $regex: getDateString(), $options: 'i' },
        type: 'request',
        status: 'success',
        statusRequestPay: 'success',
      });

      const donatesBuy = await this.transactionModel.find({
        createdAt: { $regex: getDateString(), $options: 'i' },
        type: 'buy',
        status: 'success',
        statusRequestPay: 'success',
      });

      const donates = [...donatesBuy, ...donatesSell].sort(
        (a, b) => a.timestamp - b.timestamp,
      );

      const dataTransactionToday = [
        ...donates.map((item) => ({
          transactionID: item.id,
          createdAt: item.createdAt,
          updatedAt: item.updatedAt,
          timestamp: item.timestamp,
          price: item.value,
          donate: item.fee,
          sellerName: formatText(item.receiver?.name ?? ''),
          status: item.status,
        })),
      ];

      const dataTransactionYesterday = await this.getDataTransactionYesterday();

      const result = {
        thisMonthSum: {
          donate: sumThisMonth[0]?.sum ?? 0,
          count: sumThisMonth[0]?.count ?? 0,
          total: sumThisMonth[0]?.total ?? 0,
        },
        lastMonthSum: {
          donate: sumLastMonth[0]?.sum ?? 0,
          count: sumLastMonth[0]?.count ?? 0,
          total: sumLastMonth[0]?.total ?? 0,
        },
        todaySum: {
          donate: [...donates].reduce((a, b) => a + b.fee, 0),
          count: dataTransactionToday.length,
          total: [...donates].reduce((a, b) => a + b.value, 0),
        },
        yesterdaySum: dataTransactionYesterday.data,
      };
      return { data: result };
    } catch (err) {
      console.log(err);
      throw new BadGatewayException('');
    }
  }

  async fetchTransactionLastWeek() {
    try {
      const start = new Date();
      start.setDate(start.getDate() - 7);
      const data = await this.transactionModel.aggregate([
        {
          $match: {
            timestamp: {
              $gte: start.getTime(),
              $lte: new Date().getTime(),
            },
          },
        },
        { $match: { status: 'success' } },
        { $match: { statusRequestPay: 'success' } },
        {
          $group: {
            _id: {
              $dateToString: {
                format: '%d-%m-%Y',
                date: {
                  $toDate: {
                    $subtract: [
                      '$timestamp',
                      { $mod: ['$timestamp', 86400000] },
                    ],
                  },
                },
              },
            },
            totalValue: { $sum: '$value' },
            count: { $sum: 1 },
          },
        },
        {
          $sort: {
            _id: 1,
          },
        },
      ]);
      // const data = await this.transactionModel
      //   .find({
      //     timestamp: {
      //       $gte: startDate.getTime(),
      //       $lte: new Date().getTime(),
      //     },
      //     status: 'success',
      //     statusRequestPay: 'success',
      //   })
      //   .lean()
      //   .exec();
      // );

      return { data: data };
    } catch (error) {
      console.log(error);
      throw new BadGatewayException('');
    }
  }

  async fetchTransactionInYear() {
    try {
      const promise = [];
      const currentMonth = new Date().getMonth() + 1;
      for (let i = 0; i < currentMonth; i++) {
        promise.push(
          this.transactionModel.aggregate([
            { $match: { status: 'success' } },
            { $match: { statusRequestPay: 'success' } },
            {
              $group: {
                _id: {
                  month: {
                    $month: {
                      date: {
                        $toDate: {
                          $toLong: '$timestamp',
                        },
                      },
                    },
                  },
                  year: {
                    $year: {
                      date: {
                        $toDate: {
                          $toLong: '$timestamp',
                        },
                      },
                    },
                  },
                },
                sum: { $sum: '$fee' },
                count: { $count: {} },
                total: { $sum: '$value' },
              },
            },
            {
              $match: { _id: { month: i + 1, year: new Date().getFullYear() } },
            },
          ]),
        );
      }

      const data = await Promise.all(promise);

      return { data: data?.map((item) => item[0]) ?? [] };
    } catch (error) {
      console.log(error);
      throw new BadGatewayException('');
    }
  }
}
