import { Exclude } from 'class-transformer';
import { TransactionDocument } from 'src/schema/transaction.schema';

export class PublicTransaction {
  _id: any;
  status: 'pending' | 'failed' | 'success' | 'error';
  createdAt: string;
  updatedAt: string;
  type: string;

  @Exclude({ toPlainOnly: true })
  code: string;

  @Exclude({ toPlainOnly: true })
  ticket: any;
  creator: any;

  constructor(partial: any) {
    Object.assign(this, partial);
    this._id = partial._id.toString();
  }
}
