export class MomoIpn {
  partnerCode: string;
  orderId: string;
  requestId: string;
  amount: number;
  orderInfo: string;
  orderType: string;
  transId: any;
  resultCode: number;
  message: string;
  payType: any;
  responseTime: any;
  extraData: string;
  signature: string;
}
