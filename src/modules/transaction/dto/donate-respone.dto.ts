export class DonateResponse {
  thisMonthSum: number;
  todayDonate: Donate[];
}

class Donate {
  transactionID: any;
  createdAt: string;
  updatedAt: string;
  price: number;
  donate: number;
  timestamp: number;
  sellerName: string;
  status: string;
}
