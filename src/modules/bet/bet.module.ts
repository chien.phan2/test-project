import { forwardRef, Module } from '@nestjs/common';
import { BetService } from './bet.service';
import { BetController } from './bet.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Bet, BetSchema } from 'src/schema/bet.schema';
import { EventsModule } from '../../events/events.module';
import { UserModule } from '../user/user.module';
import { TransactionModule } from '../transaction/transaction.module';
import { BetTransactionModule } from '../bet-transaction/bet-transaction.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Bet.name, schema: BetSchema }]),
    EventsModule,
    UserModule,
    BetTransactionModule,
    forwardRef(() => TransactionModule),
  ],
  controllers: [BetController],
  providers: [BetService],
  exports: [BetService],
})
export class BetModule {}
