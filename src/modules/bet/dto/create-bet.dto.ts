import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateBetDto {
  @IsString()
  @ApiProperty()
  playerCode: string;

  @IsString()
  @ApiProperty()
  match: string;
}

export class UpdateBetDto {
  @IsString()
  @ApiProperty()
  status: string;
}
