/* eslint-disable prettier/prettier */
import {
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Bet, BetDocument } from 'src/schema/bet.schema';
import getDateString from 'src/utils/getDateString';
import { BetTransactionService } from '../bet-transaction/bet-transaction.service';
import { UserService } from '../user/user.service';
import { CreateBetDto, UpdateBetDto } from './dto/create-bet.dto';

@Injectable()
export class BetService {
  token = '';

  constructor(
    @InjectModel(Bet.name) private betModel: Model<BetDocument>,
    private readonly userService: UserService,
    @Inject(forwardRef(() => BetTransactionService))
    private readonly betTransactionService: BetTransactionService,
  ) {}

  async create(userID: string, createPayload: CreateBetDto) {
    try {
      const user = await this.userService.getUserInfo(userID);

      // not allow user to bet twice on the same match
      // const isBetted = await this.betModel.findOne({
      //   user: userID,
      //   match: createPayload.match,
      // });
      // if (isBetted) {
      //   throw new Error('You have betted on this match');
      // }

      const newEntity = new this.betModel({
        ...createPayload,
        user: user._id.toString(),
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString(),
        timestamp: Date.now(),
      });
      const data = await (await newEntity.save()).populate('user match');

      const transactionData =
        await this.betTransactionService.createBetTransaction(userID, data);

      return transactionData;
    } catch (err) {
      throw err;
    }
  }

  async setBetTransaction(id: string, transaction: string, session: any) {
    await this.betModel.findOneAndUpdate(
      { _id: id },
      { transaction: transaction },
      {
        session,
      },
    );
  }

  async findAll() {
    try {
      const data = await this.betModel
        .find({
          createdAt: { $regex: getDateString(), $options: 'i' },
        })
        .populate('user match')
        .exec();

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findByMatch(matchID: string) {
    try {
      const data = await this.betModel
        .find({
          createdAt: { $regex: getDateString(), $options: 'i' },
          match: matchID,
        })
        .populate('user match')
        .exec();

      return data;
    } catch (error) {
      throw error;
    }
  }

  async findMy(userID: string) {
    const data = await this.betModel
      .find({
        user: userID,
      })
      .populate('user match')
      .exec();

    return data;
  }

  async findOne(id: string) {
    const data = await this.betModel.findById(id).populate('user match');
    if (!data) throw new NotFoundException('not found');
    return data;
  }

  async setTransaction(id: string, transaction: string, session: any) {
    await this.betModel.findOneAndUpdate(
      { _id: id },
      { transaction: transaction },
      {
        session,
      },
    );
  }

  async findOneMy(id: string, userID: string) {
    const data = await this.betModel
      .findOne({
        user: userID,
        _id: id,
      })
      .populate('user match');

    if (!data) throw new NotFoundException('not found');

    return data;
  }

  async update(id: string, updatePayload: UpdateBetDto) {
    const data = await this.betModel
      .findByIdAndUpdate(id, updatePayload)
      .populate('user match');

    if (!data) throw new NotFoundException('not found');

    return data;
  }
}
