import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  UseInterceptors,
  Req,
  UseGuards,
} from '@nestjs/common';
import { BetService } from './bet.service';
import { CreateBetDto } from './dto/create-bet.dto';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { Request } from 'express';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { ApiBearerAuth } from '@nestjs/swagger';

@Controller('bet')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class BetController {
  constructor(private readonly betService: BetService) {}

  @Post()
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  create(@Body() createBetDto: CreateBetDto, @Req() req: Request) {
    const userID = req.userID;
    return this.betService.create(userID, createBetDto);
  }

  @Get()
  findAll() {
    return this.betService.findAll();
  }

  @Get('/match/:id')
  findByMatch(@Param('id') id: string) {
    return this.betService.findByMatch(id);
  }

  @Get('/my-bets/:id')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  findOneMy(@Param('id') id: string, @Req() req: Request) {
    const userID = req.userID;
    return this.betService.findOneMy(id, userID);
  }

  @Get('/my-bets')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  findMy(@Req() req: Request) {
    const userID = req.userID;
    return this.betService.findMy(userID);
  }

  @Get(':id')
  @ApiBearerAuth('JWT-auth')
  findOne(@Param('id') id: string) {
    return this.betService.findOne(id);
  }
}
