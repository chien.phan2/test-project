import { Test, TestingModule } from '@nestjs/testing';
import { MissionHistoryController } from './mission-history.controller';

describe('MissionHistoryController', () => {
  let controller: MissionHistoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MissionHistoryController],
    }).compile();

    controller = module.get<MissionHistoryController>(MissionHistoryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
