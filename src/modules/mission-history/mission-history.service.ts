import {
  BadRequestException,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  MissionHistory,
  MissionHistoryDocument,
} from 'src/schema/missionHistory.schema';
import { Model } from 'mongoose';
import { MissionService } from '../mission/mission.service';
import { RunnerService } from '../runner/runner.service';
import { SyncMissionHistoryDto } from './dto/sync-mission-history.dto';
import { Mission } from 'src/schema/mission.schema';

@Injectable()
export class MissionHistoryService {
  constructor(
    @InjectModel(MissionHistory.name)
    private readonly missionHistoryModel: Model<MissionHistoryDocument>,

    private readonly missionService: MissionService,

    @Inject(forwardRef(() => RunnerService))
    private readonly runnerService: RunnerService,
  ) {}

  async createMissionHistoryForAllRunner(payload: SyncMissionHistoryDto) {
    try {
      const { missionId } = payload;
      const missionInfo = await this.missionService.getMissionDetail(missionId);
      if (!missionInfo) {
        throw new Error('Nhiệm vụ không tồn tại');
      }
      const listRunner = await this.runnerService.getAllUser();
      const listIdsAllRunner = listRunner.map((item) => item._id?.toString());
      const createPromises = listIdsAllRunner.map(async (runnerId) => {
        try {
          return await this.missionHistoryModel.create({
            runner: runnerId,
            mission: missionInfo?._id?.toString(),
          });
        } catch (error) {
          console.log('createPromiseMission', error);
        }
      });
      const updateResults = await Promise.all(createPromises);
      return {
        message: 'Tạo lịch sử nhiệm vụ thành công',
      };
    } catch (error) {
      throw error;
    }
  }

  async updateMissionHistory(runnerId: string, mission: Mission) {
    try {
      const missionHistoryInfo = await this.missionHistoryModel.findOne({
        runner: runnerId,
        mission: mission?._id?.toString(),
      });
      if (!missionHistoryInfo) {
        throw new Error('Lịch sử nhiệm vụ không tồn tại');
      }

      const currTaskCount = missionHistoryInfo?.taskCount || 0;
      const newTaskCount = currTaskCount + 1;

      let isCompleted = false;
      if (newTaskCount > mission?.taskRequired) {
        throw new BadRequestException('Bạn đã hoàn thành nhiệm vụ này rồi');
      }
      if (newTaskCount === mission?.taskRequired) {
        isCompleted = true;
      }

      await missionHistoryInfo.updateOne(
        {
          isCompleted,
          taskCount: newTaskCount,
        },
        {
          new: true,
        },
      );

      return missionHistoryInfo;
    } catch (error) {
      console.log('updateMissionHistory', error);
      throw error;
    }
  }

  async getMyMissions(runnerId: string) {
    try {
      const listMissionHistory = await this.missionHistoryModel
        .find({
          runner: runnerId,
        })
        .populate({
          path: 'mission',
          model: Mission.name,
        });
      return listMissionHistory;
    } catch (error) {
      throw error;
    }
  }
}
