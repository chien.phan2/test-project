import {
  Body,
  Controller,
  Get,
  Post,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { SyncMissionHistoryDto } from './dto/sync-mission-history.dto';
import { MissionHistoryService } from './mission-history.service';

@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
@ApiTags('Challenge')
@Controller('challenge/mission-history')
export class MissionHistoryController {
  constructor(private readonly missionHistoryService: MissionHistoryService) {}

  @Post('/sync-mission-history')
  syncMissionHistory(@Body() payload: SyncMissionHistoryDto) {
    return this.missionHistoryService.createMissionHistoryForAllRunner(payload);
  }

  @Get('/my-missions')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  getMyMissions(@Req() req: Request) {
    const userID = req.userID;
    return this.missionHistoryService.getMyMissions(userID);
  }
}
