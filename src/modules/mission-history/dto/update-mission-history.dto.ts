import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsOptional } from 'class-validator';

export class UpdateMissionHistoryDto {
  @ApiProperty()
  @IsOptional()
  runner: string;

  @ApiProperty()
  @IsOptional()
  mission: string;

  @ApiProperty()
  @IsOptional()
  challenge: string;

  @ApiProperty({ default: 1 })
  @IsOptional()
  taskCount: number;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  isCompleted: boolean;

  @ApiProperty()
  @IsOptional()
  completedAt: Date;
}
