import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateMissionHistoryDto {
  @ApiProperty()
  @IsNotEmpty()
  runner: string;

  @ApiProperty()
  @IsNotEmpty()
  mission: string;

  @ApiProperty()
  @IsOptional()
  challenge: string;
}
