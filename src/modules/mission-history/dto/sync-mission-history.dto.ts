import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class SyncMissionHistoryDto {
  @ApiProperty()
  @IsNotEmpty()
  missionId: string;
}
