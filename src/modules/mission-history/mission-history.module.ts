import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  MissionHistory,
  MissionHistorySchema,
} from 'src/schema/missionHistory.schema';
import { MissionHistoryController } from './mission-history.controller';
import { MissionHistoryService } from './mission-history.service';
import { MissionModule } from '../mission/mission.module';
import { RunnerModule } from '../runner/runner.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: MissionHistory.name, schema: MissionHistorySchema },
    ]),
    MissionModule,
    forwardRef(() => RunnerModule),
  ],
  controllers: [MissionHistoryController],
  exports: [MissionHistoryService],
  providers: [MissionHistoryService],
})
export class MissionHistoryModule {}
