import { Module, forwardRef } from '@nestjs/common';
import { EnergyService } from './energy.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Energy, EnergySchema } from 'src/schema/energy.schema';
import { RunnerModule } from '../runner/runner.module';
import { EnergyTransactionModule } from '../energy-transaction/energy-transaction.module';
import { EnergyController } from './energy.controller';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Energy.name, schema: EnergySchema }]),
    forwardRef(() => RunnerModule),
    forwardRef(() => EnergyTransactionModule),
  ],
  providers: [EnergyService],
  exports: [EnergyService],
  controllers: [EnergyController],
})
export class EnergyModule {}
