import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';

export class CreateEnergyDto {
  @ApiProperty()
  @IsNotEmpty()
  productName: string;

  @ApiProperty()
  @IsOptional()
  productImage: string;

  @ApiProperty()
  @IsOptional()
  productDescription: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @IsPositive()
  productQuantity: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  @IsPositive()
  productPrice: number;

  @ApiProperty({ default: 'energy' })
  @IsNotEmpty()
  @IsString()
  type: string;

  @ApiProperty({ default: '' })
  @IsOptional()
  key: string;
}
