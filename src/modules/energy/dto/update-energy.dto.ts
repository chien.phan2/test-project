import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsPositive } from 'class-validator';

export class UpdateEnergyDto {
  @ApiProperty()
  @IsOptional()
  productName: string;

  @ApiProperty()
  @IsOptional()
  productImage: string;

  @ApiProperty()
  @IsOptional()
  productDescription: string;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  @IsPositive()
  productQuantity: number;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  @IsPositive()
  productPrice: number;

  @ApiProperty()
  @IsOptional()
  type: string;

  @ApiProperty()
  @IsOptional()
  key: string;

  @ApiProperty({ default: new Date().toLocaleString() })
  @IsOptional()
  updatedAt: string;
}
