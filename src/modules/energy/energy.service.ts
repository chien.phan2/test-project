import {
  BadRequestException,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Energy, EnergyDocument } from 'src/schema/energy.schema';
import { CreateEnergyDto } from './dto/create-energy.dto';
import { RequestEnergyDto } from './dto/request-energy.dto';
import { RunnerService } from '../runner/runner.service';
import { EnergyTransactionService } from '../energy-transaction/energy-transaction.service';

@Injectable()
export class EnergyService {
  constructor(
    @InjectModel(Energy.name)
    private readonly energyModel: Model<EnergyDocument>,

    @Inject(forwardRef(() => RunnerService))
    private readonly runnerservice: RunnerService,

    @Inject(forwardRef(() => EnergyTransactionService))
    private readonly energyTransactionService: EnergyTransactionService,
  ) {}

  async createProductEnergy(createEnergyDto: CreateEnergyDto) {
    try {
      const newEnergy = await this.energyModel.create({
        ...createEnergyDto,
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString(),
      });
      await newEnergy.save();
      return {
        data: newEnergy.toJSON(),
        message: 'Create energy successfully',
      };
    } catch (error) {
      console.log('createEnergy: ', error);
      throw error;
    }
  }

  async updateProductEnergy(id: string, data: any) {
    try {
      const energy = await this.energyModel.findByIdAndUpdate(
        {
          _id: id,
        },
        { ...data },
        { returnOriginal: false },
      );
      return {
        data: energy.toJSON(),
        message: 'Update energy successfully',
      };
    } catch (error) {
      console.log('updateEnergy: ', error);
      throw error;
    }
  }

  async deleteProductEnergy(id: string) {
    try {
      const energy = await this.energyModel.findByIdAndDelete({
        _id: id,
      });
      return {
        data: energy.toJSON(),
        message: 'Delete energy successfully',
      };
    } catch (error) {
      console.log('deleteEnergy: ', error);
      throw error;
    }
  }

  async getProductEnergy() {
    try {
      // const energyProducts = await this.energyModel.find({
      //   $or: [{ isHidden: { $exists: false } }, { isHidden: false }],
      // });
      const energyProducts = await this.energyModel.find({
        $or: [{ type: 'energy' }, { type: 'ticket' }],
      });
      return energyProducts;
    } catch (error) {
      console.log('getEnergy: ', error);
      throw error;
    }
  }

  async getProductSponsor() {
    try {
      const energyProducts = await this.energyModel
        .find({
          type: 'sponsor',
        })
        .sort({
          productPrice: -1,
        });
      return energyProducts;
    } catch (error) {
      console.log('getSponsor: ', error);
      throw error;
    }
  }

  async getAllProduct() {
    try {
      const energyProducts = await this.energyModel.find();
      return energyProducts;
    } catch (error) {
      console.log('getAllProduct: ', error);
      throw error;
    }
  }

  async getProductByKey(key: string) {
    try {
      const product = await this.energyModel.findOne({
        key: key,
      });
      if (!product) {
        throw new BadRequestException('Energy Product not found');
      }
      return product;
    } catch (error) {
      console.log('getProductByKey: ', error);
      throw error;
    }
  }

  async getProductEnergyById(id: string) {
    try {
      const energyProduct = await this.energyModel.findById(id);
      if (!energyProduct) {
        throw new BadRequestException('Energy Product not found');
      }
      return energyProduct;
    } catch (error) {
      console.log('getEnergyById: ', error);
      throw error;
    }
  }

  async requestBuyEnergy(userID: string, requestPayload: RequestEnergyDto) {
    try {
      if (requestPayload?.quantity < 0) {
        throw new BadRequestException('Quantity must be greater than 0');
      }
      const user = await this.runnerservice.findRunnerById(userID);
      const energyProduct = await this.energyModel
        .findById(requestPayload?.productId)
        .lean();

      const data = {
        ...requestPayload,
        product: {
          ...energyProduct,
        },
        value:
          (energyProduct?.productPrice || 0) * (requestPayload?.quantity || 0),
        creator: user?._id.toString(),
      };

      const transactionData =
        await this.energyTransactionService.createRequestTransaction(
          user?._id.toString(),
          data,
          'buy',
        );

      return transactionData;
    } catch (error) {
      console.log('requestBuyEnergy: ', error);
      throw error;
    }
  }
}
