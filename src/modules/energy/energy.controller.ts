import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { EnergyService } from './energy.service';
import { CreateEnergyDto } from './dto/create-energy.dto';
import { UpdateEnergyDto } from './dto/update-energy.dto';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { RequestEnergyDto } from './dto/request-energy.dto';
import { Request } from 'express';

@ApiTags('Challenge')
@Controller('challenge')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class EnergyController {
  constructor(private readonly energyService: EnergyService) {}

  @Post('create-product-energy')
  createProductEnergy(@Body() createEnergyDto: CreateEnergyDto) {
    return this.energyService.createProductEnergy(createEnergyDto);
  }

  @Get('get-product-energy')
  getProductEnergy() {
    return this.energyService.getProductEnergy();
  }

  @Get('get-product-sponsor')
  getProductSponsor() {
    return this.energyService.getProductSponsor();
  }

  @Get('get-all-product')
  getAllProduct() {
    return this.energyService.getAllProduct();
  }

  @Patch('update-product-energy/:id')
  updateProductEnergy(
    @Body() updateEnergyDto: UpdateEnergyDto,
    @Param('id') id: string,
  ) {
    return this.energyService.updateProductEnergy(id, updateEnergyDto);
  }

  @Delete('delete-product-energy/:id')
  deleteProductEnergy(@Param('id') id: string) {
    return this.energyService.deleteProductEnergy(id);
  }

  @Post('buy-energy')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  buyEnergy(@Body() requestEnergyDto: RequestEnergyDto, @Req() req: Request) {
    const userID = req.userID;
    return this.energyService.requestBuyEnergy(userID, requestEnergyDto);
  }
}
