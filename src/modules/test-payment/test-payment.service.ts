import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import fetch from 'node-fetch';
import { TestPayment, TestPaymentDocument } from 'src/schema/test-payment';
import { v4 as uuidv4 } from 'uuid';
import { MomoIpn } from '../transaction/dto/momo-ipn.dto';
import genBase64 from 'src/utils/genBase64String';
import * as hmacSHA256 from 'crypto-js/hmac-sha256';
import { CreateTestPaymentDto } from './dto/create-test-payment.dto';

@Injectable()
export class TestPaymentService {
  constructor(
    @InjectModel(TestPayment.name)
    private testPaymentModel: Model<TestPaymentDocument>,
    @InjectConnection() private connection: Connection,
  ) {}

  async createTestMomoTransaction(
    transaction: TestPayment,
    payload: CreateTestPaymentDto,
    session: any,
  ) {
    const refId = payload?.isDevTool
      ? 'dev_tool&tripId=GKX2SYA&tranxId=mipay_4179'
      : 'minitemplate.base.miniappchecklist';
    try {
      const data = {
        partnerCode: 'MOMOKYLZ20220427',
        ipnUrl: process.env.IPN_URL_TEST_PAYMENT_RECEIVE,
        partnerName: 'Check List',
        storeId: 'MomoTestStore',
        requestType: 'captureWallet',
        orderId: transaction._id.toString(),
        orderInfo: 'Thanh toán cho Check List',
        amount: 1000,
        lang: 'vi',
        redirectUrl: `momo://?refId=${refId}&transaction=${transaction._id.toString()}&note=${
          payload?.note
        }&appId=minitemplate.base.miniappchecklist&deeplink=true`,
        requestId: transaction._id.toString(),
        extraData: '',
        autoCapture: false,
        signature: '',
      };
      data.extraData = genBase64(`{"id": "${transaction._id.toString()}"}`);

      const rawSignature = `accessKey=${'g5UluFSOSdGIaR2o'}&amount=${
        data.amount
      }&extraData=${data.extraData}&ipnUrl=${data.ipnUrl}&orderId=${
        data.orderId
      }&orderInfo=${data.orderInfo}&partnerCode=${
        data.partnerCode
      }&redirectUrl=${data.redirectUrl}&requestId=${
        data.requestId
      }&requestType=${data.requestType}`;
      const signature = hmacSHA256(
        rawSignature,
        'joLVlb82fwMzYiyNXDEskdubXSah6IQ6',
      ).toString();

      data.signature = signature;

      const response = await fetch(process.env.URL_CREATE_TRANSACTION_TEST, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData = await response.json();
      console.log('===> responseData:', responseData);

      if (responseData['resultCode'] != 0) {
        await this.testPaymentModel.findOneAndUpdate(
          { _id: transaction._id.toString() },
          {
            status: 'error',
            paymentInfo: responseData,
            updatedAt: new Date().toLocaleString(),
          },
          { session },
        );
        throw new BadRequestException('Lỗi tạo giao dịch');
      }

      return [signature, responseData];
    } catch (err) {
      console.log('===== err:', err);
      await this.testPaymentModel.findOneAndUpdate(
        { _id: transaction._id.toString() },
        { status: 'error', updatedAt: new Date().toLocaleString() },
        { session },
      );
      throw new BadRequestException('Lỗi tạo giao dịch');
    }
  }

  async createTestRequestPayment(payload: CreateTestPaymentDto) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const createTime = new Date();
      const creator = uuidv4();
      const newTransactions = await this.testPaymentModel.create(
        [
          {
            creator: creator,
            status: 'pending',
            value: 1000,
            paymentInfo: {},
            statusRequestPay: 'pending',
            requestPayInfo: {},
            createdAt: createTime.toLocaleString(),
            updatedAt: createTime.toLocaleString(),
            timestamp: createTime.getTime(),
          },
        ],
        { session },
      );
      const newTransaction = newTransactions[0];

      const [signature, paymentInfo] = await this.createTestMomoTransaction(
        newTransaction,
        payload,
        session,
      );

      newTransaction.paymentInfo = paymentInfo;
      newTransaction.signature = signature;
      await newTransaction.save({ session });

      await session.commitTransaction();
      session.endSession();

      return {
        data: paymentInfo,
      };
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      console.log(error);
      throw error;
    }
  }

  async sendConfirmToMomo(transaction: TestPayment) {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        requestId: transaction._id.toString(),
        orderId: transaction._id.toString(),
        storeId: 'MomoTestStore',
        requestType: 'capture',
        amount: transaction.value,
        lang: 'vi',
        description: 'Thanh toán cho Check List',
        signature: '',
      };
      const rawSignature = `accessKey=${'g5UluFSOSdGIaR2o'}&amount=${
        data.amount
      }&description=${data.description}&orderId=${data.orderId}&partnerCode=${
        data.partnerCode
      }&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = hmacSHA256(
        rawSignature,
        'joLVlb82fwMzYiyNXDEskdubXSah6IQ6',
      ).toString();

      data.signature = signature;

      const response = await fetch(process.env.URL_CONFIRM_PAYMENT, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData: any = await response.json();
      Logger.log('Receive response', responseData);

      const transactionID = responseData.orderId;

      if (responseData.resultCode == 0) {
        await this.testPaymentModel.findOneAndUpdate(
          { _id: transactionID, status: 'pending' },
          {
            status: 'success',
            statusRequestPay: 'pending',
            paymentInfo: { ...transaction.paymentInfo, ...responseData },
          },
          session,
        );

        await session.commitTransaction();
      }
    } catch (err) {
      Logger.error('Send confirm to momo failed', err);
      throw new BadRequestException('Lỗi xác nhận giao dịch');
    }
  }

  async sendCancelToMomo(transaction: TestPayment) {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const data = {
        partnerCode: process.env.PARTNER_CODE,
        requestId: transaction._id.toString(),
        orderId: transaction._id.toString(),
        storeId: 'MomoTestStore',
        requestType: 'cancel',
        amount: transaction.value,
        lang: 'vi',
        description: 'Thanh toán cho Check List',
        signature: '',
      };
      const rawSignature = `accessKey=${'g5UluFSOSdGIaR2o'}&amount=${
        data.amount
      }&description=${data.description}&orderId=${data.orderId}&partnerCode=${
        data.partnerCode
      }&requestId=${data.requestId}&requestType=${data.requestType}`;
      const signature = hmacSHA256(
        rawSignature,
        'joLVlb82fwMzYiyNXDEskdubXSah6IQ6',
      ).toString();

      data.signature = signature;

      const response = await fetch(process.env.URL_CONFIRM_PAYMENT, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: { 'Content-Type': 'application/json' },
      });
      const responseData = await response.json();
      Logger.log('Receive response', responseData);
    } catch (err) {
      Logger.error('Send confirm to momo failed', err);
      throw new BadRequestException('Lỗi xác nhận giao dịch');
    }
  }

  async handleIpnNotiTestPayment(data: MomoIpn) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const transactionID = data.orderId;

      //check signature later
      const transaction = await this.testPaymentModel
        .findOne({
          _id: transactionID,
        })
        .populate('creator');
      // .populate('receiver');

      if (transaction) {
        if (data.resultCode == 9000) {
          if (transaction.status == 'pending')
            this.sendConfirmToMomo(transaction);
          else this.sendCancelToMomo(transaction);
        } else if (data.resultCode != 0 && data.resultCode != 9000) {
          // transaction failed

          await this.testPaymentModel.findOneAndUpdate(
            { _id: transactionID },
            {
              status: 'failed',
              paymentInfo: { ...transaction.paymentInfo, ...data },
            },
            { session },
          );

          await session.commitTransaction();
        }
      }
    } catch (err) {
      Logger.log('Update transaction status failed', err);
    } finally {
      session.endSession();
      return { data: 'Success' };
    }
  }
}
