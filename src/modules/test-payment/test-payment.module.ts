import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { TestPayment, TestPaymentSchema } from 'src/schema/test-payment';
import { TestPaymentService } from './test-payment.service';
import { TestPaymentController } from './test-payment.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: TestPayment.name, schema: TestPaymentSchema },
    ]),
  ],
  providers: [TestPaymentService],
  exports: [TestPaymentService],
  controllers: [TestPaymentController],
})
export class TestPaymentModule {}
