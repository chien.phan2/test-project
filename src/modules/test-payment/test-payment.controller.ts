import {
  Body,
  Controller,
  HttpCode,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { TestPaymentService } from './test-payment.service';
import { CreateTestPaymentDto } from './dto/create-test-payment.dto';

@ApiTags('Payment Checklist - Test')
@Controller('payment-test')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class TestPaymentController {
  constructor(private readonly testPaymentService: TestPaymentService) {}

  @Post()
  create(@Body() payload: CreateTestPaymentDto) {
    return this.testPaymentService.createTestRequestPayment(payload);
  }

  @Post('/ipn')
  @HttpCode(204)
  ipnNoti(@Body() body: any) {
    return this.testPaymentService.handleIpnNotiTestPayment(body);
  }
}
