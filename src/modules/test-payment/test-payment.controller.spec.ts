import { Test, TestingModule } from '@nestjs/testing';
import { TestPaymentController } from './test-payment.controller';

describe('TestPaymentController', () => {
  let controller: TestPaymentController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TestPaymentController],
    }).compile();

    controller = module.get<TestPaymentController>(TestPaymentController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
