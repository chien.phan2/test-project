import { Test, TestingModule } from '@nestjs/testing';
import { TestPaymentService } from './test-payment.service';

describe('TestPaymentService', () => {
  let service: TestPaymentService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TestPaymentService],
    }).compile();

    service = module.get<TestPaymentService>(TestPaymentService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
