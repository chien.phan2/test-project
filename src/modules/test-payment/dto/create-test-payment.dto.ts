import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsOptional } from 'class-validator';

export class CreateTestPaymentDto {
  @ApiProperty()
  @IsNotEmpty()
  note: string;

  @ApiProperty({ default: false })
  @IsOptional()
  @IsBoolean()
  isDevTool: boolean;
}
