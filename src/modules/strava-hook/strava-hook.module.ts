import { Module } from '@nestjs/common';
import { StravaHookService } from './strava-hook.service';
import { StravaHookController } from './strava-hook.controller';
import { ActivityModule } from '../activity/activity.module';

@Module({
  imports: [ActivityModule],
  providers: [StravaHookService],
  exports: [StravaHookService],
  controllers: [StravaHookController],
})
export class StravaHookModule {}
