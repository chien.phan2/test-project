import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Post,
  Req,
  Res,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { ActivityService } from '../activity/activity.service';
import { Request, Response } from 'express';
import { TransformHookResponseInterceptor } from 'src/interceptor/hook.interceptor';

@UseInterceptors(new ErrorsInterceptor())
@ApiTags('Strava Hook')
@Controller('strava')
export class StravaHookController {
  constructor(private readonly activityService: ActivityService) {}
  @UseInterceptors(TransformHookResponseInterceptor)
  @Get('/webhook')
  getStravaWebhook(@Req() req: Request) {
    return this.activityService.getStravaWebhook(req);
  }

  @Post('/webhook')
  async receiveStravaWebhook(@Body() data: any, @Res() res: Response) {
    const dataResponse = await this.activityService.receiveStravaWebhook(data);
    console.log('=> receiveStravaWebhook => res: ', dataResponse);
    res.status(HttpStatus.OK).send();
  }
}
