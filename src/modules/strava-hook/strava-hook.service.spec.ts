import { Test, TestingModule } from '@nestjs/testing';
import { StravaHookService } from './strava-hook.service';

describe('StravaHookService', () => {
  let service: StravaHookService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StravaHookService],
    }).compile();

    service = module.get<StravaHookService>(StravaHookService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
