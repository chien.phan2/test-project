import { Test, TestingModule } from '@nestjs/testing';
import { StravaHookController } from './strava-hook.controller';

describe('StravaHookController', () => {
  let controller: StravaHookController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StravaHookController],
    }).compile();

    controller = module.get<StravaHookController>(StravaHookController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
