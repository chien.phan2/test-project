import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  UseInterceptors,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { TicketService } from './ticket.service';
import { CreateTicketDto } from './dto/create-ticket.dto';
import { UpdateTicketDto } from './dto/update-ticket.dto';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { FilterTicketDto } from './dto/filter-ticket.dto';
import { Request } from 'express';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { ApiBearerAuth } from '@nestjs/swagger';
import { ReportTicketDto } from './dto/report-ticket.dto';
import { CreateTicketRequestDto } from './dto/create-ticket-request.dto';
import { FillTicketRequestDto } from './dto/fill-ticket-request.dto';

@Controller('ticket')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class TicketController {
  constructor(private readonly ticketService: TicketService) {}

  @Post()
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  create(@Body() createTicketDto: CreateTicketDto, @Req() req: Request) {
    const userID = req.userID;
    return this.ticketService.create(userID, createTicketDto);
  }

  @Post('/fill/:id')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  fillRequest(
    @Body() fillTicketRequestDto: FillTicketRequestDto,
    @Req() req: Request,
    @Param('id') id: string,
  ) {
    const userID = req.userID;
    return this.ticketService.fillRequest(userID, id, fillTicketRequestDto);
  }

  @Post('/request')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  createRequest(
    @Body() createTicketRequestDto: CreateTicketRequestDto,
    @Req() req: Request,
  ) {
    const userID = req.userID;
    return this.ticketService.createRequest(userID, createTicketRequestDto);
  }

  @Get('/request')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  findAllRequest(@Query() query: FilterTicketDto, @Req() req: Request) {
    const userID = req.userID;
    return this.ticketService.findMyRequest(userID, query);
  }

  @Post('/request/:id')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  cancelRequest(@Req() req: Request, @Param('id') id: string) {
    const userID = req.userID;
    return this.ticketService.cancelRequest(userID, id);
  }

  @Get()
  findAll(@Query() query: FilterTicketDto) {
    return this.ticketService.findAll(query);
  }

  @Get('/my-tickets/:id')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  findOneMy(@Param('id') id: string, @Req() req: Request) {
    const userID = req.userID;
    return this.ticketService.findOneMy(id, userID);
  }

  @Get('/my-tickets')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  findMy(@Query() query: FilterTicketDto, @Req() req: Request) {
    const userID = req.userID;
    return this.ticketService.findMy(userID, query);
  }

  @Get('/my-purchase')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  findMyPurchase(@Query() query: FilterTicketDto, @Req() req: Request) {
    const userID = req.userID;
    return this.ticketService.findMyPurchase(userID, query);
  }

  @Get('/my-pending')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  findMyPending(@Query() query: FilterTicketDto, @Req() req: Request) {
    const userID = req.userID;
    return this.ticketService.findMyPending(userID, query);
  }

  @Get('/code/:id')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  reviveCode(@Param('id') id: string, @Req() req: Request) {
    const userID = req.userID;

    return this.ticketService.getCode(id, userID);
  }

  @Get('/report/:id')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  getReport(@Param('id') id: string, @Req() req: Request) {
    const userID = req.userID;

    return this.ticketService.getReport(id, userID);
  }

  @Post('/report/:id')
  @UseGuards(new AuthGuard())
  @ApiBearerAuth('JWT-auth')
  reportTicket(
    @Param('id') id: string,
    @Body() reportDto: ReportTicketDto,
    @Req() req: Request,
  ) {
    const userID = req.userID;

    return this.ticketService.reportTicket(id, userID, reportDto);
  }

  @Get(':id')
  @ApiBearerAuth('JWT-auth')
  findOne(@Param('id') id: string) {
    return this.ticketService.findOne(id);
  }

  @Patch(':id')
  @ApiBearerAuth('JWT-auth')
  update(@Param('id') id: string, @Body() updateTicketDto: UpdateTicketDto) {
    return this.ticketService.update(id, updateTicketDto);
  }
}
