import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, Max, Min } from 'class-validator';

export class CreateTicketRequestDto {
  @ApiProperty()
  @IsNumber()
  @Min(10000, { message: 'MIN_MAX_PRICE' })
  @Max(500000, { message: 'MIN_MAX_PRICE' })
  price: number;

  @ApiProperty()
  @IsNumber()
  @Min(1000, { message: 'MIN_MAX_DONATE' })
  @Max(500000, { message: 'MIN_MAX_DONATE' })
  donate: number;
}
