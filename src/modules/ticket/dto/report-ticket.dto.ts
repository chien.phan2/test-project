import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsArray } from 'class-validator';

export class ReportTicketDto {
  @ApiProperty()
  @IsString()
  message: string;

  @IsArray()
  @IsString({ each: true })
  @ApiProperty()
  attachments: string[];
}
