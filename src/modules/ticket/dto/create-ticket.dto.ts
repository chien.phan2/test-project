import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsNumberString, IsString, Max, Min } from 'class-validator';

export class CreateTicketDto {
  @ApiProperty()
  @IsString()
  @IsNumberString()
  code: string;

  @ApiProperty()
  @IsNumber()
  @Min(10000, { message: 'MIN_MAX_PRICE' })
  @Max(500000, { message: 'MIN_MAX_PRICE' })
  price: number;

  @ApiProperty()
  @IsNumber()
  @Min(1000, { message: 'MIN_MAX_DONATE' })
  @Max(500000, { message: 'MIN_MAX_DONATE' })
  donate: number;
}
