import { Exclude } from 'class-transformer';
import { TicketDocument } from 'src/schema/ticket.schema';
import formatText from 'src/utils/formatText.utils';

export class PublicTicket {
  _id: any;
  ownerName: string;
  ownerPhone: string;
  buyerName: string;
  buyerPhone: string;
  transaction: string;
  buyer: string;
  owner: string;
  type: string;

  @Exclude({ toPlainOnly: true })
  code: string;

  // @Exclude({ toPlainOnly: true })
  // owner: string;

  // @Exclude({ toPlainOnly: true })
  // buyer: string;

  @Exclude({ toPlainOnly: true })
  user: any;

  constructor(partial: any) {
    Object.assign(this, partial);
    this._id = partial._id.toString();
    this.ownerName = formatText(partial.owner?.name || '');
    this.ownerPhone = formatText(partial.owner?.phone || '');
    this.buyerName = formatText(partial.buyer?.name || '');
    this.buyerPhone = formatText(partial.buyer?.phone || '');
    this.buyer =
      partial?.buyer?._id?.toString() || (partial.buyer as unknown as string);
    this.owner =
      partial?.owner?._id?.toString() || (partial.owner as unknown as string);
    this.transaction = partial.transaction?._id
      ? partial.transaction?._id.toString()
      : ((partial.transaction || '') as unknown as string);
  }
}
