import { ApiProperty } from '@nestjs/swagger';
import {
  IsIn,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsString,
  Min,
} from 'class-validator';

export class UpdateTicketDto {
  @ApiProperty()
  @IsIn(['cancel', 'available'])
  @IsOptional()
  status: string;

  @ApiProperty()
  @IsString()
  @IsNumberString()
  @IsOptional()
  code: string;

  @ApiProperty()
  @IsNumber()
  @Min(1000)
  @IsOptional()
  price: number;

  @ApiProperty()
  @IsNumber()
  @Min(1000)
  @IsOptional()
  donate: number;
}
