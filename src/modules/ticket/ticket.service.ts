/* eslint-disable prettier/prettier */
import {
  BadGatewayException,
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import fetch from 'node-fetch';
import { DEFAULT_LIMIT, DEFAULT_SKIP } from 'src/contants/filter.contants';
import { MY_MESSAGE } from 'src/contants/message.contants';
import { EventsGateway } from 'src/events/events.gateway';
import { Ticket, TicketDocument } from 'src/schema/ticket.schema';
import getDateString from 'src/utils/getDateString';
import { sendHangoutMessageWithLink } from 'src/utils/sendMessageToHangout';
import { TransactionService } from '../transaction/transaction.service';
import { UserService } from '../user/user.service';
import { CreateTicketRequestDto } from './dto/create-ticket-request.dto';
import { CreateTicketDto } from './dto/create-ticket.dto';
import { FillTicketRequestDto } from './dto/fill-ticket-request.dto';
import { FilterTicketDto } from './dto/filter-ticket.dto';
import { PublicTicket } from './dto/public-ticket.dto';
import { ReportTicketDto } from './dto/report-ticket.dto';
import { UpdateTicketDto } from './dto/update-ticket.dto';
import { Connection } from 'mongoose';

@Injectable()
export class TicketService {
  token = '';

  constructor(
    @InjectModel(Ticket.name) private ticketModel: Model<TicketDocument>,
    private readonly eventsGateway: EventsGateway,
    private readonly userService: UserService,
    @InjectConnection() private connection: Connection,
    @Inject(forwardRef(() => TransactionService))
    private readonly transactionService: TransactionService,
  ) {
    // if (process.env.VERIFY_CODE === 'true') this.loginToHrm();
  }

  async create(userID: string, createPayload: CreateTicketDto) {
    try {
      const user = await this.userService.getUserInfo(userID);

      if (
        createPayload.donate > createPayload.price ||
        (createPayload.price - createPayload.donate < 10000 &&
          createPayload.price != createPayload.donate)
      )
        throw new BadRequestException(MY_MESSAGE.INVALID_PRICE);

      if (process.env.VERIFY_CODE === 'true')
        await this.verifyTicket(createPayload.code, user.phone);

      const newEntity = new this.ticketModel({
        ...createPayload,
        owner: user._id.toString(),
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString(),
        timestamp: Date.now(),
      });
      const data = await (await newEntity.save()).populate('owner');
      this.eventsGateway.onCreateTicket(new PublicTicket(data.toJSON()));
      return {
        data: new PublicTicket(data.toJSON()),
        meta: createPayload,
      };
    } catch (err) {
      throw err;
    }
  }

  async createRequest(userID: string, createPayload: CreateTicketRequestDto) {
    try {
      const user = await this.userService.getUserInfo(userID);

      if (process.env.VERIFY_CODE === 'true')
        await this.checkCanCreateRequest(userID);

      if (
        createPayload.donate > createPayload.price ||
        (createPayload.price - createPayload.donate < 10000 &&
          createPayload.price != createPayload.donate)
      )
        throw new BadRequestException(MY_MESSAGE.INVALID_PRICE);

      const newEntity = new this.ticketModel({
        ...createPayload,
        price: createPayload.price + createPayload.donate,
        type: 'buy',
        status: 'pending',
        code: '',
        buyer: user._id.toString(),
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString(),
        timestamp: Date.now(),
      });
      const data = await newEntity.save();

      // not sending the socket until request is pay successfully
      // this.eventsGateway.onCreateTicket(new PublicTicket(data.toJSON()));

      // create transaction here

      const transactionData =
        await this.transactionService.createRequestTransaction(userID, data);

      this.timeoutRefundRequest(newEntity._id, 5400000);

      return transactionData;
    } catch (err) {
      throw err;
    }
  }

  async cancelRequest(userID: string, requestID: string) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const ticket = await this.ticketModel.findOne(
        {
          buyer: userID,
          _id: requestID,
          status: 'available',
        },
        null,
        {
          session,
        },
      );

      if (!ticket) throw new NotFoundException(MY_MESSAGE.NOT_FOUND_TICKET);

      const updatedTicket = await this.ticketModel.findOneAndUpdate(
        {
          _id: ticket.id,
        },
        {
          status: 'cancel',
        },
        { session, returnOriginal: false },
      );

      const updateTicketData = new PublicTicket(updatedTicket.toJSON());
      this.eventsGateway.onChangeTicket({ ...updateTicketData });

      await this.transactionService.updateReceiver(
        String(ticket.transaction),
        userID,
        session,
      );

      //back money for buyer

      await this.transactionService.sendBackMoneyForBuyer(
        String(ticket.transaction),
        session,
      );

      await session.commitTransaction();
      session.endSession();

      return {
        data: updateTicketData,
        message: 'Cancel successfully',
      };
    } catch (err) {
      await session.abortTransaction();
      session.endSession();

      Logger.error('Cancel request failed', err);
      throw err;
    }
  }

  async fillRequest(
    userID: string,
    id: string,
    fillPayload: FillTicketRequestDto,
  ) {
    const session = await this.connection.startSession();
    session.startTransaction();

    try {
      const user = await this.userService.getUserInfo(userID);
      const ticket = await this.ticketModel
        .findByIdAndUpdate(
          {
            _id: id,
            status: 'available',
          },
          {
            status: 'pending',
          },
          { session, returnOriginal: false },
        )
        .populate('owner')
        .populate('buyer');

      if (!ticket) throw new NotFoundException(MY_MESSAGE.NOT_FOUND_TICKET);

      await this.sendSocketPending(id, session);

      if (process.env.VERIFY_CODE === 'true')
        await this.verifyTicket(fillPayload.code, user.phone);

      const newTicket = await this.ticketModel
        .findOneAndUpdate(
          { _id: ticket._id },
          { code: fillPayload.code, owner: userID, status: 'captured' },
          { session, returnOriginal: false },
        )
        .populate('owner')
        .populate('buyer');

      await this.transactionService.updateReceiver(
        String(ticket.transaction),
        userID,
        session,
      );

      await this.transactionService.sendMoneyToSeller(
        String(ticket.transaction),
        session,
      );

      const publicTicket = new PublicTicket(newTicket.toJSON());

      this.eventsGateway.onChangeTicket({ ...publicTicket });

      await session.commitTransaction();
      session.endSession();

      return {
        message: 'Fill successfully',
      };
    } catch (err) {
      await this.releaseToAvailable(id, session, true);

      await session.abortTransaction();
      session.endSession();

      throw err;
    }
  }

  async findAll(filter: FilterTicketDto) {
    const data = await this.ticketModel
      .find({
        $or: [
          {
            status: 'available',
            type: 'sell',
          },
          {
            status: 'pending',
            type: 'sell',
          },
        ],
        createdAt: { $regex: getDateString(), $options: 'i' },
      })
      .limit(filter.limit ?? DEFAULT_LIMIT)
      .skip(filter.skip ?? DEFAULT_SKIP)
      .populate('owner')
      .exec();

    const dataRequest = await this.ticketModel
      .find({
        $or: [
          {
            status: 'available',
            type: 'buy',
          },
        ],
        createdAt: { $regex: getDateString(), $options: 'i' },
      })
      .limit(filter.limit ?? DEFAULT_LIMIT)
      .skip(filter.skip ?? DEFAULT_SKIP)
      .populate('buyer')
      .exec();

    return {
      data: [...data, ...dataRequest].map(
        (item) => new PublicTicket(item.toJSON()),
      ),
      meta: { filter },
    };
  }

  async findMyRequest(userID: string, filter: FilterTicketDto) {
    const data = await this.ticketModel
      .find({
        buyer: userID,
        type: 'buy',
        createdAt: { $regex: getDateString(), $options: 'i' },
      })
      .limit(filter.limit ?? DEFAULT_LIMIT)
      .skip(filter.skip ?? DEFAULT_SKIP)
      .exec();

    return {
      data: data.map((item) => ({
        ...new PublicTicket(item.toJSON()),
        code: item.code,
      })),
      meta: { filter },
    };
  }

  async findMy(userID: string, filter: FilterTicketDto) {
    const data = await this.ticketModel
      .find({
        owner: userID,
      })
      .limit(filter.limit ?? DEFAULT_LIMIT)
      .skip(filter.skip ?? DEFAULT_SKIP)
      .exec();

    return {
      data: data.map((item) => ({
        ...new PublicTicket(item.toJSON()),
        code: item.code,
      })),
      meta: { filter },
    };
  }

  async findMyPurchase(userID: string, filter: FilterTicketDto) {
    const data = await this.ticketModel
      .find({
        buyer: userID,
        status: 'captured',
      })
      .limit(filter.limit ?? DEFAULT_LIMIT)
      .skip(filter.skip ?? DEFAULT_SKIP)
      .populate('owner')
      .exec();

    return {
      data: data.map((item) => new PublicTicket(item.toJSON())),
      meta: { filter },
    };
  }

  async findMyPending(userID: string, filter: FilterTicketDto) {
    const data = await this.ticketModel
      .find({
        buyer: userID,
        status: 'pending',
        type: 'sell',
      })
      .limit(filter.limit ?? DEFAULT_LIMIT)
      .skip(filter.skip ?? DEFAULT_SKIP)
      .populate('owner')
      .exec();

    return {
      data: data.map((item) => new PublicTicket(item.toJSON())),
      meta: { filter },
    };
  }

  async findOne(id: string) {
    const data = await this.ticketModel.findById(id).populate('owner');
    if (!data) throw new NotFoundException(MY_MESSAGE.NOT_FOUND_TICKET);
    return { data: new PublicTicket(data.toJSON()), meta: { id } };
  }

  async getCode(id: string, userID: string) {
    const data = await this.ticketModel.findOne({
      _id: id,
      $or: [
        {
          owner: userID,
        },
        {
          buyer: userID,
          status: 'captured',
        },
      ],
    });
    if (!data) throw new NotFoundException(MY_MESSAGE.NOT_FOUND_TICKET);

    return { data: data.code, meta: { id } };
  }

  async getReport(id: string, userID: string) {
    const data = await this.ticketModel.findOne({
      _id: id,
      buyer: userID,
      status: 'captured',
    });
    if (!data) throw new NotFoundException(MY_MESSAGE.NOT_FOUND_TICKET);

    return { data: data.reportInfo, meta: { id } };
  }

  async getAvailable(id: string, session: any) {
    const data = await this.ticketModel
      .findOne(
        {
          _id: id,
          status: 'available',
          createdAt: { $regex: getDateString(), $options: 'i' },
        },
        null,
        { session },
      )
      .populate('owner')
      .exec();

    if (!data) throw new BadRequestException(MY_MESSAGE.NOT_FOUND_TICKET);

    return data;
  }

  async update(id: string, updatePayload: UpdateTicketDto) {
    const data = await this.ticketModel.findOne({
      _id: id,
      type: 'sell',
      status: { $in: ['available', 'cancel'] },
      // status: 'available' | 'cancel',  or condition here
    });
    if (!data) throw new NotFoundException(MY_MESSAGE.NOT_FOUND_TICKET);
    const price = updatePayload.price ?? data.price ?? 0;
    const donate = updatePayload.donate ?? data.donate ?? 0;

    if (price < donate || (price - donate < 10000 && price != donate))
      throw new BadRequestException(MY_MESSAGE.INVALID_PRICE);

    const result = await this.ticketModel.findOneAndUpdate(
      { _id: data.id },
      { ...updatePayload, updatedAt: new Date().toLocaleString() },
      { returnOriginal: false },
    );
    await data.updateOne({ ...updatePayload }, { returnOriginal: false });
    const ticket = await this.ticketModel
      .findOne({ _id: id })
      .populate('owner')
      .exec();
    const publicTicket = new PublicTicket(ticket.toJSON());

    this.eventsGateway.onChangeTicket({ ...publicTicket });
    return {
      data: new PublicTicket(result.toJSON()),
      meta: { payload: updatePayload },
    };
  }

  async setPending(id: string, userID: string, session: any) {
    try {
      await this.ticketModel.findOneAndUpdate(
        { _id: id },
        { status: 'pending', buyer: userID },
        {
          session,
        },
      );
    } catch (err) {
      throw new BadRequestException(MY_MESSAGE.NOT_FOUND_TICKET);
    }
  }

  async sendSocketPending(id: string, session: any) {
    const ticket = await this.ticketModel
      .findOne({ _id: id }, null, { session })
      .populate('owner')
      .populate('buyer')
      .exec();

    const publicTicket = new PublicTicket(ticket.toJSON());

    this.eventsGateway.onChangeTicket({ ...publicTicket });
  }

  async releaseToAvailable(id: string, session: any, isRequest = false) {
    if (!isRequest) {
      await this.ticketModel.findOneAndUpdate(
        { _id: id },
        { status: 'available', transaction: '', buyer: '' },
        { session },
      );

      const ticket = await this.ticketModel
        .findOne({ _id: id }, null, { session })
        .populate('owner')
        .exec();

      const publicTicket = new PublicTicket(ticket.toJSON());
      this.eventsGateway.onChangeTicket({ ...publicTicket });
    } else {
      await this.ticketModel.findOneAndUpdate(
        { _id: id },
        { status: 'available' },
        { session },
      );

      const ticket = await this.ticketModel
        .findOne({ _id: id }, null, { session })
        .populate('buyer')
        .exec();

      const publicTicket = new PublicTicket(ticket.toJSON());
      this.eventsGateway.onChangeTicket({ ...publicTicket });
    }
  }

  async setCaptured(id: string, buyerID: string, session: any) {
    await this.ticketModel
      .findOneAndUpdate(
        { _id: id },
        { status: 'captured', buyer: buyerID },
        session,
      )
      .then(async () => {
        const ticket = await this.ticketModel
          .findOne({ _id: id })
          .populate('owner')
          .exec();
        const publicTicket = new PublicTicket(ticket.toJSON());

        this.eventsGateway.onChangeTicket({ ...publicTicket });
      });
  }

  async reportTicket(id: string, userID: string, reportDto: ReportTicketDto) {
    const ticket = await this.ticketModel
      .findOne({
        _id: id,
        status: 'captured',
        buyer: userID,
      })
      .populate('owner')
      .populate('buyer');

    if (!ticket)
      throw new BadRequestException(MY_MESSAGE.NOT_FOUND_TICKET_BUYER);

    await this.ticketModel.findOneAndUpdate(
      { _id: id },
      {
        reportInfo: { ...reportDto, createdAt: new Date().toLocaleString() },
      },
    );

    sendHangoutMessageWithLink(
      `Report ticket \n-------------- \nUser: ${
        ticket?.buyer?.phone
      } \nTicket: ${id} \nTicket code: ${ticket.code} \nTicket owner: ${
        ticket?.owner?.phone
      } \nTransaction: ${ticket.transaction} \nMessage: ${
        reportDto.message
      } \nTime: ${new Date().toLocaleString()}`,
      reportDto.attachments,
    );

    return {
      data: {
        sellerName: ticket.owner.name,
        sellerPhone: ticket.owner.phone,
      },
    };
  }

  async setTransaction(id: string, transaction: string, session: any) {
    await this.ticketModel.findOneAndUpdate(
      { _id: id },
      { transaction: transaction },
      {
        session,
      },
    );
  }

  async findOneMy(id: string, userID: string) {
    const data = await this.ticketModel
      .findOne({
        owner: userID,
        _id: id,
      })
      .populate('owner');

    if (!data) throw new NotFoundException(MY_MESSAGE.NOT_FOUND_TICKET);

    return {
      data: { ...new PublicTicket(data.toJSON()), code: data.code },
      meta: { id },
    };
  }

  async verifyTicket(id: string, phone: string) {
    try {
      const currentTicket = await this.ticketModel.findOne({
        code: id,
        createdAt: { $regex: getDateString(), $options: 'i' },
      });

      if (currentTicket)
        throw new BadRequestException(MY_MESSAGE.INVALID_TICKET);

      const res = await fetch(
        `${process.env.FORWARDER_HOST}/lunch/lunchticket`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${this.token}`,
          },
          body: JSON.stringify({
            cm: 'check_exist_lunch_ticket_code',
            dt: {
              Momo_Number: phone,
              Ticket_Code: id,
            },
          }),
        },
      );
      const responseData: any = await res.json();
      if (responseData.message == `Unauthorized`) {
        Logger.log('Re authen to hrm');
        return await this.loginToHrm().then(async (res) => {
          await this.verifyTicket(id, phone);
        });
      } else if (responseData.err == -3)
        throw new BadRequestException(MY_MESSAGE.BLOCK_CHECK);
      else if (responseData.err != 1)
        throw new BadRequestException(MY_MESSAGE.INVALID_TICKET);
      return true;
    } catch (err) {
      throw err;
    }
  }

  async loginToHrm() {
    try {
      const res = await fetch(`${process.env.FORWARDER_HOST}/lunch/authen`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer yJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFkbWluaXN0cmF0b3JfSWQiLCJuYmYiOjE2MzU1MDA4NjksImV4cCI6MTYzNTU4NzI2OSwiaWF0IjoxNjM1NTAwODY5fQ.qz8MC9CoWg7b_sd1rsAsf13PYdDkfVGiZwKSbiaCDdE`,
        },
        body: JSON.stringify({
          Username: 'Momo_Account_Username',
          Password: 'Momo_Account_Password',
        }),
      });
      const responseData: any = await res.json();
      this.token = responseData.token;
    } catch (err) {
      Logger.error('Authen to HRM failed', err);
      throw err;
    }
  }

  async setCancel(id: string, session: any) {
    try {
      await this.ticketModel.findOneAndUpdate(
        { _id: id },
        { status: 'cancel' },
        {
          session,
        },
      );
    } catch (err) {
      throw new BadRequestException(MY_MESSAGE.NOT_FOUND_TICKET);
    }
  }

  async checkCanCreateRequest(userID: string) {
    try {
      const data = await this.ticketModel.find({
        buyer: userID,
        createdAt: { $regex: getDateString(), $options: 'i' },
      });

      if (data.length > 5)
        throw new BadRequestException(MY_MESSAGE.LIMIT_REQUEST);
    } catch (err) {
      throw new BadRequestException(MY_MESSAGE.LIMIT_REQUEST);
    }
  }

  timeoutRefundRequest(ticketID: string, time: number) {
    try {
      setTimeout(async () => {
        const data = await this.ticketModel.findOne({
          _id: ticketID,
          type: 'buy',
          status: 'available',
        });

        if (data) {
          data.status = 'cancel';

          await this.cancelRequest(String(data.buyer), data._id.toString());
        }
      }, time);
    } catch (err) {
      Logger.error("Can't cancel and refund ticket ", ticketID);
    }
  }
}
