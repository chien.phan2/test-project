import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Reward } from '../../schema/reward.schema';
import { MY_MESSAGE } from '../../contants/message.contants';
import { UpdateRewardDto } from './dto/updateReward.dto';
import { InventoryService } from '../inventory/inventory.service';
import { RankRewardService } from '../rank-reward/rank-reward.service';

const mockData = [
  {
    name: 'Chúc bạn may mắn lần sau',
    value: 0,
    type: 'No reward',
    code: 'GoodLuck',
    url: 'https://img.icons8.com/emoji/96/crossed-fingers-emoji.png',
    rate: 0.85,
  },
  {
    name: 'Trúng 1 vé Challenge',
    value: 0,
    type: 'Challenge',
    code: 'ChallengeTicket',
    url: 'https://static.momocdn.net/app/mini-app-center/games/momo-challenges/ticket.png',
    rate: 0.05,
  },
  {
    name: 'Thanh năng lượng',
    value: 0,
    type: 'Energy',
    code: 'Energy100',
    url: 'https://static.momocdn.net/app/mini-app-center/games/momo-challenges/energy_bottle.png',
    rate: 0.1,
  },
];
@Injectable()
export class RewardService {
  constructor(
    @InjectModel(Reward.name) private readonly rewardModel: Model<Reward>,
    @Inject(forwardRef(() => RankRewardService))
    private readonly rankRewardService: RankRewardService,
  ) {}

  async createNewReward({ name, value, type, isActive, code, url, rate }) {
    try {
      const newEntity = new this.rewardModel({
        name: name,
        value,
        type,
        isActive,
        code,
        url,
        rate,
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString(),
      });
      return await newEntity.save();
    } catch (error) {
      throw error;
    }
  }

  async createMultipleReward() {
    try {
      return this.rewardModel.insertMany(
        mockData.map((item) => ({
          ...item,
          createdAt: new Date().toLocaleString(),
          updatedAt: new Date().toLocaleString(),
        })),
      );
    } catch (error) {
      throw error;
    }
  }

  async getRewards() {
    try {
      return await this.rewardModel.find({
        isActive: true,
      });
    } catch (error) {
      throw error;
    }
  }

  async getRewardsByKeyCode(code: string) {
    try {
      return await this.rewardModel.find({
        isActive: true,
        code: code,
      });
    } catch (error) {
      throw error;
    }
  }

  async updateRewardById(id: string, updateRewardDto: UpdateRewardDto) {
    try {
      return await this.rewardModel.findByIdAndUpdate(id, {
        ...updateRewardDto,
        updatedAt: new Date().toLocaleString(),
      });
    } catch (error) {
      throw new BadRequestException(MY_MESSAGE.NOT_FOUND_CHALLENGE);
    }
  }

}
