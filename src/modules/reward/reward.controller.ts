import {
  Controller,
  Post,
  Body,
  UseInterceptors,
  Patch,
  Delete,
  Get,
  UseGuards,
  Param,
} from '@nestjs/common';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { RewardService } from './reward.service';
import { CreateRewardDto } from './dto/createReward.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { CreateChallengeDto } from '../challenge/dto/create-challenge.dto';
import {UpdateRewardDto} from "./dto/updateReward.dto";

@ApiTags('Challenge')
@Controller('challenge/reward')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class RewardController {
  constructor(private readonly rewardService: RewardService) {}

  @Post('/sync')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  syncMultipleRewards() {
    return this.rewardService.createMultipleReward();
  }

  @Post('/create')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  createReward(@Body() createRewardDto: CreateRewardDto) {
    return this.rewardService.createNewReward(createRewardDto);
  }

  @Delete('/delete')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  deleteReward() {
    // return this.rewardService.createNewReward(createRewardDto);
    return {
      status: 'updating',
    };
  }

  @Get()
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  getIsActiveReward() {
    return this.rewardService.getRewards();
  }

  @Patch('/update/:id')
  // @ApiBearerAuth('JWT-auth')
  // @UseGuards(new AuthGuard())
  updateRewardById(
    @Body() updateRewardDto: UpdateRewardDto,
    @Param('id') id: string,
  ) {
    return this.rewardService.updateRewardById(id, updateRewardDto);
  }
}
