import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Reward, RewardSchema } from '../../schema/reward.schema';
import { RewardController } from './reward.controller';
import { RewardService } from './reward.service';
import { RankRewardModule } from '../rank-reward/rank-reward.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Reward.name, schema: RewardSchema }]),
    forwardRef(() => RankRewardModule),
  ],
  controllers: [RewardController],
  providers: [RewardService],
  exports: [RewardService],
})
export class RewardModule {}
