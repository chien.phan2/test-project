import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDataURI,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  Min,
} from 'class-validator';
import { RewardCode, RewardType } from 'src/utils/constants';

export class UpdateRewardDto {
  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsString()
  url: string;

  @ApiProperty()
  @IsNumber()
  rate: number;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  isActive: boolean;
}
