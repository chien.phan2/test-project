import { Module, forwardRef } from '@nestjs/common';
import { ActivityService } from './activity.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Activity, ActivitySchema } from 'src/schema/activity.schema';
import { RunnerModule } from '../runner/runner.module';
import { ParticipationModule } from '../participation/participation.module';
import { ChallengeModule } from '../challenge/challenge.module';
import { ActivityController } from './activity.controller';
import { GroupParticipationRequestModule } from '../group-participation-request/group-participation-request.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Activity.name, schema: ActivitySchema },
    ]),
    forwardRef(() => RunnerModule),
    ParticipationModule,
    forwardRef(() => ChallengeModule),
    forwardRef(() => GroupParticipationRequestModule),
  ],
  providers: [ActivityService],
  exports: [ActivityService],
  controllers: [ActivityController],
})
export class ActivityModule {}
