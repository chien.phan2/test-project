import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateActivityDto {
  @ApiProperty()
  @IsNotEmpty()
  activityId: string;

  @ApiProperty()
  @IsNotEmpty()
  runnerId: string;

  @ApiProperty()
  @IsNotEmpty()
  stravaId: string;

  @ApiProperty()
  @IsNotEmpty()
  distance: number;

  @ApiProperty()
  elapsed_time: number;

  @ApiProperty()
  from_accepted_tag: boolean;

  @ApiProperty()
  manual: boolean;

  @ApiProperty()
  private: boolean;

  @ApiProperty()
  map: object;

  @ApiProperty()
  max_speed: number;

  @ApiProperty()
  moving_time: number;

  @ApiProperty()
  name: string;

  @ApiProperty()
  external_id: string;

  @ApiProperty()
  isWarning: boolean;

  @ApiProperty()
  sport_type: string;

  @ApiProperty()
  start_date: Date;

  @ApiProperty()
  start_date_local: Date;
}
