import {
  BadRequestException,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import axios from 'axios';
import * as dayjs from 'dayjs';
import * as customParseFormat from 'dayjs/plugin/customParseFormat';
import * as timezone from 'dayjs/plugin/timezone';
import * as utc from 'dayjs/plugin/utc';
import { Request } from 'express';
import * as uniqBy from 'lodash/uniqBy';
import { Model } from 'mongoose';
import { Activity, ActivityDocument } from 'src/schema/activity.schema';
import { ChallengeService } from '../challenge/challenge.service';
import { GroupParticipationRequestService } from '../group-participation-request/group-participation-request.service';
import { ParticipationService } from '../participation/participation.service';
import { RunnerService } from '../runner/runner.service';
import { Runner } from 'src/schema/runner.schema';
import genOPSignature from 'src/utils/genOPSignature';

dayjs.extend(customParseFormat);
dayjs.extend(utc);
dayjs.extend(timezone);
dayjs.tz.setDefault('Asia/Ho_Chi_Minh');
dayjs().format();
// dayjs.extend(AdvancedFormat);

const VERIFY_TOKEN = 'MOMO_RUN_STRAVA_VERIFY_TOKEN_CHALLENGE';

@Injectable()
export class ActivityService {
  constructor(
    @InjectModel(Activity.name)
    private readonly activityModel: Model<ActivityDocument>,
    @Inject(forwardRef(() => RunnerService))
    private readonly runnerService: RunnerService,
    private readonly participationService: ParticipationService,
    @Inject(forwardRef(() => ChallengeService))
    private readonly challengeService: ChallengeService,
    @Inject(forwardRef(() => GroupParticipationRequestService))
    private readonly groupParticipationRequestService: GroupParticipationRequestService,
  ) {}

  async refreshAccessToken(refreshToken: string, runnerId: string) {
    try {
      const auth = await axios.post(
        'https://www.strava.com/api/v3/oauth/token',
        {
          client_id: '90924',
          client_secret: '6b9c5642ebae53a10e5796d3c8ebb893c3ddf54d',
          refresh_token: refreshToken,
          grant_type: 'refresh_token',
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      if (auth?.data) {
        const currentRunnerData =
          await this.runnerService.getStravaIdFromRunner(runnerId.toString());
        if (!currentRunnerData)
          throw new BadRequestException('Runner not found');

        const updateRunnerData = {
          stravaData: {
            ...currentRunnerData?.stravaData,
            access_token: auth?.data?.access_token,
            expires_at: auth?.data?.expires_at,
            expires_in: auth?.data?.expires_in,
            refresh_token: auth?.data?.refresh_token,
          },
        };
        const updateRunner = await this.runnerService.updateRunner(
          runnerId,
          updateRunnerData,
        );
        return updateRunner;
      }
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async getAthleteToken(athelteId: number) {
    try {
      const runner = await this.runnerService.findRunnerByStravaId(athelteId);
      if (!runner) throw new BadRequestException('Runner not found');
      const expiredTime = runner?.stravaData?.expires_at * 1000;
      if (Date.now() >= expiredTime) {
        const newAuth = await this.refreshAccessToken(
          runner?.stravaData?.refresh_token,
          runner?._id,
        );
        return {
          userID: newAuth?.data?.userID,
          runnerId: newAuth?.data?._id,
          access_token: newAuth?.data?.stravaData?.access_token,
        };
      } else {
        return {
          userID: runner?.userID,
          runnerId: runner?._id,
          access_token: runner?.stravaData?.access_token,
        };
      }
    } catch (error) {
      console.log('getAthleteToken: ', error);
      throw new BadRequestException(error);
    }
  }

  async getActivity(activityId: number, accessToken: string) {
    try {
      const response = await axios.get(
        `https://www.strava.com/api/v3/activities/${activityId}`,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`,
          },
        },
      );
      return response.data;
    } catch (e) {
      console.log('getActivity: ', e);
      throw new BadRequestException(e);
    }
  }

  async getLastestActivity(runnerId: string) {
    try {
      const lastestActivity = await this.activityModel
        .findOne({
          runnerId: runnerId?.toString(),
        })
        .sort({ start_date: -1 });
      return lastestActivity;
    } catch (error) {
      console.log('getLastestActivity: ', error);
      throw new BadRequestException(error);
    }
  }

  async getActivityStatsByRunnerId(runnerId: string) {
    try {
      const activities = await this.activityModel.find({
        runnerId: runnerId.toString(),
      });
      const athleteStats = await this.calculateAthleteStats(activities);
      return athleteStats;
    } catch (error) {
      console.log('getActivityStatsByRunnerId: ', error);
      throw new BadRequestException(error);
    }
  }

  async sendNotificationToHangout(dataNoti: any) {
    try {
      const data = JSON.stringify({
        text:
          `${'```'}Đã phát hiện hoạt động bất thường:\n----------------------------------------\nUser Name: ${
            dataNoti?.runnerName
          }\nUser ID: ${dataNoti?.runnerId}\nActivity ID : ${
            dataNoti?.activityId
          }\nActivity Strava ID : ${dataNoti?.activityStravaId}${'```'}` +
          `\n<${`https://www.strava.com/activities/${dataNoti?.activityStravaId}`}|Click here to view this activity>`,
      });
      const response = await axios.post(
        'https://chat.googleapis.com/v1/spaces/AAAAkBfx3ro/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=tBbEWoTzBgMoGX6jniqnt1o9DGhJ1S87WwobGQKiObc&threadKey=VKpNAFqoybg',
        data,
        {
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        },
      );

      return response.data;
    } catch (error) {
      throw new BadRequestException(error);
    }
  }

  async calculateInvalidSplitMetric(data: any) {
    try {
      const invalidSplits = [];

      if (Array.isArray(data)) {
        data.forEach((split) => {
          const pace = this.getPace(split?.moving_time, split?.distance);
          if (pace < 4) {
            invalidSplits.push(split);
          }
        });
      }

      const totalDistance = invalidSplits.reduce(
        (total, split) => total + split.distance,
        0,
      );

      return totalDistance || 0;
    } catch (error) {
      console.log('calculateInvalidSplitMetric: ', error);
      throw new BadRequestException(error);
    }
  }

  async createActivity(runnerId: string, activity: any, stravaId: number) {
    try {
      const activityCreatedAt = new Date(activity.start_date);

      const betweenActivityFromNow = dayjs(new Date()).diff(
        dayjs(activityCreatedAt),
        'minute',
      );

      let isExpired = false;
      const LIMIT_TIME_TO_CREATE_ACTIVITY = 12 * 60; // 12 hours
      if (betweenActivityFromNow > LIMIT_TIME_TO_CREATE_ACTIVITY) {
        isExpired = true;
      }

      let isAfterFinishTime = false;
      const finishTime = dayjs('2023-10-26T16:59:00.999+00:00')
        .utcOffset(7)
        .format('DD/MM/YYYY HH:mm:ss');

      const currentTime = dayjs(new Date())
        .utcOffset(7)
        .format('DD/MM/YYYY HH:mm:ss');

      if (currentTime > finishTime) {
        isAfterFinishTime = true;
      }

      const runner = await this.runnerService.findAuthenRunner(runnerId);
      if (!runner) throw new BadRequestException('Runner not found');

      const existActivity = await this.activityModel.findOne({
        activityId: activity.id,
      });
      if (existActivity)
        throw new BadRequestException('Activity already exist');

      // let isDuplicate = false;
      // const lastestActivity = await this.getLastestActivity(runnerId);
      // if (lastestActivity) {
      //   const currentEndTime = new Date(lastestActivity.start_date);
      //   currentEndTime.setSeconds(
      //     currentEndTime.getSeconds() + lastestActivity.elapsed_time,
      //   );

      //   const nextStartTime = new Date(activity.start_date);

      //   if (nextStartTime < currentEndTime) {
      //     isDuplicate = true;
      //   }
      // }

      const splits_metric = activity?.splits_metric || [];

      const invalidDistance = await this.calculateInvalidSplitMetric(
        splits_metric,
      );

      const percentInvalidDistance = Math.round(
        (invalidDistance / activity?.distance) * 100,
      );

      const isWarning =
        percentInvalidDistance >= 30 &&
        invalidDistance >= 1000 &&
        ['Run', 'Walk', 'TrailRun', 'Hike'].includes(activity.sport_type)
          ? true
          : false;

      const newActivityData = {
        activityId: activity.id,
        runnerId: runnerId?.toString(),
        stravaId: stravaId,
        distance: activity.distance,
        elapsed_time: activity.elapsed_time,
        from_accepted_tag: activity.from_accepted_tag,
        manual: activity.manual,
        private: activity.private,
        map: activity.map,
        max_speed: activity.max_speed,
        moving_time: activity.moving_time,
        name: activity.name,
        isExpired: isExpired,
        // isDuplicate: isDuplicate,
        external_id: activity.external_id,
        sport_type: activity.sport_type,
        start_date: activity.start_date,
        start_date_local: activity.start_date_local,
        splits_metric: splits_metric,
        invalidDistance: invalidDistance,
        isWarning: isWarning || isAfterFinishTime,
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date(),
      };
      const newActivity = await this.activityModel.create(newActivityData);

      if (isWarning) {
        await this.sendNotificationToHangout({
          runnerName: runner?.name,
          runnerId: runnerId,
          activityStravaId: activity.id,
          activityId: newActivity?._id?.toString(),
        });
      }

      const sendNoti = await this.sendNotifyCompletedActivity(runner);
      console.log('======> sendNoti:', {
        runner: runnerId,
        resNoti: sendNoti,
      });

      return {
        message: 'Success',
      };
    } catch (error) {
      console.log('createActivity: ', error);
      throw new BadRequestException(error);
    }
  }

  async updateActivity(
    activityId: string,
    activity: any,
    access_token?: string,
  ) {
    try {
      const activityCreatedAt = new Date(activity.start_date);

      const betweenActivityFromNow = dayjs(new Date()).diff(
        dayjs(activityCreatedAt),
        'minute',
      );

      const existActivity = await this.activityModel.findOne({
        activityId: activity.id,
      });

      if (!existActivity) throw new BadRequestException('Activity not found');

      const LIMIT_TIME_TO_UPDATE_ACTIVITY = 12 * 60; // 12 hours
      if (betweenActivityFromNow > LIMIT_TIME_TO_UPDATE_ACTIVITY) {
        const updateActivityData: any = {
          private: activity?.private,
          name: activity.name,
        };
        if (access_token && !(existActivity?.splits_metric?.length > 0)) {
          const dataAct = await this.getActivity(activity.id, access_token);

          const splits_metric = dataAct?.splits_metric || [];

          const invalidDistance = await this.calculateInvalidSplitMetric(
            splits_metric,
          );

          // Calculate percent of invalid distance / distance, if >= 30% => invalid activity
          const percentInvalidDistance = Math.round(
            (invalidDistance / dataAct?.distance) * 100,
          );

          const isWarning =
            percentInvalidDistance >= 30 &&
            invalidDistance >= 1000 &&
            ['Run', 'Walk', 'TrailRun', 'Hike'].includes(activity.sport_type)
              ? true
              : false;

          updateActivityData.splits_metric = splits_metric;
          updateActivityData.invalidDistance = invalidDistance;
          updateActivityData.isWarning = isWarning;

          // Update activity
          const updateActivity = await this.activityModel.findOneAndUpdate(
            { activityId: activityId },
            updateActivityData,
            { new: true },
          );
          return updateActivity;
        } else {
          // Update activity
          const updateActivity = await this.activityModel.findOneAndUpdate(
            { activityId: activityId },
            updateActivityData,
            { new: true },
          );
          return updateActivity;
        }
      }

      const updateActivityData = {
        name: activity.name,
        map: activity.map,
        private: activity.private,
        updatedAt: new Date(),
      };
      const updateActivity = await this.activityModel.findOneAndUpdate(
        { activityId: activityId },
        updateActivityData,
        { new: true },
      );
      return updateActivity;
    } catch (error) {
      console.log('updateActivity: ', error);
      throw new BadRequestException(error);
    }
  }

  async setDuplicateActivity(activityId: string) {
    try {
      const updateActivity = await this.activityModel.findOneAndUpdate(
        { activityId: activityId },
        {
          $set: {
            isDuplicate: true,
          },
        },
        { new: true },
      );
      return updateActivity;
    } catch (error) {
      console.log('setDuplicateActivity: ', error);
      throw new BadRequestException(error);
    }
  }

  async deleteActivity(activityId: string) {
    try {
      const deleteActivity = await this.activityModel.deleteOne({
        activityId: activityId,
      });
      return deleteActivity;
    } catch (error) {
      console.log('deleteActivity: ', error);
    }
  }

  async getStravaWebhook(req: Request) {
    const mode = req.query['hub.mode'];
    const token = req.query['hub.verify_token'];
    const challenge = req.query['hub.challenge'];
    // Checks if a token and mode is in the query string of the request
    if (mode && token) {
      // Verifies that the mode and token sent are valid
      if (mode === 'subscribe' && token === VERIFY_TOKEN) {
        // Responds with the challenge token from the request
        console.log('WEBHOOK_VERIFIED');
        return challenge;
      } else {
        // Responds with '403 Forbidden' if verify tokens do not match
        throw new BadRequestException('403 Forbidden');
      }
    }
  }

  async activitiyCheck(challenge: any, activities: any) {
    // Get unique activities
    const uniqueActivities = uniqBy(activities, 'activityId');
    // Remove manual activities
    let validActivities = uniqueActivities
      .filter(
        (activity) => activity?.manual !== true && activity?.max_speed > 0,
      )
      .slice(0);
    // console.log('====> validActivities:', validActivities);

    // Ignore cheat by: Tag athlete to activity group to share distance
    validActivities = validActivities
      .filter((activity) => activity.from_accepted_tag === false)
      .slice(0);
    // console.log('====> validActivities:', validActivities);

    // Ignore activities with private
    validActivities = validActivities
      .filter((activity) => activity?.private !== true)
      .slice(0);
    // console.log('====> validActivities:', validActivities);

    // Ignore activities duplicate
    validActivities = validActivities
      .filter((activity) => activity?.isDuplicate !== true)
      .slice(0);

    // Ignore activities expired
    validActivities = validActivities
      .filter((activity) => activity?.isExpired !== true)
      .slice(0);

    // Ignore activities warning
    validActivities = validActivities
      .filter((activity) => activity?.isWarning !== true)
      .slice(0);

    // Ignore activities with not included map
    validActivities = validActivities
      .filter(
        (activity) =>
          activity?.map?.summary_polyline || activity?.map?.polyline,
      )
      .slice(0);
    // console.log('====> validActivities:', validActivities);

    // Check min distance
    if (challenge?.minDistance > 0) {
      validActivities = validActivities
        .filter((activity) => activity.distance >= challenge.minDistance)
        .slice(0);
    }

    // Check sport type
    if (Array.isArray(challenge.activities)) {
      validActivities = validActivities
        .filter((activity) =>
          challenge.activities.includes(activity.sport_type),
        )
        .slice(0);
    }
    return validActivities.slice(0);
  }

  async getActivityInRange(
    runnerId: string,
    startDate: string,
    endDate: string,
  ) {
    try {
      const runner = await this.runnerService.findInfoRunner(runnerId);
      if (!runner) throw new BadRequestException('Runner not found');

      if (!startDate || !endDate) {
        throw new BadRequestException('Start date or end date is required');
      }

      // Get all activities of runner in range
      const startOfDay = dayjs(new Date(startDate));
      const endOfDay = dayjs(new Date(endDate)).endOf('day');

      let activitiesInRange;

      if (startDate && endDate) {
        activitiesInRange = await this.activityModel.find({
          runnerId: runner?._id?.toString(),
          isInvalid: false,
          start_date: {
            $gte: startOfDay.toDate(),
            $lte: endOfDay.toDate(),
          },
          // $or: [
          //   {
          //     start_date: {
          //       $gte: startOfDay.toDate(),
          //       $lte: endOfDay.toDate(),
          //     },
          //   },
          //   // {
          //   //   start_date_local: {
          //   //     $gte: startOfDay.toDate(),
          //   //     $lte: endOfDay.toDate(),
          //   //   },
          //   // },
          // ],
        });
      } else {
        activitiesInRange = [];
      }

      return activitiesInRange;
    } catch (error) {
      console.log('getActivityInRange: ', error);
      throw new BadRequestException(error);
    }
  }

  getPace(moving_time: number, distance: number) {
    const milliseconds = moving_time;
    const pacems = milliseconds / (distance / 1000) || 0;
    const minute = Math.floor(pacems / 60);
    const second = pacems % 60;
    return minute + second / 100;
  }

  async calculateAthleteStats(activities: any) {
    // Count distance and moving time in all activities
    let distance = 0;
    let moving_time = 0;
    let elapsed_time = 0;
    let invalidDistance = 0;

    if (Array.isArray(activities)) {
      activities.forEach((ac) => {
        distance += ac.distance || 0;
        moving_time += ac.moving_time || 0;
        elapsed_time += ac.elapsed_time || 0;
        invalidDistance += ac.invalidDistance || 0;
      });
    }

    // Prepare data for syncing athlete data to challenge
    const athleteStats = {
      distance,
      validDistance:
        Number(distance - invalidDistance) < 0
          ? 0
          : Number(distance - invalidDistance),
      moving_time,
      elapsed_time,
      invalidDistance,
    };
    return athleteStats;
  }

  async getValidActivity(challenge: any, activities: any) {
    try {
      let validActivities = await this.activitiyCheck(challenge, activities);

      // Get activities valid in pace
      if (
        challenge?.paceRange &&
        Array.isArray(challenge?.paceRange) &&
        validActivities?.length > 0
      ) {
        const min = challenge.paceRange?.[0] || 0;
        const max = challenge.paceRange?.[1] || 20;
        const activitiesWithPaces = [];
        validActivities.forEach((ac) => {
          const pace = this.getPace(ac.moving_time, ac.distance);
          if (min <= pace && pace <= max) {
            const validActivity = {
              invalidDistance: ac.invalidDistance,
              distance: ac.distance,
              moving_time: ac.moving_time,
              elapsed_time: ac.elapsed_time,
              pace,
              id: ac.id,
            };
            activitiesWithPaces.push(validActivity);
          }
        });
        validActivities = activitiesWithPaces.slice(0);
      }

      return validActivities;
    } catch (error) {
      console.log('getValidActivity: ', error);
    }
  }

  async getAthleteStatsInChallenge(
    runnerId: string,
    challenge: any,
    startTime: string,
  ) {
    try {
      const runner = await this.runnerService.findInfoRunner(runnerId);
      if (!runner) throw new BadRequestException('Runner not found');

      let startDate = challenge.startTime;
      if (
        challenge?.isCheckParticipantTime == true &&
        new Date(startTime) >= new Date(challenge.startTime)
      ) {
        startDate = startTime;
      }

      // Get all activities of runner in range
      const activities = await this.getActivityInRange(
        runnerId,
        startDate,
        challenge.endTime,
      );

      // Get valid activities in challenge
      const validActivities = await this.getValidActivity(
        challenge,
        activities,
      );
      // console.log('====> validActivities:', validActivities);

      // Calculate stats of those activities
      const athleteStats = await this.calculateAthleteStats(validActivities);

      const athleteStatsData = {
        challengeId: challenge._id?.toString(),
        runnerId: runnerId?.toString(),
        ...athleteStats,
      };

      return athleteStatsData;
    } catch (error) {
      console.log('getAthleteStatsInChallenge: ', error);
    }
  }

  async getAthleteStatsInGroupParticipationByChallenge(
    groupInfo: any,
    challenge: any,
    runnerId: string,
  ) {
    try {
      const runner = await this.runnerService.findInfoRunner(runnerId);
      if (!runner) throw new BadRequestException('Runner not found');

      let startDate = challenge?.startTime;
      if (new Date(groupInfo?.createdAt) >= new Date(challenge?.startTime)) {
        startDate = groupInfo?.createdAt;
      }

      // Get all activities of runner in range
      const activities = await this.getActivityInRange(
        runnerId,
        startDate,
        challenge?.endTime,
      );

      // Get valid activities in challenge
      const validActivities = await this.getValidActivity(
        challenge,
        activities,
      );

      // Calculate stats of those activities
      const athleteStats = await this.calculateAthleteStats(validActivities);

      const athleteStatsData = {
        challengeId: challenge._id?.toString(),
        runnerId: runnerId?.toString(),
        groupId: groupInfo?._id?.toString(),
        ...athleteStats,
      };

      return athleteStatsData;
    } catch (error) {
      console.log('getAthleteStatsInGroupParticipationByChallenge: ', error);
    }
  }

  // Recieive Strava Webhook
  async receiveStravaWebhook(data: any) {
    console.log('===> receiveStravaWebhook >>> data:', data);
    // const fakeData = {
    //   aspect_type: 'update',
    //   event_time: 1697600158,
    //   object_id: 10052845300,
    //   object_type: 'activity',
    //   owner_id: 14813325,
    //   subscription_id: 244918,
    //   updates: { private: 'true', visibility: 'only_me' },
    // };

    try {
      const { object_type, object_id, aspect_type, owner_id } = data;
      if (
        object_type === 'activity' &&
        ['create', 'update'].includes(aspect_type)
      ) {
        const activityId = object_id;
        const athelteId = owner_id;

        // Get access token
        const { access_token, runnerId } = await this.getAthleteToken(
          athelteId,
        );

        // Get activity by token
        const activity = await this.getActivity(activityId, access_token);

        let data;

        const existedActivity = await this.activityModel.findOne({
          activityId: activity.id,
        });

        // Update activity
        if (aspect_type === 'update' && existedActivity) {
          data = await this.updateActivity(
            existedActivity?.activityId,
            activity,
            access_token,
          );
        }

        // Create activity
        if (aspect_type === 'create' && !existedActivity) {
          data = await this.createActivity(runnerId, activity, athelteId);
        }

        // Delete activity
        if (aspect_type === 'delete' && existedActivity) {
          data = await this.deleteActivity(existedActivity?.activityId);
        }

        // Get participation of athlete
        const participations =
          await this.participationService.getParticipationByRunnerId(runnerId);

        if (Array.isArray(participations) && participations?.length > 0) {
          participations.forEach(async (participation) => {
            // Check if challenge is active
            const challenge = await this.challengeService.findChallengeById(
              participation.challengeId,
            );

            if (challenge && challenge?.endTime > new Date()) {
              const athleteStats = await this.getAthleteStatsInChallenge(
                runnerId,
                challenge,
                participation?.createdAt,
              );

              const athleteStatsData = {
                athleteId: athelteId,
                ...athleteStats,
              };
              // console.log(
              //   'receiveStravaWebhook -> invidualStats:',
              //   athleteStatsData,
              // );

              // Save stats to this challenge
              await this.participationService.updateParticipation(
                challenge._id,
                runnerId,
                athleteStatsData,
              );

              if (challenge?.type == 'group') {
                const groupParticipationInfo =
                  await this.groupParticipationRequestService.findCurrentRequestByChallenge(
                    challenge._id?.toString(),
                    runnerId,
                  );
                if (groupParticipationInfo) {
                  const athleteStatsGroup =
                    await this.getAthleteStatsInGroupParticipationByChallenge(
                      groupParticipationInfo,
                      challenge,
                      runnerId,
                    );
                  // console.log(
                  //   'receiveStravaWebhook -> groupStats:',
                  //   athleteStatsGroup,
                  // );

                  // Save stats to this group request
                  await this.groupParticipationRequestService.updateStatsGroupParticipationRequest(
                    groupParticipationInfo._id,
                    runnerId,
                    athleteStatsGroup,
                  );
                }
              }
            }
          });
        }

        return {
          message: 'Success',
          // data: data,
        };
      }
      return null;
    } catch (error) {
      console.log('receiveStravaWebhook: ', error);
    }
  }

  // Get list activity by runnerId and challengeId (optional)
  async getListActivityByRunner(runnerId: string, challengeId?: string) {
    const generateMessageInvalid = (
      activity: any,
      challengeInfo: any,
      min: any,
      max: any,
      pace: any,
    ) => {
      if (activity?.manual === true) {
        return 'Hoạt động cập nhật thủ công.';
      } else if (activity?.isDuplicate == true) {
        return 'Hoạt động trùng lặp.';
      } else if (activity?.isExpired == true) {
        return `Hoạt động được câp nhật quá 12 giờ sau khi hoàn thành.\nThời gian cập nhật: ${dayjs(
          activity?.createdAt,
        )
          .utcOffset(7)
          .format('HH:mm DD/MM/YYYY')}`;
      } else if (activity?.max_speed <= 0) {
        return 'Hoạt động không có tốc độ.';
      } else if (activity?.from_accepted_tag === true) {
        return 'Hoạt động được chia sẻ bởi người khác.';
      } else if (activity?.private === true) {
        return 'Hoạt động riêng tư.';
      } else if (!activity?.map?.summary_polyline && !activity?.map?.polyline) {
        return 'Hoạt động không có bản đồ GPS.';
      } else if (activity?.distance < challengeInfo?.minDistance) {
        return 'Hoạt động không đủ khoảng cách tối thiểu.';
      } else if (!challengeInfo?.activities?.includes(activity?.sport_type)) {
        return 'Hoạt động có type không được chấp nhận.';
      } else if (min > pace || pace > max) {
        return 'Hoạt động có pace trung bình không hợp lệ.';
      } else if (activity?.isWarning == true) {
        return `Hoạt động bị đánh dấu không hợp lệ.`;
      } else {
        return '';
      }
    };

    try {
      // let objectId;

      // if (Types.ObjectId.isValid(runnerId)) {
      //   objectId = Types.ObjectId.createFromHexString(runnerId);
      // }

      // const query = objectId ? { runnerId: objectId } : { runnerId: runnerId };
      if (challengeId) {
        const challengeInfo = await this.challengeService.findChallengeById(
          challengeId,
        );
        const participationInfo =
          await this.participationService.getParticipationByRunnerIdAndChallengeId(
            runnerId,
            challengeId,
          );
        if (challengeInfo && participationInfo) {
          const startTime = dayjs(new Date(challengeInfo?.startTime));
          const endTime = dayjs(new Date(challengeInfo?.endTime)).endOf('day');

          let activitiesInRange;

          if (startTime && endTime) {
            activitiesInRange = await this.activityModel
              .find({
                runnerId: runnerId?.toString(),
                isInvalid: false,
                $or: [
                  {
                    start_date: {
                      $gte: startTime.toDate(),
                      $lte: endTime.toDate(),
                    },
                  },
                  // {
                  //   start_date_local: {
                  //     $gte: startTime.toDate(),
                  //     $lte: endTime.toDate(),
                  //   },
                  // },
                ],
              })
              .sort({ start_date: -1 });
          } else {
            activitiesInRange = [];
          }
          const min = challengeInfo.paceRange?.[0] || 0;
          const max = challengeInfo.paceRange?.[1] || 20;
          const dataActivities =
            activitiesInRange?.map((activity) => {
              const pace = this.getPace(
                activity.moving_time,
                activity.distance,
              );
              const validActivity =
                activity?.manual !== true &&
                activity?.max_speed > 0 &&
                activity?.from_accepted_tag === false &&
                activity?.private !== true &&
                activity?.isDuplicate !== true &&
                activity?.isExpired !== true &&
                activity?.isWarning !== true &&
                (activity?.map?.summary_polyline || activity?.map?.polyline) &&
                activity?.distance >= challengeInfo?.minDistance &&
                challengeInfo?.activities?.includes(activity?.sport_type) &&
                min <= pace &&
                pace <= max;
              return {
                ...activity.toJSON(),
                isInValid: !validActivity,
                reasonInvalid: generateMessageInvalid(
                  activity,
                  challengeInfo,
                  min,
                  max,
                  pace,
                ),
              };
            }) || [];

          return dataActivities?.sort(
            (a, b) =>
              new Date(b?.start_date).getTime() -
              new Date(a?.start_date).getTime(),
          );

          // const test = data.map((item) => ({
          //   startTime: item?.start_date,
          //   startTimeLocal: item?.start_date_local,
          //   distance: item?.distance,
          // }));

          // const filterData = test.filter((item) => {
          //   const startTime = dayjs(item.startTime).utcOffset(7);
          //   const hours = startTime.hour();
          //   const minutes = startTime.minute();

          //   if (
          //     (hours === 9 && minutes >= 30 && hours < 11) ||
          //     (hours === 13 && minutes >= 30) ||
          //     (hours > 9 && hours < 11) ||
          //     (hours === 11 && minutes <= 30) ||
          //     (hours === 13 && minutes >= 30) ||
          //     (hours > 13 && hours < 18) ||
          //     (hours === 18 && minutes <= 0)
          //   ) {
          //     return item;
          //   }
          // });

          // return filterData.map((item) => ({
          //   startTime: dayjs(item.startTime)
          //     .utcOffset(7)
          //     .format('DD/MM/YYYY HH:mm:ss'),
          //   distance: item.distance,
          // }));
        }
      }
      const activities = await this.activityModel
        .find({
          runnerId: runnerId?.toString(),
        })
        .sort({ start_date: -1 })
        .limit(10);
      return activities;
    } catch (error) {
      console.log('getListActivityByRunner: ', error);
    }
  }

  async getListActivities(accessToken: string) {
    try {
      const now = dayjs();
      const twoDaysAgo = now.subtract(2, 'day'); // 2 days ago
      const afterTimeInSeconds = twoDaysAgo.unix(); // Convert to seconds

      const response = await axios.get(
        `https://www.strava.com/api/v3/athlete/activities?after=${afterTimeInSeconds}&per_page=30`,
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${accessToken}`,
          },
        },
      );
      return response.data;
    } catch (e) {
      console.log('getListActivities: ', e);
      throw new BadRequestException(e);
    }
  }

  async actionCreateOrUpdateActivity(
    runnerId: string,
    activity: any,
    stravaId: number,
    access_token?: string,
  ) {
    try {
      const existedActivity = await this.activityModel.findOne({
        activityId: activity.id,
      });
      if (existedActivity) {
        // Update activity
        await this.updateActivity(
          existedActivity?.activityId,
          activity,
          access_token,
        );
      } else {
        // Create activity
        await this.createActivity(runnerId, activity, stravaId);
      }
    } catch (error) {
      console.log('actionCreateOrUpdateActivity: ', error);
      throw new BadRequestException(error);
    }
  }

  // Sync activities from Strava
  async syncActivitiesFromStrava(runnerId: string) {
    try {
      const runner = await this.runnerService.getStravaIdFromRunner(runnerId);

      // Check if runner is linked with Strava
      const athelteId = runner?.stravaData?.athlete?.id || null;
      if (!athelteId)
        throw new BadRequestException('Vận động viên chưa liên kết với Strava');

      // Get access token
      const { access_token } = await this.getAthleteToken(athelteId);

      // Get activities from Strava
      const activities = await this.getListActivities(access_token);
      if (Array.isArray(activities)) {
        activities.forEach((ac) => {
          this.actionCreateOrUpdateActivity(
            runnerId,
            ac,
            athelteId,
            access_token,
          );
        });
      }

      // Get participation of athlete
      const participations =
        await this.participationService.getParticipationByRunnerId(runnerId);

      if (Array.isArray(participations) && participations?.length > 0) {
        participations.forEach(async (participation) => {
          // Check if challenge is active
          const challenge = await this.challengeService.findChallengeById(
            participation.challengeId,
          );

          if (challenge && new Date(challenge?.endTime) > new Date()) {
            const athleteStats = await this.getAthleteStatsInChallenge(
              runnerId,
              challenge,
              participation?.createdAt,
            );

            const athleteStatsData = {
              athleteId: athelteId,
              ...athleteStats,
            };
            console.log(
              '=====> syncActivitiesFromStrava -> athleteStatsData:',
              athleteStatsData,
            );

            // Save stats to this challenge
            await this.participationService.updateParticipation(
              challenge._id,
              runnerId,
              athleteStatsData,
            );

            if (challenge?.type == 'group') {
              const groupParticipationInfo =
                await this.groupParticipationRequestService.findCurrentRequestByChallenge(
                  challenge._id?.toString(),
                  runnerId,
                );
              if (groupParticipationInfo) {
                const athleteStatsGroup =
                  await this.getAthleteStatsInGroupParticipationByChallenge(
                    groupParticipationInfo,
                    challenge,
                    runnerId,
                  );

                console.log(
                  '====> syncActivitiesFromStrava -> athleteStatsGroup:',
                  athleteStatsGroup,
                );

                // Save stats to this group request
                await this.groupParticipationRequestService.updateStatsGroupParticipationRequest(
                  groupParticipationInfo._id,
                  runnerId,
                  athleteStatsGroup,
                );
              }
            }
          }
        });
      }
      return {
        message: 'Sync activities successfully',
      };
    } catch (error) {
      console.log('syncActivitiesFromStrava: ', error);
      throw new BadRequestException(error);
    }
  }

  private delay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  async syncStatsChallengeByRunner(runnerId: string) {
    try {
      const runner = await this.runnerService.getStravaIdFromRunner(runnerId);
      if (!runner) throw new BadRequestException('Runner not found');

      const athelteId = runner?.stravaData?.athlete?.id || 18232578;
      if (!athelteId) throw new BadRequestException('Runner not found');

      const participations =
        await this.participationService.getParticipationByRunnerId(runnerId);

      if (Array.isArray(participations) && participations?.length > 0) {
        participations.forEach(async (participation) => {
          // Check if challenge is active
          const challenge = await this.challengeService.findChallengeById(
            participation.challengeId,
          );

          if (challenge && new Date(challenge?.endTime) > new Date()) {
            const athleteStats = await this.getAthleteStatsInChallenge(
              runnerId,
              challenge,
              participation?.createdAt,
            );

            const athleteStatsData = {
              athleteId: athelteId,
              ...athleteStats,
            };
            console.log(
              '=====> syncStatsChallengeByRunner -> athleteStatsData:',
              athleteStatsData,
            );

            // Save stats to this challenge
            await this.participationService.updateParticipation(
              challenge._id,
              runnerId,
              athleteStatsData,
            );

            if (challenge?.type == 'group') {
              const groupParticipationInfo =
                await this.groupParticipationRequestService.findCurrentRequestByChallenge(
                  challenge._id?.toString(),
                  runnerId,
                );
              if (groupParticipationInfo) {
                const athleteStatsGroup =
                  await this.getAthleteStatsInGroupParticipationByChallenge(
                    groupParticipationInfo,
                    challenge,
                    runnerId,
                  );

                console.log(
                  '=====> syncStatsChallengeByRunner -> athleteStatsGroup:',
                  athleteStatsGroup,
                );

                // Save stats to this group request
                await this.groupParticipationRequestService.updateStatsGroupParticipationRequest(
                  groupParticipationInfo._id,
                  runnerId,
                  athleteStatsGroup,
                );
              }
            }
          }
        });
      }

      return {
        message: 'Sync stats challenge by runner successfully',
      };
    } catch (error) {
      console.log('syncStatsChallengeByRunner: ', error);
      throw new BadRequestException(error);
    }
  }

  async checkDuplicateActivity(runnerId: string) {
    try {
      const startTime = dayjs(new Date('2023-10-05T17:02:00.000+00:00'));
      const endTime = dayjs(new Date('2023-10-26T16:59:59.999+00:00')).endOf(
        'day',
      );
      const activities = await this.activityModel
        .find({
          runnerId: runnerId,
          $or: [
            {
              start_date: {
                $gte: startTime.toDate(),
                $lte: endTime.toDate(),
              },
            },
          ],
        })
        .select(
          'start_date start_date_local elapsed_time activityId moving_time runnerId',
        );

      activities.sort((a, b) => {
        const dateA = new Date(a.start_date);
        const dateB = new Date(b.start_date);
        return dateA.getTime() - dateB.getTime();
      });

      const duplicateActivities = [];
      for (let i = 0; i < activities.length - 1; i++) {
        const currentActivity = activities[i];
        const nextActivity = activities[i + 1];

        const currentEndTime = new Date(currentActivity.start_date);
        currentEndTime.setSeconds(
          currentEndTime.getSeconds() + currentActivity.moving_time,
        );

        const nextStartTime = new Date(nextActivity.start_date);

        // Check if the next activity starts before the current one ends
        if (nextStartTime < currentEndTime) {
          duplicateActivities.push(currentActivity, nextActivity);
        }
      }

      const groupedData = {};

      // Iterate through the data and group by the same date (DD/MM/YYYY)
      duplicateActivities.forEach((activity) => {
        // const startDateLocal = new Date(activity.start_date);
        const formattedDate = dayjs(activity?.start_date)
          .utcOffset(7)
          .format('DD/MM/YYYY');

        if (!groupedData[formattedDate]) {
          groupedData[formattedDate] = [];
        }

        groupedData[formattedDate].push(activity);
      });

      // const numberOfDuplicateActivities = Object.keys(groupedData).length;
      // for (let i = 0; i < numberOfDuplicateActivities; i++) {
      //   const key = Object.keys(groupedData)[i];
      //   const value = groupedData[key];
      //   const acceptedActivity = value[0];
      //   // duplicate activity is items remain
      //   const duplicateActivity = value.slice(1);
      //   // console.log('======> duplicateActivity:', duplicateActivity);
      //   // Set duplicate activity for items remain
      //   // const promise = [];
      //   // duplicateActivity.forEach(async (activity) => {
      //   //   promise.push(this.setDuplicateActivity(activity.activityId));
      //   // });
      //   // await Promise.all(promise);
      // }
      // await this.syncStatsChallengeByRunner(runnerId);

      // return groupedData;

      function findDuplicateItems(data) {
        const result = [];

        for (const date in data) {
          const activities = data[date];

          // Sort activities by start_date_local
          activities.sort(
            (a, b) =>
              new Date(a.start_date).getTime() -
              new Date(b.start_date).getTime(),
          );

          for (let i = 0; i < activities.length; i++) {
            const current = activities[i];
            const startTime = new Date(current.start_date).getTime();
            const endTime = startTime + current.moving_time * 1000;

            if (i > 0) {
              const previous = activities[i - 1];
              const prevEndTime =
                new Date(previous.start_date).getTime() +
                previous.moving_time * 1000;

              if (startTime < prevEndTime) {
                result.push(current);
              }
            }

            if (i < activities.length - 1) {
              const next = activities[i + 1];
              const nextStartTime = new Date(next.start_date).getTime();

              if (endTime > nextStartTime && i !== 0) {
                result.push(current);
              }
            }
          }
        }

        return result;
      }

      const duplicates = findDuplicateItems(groupedData);
      // for (let i = 0; i < duplicates?.length; i++) {
      //   // Set duplicate activity for items remain
      //   const promise = [];
      //   duplicates.forEach(async (activity) => {
      //     // promise.push(this.setDuplicateActivity(activity.activityId));
      //   });
      //   await Promise.all(promise);
      // }
      // await this.syncStatsChallengeByRunner(runnerId);

      // console.log('====> runnerId:', duplicates?.[0]?.runnerId);
      return {
        user: duplicates?.[0]?.runnerId,
        activities: duplicates?.map((item) => item.activityId),
      };
    } catch (error) {
      console.log('getActivitiesByRunnerId: ', error);
      throw new BadRequestException(error);
    }
  }

  async checkDuplicateActivityAllRunner() {
    try {
      const runners = await this.runnerService.getAllUser();
      const promise = [];
      runners.forEach((runner) => {
        promise.push(this.checkDuplicateActivity(runner._id?.toString()));
        // promise.push(this.syncStatsChallengeByRunner(runner._id));
      });
      const data = await Promise.all(promise);

      const removeNull = data?.filter((item) => item?.user);
      console.log('==== removeNull:', removeNull);

      // const userHasDuplicateActivityCount = data.filter(
      //   (item) => item > 0,
      // )?.length;

      return {
        data: {
          // userCount: userHasDuplicateActivityCount,
          duplicateActivityCount: removeNull,
        },
        message: 'Check duplicate activity successfully',
      };
    } catch (error) {
      console.log('checkDuplicateActivityAllRunner: ', error);
      throw new BadRequestException(error);
    }
  }

  async sendNotifyCompletedActivity(runner: Runner) {
    const timestamp = Date.now();

    const body = {
      templateId: 'MOMO_RUN_KDNKBY',
      id: timestamp,
      lang: 'en',
      variables: {
        Status: 'Success',
      },
    };

    const queryParam = `partnerUserId=${runner?.partnerUserId}`;

    const OP_Signature_Push_Noti = genOPSignature(body, timestamp);
    const OP_Signature_Get_Access_Token = genOPSignature(queryParam, timestamp);

    try {
      if (runner?.authCode && runner?.partnerUserId) {
        // Get access_token
        const resAccessToken = await axios.get(
          `https://openapi.momo.vn/gateway/open/v1/oauth/accessToken?partnerUserId=${runner?.partnerUserId}`,
          {
            headers: {
              Authorization: 'Bearer ' + runner?.authCode,
              'OP-Signature': OP_Signature_Get_Access_Token,
              'M-Timestamp': timestamp,
            },
          },
        );

        const access_token = resAccessToken?.data?.accessToken || null;

        if (!access_token) {
          console.log('Lấy access_token thất bại!');
          return;
        }

        // Send notify
        const res = await axios.post(
          `https://openapi.momo.vn/gateway/open/v1/msd/notifications?partnerUserId=${runner?.partnerUserId}`,
          body,
          {
            headers: {
              Authorization: 'Bearer ' + access_token,
              'Content-type': 'application/json',
              'OP-Signature': OP_Signature_Push_Noti,
              'M-Timestamp': timestamp,
            },
          },
        );
        console.log('sendNotifyCompletedActivity -> res:', res.data);
      }

      return {};
    } catch (error) {
      console.log('sendNotifyCompletedActivity: ', error);
      throw new BadRequestException(error.response.data);
    }
  }

  // // Get data activities of user and group by day
  async getTrendOfUser(runnerId: string, challengeId: string) {
    try {
      // Get info of runner
      const runner = await this.runnerService.findInfoRunner(runnerId);

      // Get all activities of runner
      const data = await this.getListActivityByRunner(
        runnerId?.toString(),
        challengeId,
      );

      // Get data of activities is valid
      const activities = data
        .filter((activity) => activity?.isInValid !== true)
        ?.map((activity) => ({
          activityId: activity?.activityId,
          runnerId: activity?.runnerId,
          stravaId: activity?.stravaId,
          distance: activity?.distance,
          start_date: activity?.start_date,
          start_date_local: activity?.start_date_local,
          isInValid: activity?.isInValid,
        }));

      // Group activities by start_date
      const groupByDate = activities?.reduce((acc, activity) => {
        const date = dayjs(activity?.start_date)
          .utcOffset(7)
          .format('MM/DD/YYYY');
        if (!acc[date]) {
          acc[date] = [];
        }
        acc[date].push(activity);
        return acc;
      }, {});

      // Sort by date
      const sortedGroupByDate = Object.keys(groupByDate)
        ?.sort((a, b) => new Date(b).getTime() - new Date(a).getTime())
        ?.reduce((acc, key) => {
          acc[key] = groupByDate[key];
          return acc;
        }, {});

      // Create new variable and Calculate total distance of each day
      Object.keys(sortedGroupByDate)?.forEach((key) => {
        const totalDistance = sortedGroupByDate[key]
          ?.reduce((acc, activity) => acc + activity?.distance, 0)
          ?.toFixed(2);
        sortedGroupByDate[key] = {
          totalDistance,
          activities: sortedGroupByDate[key],
        };
      });

      // Return data with date and total distance
      const dataDistance = Object.keys(sortedGroupByDate)?.map((key) => {
        return {
          date: key,
          totalDistance: sortedGroupByDate[key]?.totalDistance,
        };
      });

      return {
        trend: dataDistance,
        runner: {
          runnerId: runner?._id?.toString(),
          name: runner?.name,
        },
      };
    } catch (error) {
      console.log('getTrendOfUser: ', error);
      throw new BadRequestException(error);
    }
  }
}
