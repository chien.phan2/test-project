import {
  Controller,
  Get,
  Param,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { Request } from 'express';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { ActivityService } from './activity.service';

@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
@ApiTags('Challenge')
@Controller('challenge/activities')
export class ActivityController {
  constructor(private readonly activityService: ActivityService) {}

  @Get('/get-list-activities/:id')
  @ApiQuery({
    name: 'challengeId',
    required: false,
    type: String,
  })
  getListActivity(
    @Param('id') id: string,
    @Query('challengeId') challengeId: string,
  ) {
    return this.activityService.getListActivityByRunner(id, challengeId);
  }

  @Get('/sync-activities-from-strava')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  syncActivitiesFromStrava(@Req() req: Request) {
    const userID = req.userID;
    return this.activityService.syncActivitiesFromStrava(userID);
  }

  @Get('/sync-stats-challenge-by-runner/:id')
  syncStatsChallengeByRunner(@Param('id') id: string) {
    return this.activityService.syncStatsChallengeByRunner(id);
  }

  @Get('sync-activities-from-strava-by-runner/:id')
  syncActivitiesFromStravaByRunner(@Param('id') id: string) {
    return this.activityService.syncActivitiesFromStrava(id);
  }

  @Get('/check-duplicate-activity-by-runner/:id')
  checkDuplicateActivity(@Param('id') id: string) {
    return this.activityService.checkDuplicateActivity(id);
  }

  @Get('/check-duplicate-activity-for-all-runner')
  checkDuplicateActivityAllRunner() {
    return this.activityService.checkDuplicateActivityAllRunner();
  }

  // @Get('test-push-notification')
  // testPushNotification() {
  //   return this.activityService.sendNotifyCompletedActivity();
  // }

  @Get('/trend/:runnerId/:challengeId')
  @ApiOperation({
    description: 'Get the change in distance of that runner by day.',
  })
  getTrendOfUser(
    @Param('runnerId') runnerId: string,
    @Param('challengeId') challengeId: string,
  ) {
    return this.activityService.getTrendOfUser(runnerId, challengeId);
  }
}
