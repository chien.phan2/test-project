import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateParticipationDto {
  @ApiProperty()
  @IsNotEmpty()
  challengeId: string;
}
