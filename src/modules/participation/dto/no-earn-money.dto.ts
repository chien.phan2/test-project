import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean } from 'class-validator';

export class NoEarnMoneyDto {
  @ApiProperty({ default: false, required: true })
  @IsBoolean()
  isNoEarn: boolean;
}
