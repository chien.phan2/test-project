import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { Request } from 'express';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { CreateParticipationDto } from './dto/create-participation.dto';
import { ParticipationService } from './participation.service';
import { NoEarnMoneyDto } from './dto/no-earn-money.dto';

@ApiTags('Challenge')
@Controller('challenge')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class ParticipationController {
  constructor(private readonly participationService: ParticipationService) {}

  @Post('/join-challenge')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  joinChallenge(
    @Body() createParticipationDto: CreateParticipationDto,
    @Req() req: Request,
  ) {
    const userID = req.userID;
    return this.participationService.createParticipation(
      createParticipationDto,
      userID,
    );
  }

  @Post('/leave-challenge')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  leaveChallenge(
    @Body() CreateParticipationDto: CreateParticipationDto,
    @Req() req: Request,
  ) {
    const userID = req.userID;
    return this.participationService.deleteParticipation(
      CreateParticipationDto,
      userID,
    );
  }

  @Get('/my-challenge')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  getMyChallenge(@Req() req: Request) {
    const userID = req.userID;
    return this.participationService.getParticipationByRunnerId(userID);
  }

  @Get('challenge-detail/:id')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  getListAthletesOfChallenge(@Req() req: Request, @Param('id') id: string) {
    const userID = req.userID;
    return this.participationService.getChallengeDetail(userID, id);
  }

  @Patch('/participation/no-earn-money')
  updateParticipationNoEarn(
    @Query('runnerId') runnerId: string,
    @Query('challengeId') challengeId: string,
    @Body() updateParticipationPayload: NoEarnMoneyDto,
  ) {
    return this.participationService.updateParticipationNoEarn(
      runnerId,
      challengeId,
      updateParticipationPayload,
    );
  }

  @Get('/get-ranking-athlete/:challengeId')
  @ApiOperation({
    description:
      'Get the list ranking of runners in that challenge, support filter by sex.',
  })
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  @ApiQuery({
    name: 'sex',
    required: false,
    type: String,
  })
  getRankingAthlete(
    @Req() req: Request,
    @Param('challengeId') challengeId: string,
    @Query('sex') sex: string,
  ) {
    const userID = req.userID;
    return this.participationService.getRankingOfRunner(
      userID,
      challengeId,
      sex,
    );
  }
}
