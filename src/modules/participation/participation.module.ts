import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  Participation,
  ParticipationSchema,
} from 'src/schema/participation.schema';
import { BudgetHistoryModule } from '../budget-history/budget-history.module';
import { ChallengeModule } from '../challenge/challenge.module';
import { EnergyTransactionModule } from '../energy-transaction/energy-transaction.module';
import { EnergyModule } from '../energy/energy.module';
import { InventoryModule } from '../inventory/inventory.module';
import { ProductTransactionModule } from '../product-transaction/product-transaction.module';
import { RunnerModule } from '../runner/runner.module';
import { ParticipationController } from './participation.controller';
import { ParticipationService } from './participation.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Participation.name, schema: ParticipationSchema },
    ]),
    forwardRef(() => ChallengeModule),
    forwardRef(() => RunnerModule),
    InventoryModule,
    EnergyTransactionModule,
    forwardRef(() => BudgetHistoryModule),
    EnergyModule,
    ProductTransactionModule,
  ],
  providers: [ParticipationService],
  exports: [ParticipationService],
  controllers: [ParticipationController],
})
export class ParticipationModule {}
