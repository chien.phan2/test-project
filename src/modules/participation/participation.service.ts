import {
  BadRequestException,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import {
  Participation,
  ParticipationDocument,
} from 'src/schema/participation.schema';
import { ActionType, RewardCode } from 'src/utils/constants';
import { BudgetHistoryService } from '../budget-history/budget-history.service';
import { ChallengeService } from '../challenge/challenge.service';
import { EnergyService } from '../energy/energy.service';
import { InventoryService } from '../inventory/inventory.service';
import { ProductTransactionService } from '../product-transaction/product-transaction.service';
import { RunnerService } from '../runner/runner.service';
import { CreateParticipationDto } from './dto/create-participation.dto';

@Injectable()
export class ParticipationService {
  constructor(
    @InjectModel(Participation.name)
    private readonly participationModel: Model<ParticipationDocument>,

    @Inject(forwardRef(() => ChallengeService))
    private readonly challengeService: ChallengeService,

    @Inject(forwardRef(() => RunnerService))
    private readonly runnerService: RunnerService,

    private readonly inventoryService: InventoryService,

    @Inject(forwardRef(() => BudgetHistoryService))
    private readonly budgetHistoryService: BudgetHistoryService,

    private readonly energyService: EnergyService,

    private readonly productTransactionService: ProductTransactionService,

    @InjectConnection() private connection: Connection,
  ) {}

  async createParticipation(
    createParticipation: CreateParticipationDto,
    runnerId: string,
  ) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      let isNoEarn = false;

      const existedParticipation = await this.participationModel.findOne({
        runnerId,
        challengeId: createParticipation.challengeId,
      });

      if (existedParticipation) {
        throw new BadRequestException('Bạn đã tham gia thử thách này rồi');
      }

      const runner = await this.runnerService.findRunnerById(runnerId);
      if (!runner) {
        throw new BadRequestException('Runner not found');
      }

      const challenge = await this.challengeService.findChallengeById(
        createParticipation.challengeId,
      );
      if (!challenge) {
        throw new BadRequestException('Challenge not found');
      }

      if (new Date(challenge?.endTime) < new Date()) {
        throw new BadRequestException('Thử thách đã kết thúc');
      }

      if (challenge?.paidChallenge) {
        const inventoryItem =
          await this.inventoryService.checkTicketCanJoinByUserId(runnerId);

        const hasTicket = inventoryItem.find(
          (item) =>
            item?.code === RewardCode.Challenge_Ticket &&
            item?.isInvalid !== true &&
            item?.isSold !== true,
        );
        if (!hasTicket) {
          throw new BadRequestException(
            'Bạn không có vé tham gia thử thách này',
          );
        }
        await this.inventoryService.updateInventoryById(
          hasTicket._id.toString(),
          {
            isUsed: true,
            challengeId: challenge?._id?.toString(),
            updatedAt: new Date().toLocaleString(),
          },
        );
        if (hasTicket?.typeAction === ActionType.BUY) {
          const priceTicket = await this.energyService.getProductByKey(
            'TICKET',
          );

          await this.challengeService.updateBudgetChallenge(
            runner?._id?.toString(),
            priceTicket?.productPrice,
            'buy_ticket',
            null,
          );
        }
        if (hasTicket?.typeAction === ActionType.GIVE_AWAY) {
          isNoEarn = true;
        }
      }

      const createOneParticipation = new this.participationModel({
        ...createParticipation,
        isNoEarn,
        runnerId,
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString(),
      });

      const dataCreateParticipant = await createOneParticipation.save();

      const countParticipants =
        await this.challengeService.calculalteParticipants(
          createParticipation.challengeId,
          'participate',
        );

      const data = {
        ...dataCreateParticipant.toJSON(),
        atheleteCount: countParticipants,
      };

      return data;
    } catch (error) {
      console.log('createParticipation', error);
      throw error;
    }
  }

  async deleteParticipation(
    createParticipation: CreateParticipationDto,
    runnerId: string,
  ) {
    try {
      const existedParticipation = await this.participationModel.findOne({
        runnerId,
        challengeId: createParticipation.challengeId,
      });
      if (!existedParticipation) {
        throw new BadRequestException(
          'User not participated in this challenge',
        );
      }

      const challenge = await this.challengeService.findChallengeById(
        createParticipation.challengeId,
      );
      if (!challenge) {
        throw new BadRequestException('Challenge not found');
      }

      const runner = await this.participationModel.findOne({
        runnerId,
      });
      if (!runner) {
        throw new BadRequestException('Runner not found');
      }

      const data = await this.participationModel.findOneAndDelete({
        runnerId,
        challengeId: createParticipation.challengeId,
      });

      await this.challengeService.calculalteParticipants(
        createParticipation.challengeId,
        'leave',
      );

      return data;
    } catch (error) {
      console.log('deleteParticipation', error);
      throw error;
    }
  }

  async updateParticipation(
    challengeId: string,
    runnerId: string,
    athleteStats: any,
  ) {
    try {
      if (!athleteStats?.distance) {
        return null;
      }
      const existedParticipation = await this.participationModel.findOne({
        runnerId: runnerId.toString(),
        challengeId: challengeId.toString(),
      });
      if (!existedParticipation) {
        throw new BadRequestException(
          'User not participated in this challenge',
        );
      }

      const challenge = await this.challengeService.findChallengeById(
        challengeId,
      );
      const challengeTarget = challenge?.target;
      const challengeMedal = challenge?.medal || null;

      const data = await this.participationModel.findByIdAndUpdate(
        {
          _id: existedParticipation._id,
        },
        {
          athleteStats: {
            distance: Number(athleteStats.distance),
            validDistance: Number(athleteStats.validDistance),
            invalidDistance: Number(athleteStats.invalidDistance),
            moving_time: Number(athleteStats.moving_time),
            elapsed_time: Number(athleteStats.elapsed_time),
          },
          updatedAt: new Date().toLocaleString(),
        },
      );

      const newDistance = Number(athleteStats.distance);

      if (challengeMedal && newDistance >= challengeTarget) {
        const updateMedal = await this.runnerService.updateMedalForRunner(
          runnerId,
          challengeMedal?._id,
        );
        // console.log('updateMedal: ', updateMedal);
      }

      return data;
    } catch (error) {
      console.log('updateParticipation', error);
      throw error;
    }
  }

  async updateParticipationNoEarn(
    runnerId: string,
    challengeId: string,
    payload: any,
  ) {
    try {
      const data = await this.participationModel.findOneAndUpdate(
        {
          runnerId: runnerId.toString(),
          challengeId: challengeId.toString(),
        },
        {
          ...payload,
          updatedAt: new Date().toLocaleString(),
        },
        {
          new: true,
        },
      );

      return data;
    } catch (error) {
      console.log('updateParticipationById', error);
      throw error;
    }
  }

  async getParticipationByRunnerId(runnerId: string) {
    try {
      const runner = await this.runnerService.findInfoRunner(runnerId);
      if (!runner) {
        throw new BadRequestException('Runner not found');
      }

      const data = await this.participationModel
        .find({
          runnerId: runner._id.toString(),
        })
        .populate('challengeId');
      if (!data) {
        throw new BadRequestException('Participation not found');
      }

      return data;
    } catch (error) {
      console.log('getParticipationByRunnerId', error);
      throw error;
    }
  }

  async checkParticipationByRunnerId(runnerId: string, challengeId: string) {
    try {
      const existedParticipation = await this.participationModel.findOne({
        runnerId: runnerId,
        challengeId: challengeId,
      });
      return existedParticipation !== null;
    } catch (error) {
      throw error;
    }
  }

  async getParticipationByRunnerIdAndChallengeId(
    runnerId: string,
    challengeId: string,
  ) {
    try {
      const data = await this.participationModel
        .findOne({
          runnerId: runnerId.toString(),
          challengeId: challengeId.toString(),
        })
        .populate('challengeId');

      if (!data) {
        throw new BadRequestException('Participation not found');
      }

      return data;
    } catch (error) {
      console.log('getParticipationByRunnerIdAndChallengeId: ', error);
      throw error;
    }
  }

  async getChallengeDetail(runnerId: string, challengeId: string) {
    try {
      const runner = await this.runnerService.findInfoRunner(runnerId);
      if (!runner) {
        throw new BadRequestException('Runner not found');
      }

      const challenge = await this.challengeService.findChallengeById(
        challengeId,
      );
      if (!challenge) {
        throw new BadRequestException('Challenge not found');
      }

      const dataChallenge = await this.participationModel
        .find({
          challengeId: challenge._id.toString(),
        })
        .select('-challengeId -createdAt -updatedAt -__v')
        .populate({
          path: 'runnerId',
          select: 'name avatar sex phone size typeClothes sizeOption',
        });

      const transformDataChallenge: any = dataChallenge;

      const currentAthleteStats = transformDataChallenge.find(
        (participation) =>
          participation.runnerId?._id.toString() === runner._id.toString(),
      );

      const existedSponsor =
        await this.budgetHistoryService.findSponsorByChallengeIdAndRunnerId(
          challengeId,
          runnerId,
        );

      const data = {
        athleteStats: currentAthleteStats?.athleteStats,
        listAthletes: dataChallenge,
        existedSponsor: existedSponsor ? true : false,
      };

      if (!data) {
        throw new BadRequestException('Participation not found');
      }

      return data;
    } catch (error) {
      console.log('getChallengeDetail', error);
      throw error;
    }
  }

  async getRunnerCanRewardDistribution(challengeId: string) {
    try {
      const data = await this.participationModel
        .find({
          challengeId: challengeId,
        })
        .populate('runnerId');

      if (!data) {
        throw new BadRequestException('Participation not found');
      }

      return data;
    } catch (error) {
      console.log('getRunnerCanRewardDistribution', error);
      throw error;
    }
  }

  async getRankingOfRunner(runnerId: string, challengeId: string, sex?: any) {
    try {
      const runner = await this.runnerService.findInfoRunner(runnerId);
      if (!runner) {
        throw new BadRequestException('Runner not found');
      }

      if (sex) {
        const listRunner = await this.runnerService.getRunnerBySex(sex);
        const listRunnerId = listRunner.map((runner) => runner._id.toString());

        const challenge = await this.challengeService.findChallengeById(
          challengeId,
        );
        if (!challenge) {
          throw new BadRequestException('Challenge not found');
        }

        const dataChallenge = await this.participationModel
          .find({
            challengeId: challenge._id.toString(),
            runnerId: { $in: listRunnerId },
          })
          .select('-challengeId -createdAt -updatedAt -__v')
          .populate({
            path: 'runnerId',
            select: 'name avatar',
          });

        const transformDataChallenge: any = dataChallenge;

        const currentAthleteStats = transformDataChallenge.find(
          (participation) =>
            participation.runnerId?._id.toString() === runner._id.toString(),
        );

        const data = {
          athleteStats: currentAthleteStats?.athleteStats,
          listAthletes: dataChallenge,
        };

        if (!data) {
          throw new BadRequestException('Participation not found');
        }

        return data;
      }

      const challenge = await this.challengeService.findChallengeById(
        challengeId,
      );
      if (!challenge) {
        throw new BadRequestException('Challenge not found');
      }

      const dataChallenge = await this.participationModel
        .find({
          challengeId: challenge._id.toString(),
        })
        .select('-challengeId -createdAt -updatedAt -__v')
        .populate({
          path: 'runnerId',
          select: 'name avatar',
        });

      const transformDataChallenge: any = dataChallenge;

      const currentAthleteStats = transformDataChallenge.find(
        (participation) =>
          participation.runnerId?._id.toString() === runner._id.toString(),
      );

      const data = {
        athleteStats: currentAthleteStats?.athleteStats,
        listAthletes: dataChallenge,
      };

      if (!data) {
        throw new BadRequestException('Participation not found');
      }

      return data;
    } catch (error) {
      console.log('getChallengeDetail', error);
      throw error;
    }
  }
}
