import {
  Controller,
  Post,
  Body,
  UseInterceptors,
  Patch,
  Delete,
  Get,
  UseGuards,
  Param,
} from '@nestjs/common';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { RankRewardService } from './rank-reward.service';
import { CreateRankRewardDto } from './dto/createRankReward.dto';
import { ApiTags } from '@nestjs/swagger';
import {UpdateRankRewardDto} from "./dto/updateRankReward.dto";
import {SwapRankRewardDto} from "./dto/swapRankReward.dto";

@ApiTags('Challenge')
@Controller('challenge/rank-reward')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class RankRewardController {
  constructor(private readonly rankRewardService: RankRewardService) {}

  @Get()
  // @ApiBearerAuth('JWT-auth')
  // @UseGuards(new AuthGuard())
  getRankReward() {
    return this.rankRewardService.getRankReward();
  }

  @Post()
  // @ApiBearerAuth('JWT-auth')
  // @UseGuards(new AuthGuard())
  createRankReward(@Body() createRankRewardDto: CreateRankRewardDto) {
    return this.rankRewardService.createNewRankReward(createRankRewardDto);
  }

  @Delete('/:id')
  // @ApiBearerAuth('JWT-auth')
  // @UseGuards(new AuthGuard())
  deleteRankReward(@Param('id') id: string) {
    return this.rankRewardService.deleteRankRewardById(id);
  }

  // @Patch('/:id')
  // // @ApiBearerAuth('JWT-auth')
  // // @UseGuards(new AuthGuard())
  // updateRankById(@Body() updateRankRewardDto: UpdateRankRewardDto, @Param('id') id: string) {
  //   return this.rankRewardService.updateRankRewardById(id, updateRankRewardDto);
  // }

  @Post('/swap-rank')
  // @ApiBearerAuth('JWT-auth')
  // @UseGuards(new AuthGuard())
  swapRankBetweenTwoRank(@Body() swapRankRewardDto: SwapRankRewardDto) {
    return this.rankRewardService.swapRankBetweenTwoRank(swapRankRewardDto);
  }
}
