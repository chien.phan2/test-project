import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDataURI,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  Min,
} from 'class-validator';
import { RewardCode, RewardType } from 'src/utils/constants';

export class UpdateRankRewardDto {
  @ApiProperty()
  @IsString()
  rank: string;
}
