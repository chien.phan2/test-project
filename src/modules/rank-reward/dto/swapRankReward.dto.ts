import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDataURI,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  Min,
} from 'class-validator';
import { RewardCode, RewardType } from 'src/utils/constants';

export class SwapRankRewardDto {
  @ApiProperty()
  @IsString()
  firstId: string;

  @ApiProperty()
  @IsString()
  secondId: string;
}
