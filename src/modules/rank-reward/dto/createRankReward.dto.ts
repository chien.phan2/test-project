import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDataURI,
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  Max,
  Min,
} from 'class-validator';
import { RewardCode, RewardType } from 'src/utils/constants';

export class CreateRankRewardDto {
  @ApiProperty()
  @IsString()
  rewardId: string;

  @ApiProperty()
  @IsString()
  rank: string;

  @ApiProperty()
  @IsString()
  prizeKey: string;
}
