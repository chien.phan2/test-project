import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { RankReward } from '../../schema/rankReward.schema';
import { CreateRankRewardDto } from './dto/createRankReward.dto';
import { MY_MESSAGE } from '../../contants/message.contants';
import { SwapRankRewardDto } from './dto/swapRankReward.dto';

@Injectable()
export class RankRewardService {
  constructor(
    @InjectModel(RankReward.name)
    private readonly rankRewardModel: Model<RankReward>,
  ) {}

  async createNewRankReward(createRankRewardDto: CreateRankRewardDto) {
    try {
      const findAll = await this.rankRewardModel.find({});
      if (findAll?.length > 8) {
        throw new BadRequestException(MY_MESSAGE.LIMITED_RANK_REWARD);
      }
      return await this.rankRewardModel.create(createRankRewardDto);
    } catch (err) {
      throw err;
    }
  }

  async getRankReward() {
    try {
      const data = await this.rankRewardModel
        .find({})
        .limit(8)
        .sort({ rank: 1 })
        .populate({
          path: 'rewardId',
        });
      return data;
    } catch (err) {
      throw err;
    }
  }

  async deleteRankRewardById(id: string) {
    try {
      return await this.rankRewardModel.findByIdAndDelete(id);
    } catch (err) {
      throw err;
    }
  }

  async updateRankRewardById(id: string, rank: string) {
    try {
      return await this.rankRewardModel.findByIdAndUpdate(id, {
        rank: rank,
        // prizeKey: prizeKey,
        updatedAt: new Date().toLocaleString(),
      });
    } catch (err) {
      throw err;
    }
  }

  async swapRankBetweenTwoRank(swapRankRewardDto: SwapRankRewardDto) {
    try {
      const { firstId, secondId } = swapRankRewardDto;
      const firstRewardRank = await this.rankRewardModel.findById(firstId);
      const secondRewardRank = await this.rankRewardModel.findById(secondId);
      if (!firstRewardRank || !secondRewardRank) {
        throw new BadRequestException(MY_MESSAGE.NOT_FOUND_RANK_REWARD);
      }
      const updateFirstReward = new Promise((resolve) => {
        this.updateRankRewardById(firstId, secondRewardRank?.rank)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            resolve(err);
          });
      });
      const updateSecondReward = new Promise((resolve) => {
        this.updateRankRewardById(secondId, firstRewardRank?.rank)
          .then((res) => {
            resolve(res);
          })
          .catch((err) => {
            resolve(err);
          });
      });
      return Promise.all([updateFirstReward, updateSecondReward])
        .then((res) => {
          console.log('res', res);
          return res;
        })
        .catch((err) => {
          console.log('swapRankBetweenTwoRank', err);
          throw new BadRequestException(MY_MESSAGE.SOMETHING_WRONG);
        });
    } catch (err) {
      throw err;
    }
  }
}
