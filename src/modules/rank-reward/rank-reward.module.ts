import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RankRewardController } from './rank-reward.controller';
import { RankRewardService } from './rank-reward.service';
import { RankReward, RankRewardSchema } from '../../schema/rankReward.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: RankReward.name, schema: RankRewardSchema },
    ]),
  ],
  controllers: [RankRewardController],
  providers: [RankRewardService],
  exports: [RankRewardService],
})
export class RankRewardModule {}
