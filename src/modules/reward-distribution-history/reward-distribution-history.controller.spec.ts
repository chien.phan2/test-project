import { Test, TestingModule } from '@nestjs/testing';
import { RewardDistributionHistoryController } from './reward-distribution-history.controller';

describe('RewardDistributionHistoryController', () => {
  let controller: RewardDistributionHistoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RewardDistributionHistoryController],
    }).compile();

    controller = module.get<RewardDistributionHistoryController>(
      RewardDistributionHistoryController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
