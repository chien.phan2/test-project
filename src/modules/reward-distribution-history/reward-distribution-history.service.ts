import { Injectable, Logger } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { RewardDistributionHistory } from 'src/schema/reward-distribution-history';
import { Model, Connection } from 'mongoose';
import { RunnerService } from '../runner/runner.service';
import { ChallengeService } from '../challenge/challenge.service';
import { ParticipationService } from '../participation/participation.service';
import { TransactionService } from '../transaction/transaction.service';
import { MomoIpn } from '../transaction/dto/momo-ipn.dto';
import { decodeBase64 } from 'src/utils/genBase64String';

@Injectable()
export class RewardDistributionHistoryService {
  constructor(
    @InjectModel(RewardDistributionHistory.name)
    private readonly rewardDistributionHistoryModel: Model<RewardDistributionHistory>,

    private readonly runnerService: RunnerService,

    private readonly challengeService: ChallengeService,

    private readonly participationService: ParticipationService,

    private readonly transactionService: TransactionService,

    @InjectConnection()
    private connection: Connection,
  ) {}

  async createRequestDistribution(challengeID: string) {
    try {
      const challenge = await this.challengeService.getAvailablePaidChallenge();
      // const budgetChallenge = challenge?.budget || 0;
      const targetChallenge = challenge?.target || 0;

      const runnerHasDistribution =
        await this.participationService.getRunnerCanRewardDistribution(
          challengeID,
        );

      const validRunner = runnerHasDistribution?.filter(
        (item) =>
          item?.isNoEarn !== true &&
          item?.athleteStats?.distance >= targetChallenge,
      );

      // console.log('====> validRunner:', validRunner?.length); // 53

      const validRunnerId = validRunner?.map((item: any) =>
        item?.runnerId?._id?.toString(),
      );

      const DELAY_BETWEEN_REQUESTS = 2000;

      // const shareProfit = budgetChallenge / validRunner?.length; // 138624.735

      const listPromise = [];
      // validRunnerId?.forEach(async (runnerId) => {
      //   const runner = await this.runnerService.findPublicRunnerById(runnerId);
      //   const phoneNumber = runner?.phone;

      //   const newRewardDistributionHistory =
      //     new this.rewardDistributionHistoryModel({
      //       runner: runnerId,
      //       challenge: challengeID,
      //       status: 'pending',
      //       value: 138624.735,
      //       type: 'distribution',
      //       paymentInfo: {},
      //     });

      //   await newRewardDistributionHistory.save();

      //   const historyId = newRewardDistributionHistory?._id?.toString();

      //   const promise = await this.transactionService.distributionReward(
      //     phoneNumber,
      //     138624.735,
      //     historyId,
      //   );
      //   listPromise.push(promise);
      // });

      for (const runnerId of validRunnerId) {
        const promise = (async () => {
          const runner = await this.runnerService.findRunnerById(
            runnerId?.toString(),
          );

          const phoneNumber = runner?.phone;

          const newRewardDistributionHistory =
            new this.rewardDistributionHistoryModel({
              runner: runnerId?.toString(),
              challenge: challengeID,
              status: 'pending',
              value: 138624.735,
              type: 'distribution',
              paymentInfo: {},
            });

          await newRewardDistributionHistory.save();

          const historyId = newRewardDistributionHistory?._id?.toString();

          const promise = await this.transactionService.distributionReward(
            phoneNumber,
            138624.735,
            historyId,
          );
          listPromise.push(promise);

          // Return the promise
          return promise;
        })();

        listPromise.push(promise);

        // Wait for the specified delay before processing the next runner
        await new Promise((resolve) =>
          setTimeout(resolve, DELAY_BETWEEN_REQUESTS),
        );
      }

      const result = await Promise.all(listPromise);
      console.log('====> result:', result);
      return result;
    } catch (error) {
      console.log('createRequestDistribution', error);
      throw error;
    }
  }

  async handleIpnDistribution(data: MomoIpn) {
    console.log('===> data:', data);
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const extraData = JSON.parse(decodeBase64(data.extraData));
      const transactionID = extraData.id;
      console.log('===> transactionID:', transactionID);

      const transaction = await this.rewardDistributionHistoryModel.findOne({
        _id: transactionID,
        status: 'pending',
      });
      console.log('=====> transaction:', transaction);

      if (transaction) {
        if (data.resultCode == 0) {
          transaction.updateOne(
            { status: 'success', paymentInfo: data },
            { session },
          );
          await session.commitTransaction();
        } else {
          transaction.updateOne(
            { status: 'failed', paymentInfo: data },
            { session },
          );
          await session.commitTransaction();
        }
      }
    } catch (error) {
      Logger.error(`Update pay info for transaction error`, error);
    } finally {
      session.endSession();
      return { data: 'Success' };
    }
  }
}
