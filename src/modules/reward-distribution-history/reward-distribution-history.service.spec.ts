import { Test, TestingModule } from '@nestjs/testing';
import { RewardDistributionHistoryService } from './reward-distribution-history.service';

describe('RewardDistributionHistoryService', () => {
  let service: RewardDistributionHistoryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RewardDistributionHistoryService],
    }).compile();

    service = module.get<RewardDistributionHistoryService>(
      RewardDistributionHistoryService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
