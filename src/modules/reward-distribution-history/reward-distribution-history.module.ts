import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  RewardDistributionHistory,
  RewardDistributionHistorySchema,
} from 'src/schema/reward-distribution-history';
import { RewardDistributionHistoryService } from './reward-distribution-history.service';
import { RewardDistributionHistoryController } from './reward-distribution-history.controller';
import { RunnerModule } from '../runner/runner.module';
import { ChallengeModule } from '../challenge/challenge.module';
import { ParticipationModule } from '../participation/participation.module';
import { TransactionModule } from '../transaction/transaction.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: RewardDistributionHistory.name,
        schema: RewardDistributionHistorySchema,
      },
    ]),
    RunnerModule,
    forwardRef(() => ChallengeModule),
    ParticipationModule,
    TransactionModule,
  ],
  providers: [RewardDistributionHistoryService],
  exports: [RewardDistributionHistoryService],
  controllers: [RewardDistributionHistoryController],
})
export class RewardDistributionHistoryModule {}
