import {
  Body,
  Controller,
  Get,
  HttpCode,
  Logger,
  Post,
  Query,
  Req,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { RewardDistributionHistoryService } from './reward-distribution-history.service';
import { Request } from 'express';

@ApiTags('Challenge')
@Controller('/challenge/reward-distribution-history')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class RewardDistributionHistoryController {
  constructor(
    private readonly rewardDistributionHistoryService: RewardDistributionHistoryService,
  ) {}

  @Get('/reward-distribution')
  async getRewardDistributionHistory() {
    return this.rewardDistributionHistoryService.createRequestDistribution(
      '64bdf9cafbc566395b6a4346',
    );
  }

  @Post('/ipn')
  @HttpCode(204)
  ipnNoti(@Body() body: any, @Req() req: Request, @Query() query: any) {
    Logger.log('Receive Dis IPN:', body);
    Logger.log('Dis', query.type);
    return this.rewardDistributionHistoryService.handleIpnDistribution(body);
  }
}
