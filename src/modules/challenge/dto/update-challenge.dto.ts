import { ApiProperty } from '@nestjs/swagger';
import {
  IsNumber,
  IsOptional,
  IsDateString,
  IsArray,
  IsBoolean,
  IsString,
  IsUrl,
} from 'class-validator';
import { RaceKit, RewardContent } from './create-challenge.dto';

export class UpdateChallengeDto {
  @ApiProperty()
  @IsOptional()
  activities: string[];

  @ApiProperty()
  @IsOptional()
  bannerUrl: string;

  @ApiProperty()
  @IsOptional()
  name: string;

  @ApiProperty()
  @IsOptional()
  description: string;

  @ApiProperty()
  @IsOptional()
  @IsDateString()
  startTime: Date;

  @ApiProperty()
  @IsOptional()
  @IsDateString()
  endTime: Date;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  minDistance: number;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  target: number;

  @ApiProperty()
  @IsOptional()
  @IsArray()
  paceRange: number[];

  @ApiProperty({ type: [RewardContent] })
  @IsOptional()
  reward: RewardContent[];

  @ApiProperty({ type: [RaceKit] })
  @IsOptional()
  raceKit: RaceKit[];

  @ApiProperty()
  @IsOptional()
  rule: string;

  @ApiProperty()
  @IsOptional()
  medal: string;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  maxParticipants: number;

  @ApiProperty()
  @IsOptional()
  @IsBoolean()
  paidChallenge: boolean;

  @ApiProperty({ default: false })
  @IsOptional()
  @IsBoolean()
  isDisabled: boolean;

  @ApiProperty({ default: 'individual' })
  @IsOptional()
  @IsString()
  type: string;

  @ApiProperty({ default: false })
  @IsOptional()
  @IsBoolean()
  isCheckParticipantTime: boolean;

  @ApiProperty()
  @IsOptional()
  conversionEvent: any;

  @ApiProperty()
  @IsOptional()
  @IsUrl()
  supportUrl: string;
}
