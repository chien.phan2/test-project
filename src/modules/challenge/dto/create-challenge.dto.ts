import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsDateString,
  IsArray,
  IsBoolean,
  IsString,
  IsUrl,
} from 'class-validator';

export class RewardContent {
  @ApiProperty()
  @IsString()
  content: string;

  @ApiProperty()
  @IsUrl()
  url: string;
}

export class RaceKit {
  @ApiProperty()
  @IsString()
  content: string;

  @ApiProperty()
  @IsUrl()
  url: string;

  @ApiProperty()
  @IsString()
  groupName: string;

  @ApiProperty()
  @IsString()
  groupKey: string;
}

export class CreateChallengeDto {
  @ApiProperty()
  @IsOptional()
  activities: string[];

  @ApiProperty()
  @IsNotEmpty()
  bannerUrl: string;

  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  description: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsDateString()
  startTime: Date;

  @ApiProperty()
  @IsNotEmpty()
  @IsDateString()
  endTime: Date;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  minDistance: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  target: number;

  @ApiProperty()
  @IsArray()
  paceRange: number[];

  @ApiProperty({ type: [RewardContent] })
  @IsOptional()
  reward: RewardContent[];

  @ApiProperty({ type: [RaceKit] })
  @IsOptional()
  raceKit: RaceKit[];

  @ApiProperty()
  @IsOptional()
  rule: string;

  @ApiProperty()
  @IsOptional()
  medal: string;

  @ApiProperty()
  @IsOptional()
  @IsNumber()
  maxParticipants: number;

  @ApiProperty({ default: false })
  @IsNotEmpty()
  @IsBoolean()
  paidChallenge: boolean;

  @ApiProperty({ default: 0 })
  @IsOptional()
  @IsNumber()
  budget: number;

  @ApiProperty({ default: 'individual' })
  @IsOptional()
  @IsString()
  type: string;

  @ApiProperty({ default: false })
  @IsOptional()
  @IsBoolean()
  isCheckParticipantTime: boolean;

  @ApiProperty({ default: false })
  @IsOptional()
  @IsBoolean()
  isDisabled: boolean;

  @ApiProperty()
  @IsOptional()
  conversionEvent: any;

  @ApiProperty()
  @IsOptional()
  @IsUrl()
  supportUrl: string;
}
