import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ApiBearerAuth, ApiQuery, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { AuthGuard } from 'src/middleware/auth.middleware';
import { authenticatedPortal } from '../../utils/constants';
import { RequestEnergyDto } from '../energy/dto/request-energy.dto';
import { InventoryService } from '../inventory/inventory.service';
import { RunnerService } from '../runner/runner.service';
import { QueryWheelPagination } from '../wheel/dto/queryWheelPagination.dto';
import { WheelService } from '../wheel/wheel.service';
import { ChallengeService } from './challenge.service';
import { CreateChallengeDto } from './dto/create-challenge.dto';
import { GiveAwayDto } from './dto/give-away.dto';
import { UpdateChallengeDto } from './dto/update-challenge.dto';

@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
@ApiTags('Challenge')
@Controller('challenge')
export class ChallengeController {
  constructor(
    private readonly challengeService: ChallengeService,
    private readonly wheelService: WheelService,
    private readonly inventoryService: InventoryService,
    private readonly runnerService: RunnerService,
  ) {}

  @Post('/create-challenge')
  createChallenge(@Body() createChallengeDto: CreateChallengeDto) {
    return this.challengeService.createChallenge(createChallengeDto);
  }

  @Get('/find-all-challenge')
  @ApiQuery({
    name: 'isDisabled',
    required: false,
    type: String,
  })
  findAllChallenge(@Query('isDisabled') isDisabled: string) {
    return this.challengeService.findAllChallenge(isDisabled);
  }

  @Get('/find-challenge/:id')
  findChallengeById(@Param('id') id: string) {
    return this.challengeService.findChallengeById(id);
  }

  @Patch('/update-challenge/:id')
  updateChallenge(
    @Body() updateChallengeDto: UpdateChallengeDto,
    @Param('id') id: string,
  ) {
    return this.challengeService.updateChallenge(id, updateChallengeDto);
  }

  @Delete('/delete-challenge/:id')
  deleteChallenge(@Param('id') id: string) {
    return this.challengeService.deleteChallenge(id);
  }

  @Post('/lucky-spin')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  spinLuckWheel(@Req() req: Request) {
    const userID = req.userID;
    return this.wheelService.spinLuckyWheelService(userID);
  }

  @Post('sponsor')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  sponsor(@Body() requestEnergyDto: RequestEnergyDto, @Req() req: Request) {
    const userID = req.userID;
    return this.challengeService.requestSponsor(userID, requestEnergyDto);
  }

  @Get('get-my-inventory')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  getMyInventory(@Req() req: Request) {
    const userID = req.userID;
    return this.inventoryService.getInventoriesByUserId(userID);
  }

  @Post('give-away')
  giveAwayTicket(@Req() req: Request, @Body() giveAwayDto: GiveAwayDto) {
    const secretKey = req?.headers?.secretkey;
    authenticatedPortal(secretKey as string);
    const userId = giveAwayDto?.userId;
    return this.challengeService.findAndGiveAwayTicket(userId);
  }

  @Get('runners')
  getListRunners(
    @Req() req: Request,
    @Query() queryWheelPagination: QueryWheelPagination,
  ) {
    const secretKey = req?.headers?.secretkey;
    authenticatedPortal(secretKey as string);
    return this.runnerService.getListRunner(queryWheelPagination);
  }

  @Get('top-runners')
  @ApiBearerAuth('JWT-auth')
  @UseGuards(new AuthGuard())
  getTopRunners(
    @Req() req: Request,
    @Query() queryWheelPagination: QueryWheelPagination,
  ) {
    return this.runnerService.getListRunner(queryWheelPagination);
  }
}
