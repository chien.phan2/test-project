import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EventsModule } from 'src/events/events.module';
import { Challenge, ChallengeSchema } from 'src/schema/challenge.schema';
import { BudgetHistoryModule } from '../budget-history/budget-history.module';
import { EnergyTransactionModule } from '../energy-transaction/energy-transaction.module';
import { EnergyModule } from '../energy/energy.module';
import { InventoryModule } from '../inventory/inventory.module';
import { RankRewardModule } from '../rank-reward/rank-reward.module';
import { RewardModule } from '../reward/reward.module';
import { RunnerModule } from '../runner/runner.module';
import { WheelModule } from '../wheel/wheel.module';
import { ChallengeController } from './challenge.controller';
import { ChallengeService } from './challenge.service';
import { MedalModule } from '../medal/medal.module';
import { ParticipationModule } from '../participation/participation.module';
import { TicketChallengeModule } from '../ticket-challenge/ticket-challenge.module';
import { ProductTransactionModule } from '../product-transaction/product-transaction.module';
import { MissionModule } from '../mission/mission.module';
import { MissionHistoryModule } from '../mission-history/mission-history.module';
import { RewardDistributionHistoryModule } from '../reward-distribution-history/reward-distribution-history.module';
import { GroupParticipationModule } from '../group-participation/group-participation.module';
import { GroupParticipationRequestModule } from '../group-participation-request/group-participation-request.module';
import { SizeShirtModule } from '../size-shirt/size-shirt.module';
import { ExcelExportModule } from '../excel-export/excel-export.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Challenge.name, schema: ChallengeSchema },
    ]),
    RunnerModule,
    RewardModule,
    EnergyModule,
    EnergyTransactionModule,
    WheelModule,
    InventoryModule,
    BudgetHistoryModule,
    EventsModule,
    RankRewardModule,
    MedalModule,
    ParticipationModule,
    TicketChallengeModule,
    ProductTransactionModule,
    MissionModule,
    MissionHistoryModule,
    RewardDistributionHistoryModule,
    GroupParticipationModule,
    GroupParticipationRequestModule,
    SizeShirtModule,
    ExcelExportModule,
  ],
  controllers: [ChallengeController],
  providers: [ChallengeService],
  exports: [ChallengeService],
})
export class ChallengeModule {}
