import {
  BadRequestException,
  Inject,
  Injectable,
  forwardRef,
} from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import { MY_MESSAGE } from 'src/contants/message.contants';
import { EventsGateway } from 'src/events/events.gateway';
import { Challenge, ChallengeDocument } from 'src/schema/challenge.schema';
import { ActionType } from 'src/utils/constants';
import { BudgetHistoryService } from '../budget-history/budget-history.service';
import { EnergyTransactionService } from '../energy-transaction/energy-transaction.service';
import { RequestEnergyDto } from '../energy/dto/request-energy.dto';
import { EnergyService } from '../energy/energy.service';
import { InventoryService } from '../inventory/inventory.service';
import { ParticipationService } from '../participation/participation.service';
import { RewardService } from '../reward/reward.service';
import { RunnerService } from '../runner/runner.service';
import { CreateChallengeDto } from './dto/create-challenge.dto';
import { UpdateChallengeDto } from './dto/update-challenge.dto';

@Injectable()
export class ChallengeService {
  constructor(
    @InjectModel(Challenge.name)
    private challengeModel: Model<ChallengeDocument>,
    @InjectConnection() private connection: Connection,

    @Inject(forwardRef(() => BudgetHistoryService))
    private readonly budgetHistoryService: BudgetHistoryService,

    @Inject(forwardRef(() => RunnerService))
    private readonly runnerService: RunnerService,

    @Inject(forwardRef(() => EnergyService))
    private readonly ennergyService: EnergyService,

    @Inject(forwardRef(() => EnergyTransactionService))
    private readonly energyTransactionService: EnergyTransactionService,

    @Inject(forwardRef(() => ParticipationService))
    private readonly participationService: ParticipationService,

    @Inject(forwardRef(() => InventoryService))
    private readonly inventoryService: InventoryService,

    @Inject(forwardRef(() => RewardService))
    private readonly rewardService: RewardService,

    private readonly eventsGateway: EventsGateway,
  ) {}

  async createChallenge(createChallengeDto: CreateChallengeDto) {
    try {
      const createOneChallenge = new this.challengeModel({
        ...createChallengeDto,
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString(),
      });
      const data = await createOneChallenge.save();
      return data;
    } catch (error) {
      throw error;
    }
  }

  async updateChallenge(id: string, updateChallengeDto: UpdateChallengeDto) {
    try {
      const data = await this.challengeModel.findByIdAndUpdate(
        {
          _id: id,
        },
        { ...updateChallengeDto, updatedAt: new Date() },
      );

      return data;
    } catch (error) {
      throw new BadRequestException(MY_MESSAGE.NOT_FOUND_CHALLENGE);
    }
  }

  async deleteChallenge(id: string) {
    try {
      const data = await this.challengeModel.findByIdAndDelete(id);
      return data;
    } catch (error) {
      throw new BadRequestException(MY_MESSAGE.NOT_FOUND_CHALLENGE);
    }
  }

  async findAllChallenge(isDisabled?: any) {
    try {
      const query = {};
      if (isDisabled) {
        query['isDisabled'] = isDisabled;
      } else {
        query['isDisabled'] = false;
      }

      const challenges = await this.challengeModel
        .find({
          // ...(isDisabled && { isDisabled }),
          ...query,
        })
        .sort({ createdAt: -1 })
        .populate('medal');
      return challenges;
    } catch (err) {
      throw err;
    }
  }

  async findChallengeById(id: string) {
    try {
      const challenges = await this.challengeModel
        .findById(id)
        .populate('medal')
        .exec();
      return challenges;
    } catch (err) {
      throw err;
    }
  }

  async calculalteParticipants(challengeId: string, type: string) {
    try {
      const challenge = await this.findChallengeById(challengeId);
      if (!challenge) {
        throw new BadRequestException(MY_MESSAGE.NOT_FOUND_CHALLENGE);
      }

      const participants = await this.challengeModel.findById(challengeId);
      const currentParticipants = participants.athleteCount;

      let newParticipants = 0;
      if (type === 'participate') {
        newParticipants = currentParticipants + 1;
      } else {
        newParticipants = currentParticipants - 1;
      }

      const updateParticipants = {
        athleteCount: newParticipants,
      };

      await this.challengeModel.findByIdAndUpdate(
        {
          _id: challengeId,
        },
        { ...updateParticipants, updatedAt: new Date().toLocaleString() },
      );

      return newParticipants;
    } catch (error) {
      throw error;
    }
  }

  async updateBudgetChallenge(
    userId: string,
    budget: number,
    type: string,
    product?: string,
  ) {
    // console.info('updateBudgetChallenge', {
    //   userId: userId,
    //   budget: budget,
    //   type: type,
    //   product: product,
    // });
    const session = await this.connection.startSession();
    session.startTransaction({
      readConcern: { level: 'snapshot' },
      writeConcern: { w: 'majority' },
    });
    try {
      const paidChallenges = await this.getAvailablePaidChallenge();

      const id = paidChallenges?._id;
      const currentBudget = paidChallenges?.budget || 0;
      const newBudget = currentBudget + budget;

      const dataChallenge = await this.challengeModel.findByIdAndUpdate(
        {
          _id: id,
        },
        {
          $set: {
            budget: newBudget,
            updatedAt: new Date().toLocaleString(),
          },
        },
        {
          returnNewDocument: true,
          session,
        },
      );

      await session.commitTransaction();
      session.endSession();

      await this.budgetHistoryService.createBudgetHistory({
        creator: userId,
        challenge: id.toString(),
        type: type,
        amount: budget,
        product: product || '',
      });

      setTimeout(() => {
        this.eventsGateway.onChangeBudget({
          challengeId: dataChallenge?._id.toString(),
          status: 'success',
          budget: newBudget,
        });
      }, 7000);

      return dataChallenge;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      console.log('updateBudgetChallenge', error);
      throw error;
    }
  }

  async requestSponsor(userID: string, requestPayload: RequestEnergyDto) {
    try {
      if (requestPayload?.quantity < 0) {
        throw new BadRequestException('Quantity must be greater than 0');
      }
      const user = await this.runnerService.findRunnerById(userID);
      const energyProduct = await this.ennergyService.getProductEnergyById(
        requestPayload?.productId,
      );

      const existSponsor =
        await this.budgetHistoryService.findSponsorByChallengeIdAndRunnerId(
          userID,
          requestPayload?.challengeId,
        );
      if (existSponsor?.length > 0) {
        throw new BadRequestException('Bạn đã tài trợ cho thử thách này!');
      }

      const data = {
        ...requestPayload,
        product: {
          ...energyProduct.toJSON(),
        },
        value:
          (energyProduct?.productPrice || 0) * (requestPayload?.quantity || 0),
        creator: user?._id.toString(),
      };

      const transactionData =
        await this.energyTransactionService.createRequestTransaction(
          user?._id.toString(),
          data,
          'sponsor',
          requestPayload?.challengeId || '',
        );

      return transactionData;
    } catch (error) {
      console.log('requestSponsor: ', error);
      throw error;
    }
  }

  async getAvailablePaidChallenge() {
    try {
      // const currentDate = new Date();
      const paidChallenges = await this.challengeModel.find({
        paidChallenge: true,
        isDisabled: false,
        // startTime: { $lt: currentDate },
        // endTime: { $gt: currentDate },
      });

      if (!paidChallenges?.length || paidChallenges?.length > 1) {
        throw new BadRequestException(MY_MESSAGE.NOT_FOUND_CHALLENGE);
      }

      return paidChallenges?.[0];
    } catch (err) {
      console.log('getAvailablePaidChallenge: ', err);
      throw err;
    }
  }

  async findAndGiveAwayTicket(userId: string) {
    try {
      const paidChallenges = await this.getAvailablePaidChallenge();
      if (!paidChallenges) {
        throw new BadRequestException(MY_MESSAGE.NOT_FOUND_CHALLENGE);
      }
      return await this.giveAwayChallengeTicket(userId, ActionType.GIVE_AWAY);
    } catch (err) {
      console.log('findAndGiveAwayTicket: ', err);
      throw err;
    }
  }

  async giveAwayChallengeTicket(
    userId: string,
    typeAction: string,
    ticketCode?: string,
  ) {
    try {
      const reward = await this.rewardService.getRewardsByKeyCode(
        'ChallengeTicket',
      );
      if (reward?.length === 0) {
        throw new BadRequestException(MY_MESSAGE.NOT_FOUND_REWARD);
      }
      return await this.inventoryService.giveAwayInventory(
        userId,
        reward?.[0],
        typeAction,
        ticketCode,
      );
    } catch (err) {
      console.log('giveAwayChallengeTicket: ', err);
      throw err;
    }
  }
}
