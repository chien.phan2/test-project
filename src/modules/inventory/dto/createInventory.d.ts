import { IsString } from 'class-validator';

export class CreateInventory {
  @IsString()
  type: string;
}
