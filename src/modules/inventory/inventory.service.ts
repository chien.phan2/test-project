import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import { Connection, Model } from 'mongoose';
import { ActionType } from 'src/utils/constants';
import { Inventory, InventoryDocument } from '../../schema/inventory.schema';
import { Reward } from '../../schema/reward.schema';
@Injectable()
export class InventoryService {
  constructor(
    @InjectModel(Inventory.name)
    private readonly inventoryModel: Model<InventoryDocument>,
    @InjectConnection() private connection: Connection,
  ) {}

  async createInventoryAfterWinPrize(
    userId: string,
    reward: Reward,
    prizeKey: string,
  ) {
    try {
      console.info('createInventoryAfterWinPrize', {
        userId: userId,
        reward: reward,
      });
      const newEntity = new this.inventoryModel({
        creator: userId,
        rewardId: reward?._id,
        type: reward?.type,
        code: reward?.code,
        name: reward?.name,
        prizeKey: prizeKey,
        url: reward?.url,
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString(),
      });
      return await newEntity.save();
    } catch (error) {
      console.log('createInventoryAfterWinPrize', error);
      throw error;
    }
  }

  async giveAwayInventory(
    userId: string,
    reward: Reward,
    typeAction: string,
    ticketCode?: string,
  ) {
    try {
      const newEntity = new this.inventoryModel({
        creator: userId,
        rewardId: reward?._id,
        type: reward?.type,
        code: reward?.code,
        name: reward?.name,
        prizeKey: '',
        url: reward?.url,
        typeAction: typeAction,
        ticketCode: ticketCode || '',
        createdAt: new Date().toLocaleString(),
        updatedAt: new Date().toLocaleString(),
      });
      return await newEntity.save();
    } catch (error) {
      console.log('giveAwayInventory', error);
      throw error;
    }
  }

  async getInventoriesById(id: string) {
    try {
      return await this.inventoryModel.findById(id);
    } catch (error) {
      throw error;
    }
  }

  async getInventoriesByUserId(userId: string, isAll?: boolean) {
    try {
      if (isAll) {
        return await this.inventoryModel
          .find({
            creator: userId,
            $or: [{ isSold: false }, { isSold: { $exists: false } }],
          })
          .populate('rewardId');
      }
      return await this.inventoryModel
        .find({
          creator: userId,
          isUsed: false,
          $or: [{ isSold: false }, { isSold: { $exists: false } }],
          // $or: [{ isInvalid: false }, { isInvalid: { $exists: false } }],
        })
        .populate('rewardId');
    } catch (error) {
      throw error;
    }
  }

  async checkTicketCanJoinByUserId(userId: string, isAll?: boolean) {
    try {
      if (isAll) {
        return await this.inventoryModel
          .find({
            creator: userId,
          })
          .populate('rewardId');
      }
      return await this.inventoryModel
        .find({
          creator: userId,
          isUsed: false,
          $or: [
            { isSold: false },
            { isSold: { $exists: false } },
            { isInvalid: false },
            { isInvalid: { $exists: false } },
          ],
        })
        .populate('rewardId');
    } catch (error) {
      console.log('checkTicketCanJoinByUserId', error);
      throw error;
    }
  }

  async updateInventoryById(id: string, data: any) {
    try {
      return await this.inventoryModel.findByIdAndUpdate(id, data);
    } catch (error) {
      console.log('updateInventoryById', error);
      throw error;
    }
  }

  async setTicketSold(id: string) {
    try {
      const data = await this.inventoryModel.findByIdAndUpdate(
        {
          _id: id,
        },
        {
          isSold: true,
          soldAt: new Date().toLocaleString(),
        },
        { new: true },
      );
      return data;
    } catch (error) {
      console.log('setTicketSold', error);
      throw error;
    }
  }

  async validateTicketInventory(userId: string, id: string) {
    try {
      const inventory = await this.inventoryModel.findById({
        _id: id,
        creator: userId,
      });
      if (!inventory) {
        throw new Error('Inventory not found');
      }
    } catch (error) {
      console.log('validateTicketInventory', error);
      throw error;
    }
  }

  async setInValidInventoryItem(id: string, status: boolean) {
    const session = await this.connection.startSession();
    session.startTransaction();
    try {
      const data = await this.inventoryModel.findByIdAndUpdate(
        id,
        {
          isInvalid: status,
        },
        { new: true },
      );

      await session.commitTransaction();
      session.endSession();

      return data;
    } catch (error) {
      await session.abortTransaction();
      session.endSession();
      console.log('setInValidInventoryItem', error);
      throw error;
    }
  }

  async checkItemIsGiveAway(id: string) {
    try {
      const item = await this.inventoryModel.findById(id);
      const isGiveAway = item?.typeAction === ActionType.GIVE_AWAY;
      return isGiveAway;
    } catch (error) {
      console.log('checkItemIsGiveAway', error);
      throw error;
    }
  }
}
