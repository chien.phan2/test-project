import { Controller, Post, UseInterceptors, UseGuards } from '@nestjs/common';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
// @ApiTags('Challenge')
// @Controller('challenge/reward')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class InventoryController {
  // constructor(private readonly inventoryService: InventoryService) {}
}
