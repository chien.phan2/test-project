import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { SizeShirt, SizeShirtDocument } from 'src/schema/sizeShirt.schema';
import { Model } from 'mongoose';
import { CreateSizeShirtDto } from './dto/createSizeShirt.dto';
import { UpdateSizeShirtDto } from './dto/updateSizeShirt.dto';

@Injectable()
export class SizeShirtService {
  constructor(
    @InjectModel(SizeShirt.name)
    private readonly sizeShirtModel: Model<SizeShirtDocument>,
  ) {}

  async getAllSizeShirt() {
    try {
      const sizeShirt = await this.sizeShirtModel.find();
      return {
        sizeList: sizeShirt,
        sizeChart:
          'https://static.momocdn.net/app/img/mini-app-center/icon-003b83fd-e598-433c-bc86-90e1a25fe93f.jpeg',
      };
    } catch (error) {
      console.log('getAllSizeShirt', error);
      throw new BadRequestException(error);
    }
  }

  async createSizeShirt(payload: CreateSizeShirtDto) {
    try {
      const newSizeShirt = await this.sizeShirtModel.create(payload);
      await newSizeShirt.save();
      return newSizeShirt;
    } catch (error) {
      console.log('createSizeShirt', error);
      throw new BadRequestException(error);
    }
  }

  async updateSizeShirt(id: string, payload: UpdateSizeShirtDto) {
    try {
      const sizeShirt = await this.sizeShirtModel.findById(id);
      if (!sizeShirt) {
        throw new BadRequestException('Size shirt not found');
      }

      await sizeShirt.updateOne(
        {
          $set: payload,
        },
        {
          new: true,
        },
      );
      return sizeShirt;
    } catch (error) {
      console.log('updateSizeShirt', error);
      throw new BadRequestException(error);
    }
  }

  async deleteSizeShirt(id: string) {
    try {
      const sizeShirt: any = await this.sizeShirtModel.findById(id);
      if (!sizeShirt) {
        throw new BadRequestException('Size shirt not found');
      }
      await sizeShirt.delete();
      return sizeShirt;
    } catch (error) {
      console.log('deleteSizeShirt', error);
      throw new BadRequestException(error);
    }
  }
}
