import { Test, TestingModule } from '@nestjs/testing';
import { SizeShirtController } from './size-shirt.controller';

describe('SizeShirtController', () => {
  let controller: SizeShirtController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SizeShirtController],
    }).compile();

    controller = module.get<SizeShirtController>(SizeShirtController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
