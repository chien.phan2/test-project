import { Test, TestingModule } from '@nestjs/testing';
import { SizeShirtService } from './size-shirt.service';

describe('SizeShirtService', () => {
  let service: SizeShirtService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SizeShirtService],
    }).compile();

    service = module.get<SizeShirtService>(SizeShirtService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
