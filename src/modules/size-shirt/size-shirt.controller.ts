import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ErrorsInterceptor } from 'src/interceptor/error.interceptor';
import { TransformResponseInterceptor } from 'src/interceptor/response.interceptor';
import { SizeShirtService } from './size-shirt.service';
import { CreateSizeShirtDto } from './dto/createSizeShirt.dto';
import { UpdateSizeShirtDto } from './dto/updateSizeShirt.dto';

@ApiTags('Challenge')
@Controller('challenge')
@UseInterceptors(new TransformResponseInterceptor())
@UseInterceptors(new ErrorsInterceptor())
export class SizeShirtController {
  constructor(private readonly sizeShirtService: SizeShirtService) {}

  @Get('/size-shirt')
  getAllSizeShirt() {
    return this.sizeShirtService.getAllSizeShirt();
  }

  @Post('/size-shirt')
  createSizeShirt(@Body() payload: CreateSizeShirtDto) {
    return this.sizeShirtService.createSizeShirt(payload);
  }

  @Patch('/size-shirt/:id')
  updateSizeShirt(
    @Body() payload: UpdateSizeShirtDto,
    @Param('id') id: string,
  ) {
    return this.sizeShirtService.updateSizeShirt(id, payload);
  }

  @Delete('/size-shirt/:id')
  deleteSizeShirt(@Param('id') id: string) {
    return this.sizeShirtService.deleteSizeShirt(id);
  }
}
