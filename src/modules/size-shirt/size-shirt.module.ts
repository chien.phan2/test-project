import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SizeShirt, SizeShirtSchema } from 'src/schema/sizeShirt.schema';
import { SizeShirtController } from './size-shirt.controller';
import { SizeShirtService } from './size-shirt.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: SizeShirt.name, schema: SizeShirtSchema },
    ]),
  ],
  controllers: [SizeShirtController],
  providers: [SizeShirtService],
  exports: [SizeShirtService],
})
export class SizeShirtModule {}
