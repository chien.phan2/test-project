import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  Logger,
} from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MY_MESSAGE } from 'src/contants/message.contants';

@Injectable()
export class ErrorsInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      catchError((err) => {
        Logger.error(err);
        return throwError(() => {
          if (typeof err.response?.message == 'object')
            err.response = {
              ...err.response,
              message:
                MY_MESSAGE[err.response.message[0]] ?? err.response.message[0],
            };
          else {
            err.response = {
              message: err.response,
            };
          }
          throw err;
        });
      }),
    );
  }
}
