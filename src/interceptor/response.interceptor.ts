import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { instanceToPlain } from 'class-transformer';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Response<T> {
  data: T;
  meta: any;
  message: string;
  statusCode: number;
}

@Injectable()
export class TransformResponseInterceptor<T>
  implements NestInterceptor<T, Response<T>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response<T>> {
    return next.handle().pipe(
      map((data) => {
        return {
          statusCode:
            data?.status ?? context.switchToHttp().getResponse().statusCode,
          data: data.status
            ? null
            : data.data
            ? instanceToPlain(data.data)
            : data,
          message: data.message ?? 'Successfully!',
          meta: data.meta ?? null,
        };
      }),
    );
  }
}
