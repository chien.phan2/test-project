import { IsOptional } from 'class-validator';
import { DEFAULT_LIMIT, DEFAULT_SKIP } from 'src/contants/filter.contants';

export default class FilterDto {
  @IsOptional()
  limit = DEFAULT_LIMIT;

  @IsOptional()
  skip = DEFAULT_SKIP;
}
