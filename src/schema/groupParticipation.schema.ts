import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { Runner } from './runner.schema';
import { Challenge } from './challenge.schema';

export type GroupParticipationDocument = GroupParticipation & Document;

@Schema()
export class GroupParticipation {
  _id: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Runner' })
  owner: Runner;

  @Prop({
    type: [
      {
        type: mongoose.Types.ObjectId,
        ref: 'Runner',
      },
    ],
    default: [],
  })
  members: Runner[];

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Challenge' })
  challenge: Challenge;

  @Prop({ default: 10 })
  memberSize: number;

  @Prop()
  name: string;

  @Prop()
  description: string;

  @Prop()
  bannerUrl: string;

  @Prop({ default: null, required: false })
  department: string;

  @Prop({ default: false })
  isDeleted: boolean;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;
}

export const GroupParticipationSchema =
  SchemaFactory.createForClass(GroupParticipation);
