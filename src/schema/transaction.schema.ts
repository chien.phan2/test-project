import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Document } from 'mongoose';
import { Ticket } from './ticket.schema';
import { User } from './user.schema';

export type TransactionDocument = Transaction & Document;

@Schema()
export class Transaction {
  _id: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Ticket' })
  ticket: Ticket;

  @Prop({ default: 'pending' })
  status: 'pending' | 'failed' | 'success' | 'error' | 'refund';

  @Prop({ type: mongoose.Types.ObjectId, ref: 'User' })
  creator: User;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'User' })
  receiver: User;

  @Prop({ min: 1000 })
  value: number;

  @Prop({ min: 1000 })
  fee: number;

  @Prop({ default: 'buy' })
  type: 'buy' | 'request';

  @Prop()
  signature: string;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  paymentInfo: any;

  @Prop({ required: 'pending' })
  statusRequestPay: 'pending' | 'failed' | 'success' | 'error';

  @Prop({ type: mongoose.Schema.Types.Mixed })
  requestPayInfo: any;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;

  @Prop()
  timestamp: number;
}

export const TransactionSchema = SchemaFactory.createForClass(Transaction);
