import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Document } from 'mongoose';
import { Medal } from './medal.schema';

export type RunnerDocument = Runner & Document;

@Schema()
export class Runner {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true })
  userID: string;

  @Prop()
  phone: string;

  @Prop()
  name: string;

  @Prop()
  avatar: string;

  @Prop({ default: null })
  size: 'XS' | 'S' | 'M' | 'L' | 'XL' | '2XL' | '3XL';

  @Prop({ default: null })
  sizeOption: 'XS' | 'S' | 'M' | 'L' | 'XL' | '2XL' | '3XL';

  @Prop({ default: null })
  typeClothes: 'T-shirt' | 'Singlet';

  @Prop({ default: null })
  sex: 'Male' | 'Female';

  @Prop({ default: null })
  department: string;

  @Prop({ default: false })
  isBlocked: boolean;

  @Prop({ type: Object, default: {} })
  stravaData: any;

  @Prop({ required: true, default: 0, min: 0 })
  energy: number;

  @Prop({ default: 0, min: 0 })
  giftEnergy: number;

  @Prop({ default: false })
  isFirstPay: boolean;

  @Prop({ default: null })
  referralCode: mongoose.Types.ObjectId;

  @Prop({ default: [] })
  referralList: mongoose.Types.ObjectId[];

  @Prop({
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Medal' }],
    default: [],
  })
  medals: string[];

  @Prop({
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Wheel' }],
    default: [],
  })
  wheels: mongoose.Types.ObjectId[];

  @Prop({ default: null })
  authCode: string;

  @Prop({ default: null })
  partnerUserId: string;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;
}

export const RunnerSchema = SchemaFactory.createForClass(Runner);
