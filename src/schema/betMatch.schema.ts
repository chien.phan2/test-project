import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Document } from 'mongoose';
import { User } from './user.schema';

export type BetMatchDocument = BetMatch & Document;

@Schema()
export class BetMatch {
  _id: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'User' })
  user: User;

  @Prop()
  code: string;

  @Prop()
  name: string;

  @Prop()
  playerACode: string;

  @Prop()
  playerAName: string;

  @Prop()
  playerAImage: string;

  @Prop()
  playerBCode: string;

  @Prop()
  playerBName: string;

  @Prop()
  playerBImage: string;

  @Prop({ min: 1000 })
  value: number;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;

  @Prop()
  timestamp: number;
}

export const BetMatchSchema = SchemaFactory.createForClass(BetMatch);
