import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import {Reward} from "./reward.schema";

export type RankRewardDocument = RankReward & Document;

@Schema()
export class RankReward {
  _id: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Reward' })
  rewardId: Reward;

  @Prop()
  rank: string;

  @Prop()
  prizeKey: string;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;
}

export const RankRewardSchema = SchemaFactory.createForClass(RankReward);
