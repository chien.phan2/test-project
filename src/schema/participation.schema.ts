import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';

export type ParticipationDocument = Participation & mongoose.Document;

@Schema()
export class Participation {
  @Prop({ type: mongoose.Types.ObjectId, ref: 'Challenge', required: true })
  challengeId: string;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Runner', required: true })
  runnerId: string;

  @Prop({ default: false })
  isNoEarn: boolean;

  @Prop({ type: Object, default: {} })
  athleteStats: any;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;
}

export const ParticipationSchema = SchemaFactory.createForClass(Participation);
