import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { User } from './user.schema';
import { Reward } from './reward.schema';
import { Challenge } from './challenge.schema';

export type WheelDocument = Wheel & Document;

@Schema()
export class Wheel {
  _id: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'User' })
  creator: User;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Reward' })
  rewardId: Reward;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Challenge' })
  challengeId: Challenge;

  @Prop()
  name: string;

  @Prop()
  value: number;

  @Prop()
  type: string;

  @Prop()
  code: string;

  @Prop()
  isSuccess: boolean;

  @Prop()
  url: string;

  @Prop()
  prizeKey: string;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;
}

export const WheelSchema = SchemaFactory.createForClass(Wheel);
