import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Runner } from './runner.schema';
import { Challenge } from './challenge.schema';
import mongoose from 'mongoose';

export type RewardDistributionHistoryDocument = RewardDistributionHistory &
  Document;

@Schema()
export class RewardDistributionHistory {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true, type: mongoose.Types.ObjectId, ref: 'Runner' })
  runner: Runner;

  @Prop({ required: true, type: mongoose.Types.ObjectId, ref: 'Challenge' })
  challenge: Challenge;

  @Prop({ default: 'pending' })
  status: 'pending' | 'failed' | 'success' | 'error';

  @Prop({ required: true, default: 1000 })
  value: number;

  @Prop({ default: 'distribution' })
  type: 'distribution' | 'refund';

  @Prop({ default: '' })
  signature: string;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  paymentInfo: any;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;

  @Prop()
  timestamp: number;
}

export const RewardDistributionHistorySchema = SchemaFactory.createForClass(
  RewardDistributionHistory,
);
