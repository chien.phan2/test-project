import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Challenge } from './challenge.schema';

export type MissionDocument = Mission & Document;

@Schema()
export class Mission {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  description: string;

  @Prop()
  bannerUrl: string;

  @Prop()
  startTime: Date;

  @Prop()
  endTime: Date;

  @Prop({ default: 0 })
  reward: number;

  @Prop({ default: 1 })
  taskRequired: number;

  @Prop({ required: true })
  key: string;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Challenge' })
  challenge: Challenge;

  @Prop({ default: Date.now() })
  createdAt: Date;

  @Prop({ default: Date.now() })
  updatedAt: Date;

  @Prop({ default: false })
  isDisabled: boolean;
}

export const MissionSchema = SchemaFactory.createForClass(Mission);
