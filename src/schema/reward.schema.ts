import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

export type RewardDocument = Reward & Document;

@Schema()
export class Reward {
  _id: mongoose.Types.ObjectId;

  @Prop()
  name: string;

  @Prop()
  value: number;

  @Prop()
  type: string;

  @Prop()
  code: string;

  @Prop({ default: true })
  isActive: boolean;

  @Prop()
  url: string;

  @Prop()
  source: string;

  @Prop()
  rate: number;

  @Prop()
  rate_30: number;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;
}

export const RewardSchema = SchemaFactory.createForClass(Reward);
