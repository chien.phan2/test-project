import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { Challenge } from './challenge.schema';
import { GroupParticipation } from './groupParticipation.schema';
import { Runner } from './runner.schema';

export type GroupParticipationRequestDocument = GroupParticipationRequest &
  Document;

@Schema()
export class GroupParticipationRequest {
  _id: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Runner' })
  creator: Runner;

  @Prop({ default: 'pending' })
  status: 'pending' | 'accepted' | 'rejected' | 'cancelled' | 'left';

  @Prop({ type: mongoose.Types.ObjectId, ref: 'GroupParticipation' })
  group: GroupParticipation;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Challenge' })
  challenge: Challenge;

  @Prop({ type: Object, default: {} })
  athleteStats: any;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop({ default: new Date().toLocaleString() })
  updatedAt: string;
}

export const GroupParticipationRequestSchema = SchemaFactory.createForClass(
  GroupParticipationRequest,
);
