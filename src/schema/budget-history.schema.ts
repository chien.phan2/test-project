import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Challenge } from './challenge.schema';
import { Runner } from './runner.schema';
import { Energy } from './energy.schema';

export type BudgetHistoryDocument = BudgetHistory & Document;

@Schema()
export class BudgetHistory {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true, type: mongoose.Types.ObjectId, ref: 'Runner' })
  creator: Runner;

  @Prop({ required: true, type: mongoose.Types.ObjectId, ref: 'Challenge' })
  challenge: Challenge;

  @Prop({ required: true, type: mongoose.Types.ObjectId, ref: 'Energy' })
  product: Energy;

  @Prop({ required: true, default: 'wheel' })
  type: 'wheel' | 'sponsor';

  @Prop({ required: true, min: 0, default: 0 })
  amount: number;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;
}

export const BudgetHistorySchema = SchemaFactory.createForClass(BudgetHistory);
