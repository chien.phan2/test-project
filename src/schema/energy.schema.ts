import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Document } from 'mongoose';

export type EnergyDocument = Energy & Document;

@Schema()
export class Energy {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true })
  productName: string;

  @Prop()
  productImage: string;

  @Prop()
  productDescription: string;

  @Prop({ required: true, default: 0, min: 0 })
  productQuantity: number;

  @Prop({ required: true, default: 0, min: 0 })
  productPrice: number;

  @Prop({ default: 'energy' })
  type: 'energy' | 'sponsor';

  @Prop({ default: '' })
  key: string;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;
}

export const EnergySchema = SchemaFactory.createForClass(Energy);
