import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Exclude } from 'class-transformer';
import { IsEmail } from 'class-validator';
import mongoose from 'mongoose';
import { Document } from 'mongoose';
import { Transaction } from './transaction.schema';
import { User } from './user.schema';

export type TicketDocument = Ticket & Document;

@Schema()
export class Ticket {
  _id: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'User' })
  owner: User;

  @Prop({ default: 'available' })
  status: 'available' | 'pending' | 'captured' | 'outdate' | 'cancel';

  @Prop({ default: 'sell' })
  type: 'buy' | 'sell';

  @Prop({ type: mongoose.Types.ObjectId, ref: 'User' })
  buyer: User;

  @IsEmail()
  @Exclude({ toPlainOnly: true })
  @Prop({ default: true })
  code: string;

  @Prop({ min: 1000 })
  price: number;

  @Prop({ default: 1000 })
  donate: number;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Transaction' })
  transaction: Transaction;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  reportInfo: any;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;

  @Prop()
  timestamp: number;
}

export const TicketSchema = SchemaFactory.createForClass(Ticket);
