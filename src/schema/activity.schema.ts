import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

export type ActivityDocument = Activity & Document;

@Schema()
export class Activity {
  @Prop({ required: true })
  activityId: string;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Runner', required: true })
  runnerId: string;

  @Prop({ required: true })
  stravaId: string;

  @Prop({ required: true })
  distance: number;

  @Prop({ default: 0 })
  invalidDistance: number;

  @Prop()
  elapsed_time: number;

  @Prop()
  from_accepted_tag: boolean;

  @Prop()
  manual: boolean;

  @Prop()
  private: boolean;

  @Prop({ type: Object, default: {} })
  map: object;

  @Prop()
  max_speed: number;

  @Prop()
  moving_time: number;

  @Prop({ type: Array, default: [] })
  splits_metric: any;

  @Prop()
  name: string;

  @Prop()
  external_id: string;

  @Prop({ default: false })
  isWarning: boolean;

  @Prop({ default: false })
  isDuplicate: boolean;

  @Prop({ default: false })
  isInvalid: boolean;

  @Prop({ default: false })
  isExpired: boolean;

  @Prop()
  sport_type: string;

  @Prop()
  start_date: Date;

  @Prop()
  start_date_local: Date;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;
}

export const ActivitySchema = SchemaFactory.createForClass(Activity);
