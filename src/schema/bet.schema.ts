import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Document } from 'mongoose';
import { BetMatch } from './betMatch.schema';
import { BetTransaction } from './betTransaction.schema';
import { User } from './user.schema';

export type BetDocument = Bet & Document;

@Schema()
export class Bet {
  _id: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'User' })
  user: User;

  @Prop({ default: 'init' })
  status: 'success' | 'init' | 'fail';

  @Prop({ type: mongoose.Types.ObjectId, ref: 'BetMatch' })
  match: BetMatch;

  @Prop()
  playerCode: string;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'BetTransaction' })
  transaction: BetTransaction;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;

  @Prop()
  timestamp: number;
}

export const BetSchema = SchemaFactory.createForClass(Bet);
