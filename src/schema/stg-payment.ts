import { Prop, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';

export type StgPaymentDocument = StgPayment & Document;

export class StgPayment {
  _id: mongoose.Types.ObjectId;

  @Prop({ default: 'pending' })
  status: 'pending' | 'failed' | 'success' | 'error' | 'refund';

  @Prop()
  creator: string;

  @Prop({ default: 1000 })
  value: number;

  @Prop()
  signature: string;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  paymentInfo: any;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;

  @Prop()
  timestamp: number;
}

export const StgPaymentSchema = SchemaFactory.createForClass(StgPayment);
