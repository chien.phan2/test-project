import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Runner } from './runner.schema';
import { Exclude } from 'class-transformer';
import { ProductTransaction } from './productTransaction.schema';

export type TicketChallengeDocument = TicketChallenge & Document;

@Schema()
export class TicketChallenge {
  _id: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Runner' })
  owner: Runner;

  @Prop({ default: 'available' })
  status: 'available' | 'pending' | 'captured' | 'outdate' | 'cancel';

  @Prop({ default: 'sell' })
  type: 'buy' | 'sell';

  @Prop({ type: mongoose.Types.ObjectId, ref: 'User' })
  buyer: Runner;

  @Exclude({ toPlainOnly: true })
  @Prop({ default: true })
  code: string;

  @Prop({ min: 1000 })
  price: number;

  @Prop({ default: 200 })
  donate: number;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'ProductTransaction' })
  transaction: ProductTransaction;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  reportInfo: any;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;

  @Prop()
  timestamp: number;
}

export const TicketChallengeSchema =
  SchemaFactory.createForClass(TicketChallenge);
