import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Document } from 'mongoose';
import { Medal } from './medal.schema';
import {
  RaceKit,
  RewardContent,
} from 'src/modules/challenge/dto/create-challenge.dto';

export type ChallengeDocument = Challenge & Document;

@Schema()
export class Challenge {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true })
  activities: string[];

  @Prop()
  bannerUrl: string;

  @Prop()
  name: string;

  @Prop()
  description: string;

  @Prop({ default: new Date().toLocaleString(), required: true })
  startTime: Date;

  @Prop({ required: true })
  endTime: Date;

  @Prop({ default: 0 })
  minDistance: number;

  @Prop({ required: true })
  target: number;

  @Prop({ required: true })
  paceRange: number[];

  @Prop()
  reward: RewardContent[];

  @Prop()
  raceKit: RaceKit[];

  @Prop()
  rule: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Medal' })
  medal: Medal;

  @Prop()
  maxParticipants: number;

  @Prop({ default: 0 })
  athleteCount: number;

  @Prop({ require: true, default: false })
  paidChallenge: boolean;

  @Prop({ default: 0 })
  budget: number;

  @Prop({ default: false })
  isDisabled: boolean;

  @Prop({ default: 'individual' })
  type: string;

  @Prop({ default: false })
  isCheckParticipantTime: boolean;

  @Prop({ type: Object })
  conversionEvent: any;

  @Prop({ default: null })
  supportUrl: string;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: Date;

  @Prop()
  updatedAt: Date;
}

export const ChallengeSchema = SchemaFactory.createForClass(Challenge);
