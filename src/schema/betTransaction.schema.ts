import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Document } from 'mongoose';
import { Bet } from './bet.schema';
import { BetMatch } from './betMatch.schema';
import { User } from './user.schema';

export type BetTransactionDocument = BetTransaction & Document;

@Schema()
export class BetTransaction {
  _id: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Bet' })
  bet: Bet;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'BetMatch' })
  match: BetMatch;

  @Prop({ default: 'pending' })
  status: 'pending' | 'failed' | 'success' | 'error' | 'refund';

  @Prop({ type: mongoose.Types.ObjectId, ref: 'User' })
  user: User;

  @Prop({ min: 1000 })
  value: number;

  @Prop()
  signature: string;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  paymentInfo: any;

  @Prop({ required: 'pending' })
  statusRequestPay: 'pending' | 'failed' | 'success' | 'error';

  @Prop({ type: mongoose.Schema.Types.Mixed })
  requestPayInfo: any;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;

  @Prop()
  timestamp: number;
}

export const BetTransactionSchema =
  SchemaFactory.createForClass(BetTransaction);
