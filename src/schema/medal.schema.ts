import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Challenge } from './challenge.schema';

export type MedalDocument = Medal & Document;

@Schema()
export class Medal {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  shortName: string;

  @Prop()
  description: string;

  @Prop({ required: true })
  imageUrl: string;

  @Prop({ default: 'challenge' })
  type: 'challenge' | 'individual';

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Challenge' })
  challenge: Challenge;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;
}

export const MedalSchema = SchemaFactory.createForClass(Medal);
