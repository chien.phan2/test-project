import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Runner } from './runner.schema';
import { TicketChallenge } from './ticketChallenge.schema';

export type ProductTransactionDocument = ProductTransaction & Document;

@Schema()
export class ProductTransaction {
  _id: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'TicketChallenge' })
  ticket: TicketChallenge;

  @Prop({ default: 'pending' })
  status: 'pending' | 'failed' | 'success' | 'error' | 'refund';

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Runner' })
  creator: Runner;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Runner' })
  receiver: Runner;

  @Prop({ min: 1000 })
  value: number;

  @Prop({ min: 200 })
  fee: number;

  @Prop({ default: 'buy' })
  type: 'buy' | 'request';

  @Prop()
  signature: string;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  paymentInfo: any;

  @Prop({ required: 'pending' })
  statusRequestPay: 'pending' | 'failed' | 'success' | 'error';

  @Prop({ type: mongoose.Schema.Types.Mixed })
  requestPayInfo: any;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;

  @Prop()
  timestamp: number;
}

export const ProductTransactionSchema =
  SchemaFactory.createForClass(ProductTransaction);
