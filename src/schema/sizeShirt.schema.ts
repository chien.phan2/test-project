import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';

export type SizeShirtDocument = SizeShirt & Document;

@Schema()
export class SizeShirt {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  key: string;

  @Prop({ required: true })
  description: string;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;
}

export const SizeShirtSchema = SchemaFactory.createForClass(SizeShirt);
