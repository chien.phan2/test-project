import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Challenge } from './challenge.schema';
import { Runner } from './runner.schema';

export type EnergyTransactionDocument = EnergyTransaction & Document;

@Schema()
export class EnergyTransaction {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true, type: mongoose.Types.ObjectId, ref: 'Runner' })
  creator: Runner;

  @Prop({ required: true, type: mongoose.Types.ObjectId, ref: 'Energy' })
  product: string;

  @Prop({ default: 'pending' })
  status: 'pending' | 'failed' | 'success' | 'error' | 'refund';

  @Prop({ required: true, default: 1 })
  quantity: number;

  @Prop({ required: true, min: 0 })
  value: number;

  @Prop({ default: 'buy' })
  type: 'buy' | 'sponsor' | 'ticket';

  @Prop({ default: '', type: mongoose.Types.ObjectId, ref: 'Challenge' })
  challenge: Challenge;

  @Prop()
  signature: string;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  paymentInfo: any;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;

  @Prop()
  timestamp: number;
}

export const EnergyTransactionSchema =
  SchemaFactory.createForClass(EnergyTransaction);
