import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Mission } from './mission.schema';
import { Runner } from './runner.schema';
import mongoose from 'mongoose';
import { Challenge } from './challenge.schema';

export type MissionHistoryDocument = MissionHistory & Document;

@Schema()
export class MissionHistory {
  _id: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Runner', required: true })
  runner: Runner;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Mission', required: true })
  mission: Mission;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Challenge' })
  challenge: Challenge;

  @Prop({ default: 0 })
  taskCount: number;

  @Prop({ default: false })
  isCompleted: boolean;

  @Prop()
  completedAt: Date;

  @Prop({ default: Date.now() })
  createdAt: Date;

  @Prop()
  updatedAt: Date;
}

export const MissionHistorySchema =
  SchemaFactory.createForClass(MissionHistory);
