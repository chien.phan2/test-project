import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { User } from './user.schema';
import { Reward } from './reward.schema';

export type InventoryDocument = Inventory & Document;

@Schema()
export class Inventory {
  _id: mongoose.Types.ObjectId;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'User' })
  creator: User;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Reward' })
  rewardId: User;

  @Prop()
  name: string;

  @Prop()
  value: number;

  @Prop()
  type: string;

  @Prop()
  code: string;

  @Prop({ default: '', required: false })
  ticketCode: string;

  @Prop()
  inventoryCode: string;

  @Prop()
  challengeId: string;

  @Prop({ default: false })
  isUsed: boolean;

  @Prop({ default: false })
  isInvalid: boolean;

  @Prop({ default: false })
  isSold: boolean;

  @Prop()
  soldAt: string;

  @Prop()
  typeAction: string;

  @Prop()
  prizeKey: string;

  @Prop()
  url: string;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;

  @Prop()
  updatedAt: string;
}

export const InventorySchema = SchemaFactory.createForClass(Inventory);
