import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';

export type DepartmentDocument = Department & Document;

@Schema()
export class Department {
  _id: mongoose.Types.ObjectId;

  @Prop({ required: true })
  departmentName: string;

  @Prop()
  departmentDescription: string;

  @Prop()
  departmentShortName: string;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;
}

export const DepartmentSchema = SchemaFactory.createForClass(Department);
