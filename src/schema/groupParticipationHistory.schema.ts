import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { Runner } from './runner.schema';
import { GroupParticipation } from './groupParticipation.schema';

export type GroupParticipationHistoryDocument = GroupParticipationHistory &
  Document;

@Schema()
export class GroupParticipationHistory {
  _id: mongoose.Types.ObjectId;

  @Prop({ default: 'participation' })
  type: 'participation' | 'groupInfo';

  @Prop({ type: mongoose.Types.ObjectId, ref: 'Runner' })
  runner: Runner;

  @Prop({ required: true })
  action: string;

  @Prop({ type: mongoose.Types.ObjectId, ref: 'GroupParticipation' })
  group: GroupParticipation;

  @Prop({ default: new Date().toLocaleString() })
  createdAt: string;
}

export const GroupParticipationHistorySchema = SchemaFactory.createForClass(
  GroupParticipationHistory,
);
