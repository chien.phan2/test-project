import { Injectable, OnApplicationBootstrap } from '@nestjs/common';

const VERIFY_TOKEN = 'MOMO_RUN_STRAVA_VERIFY_TOKEN_CHALLENGE';

@Injectable()
export class AppService implements OnApplicationBootstrap {
  async onApplicationBootstrap() {
    // setTimeout(async () => {
    //   try {
    //     const subscription = await axios.post(
    //       'https://www.strava.com/api/v3/push_subscriptions',
    //       {
    //         client_id: '90924',
    //         client_secret: '6b9c5642ebae53a10e5796d3c8ebb893c3ddf54d',
    //         callback_url: 'http://128.199.179.217:3050/strava/webhook',
    //         verify_token: VERIFY_TOKEN,
    //       },
    //     );
    //     console.log(
    //       'Create Strava Subscription Successfully: ',
    //       subscription.data,
    //     );
    //   } catch (error) {
    //     console.log('Create Strava Subscription Error: ', error.response?.data);
    //   }
    // }, 3000);
  }

  getHello(): string {
    return 'Hello World!';
  }
}
