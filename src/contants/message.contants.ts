export const MY_MESSAGE = {
  INVALID_TICKET: {
    vi: 'Thông tin phiếu cơm không hợp lệ. Vui lòng kiểm tra lại',
    en: 'Ticket information is not valid, try again',
  },
  BLOCK_CHECK: {
    vi: 'Bạn đã nhập sai vượt quá số lần quy định. Bạn vui lòng quay lại vào ngày làm việc tiếp theo. Haha',
    en: 'Your account has been banned, please go back tomorrow',
  },
  NOT_FOUND_TICKET: {
    vi: 'Phiếu cơm này đã bị bán hoặc không có sẵn, vui lòng lựa chọn phiếu cơm khác',
    en: 'Ticket is already purchased or not available, please try again',
  },
  NOT_FOUND_TICKET_BUYER: {
    vi: 'Không tìm thấy phiếu hoặc bạn chưa mua phiếu này',
    en: 'Ticket not found or you are not the buyer of this ticket',
  },
  NOT_FOUND_TRANSACTION: {
    vi: 'Không tìm thấy giao dịch hoặc bạn không phải là người',
    en: 'Can not found ticket or you are not the owner or ticket is already purchased',
  },
  INVALID_PRICE: {
    vi: 'Số tiền donate không thể lớn hơn giá và giá phải lớn hơn 10,000',
    en: 'Donate amount can not greater than the price or final value is less than 10,000',
  },
  BUY_OWN: {
    vi: 'Bạn không thể mua phiếu của chính mình',
    en: 'You can not make the purchase on your own ticket',
  },
  PROCESSING_TRANS: {
    vi: 'Bạn đang có một giao dịch khác đang xử lý',
    en: 'You are having another transaction on processing',
  },
  CREATE_TRANS_FAILED: {
    vi: 'Lỗi không thể tạo giao dịch với Momo',
    en: 'Cannot create Momo transaction',
  },
  TRANS_OWN: {
    vi: 'Giao dịch không tồn tại hoặc bạn không phải người tạo',
    en: 'Transaction not found or you are not the creator!',
  },
  INVALID_USER: {
    vi: 'Không tìm thấy tài khoản hoặc tài khoản bị khoá',
    en: 'User not found or blocked or lack of information!',
  },
  CREATE_USER: {
    vi: 'Tạo tài khoản thành coong',
    en: 'Create user success',
  },
  SYNC_USER: {
    vi: 'Đồng bộ thành công',
    en: 'Sync successfully',
  },
  MIN_MAX_PRICE: {
    vi: 'Giá vé phải lớn hơn 10.000đ và bé hơn 500.000đ',
    en: 'The price must be from 10.000đ to 500.000đ',
  },
  MIN_MAX_DONATE: {
    vi: 'Số tiền khuyên góp phải lớn hơn 1000đ và bé hơn 500.000đ',
    en: 'The donate value must be from 1000đ to 500.000đ',
  },
  CONFIRM_FAILED: {
    vi: 'Gửi request xác nhận thanh toán thất bại',
    en: 'Confirm to momo transaction failed',
  },
  LIMIT_REQUEST: {
    vi: 'Bạn đã đạt giới hạn tạo yêu cầu mua của hôm nay',
    en: 'You have been reached the limit of request today',
  },
  NOT_FOUND_CHALLENGE: {
    vi: 'Không tìm thấy challenge',
    en: 'The challenge not found',
  },
  DUPLICATE_ATHLETES: {
    vi: 'Người dùng đã được thêm vào challenge',
    en: 'User has been added to challenge',
  },
  NOT_AVAILABLE_TURN: {
    vi: 'Bạn đã hết lượt quay',
    en: 'You have run out of spins',
  },
  SOMETHING_WRONG: {
    vi: 'Có lỗi xảy ra, vui lòng thử lại sau.',
    en: 'Something went wrong please try again later',
  },
  MISMATCH_USERID: {
    vi: 'Không tìm thấy người dùng.',
    en: 'Not found user.',
  },
  NOT_FOUND_REWARD: {
    vi: 'Không tìm thấy Reward',
    en: 'The Reward not found',
  },
  LIMITED_RANK_REWARD: {
    vi: 'Vượt quá rank cho phép',
    en: 'Limited rank',
  },
  NOT_FOUND_RANK_REWARD: {
    vi: 'Không tìm thấy Rank Reward',
    en: 'The Reward Rank Reward not found',
  },
  AUTHENTICATE_PORTAL: {
    vi: 'Xác thực không thành công',
    en: 'Authenticated failed',
  },
  NOT_FOUND_RUNNER: {
    vi: 'Không tìm thấy Runner',
    en: 'The Runner not found',
  },
};
