import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { getToken } from 'src/utils/genToken';

@Injectable()
export class ApiKeyAuthGuard implements CanActivate {
  async canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    const [canAccess, userID] = await this.validateRequest(request);
    if (canAccess) {
      request.userID = userID;
    }
    return canAccess;
  }

  async validateRequest(
    request: any,
  ): Promise<[canAccess: boolean, userID: string]> {
    try {
      const header = request.headers;
      const API_KEY =
        process.env.API_KEY || 'e7be79d0-7842-492c-8e5c-5eb3fbceac23';
      const token = header?.api_key;
      if (!token || token != API_KEY) throw new Error('unAuthorization');
      if (token) {
        return [true, token];
      }
    } catch (err) {
      console.log(err);
      return [false, null];
    }
  }
}
