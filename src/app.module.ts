import {
  Logger,
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TicketModule } from './modules/ticket/ticket.module';
import { UserModule } from './modules/user/user.module';
import { CheckAvailableMiddleware } from './middleware/available.middleware';
import { TransactionModule } from './modules/transaction/transaction.module';
import { WinstonModule } from 'nest-winston';
import * as winston from 'winston';
import * as path from 'path';
import { EventsModule } from './events/events.module';
import { ChallengeModule } from './modules/challenge/challenge.module';
import { BetMatchModule } from './modules/bet-match/bet-match.module';
import { BetModule } from './modules/bet/bet.module';
import { BetTransaction } from './schema/betTransaction.schema';
import { StravaHookModule } from './modules/strava-hook/strava-hook.module';
import { RewardModule } from './modules/reward/reward.module';
import { TestPaymentModule } from './modules/test-payment/test-payment.module';
import { StgPaymentModule } from './modules/stg-payment/stg-payment.module';

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: '.env', isGlobal: true }),
    MongooseModule.forRootAsync({
      imports: [EventsModule, ConfigModule],
      useFactory: async (configService: ConfigService) => {
        return {
          uri: process.env.MONGO_URI,
          useNewUrlParser: true,
          useUnifiedTopology: true,
        };
      },
      inject: [ConfigService],
    }),
    WinstonModule.forRoot({
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json(),
      ),
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.simple(),
          ),
        }),
        new winston.transports.File({
          dirname: path.join(__dirname, '../log/'),
          filename: 'debug.log',
          level: 'debug',
        }),
        new winston.transports.File({
          dirname: path.join(__dirname, '../log/'),
          filename: 'info.log',
          level: 'info',
        }),
      ],
    }),
    TicketModule,
    UserModule,
    TransactionModule,
    ChallengeModule,
    BetMatchModule,
    BetModule,
    BetTransaction,
    StravaHookModule,
    TestPaymentModule,
    StgPaymentModule,
  ],
  controllers: [AppController],
  providers: [AppService, Logger],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(CheckAvailableMiddleware)
      .forRoutes({ path: 'transaction', method: RequestMethod.POST });
  }
}
// CI BUILD]
