import {
  MessageBody,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server } from 'socket.io';

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class EventsGateway {
  @WebSocketServer()
  server: Server;
  @SubscribeMessage('onChangeTicket')
  public onChangeTicket(@MessageBody() data: any) {
    const response = { ...data };
    delete response.code;
    console.log('onChangeTicket: ', response);
    return this.server.emit('onChangeTicket', response);
  }
  @SubscribeMessage('onCreateTicket')
  public onCreateTicket(@MessageBody() data: any) {
    const response = { ...data };
    delete response.code;
    console.log('onCreateTicket: ', response);
    return this.server.emit('onCreateTicket', response);
  }
  @SubscribeMessage('onTransaction')
  public onTransaction(@MessageBody() data: any) {
    console.log('onTransaction: ', data);
    return this.server.emit('onTransaction', data);
  }
  @SubscribeMessage('onEnergyTransaction')
  public onEnergyTransaction(@MessageBody() data: any) {
    console.log('onEnergyTransaction: ', data);
    return this.server.emit('onEnergyTransaction', data);
  }
  @SubscribeMessage('onChangeEnergy')
  public onChangeEnergy(@MessageBody() data: any) {
    console.log('onChangeEnergy: ', data);
    return this.server.emit('onChangeEnergy', data);
  }
  @SubscribeMessage('onChangeBudget')
  public onChangeBudget(@MessageBody() data: any) {
    console.log('onChangeBudget: ', data);
    return this.server.emit('onChangeBudget', data);
  }
  @SubscribeMessage('onNotification')
  public onNotification(@MessageBody() data: any) {
    console.log('onNotification: ', data);
    return this.server.emit('onNotification', data);
  }
  @SubscribeMessage('onCreateChallengeTicket')
  public onCreateChallengeTicket(@MessageBody() data: any) {
    const response = { ...data };
    delete response.code;
    console.log('onCreateChallengeTicket: ', response);
    return this.server.emit('onCreateChallengeTicket', response);
  }
  @SubscribeMessage('onChangeChallengeTicket')
  public onChangeChallengeTicket(@MessageBody() data: any) {
    const response = { ...data };
    delete response.code;
    console.log('onChangeChallengeTicket: ', response);
    return this.server.emit('onChangeChallengeTicket', response);
  }
  @SubscribeMessage('onTicketChallengeTransaction')
  public onTicketChallengeTransaction(@MessageBody() data: any) {
    console.log('onTicketChallengeTransaction: ', data);
    return this.server.emit('onTicketChallengeTransaction', data);
  }
}
